Hextreme Wave Project
=====================

### DDMWave

Fast Helmholtz Solvers with Domain Decomposition Methods

This project aims at the doctoral dissertation

Coding Introduction
---------------------

### Language
This is a sub-project of Hextreme Project.
This sub-project is written in C considering its robustness.

### Compiler
icc or gcc, cc.

### Dependencies
- MKL Library or PARDISO

- GMSH 4.0

- Hertreme Project

### Aim
- Main simulation in this project is 

  1. the scattering of a plane wave by a circular cylinder. (2D)

  2. the Marmousi model. (2D)

  3. the engine sound propagation model. (2D)

  4. the guassian waveguide. (3D)

- Using OpenMP

- Non-overlapping Checkerboard Domain Decomposition

- Non-overlapping Layered Domain Decomposition

- Sweeping preconditioners

- Multiple right-hand sides

### Example
- Add dependencies

- Build project

- Go to ./build
 
- export OMP_NUM_THREADS=1

- ./ddm_scatter 

- Solution written in res_x.msh

