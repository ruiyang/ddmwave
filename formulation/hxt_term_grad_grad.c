#include "hxt_term.h"
#include "hxt_linear_algebra.h"

HXTStatus hxtTermBuildGradGrad  (HXTTerm *term, const double_complex coef,
                     void (*f)(const double *xyz, double_complex *tensor))
{
  const HXTGroup *group = term->field;

  const size_t entities_n = group->entities_n;
  if( entities_n == 0 )  return HXT_STATUS_OK;

  int ierr = 0;

  size_t i;

  for(i = 0; i < entities_n; i++)
  {
    HXTEntity *entity = group->entities[i];

    const int tag = entity->tag;

    // * --------------------------------------------------------------- * //
    // * ------                                                   ------ * //
    // * --                    GMSH GET ELEMENTS                      -- * //
    // * ------                                                   ------ * //
    // * --------------------------------------------------------------- * //
    int     *elementTypes   = entity->elements.types      ;
    size_t   elementTypes_n = entity->elements.types_n    ;
  //size_t **elementTags    = entity->elements.tags       ;
    size_t  *elementTags_n  = entity->elements.tags_n     ;
  //size_t   elementTags_nn = entity->elements.tags_nn    ;
  //size_t **nodeTags       = entity->elements.nodeTags   ;
    size_t  *nodeTags_n     = entity->elements.nodeTags_n ;
  //size_t   nodeTags_nn    = entity->elements.nodeTags_nn;

    size_t j, e, g, k, l, a, b, c, q;

    const size_t   numElementTypes = elementTypes_n;
    for(j = 0; j < numElementTypes; j++)
    {
      const int  elementType = elementTypes[j];

      char   *eleName        = NULL;
      int     eleDim         =    0;
      int     eleOrder       =    0;
      int     eleNumNodes    =    0;
      double *eleNodeCoord   = NULL;
      size_t  eleNodeCoord_n =    0;
      int     eleNumPriNodes =    0;

      gmshModelMeshGetElementProperties( elementType,
                                        &eleName,
                                        &eleDim,
                                        &eleOrder,
                                        &eleNumNodes,
                                        &eleNodeCoord,
                                        &eleNodeCoord_n,
                                        &eleNumPriNodes,
                                        &ierr);
      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --               GMSH GET INTEGRATION POINTS                 -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      double    *integrationPoints    = NULL;
      size_t     integrationPoints_n  =    0;
      double    *integrationWeights   = NULL;
      size_t     integrationWeights_n =    0;
  
      char     integrationType[32] = "Gauss\0";
      snprintf(integrationType, sizeof(char)*32, "Gauss%d", 2*eleOrder);

      gmshModelMeshGetIntegrationPoints( elementType,
                                         integrationType,
                                        &integrationPoints,
                                        &integrationPoints_n,
                                        &integrationWeights,
                                        &integrationWeights_n,
                                        &ierr);

      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --               GMSH GET BASIS FUNCTIONS                    -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      double *basisFunctions   = NULL;
      size_t  basisFunctions_n =    0;
      int     numComponents    =    0;

      const char functionSpaceType[32] = "GradIsoParametric\0";

      gmshModelMeshGetBasisFunctions   ( elementType,
                                         integrationPoints,
                                         integrationPoints_n,
                                         functionSpaceType,
                                        &numComponents,
                                        &basisFunctions,
                                        &basisFunctions_n,
                                        &ierr);

      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --               GMSH GET BASIS JACOBIANS                    -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      double *jacobians           = NULL;
      size_t  jacobians_n         =    0;
      double *determinants        = NULL;
      size_t  determinants_n      =    0;
      double *points              = NULL;
      size_t  points_n            =    0;

      const size_t task        =  0;
      const size_t numTasks    =  1;
  
      gmshModelMeshGetJacobians( elementType,
                                 integrationPoints,  integrationPoints_n,
                                &jacobians        , &jacobians_n        ,
                                &determinants     , &determinants_n     ,
                                &points           , &points_n           ,
                                 tag, task, numTasks, &ierr);

      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --                   MATRIX MULTIPLICATION                   -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      const size_t nG = integrationPoints_n / 3;
      const size_t nE =                 elementTags_n  [j];
      const size_t nN = nodeTags_n[j] / elementTags_n  [j]; 

      double_complex *BM = NULL, *CM = NULL;
      HXT_CHECK( hxtCalloc(&BM,   nE   * 9*nG   , sizeof(double_complex)) );
      HXT_CHECK( hxtCalloc(&CM, 9*nG   *   nN*nN, sizeof(double_complex)) );

      // * --------------------------------------------------------------- * //
      // * CALCULATE MATRIX B                                              * //
      // * --------------------------------------------------------------- * //
      double_complex TJac[9];
      double_complex    T[9];
  
      for(e = 0; e < nE; e++)
      {
        q = 0;
        for(g = 0; g < nG; g++)
        {
          double *   jac    = &jacobians   [9*e*nG + 9*g];
          double  detJac    =  determinants[  e*nG +   g];
          double  invJac[9] = {0.};

          invJac[0] = (jac[4]*jac[8] - jac[7]*jac[5])/detJac;
          invJac[1] = (jac[7]*jac[2] - jac[1]*jac[8])/detJac;
          invJac[2] = (jac[1]*jac[5] - jac[4]*jac[2])/detJac;

          invJac[3] = (jac[6]*jac[5] - jac[3]*jac[8])/detJac;
          invJac[4] = (jac[0]*jac[8] - jac[6]*jac[2])/detJac;
          invJac[5] = (jac[3]*jac[2] - jac[0]*jac[5])/detJac;

          invJac[6] = (jac[3]*jac[7] - jac[6]*jac[4])/detJac;
          invJac[7] = (jac[6]*jac[1] - jac[0]*jac[7])/detJac;
          invJac[8] = (jac[0]*jac[4] - jac[3]*jac[1])/detJac;

          const double *gxyz = &points[ 3 * (e*nG + g) ];
          f(gxyz, T);

          memset(TJac, 0, 9*sizeof(double_complex));
          for(a = 0; a < 3; a++)
            for(b = 0; b < 3; b++)
              for(c = 0; c < 3; c++)
                TJac[3*a + b] += T[3*a + c] * invJac[3*c + b];

          for(a = 0; a < 3; a++)
          {
            for(b = 0; b < 3; b++)
            {
              BM[e*9*nG + q] = 0.;
              for(c = 0; c < 3; c++)
              {
              BM[e*9*nG + q] +=   TJac[3*c + a]*
                                invJac[3*c + b];
              }
              BM[e*9*nG + q] *= 1. * fabs(detJac);
              q++;
            }
          }

        }
      }

      // * --------------------------------------------------------------- * //
      // * CALCULATE MATRIX C                                              * //
      // * --------------------------------------------------------------- * //
      for(k = 0; k < nN; k++)
      {
        for(l = 0; l < nN; l++)
        {
          for(g = 0; g < nG; g++)
          {
            const  double   gw       =  integrationWeights[g];

            double_complex *CMPtr    = &CM            [g*9*nN*nN];
            const  double  *basisPtr = &basisFunctions[g*3*nN   ];
            for(b = 0; b < 3; b++)
            {
              for(c = 0; c < 3; c++)
              {
                CMPtr[(b*3+c)*nN*nN + k*nN+l] = gw * basisPtr[3*k + b]*
                                                     basisPtr[3*l + c];
              }
            }
          }
        }
      }

      // * --------------------------------------------------------------- * //
      // * CALCULATE MATRIX A                                              * //
      // * A = BETA * A + ALPHA * B * C                                    * //
      // * --------------------------------------------------------------- * //
      const double_complex alpha = coef;
      const double_complex beta  = 1.  ;
      hxtzgemm(BM, CM, alpha, term-> Mats[i][j], beta, nE, nN*nN, 9*nG);

      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --                          DELETE                           -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      HXT_CHECK( hxtFree(&BM) );
      HXT_CHECK( hxtFree(&CM) );
      
      free( integrationPoints );
      free( integrationWeights);
      integrationPoints    = NULL;
      integrationPoints_n  =    0;
      integrationWeights   = NULL;
      integrationWeights_n =    0;

      free( basisFunctions );
      basisFunctions   = NULL;
      basisFunctions_n =    0;
      numComponents    =    0;

      free( jacobians   );
      free( determinants);
      free( points      );
      jacobians      = NULL;
      jacobians_n    =    0;
      determinants   = NULL;
      determinants_n =    0;
      points         = NULL;
      points_n       =    0;

      free(eleName     );
      free(eleNodeCoord);
      eleName        = NULL;
      eleDim         =    0;
      eleOrder       =    0;
      eleNumNodes    =    0;
      eleNodeCoord   = NULL;
      eleNodeCoord_n =    0;
      eleNumPriNodes =    0;
    }
  }

  return HXT_STATUS_OK;
}
