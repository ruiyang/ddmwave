#ifndef HXTWAVE_FORMULATION_H
#define HXTWAVE_FORMULATION_H

#include "hxt_term.h"
#include "hxt_group.h"

/* *
  @struct HXTFormulation
  This struct calculates formulation in weak form

  Variables represent in the following
    
    * fields    pointers of elements in group
 
  Weak form is described as Ax = By + Cz 

    * One term of ATerms include matrices A and vectors x
    * One term of BTerms include matrices B and vectors y
    * One term of CTerms include matrices C and vectors z

* */

// * MAXIMUM OF NUMBER
#define MNUM 100 

typedef struct HXTFormulationStruct
{
  HXTTerm   *ATerms[MNUM];
  HXTTerm   *BTerms[MNUM];
  HXTTerm   *CTerms[MNUM];
  HXTGroup  *fields[MNUM];

  size_t   ATerms_n;
  size_t   BTerms_n;
  size_t   CTerms_n;
  size_t   fields_n;

} HXTFormulation;

HXTStatus hxtFormulationCreate  (HXTFormulation **formulation);
HXTStatus hxtFormulationDelete  (HXTFormulation **formulation);
HXTStatus hxtFormulationDummy   (HXTFormulation  *formulation);
HXTStatus hxtFormulationAddField(HXTFormulation  *formulation, const HXTGroup *group);

HXTStatus hxtFormulationHelmholtz (HXTFormulation *formulation, double_complex (*fK2)(const double *xyz));
HXTStatus hxtFormulationSommerfeld(HXTFormulation *formulation, double_complex (*fK )(const double *xyz));
HXTStatus hxtFormulationHABC      (HXTFormulation *formulation, double_complex (*fK )(const double *xyz), 
		                                                                      const double theta);
HXTStatus hxtFormulationHABCHelper(HXTFormulation *formulation, double_complex (*fK2)(const double *xyz), 
		                                                                      const double theta);
HXTStatus hxtFormulationHABCCorner(HXTFormulation *formulation, double_complex (*fK )(const double *xyz), 
		                                                                      const double theta);
HXTStatus hxtFormulationHABCUpd   (HXTFormulation *formulation, double_complex (*fK )(const double *xyz), 
		                                                                      const double theta);
HXTStatus hxtFormulationHABCCnrUpd(HXTFormulation *formulation, double_complex (*fK )(const double *xyz), 
		                                                                      const double theta);

#endif
