#ifndef HXTWAVE_TERM_H
#define HXTWAVE_TERM_H

#include "hxt_tools.h"
#include "hxt_types.h"
#include "hxt_group.h"

/* *
  @struct HXTTerm
  This struct calculates term in weak form

  Variables represent in the following
    
    * field    pointer of elements in field
    * test     pointer of elements in test 
 
  Weak form is described as Mat * Vec

    * Mats are matrices 
    * Vecs are unknowns vectors 
* */

typedef struct HXTTermStruct
{
  HXTGroup *field;
  HXTGroup * test;

  double_complex *** Mats;
  double_complex *** Vecs;

} HXTTerm;

HXTStatus hxtTermCreate(HXTTerm **term);
HXTStatus hxtTermDelete(HXTTerm **term);
HXTStatus hxtTermDummy (HXTTerm  *term);
HXTStatus hxtTermBuild (HXTTerm  *term, const HXTGroup *field, 
		                        const HXTGroup *test); 

HXTStatus hxtTermBuildFieldField(HXTTerm *term, const double_complex coef, 
                                 double_complex (*f)(const double *xyz)  );
HXTStatus hxtTermBuildProjeField(HXTTerm *term, const double_complex coef, 
                                 double_complex (*f)(const double *xyz)  );
HXTStatus hxtTermBuildGradGrad  (HXTTerm *term, const double_complex coef, 
                     void (*f)(const double *xyz, double_complex *tensor));
HXTStatus hxtTermBuildSolveField(HXTTerm *term, const double_complex coef, 
                                 double_complex (*f)(const double *xyz)  );




#endif
