#include "hxt_formulation.h"

HXTStatus hxtFormulationCreate(HXTFormulation **formulation)
{
  HXT_CHECK( hxtMalloc(formulation, sizeof(HXTFormulation)) );
  if((*formulation) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTFormulation *ptr = *formulation;

  size_t i;

  for(i = 0; i < MNUM; i++)
  {
    ptr->ATerms[i] = NULL;
    ptr->BTerms[i] = NULL;
    ptr->CTerms[i] = NULL;
    ptr->fields[i] = NULL;
  }
  ptr->ATerms_n  = 0;
  ptr->BTerms_n  = 0;
  ptr->CTerms_n  = 0;
  ptr->fields_n  = 0;

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationDelete(HXTFormulation **formulation)
{
  if((*formulation) == NULL) return HXT_STATUS_OK;

  HXTFormulation *ptr = *formulation;

  size_t i;

  for(i = 0; i < ptr->ATerms_n; i++)
  {
    HXT_CHECK( hxtTermDelete( &ptr->ATerms[i] ) );
    ptr->ATerms[i] = NULL;
  }
  for(i = 0; i < ptr->BTerms_n; i++)
  {
    HXT_CHECK( hxtTermDelete( &ptr->BTerms[i] ) );
    ptr->BTerms[i] = NULL;
  }
  for(i = 0; i < ptr->CTerms_n; i++)
  {
    HXT_CHECK( hxtTermDelete( &ptr->CTerms[i] ) );
    ptr->CTerms[i] = NULL;
  }
  for(i = 0; i < ptr->fields_n; i++) ptr->fields[i] = NULL;

  ptr->ATerms_n  = 0;
  ptr->BTerms_n  = 0;
  ptr->CTerms_n  = 0;
  ptr->fields_n  = 0;

  HXT_CHECK( hxtFree(formulation) );
  *formulation = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationDummy (HXTFormulation  *formulation)
{
  HXTFormulation *ptr = formulation;

  size_t i;

  for(i = 0; i < ptr->ATerms_n; i++)
  {
    HXT_CHECK( hxtTermDelete( &ptr->ATerms[i] ) );
    ptr->ATerms[i] = NULL;
  }
  for(i = 0; i < ptr->BTerms_n; i++)
  {
    HXT_CHECK( hxtTermDelete( &ptr->BTerms[i] ) );
    ptr->BTerms[i] = NULL;
  }
  for(i = 0; i < ptr->CTerms_n; i++)
  {
    HXT_CHECK( hxtTermDelete( &ptr->CTerms[i] ) );
    ptr->CTerms[i] = NULL;
  }
  for(i = 0; i < ptr->fields_n; i++) ptr->fields[i] = NULL;

  ptr->ATerms_n  = 0;
  ptr->BTerms_n  = 0;
  ptr->CTerms_n  = 0;
  ptr->fields_n  = 0;

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationAddField(HXTFormulation  *formulation, const HXTGroup *group)
{
  HXTFormulation *ptr = formulation;

  ptr->fields[ptr->fields_n++] = (HXTGroup*) group;

  return HXT_STATUS_OK;
}

static void fIM(const double *xyz, double_complex* tensor)
{
  tensor[0] = 1.; tensor[1] = 0.; tensor[2] = 0.;
  tensor[3] = 0.; tensor[4] = 1.; tensor[5] = 0.;
  tensor[6] = 0.; tensor[7] = 0.; tensor[8] = 1.;
}

static double_complex fI(const double *xyz)
{
  double_complex re = 1.;
  return re;
}

HXTStatus hxtFormulationHelmholtz (HXTFormulation *formulation, double_complex (*fK2)(const double *xyz ))
{
  HXTFormulation *ptr = formulation;

  HXT_CHECK( hxtTermCreate(&ptr->ATerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->ATerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));

  HXT_CHECK( hxtTermBuildGradGrad  (ptr->ATerms[0],  1., fIM) ); 
  HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[0], -1., fK2) ); 

  ptr->ATerms_n = 1;

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationSommerfeld(HXTFormulation *formulation, double_complex (*fK )(const double *xyz ))
{
  HXTFormulation *ptr = formulation;

  HXT_CHECK( hxtTermCreate(&ptr->ATerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->ATerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));

  HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[0], -I , fK ) );
  HXT_CHECK( hxtTermBuildProjeField(ptr->ATerms[0], +1., fI ) );

  ptr->ATerms_n = 1;

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationHABC(HXTFormulation *formulation, double_complex (*fK )(const double *xyz), 
		                                                                const double theta)
{
  HXTFormulation *ptr = formulation;

  const int    NPade = ptr->fields_n - 1 ;
  const double MPade = 2. *  NPade   + 1.;

  const double_complex ExpPTheta2 = cos( theta / 2.) + sin( theta / 2.)*I;

  HXT_CHECK( hxtTermCreate(&ptr->BTerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->BTerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));
  HXT_CHECK( hxtTermCreate(&ptr->ATerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->ATerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));

  HXT_CHECK( hxtTermBuildProjeField(ptr->BTerms[0], +1.          , fI) );
  HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[0], -I*ExpPTheta2, fK) );
  ptr->BTerms_n++;
  ptr->ATerms_n++;
  
  size_t j;

  for(j = 1; j <= NPade; j++)
  {
    const double CPade  =  tan( (j) * M_PI/MPade) *
                           tan( (j) * M_PI/MPade) ;

    const double_complex alpha  = -I*(ExpPTheta2 * 2./MPade * CPade);

    HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[0], alpha, fK) );

    HXT_CHECK( hxtTermCreate(&ptr->ATerms[j]));
    HXT_CHECK( hxtTermBuild ( ptr->ATerms[j],  
  			      ptr->fields[j],  
  			      ptr->fields[0]));

    HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[j], alpha, fK) );
    ptr->ATerms_n++;
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationHABCHelper(HXTFormulation *formulation, 
		      double_complex (*fK2)(const double *xyz), 
		                            const double theta) 
{
  HXTFormulation *ptr = formulation;

  const int    NPade = ptr->fields_n - 1 ;
  const double MPade = 2. *  NPade   + 1.;

  const double_complex ExpPTheta  = cos( theta ) + sin( theta )*I;

  size_t a = 0;
  size_t j = 0;

  for(j = 1; j <= NPade; j++)
  {
    const double CPade_j  = tan( (j) * M_PI/MPade) *
                            tan( (j) * M_PI/MPade) ;

    HXT_CHECK( hxtTermCreate(&ptr->ATerms[a]));
    HXT_CHECK( hxtTermBuild ( ptr->ATerms[a],  
  			      ptr->fields[j],  
  			      ptr->fields[j]));

    HXT_CHECK( hxtTermBuildGradGrad  (ptr->ATerms[a],   1.                     , fIM) );
    HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[a], -(ExpPTheta * CPade_j+1.), fK2) );
    ptr->ATerms_n++;
    a++;

    HXT_CHECK( hxtTermCreate(&ptr->ATerms[a]));
    HXT_CHECK( hxtTermBuild ( ptr->ATerms[a],  
  			      ptr->fields[0],  
  			      ptr->fields[j]));

    HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[a], - ExpPTheta *(CPade_j+1.), fK2) );
    ptr->ATerms_n++;
    a++;
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationHABCCorner(HXTFormulation *formulation, 
		      double_complex (*fK )(const double *xyz), 
		                            const double theta) 
{
  HXTFormulation *ptr = formulation;

  const int    NPade = ptr->fields_n / 2 ;
  const double MPade = 2. * NPade    + 1.;

  const double_complex ExpMTheta  = cos(-theta     ) + sin(-theta     )*I;
  const double_complex ExpPTheta2 = cos( theta / 2.) + sin( theta / 2.)*I;

  size_t j, k;
  size_t a= 0;
  size_t b= 0;

  for(j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtTermCreate(&ptr->BTerms[b]));
    HXT_CHECK( hxtTermBuild ( ptr->BTerms[b],  
  			      ptr->fields[j],  
  			      ptr->fields[j]));
    HXT_CHECK( hxtTermCreate(&ptr->ATerms[a]));
    HXT_CHECK( hxtTermBuild ( ptr->ATerms[a],  
  			      ptr->fields[j],  
  			      ptr->fields[j]));

    HXT_CHECK( hxtTermBuildProjeField(ptr->BTerms[b],  1.            , fI) );
    HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[a], -I * ExpPTheta2, fK) );
    ptr->BTerms_n++;
    b++;

    for(k = 0; k < NPade; k++)
    {
      const double CPade_j  = tan( (j+1) * M_PI/MPade) *
                              tan( (j+1) * M_PI/MPade) ;
      const double CPade_k  = tan( (k+1) * M_PI/MPade) *
                              tan( (k+1) * M_PI/MPade) ;

      const double_complex coefA = 2. / MPade * CPade_k * ( CPade_j - 1.      + ExpMTheta) 
                                                        / ( CPade_j + CPade_k + ExpMTheta);

      HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[a], -I * ExpPTheta2 * coefA, fK) );
    }
    ptr->ATerms_n++;
    a++;

    for(k = 0; k < NPade; k++)
    {
      const double CPade_j  = tan( (j+1) * M_PI/MPade) *
                              tan( (j+1) * M_PI/MPade) ;
      const double CPade_k  = tan( (k+1) * M_PI/MPade) *
                              tan( (k+1) * M_PI/MPade) ;

      const double_complex coefB = 2. / MPade * CPade_k * (-CPade_j - 1. )
                                                        / ( CPade_j + CPade_k + ExpMTheta);

      HXT_CHECK( hxtTermCreate(&ptr->ATerms[a]));
      HXT_CHECK( hxtTermBuild ( ptr->ATerms[a],  
      			        ptr->fields[NPade+k],  
  			        ptr->fields[      j]));

      HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[a], -I * ExpPTheta2 * coefB, fK) );

      ptr->ATerms_n++;
      a++;
    }
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationHABCUpd(HXTFormulation *formulation, double_complex (*fK )(const double *xyz),
		                                                                   const double theta)
{
  HXTFormulation *ptr = formulation;

  const int    NPade = ptr->fields_n - 1 ;
  const double MPade = 2. *  NPade   + 1.;

  const double_complex ExpPTheta2 = cos( theta / 2.) + sin( theta / 2.)*I;

  HXT_CHECK( hxtTermCreate(&ptr->ATerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->ATerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));
  HXT_CHECK( hxtTermCreate(&ptr->BTerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->BTerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));
  HXT_CHECK( hxtTermCreate(&ptr->CTerms[0]));
  HXT_CHECK( hxtTermBuild ( ptr->CTerms[0],  
			    ptr->fields[0],  
			    ptr->fields[0]));

  HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[0], +1.          , fI) );
  HXT_CHECK( hxtTermBuildProjeField(ptr->BTerms[0], +1.          , fI) );
  HXT_CHECK( hxtTermBuildSolveField(ptr->CTerms[0], -I*ExpPTheta2, fK) );
  ptr->ATerms_n++;
  ptr->BTerms_n++;
  ptr->CTerms_n++;
  
  size_t j;

  for(j = 1; j <= NPade; j++)
  {
    const double CPade  =  tan( (j) * M_PI/MPade) *
                           tan( (j) * M_PI/MPade) ;

    const double_complex alpha  = -I*(ExpPTheta2 * 2./MPade * CPade);

    HXT_CHECK( hxtTermBuildSolveField(ptr->CTerms[0], alpha, fK) );

    HXT_CHECK( hxtTermCreate(&ptr->CTerms[j]));
    HXT_CHECK( hxtTermBuild ( ptr->CTerms[j],  
  			      ptr->fields[j],  
  			      ptr->fields[0]));

    HXT_CHECK( hxtTermBuildSolveField(ptr->CTerms[j], alpha, fK) );
    ptr->CTerms_n++;
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtFormulationHABCCnrUpd(HXTFormulation *formulation, 
		      double_complex (*fK )(const double *xyz), 
		                            const double theta) 
{
  HXTFormulation *ptr = formulation;

  const int    NPade = ptr->fields_n / 2 ;
  const double MPade = 2. * NPade    + 1.;

  const double_complex ExpMTheta  = cos(-theta     ) + sin(-theta     )*I;
  const double_complex ExpPTheta2 = cos( theta / 2.) + sin( theta / 2.)*I;

  size_t j, k;
  size_t c= 0;
  size_t b= 0;
  size_t a= 0;

  for(j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtTermCreate(&ptr->ATerms[a]));
    HXT_CHECK( hxtTermBuild ( ptr->ATerms[a],  
  			      ptr->fields[j],  
  			      ptr->fields[j]));
    HXT_CHECK( hxtTermCreate(&ptr->BTerms[b]));
    HXT_CHECK( hxtTermBuild ( ptr->BTerms[b],  
  			      ptr->fields[j],  
  			      ptr->fields[j]));
    HXT_CHECK( hxtTermCreate(&ptr->CTerms[c]));
    HXT_CHECK( hxtTermBuild ( ptr->CTerms[c],  
  			      ptr->fields[j],  
  			      ptr->fields[j]));

    HXT_CHECK( hxtTermBuildFieldField(ptr->ATerms[a],  1.            , fI) );
    HXT_CHECK( hxtTermBuildProjeField(ptr->BTerms[b],  1.            , fI) );
    HXT_CHECK( hxtTermBuildSolveField(ptr->CTerms[c], -I * ExpPTheta2, fK) );
    ptr->ATerms_n++;
    ptr->BTerms_n++;
    a++;
    b++;

    for(k = 0; k < NPade; k++)
    {
      const double CPade_j  = tan( (j+1) * M_PI/MPade) *
                              tan( (j+1) * M_PI/MPade) ;
      const double CPade_k  = tan( (k+1) * M_PI/MPade) *
                              tan( (k+1) * M_PI/MPade) ;

      const double_complex coefA = 2. / MPade * CPade_k * ( CPade_j - 1.      + ExpMTheta) 
                                                        / ( CPade_j + CPade_k + ExpMTheta);

      HXT_CHECK( hxtTermBuildSolveField(ptr->CTerms[c], -I * ExpPTheta2 * coefA, fK) );
    }
    ptr->CTerms_n++;
    c++;

    for(k = 0; k < NPade; k++)
    {
      const double CPade_j  = tan( (j+1) * M_PI/MPade) *
                              tan( (j+1) * M_PI/MPade) ;
      const double CPade_k  = tan( (k+1) * M_PI/MPade) *
                              tan( (k+1) * M_PI/MPade) ;

      const double_complex coefB = 2. / MPade * CPade_k * (-CPade_j - 1. )
                                                        / ( CPade_j + CPade_k + ExpMTheta);

      HXT_CHECK( hxtTermCreate(&ptr->CTerms[c]));
      HXT_CHECK( hxtTermBuild ( ptr->CTerms[c],  
      			        ptr->fields[NPade+k],  
  			        ptr->fields[      j]));

      HXT_CHECK( hxtTermBuildSolveField(ptr->CTerms[c], -I * ExpPTheta2 * coefB, fK) );

      ptr->CTerms_n++;
      c++;
    }
  }

  return HXT_STATUS_OK;
}
