#include "hxt_term.h"
#include "hxt_linear_algebra.h"

HXTStatus hxtTermBuildProjeField(HXTTerm *term, const double_complex coef,
                                 double_complex (*f)(const double *xyz)  )
{
  const HXTGroup *group = term->field;

  const size_t entities_n = group->entities_n;
  if( entities_n == 0 )  return HXT_STATUS_OK;

  int ierr = 0;

  size_t i;

  for(i = 0; i < entities_n; i++)
  {
    HXTEntity *entity = group->entities[i];

    const int tag = entity->tag;

    // * --------------------------------------------------------------- * //
    // * ------                                                   ------ * //
    // * --                    GMSH GET ELEMENTS                      -- * //
    // * ------                                                   ------ * //
    // * --------------------------------------------------------------- * //
    int     *elementTypes   = entity->elements.types      ;
    size_t   elementTypes_n = entity->elements.types_n    ;
  //size_t **elementTags    = entity->elements.tags       ;
    size_t  *elementTags_n  = entity->elements.tags_n     ;
  //size_t   elementTags_nn = entity->elements.tags_nn    ;
  //size_t **nodeTags       = entity->elements.nodeTags   ;
    size_t  *nodeTags_n     = entity->elements.nodeTags_n ;
  //size_t   nodeTags_nn    = entity->elements.nodeTags_nn;

    size_t j, e, g, k, l;

    const size_t   numElementTypes = elementTypes_n;
    for(j = 0; j < numElementTypes; j++)
    {
      const int  elementType = elementTypes[j];

      char   *eleName        = NULL;
      int     eleDim         =    0;
      int     eleOrder       =    0;
      int     eleNumNodes    =    0;
      double *eleNodeCoord   = NULL;
      size_t  eleNodeCoord_n =    0;
      int     eleNumPriNodes =    0;

      gmshModelMeshGetElementProperties( elementType,
                                        &eleName,
                                        &eleDim,
                                        &eleOrder,
                                        &eleNumNodes,
                                        &eleNodeCoord,
                                        &eleNodeCoord_n,
                                        &eleNumPriNodes,
                                        &ierr);
      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --               GMSH GET INTEGRATION POINTS                 -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      double    *integrationPoints    = NULL;
      size_t     integrationPoints_n  =    0;
      double    *integrationWeights   = NULL;
      size_t     integrationWeights_n =    0;

      char     integrationType[32] = "Gauss\0";
      snprintf(integrationType, sizeof(char)*32, "Gauss%d", 2*eleOrder);

      gmshModelMeshGetIntegrationPoints( elementType,
                                         integrationType,
                                        &integrationPoints,
                                        &integrationPoints_n,
                                        &integrationWeights,
                                        &integrationWeights_n,
                                        &ierr);

      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --               GMSH GET BASIS FUNCTIONS                    -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      double *basisFunctions   = NULL;
      size_t  basisFunctions_n =    0;
      int     numComponents    =    0;

      const char functionSpaceType[32] = "IsoParametric\0";

      gmshModelMeshGetBasisFunctions   ( elementType,
                                         integrationPoints,
                                         integrationPoints_n,
                                         functionSpaceType,
                                        &numComponents,
                                        &basisFunctions,
                                        &basisFunctions_n,
                                        &ierr);

      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --               GMSH GET BASIS JACOBIANS                    -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      double *jacobians           = NULL; 
      size_t  jacobians_n         =    0;
      double *determinants        = NULL; 
      size_t  determinants_n      =    0;
      double *points              = NULL;
      size_t  points_n            =    0;
  
      const size_t task        =  0;
      const size_t numTasks    =  1;
    
      gmshModelMeshGetJacobians( elementType,
                                 integrationPoints,  integrationPoints_n,
                                &jacobians        , &jacobians_n        ,
                                &determinants     , &determinants_n     ,
                                &points           , &points_n           ,
                                 tag, task, numTasks, &ierr);
  
      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --                   MATRIX MULTIPLICATION                   -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      const size_t nG = integrationPoints_n / 3;
      const size_t nE =                 elementTags_n  [j];
      const size_t nN = nodeTags_n[j] / elementTags_n  [j]; 
  
      double_complex *BM = NULL, *CM = NULL;
      HXT_CHECK( hxtCalloc(&BM,   nE * nG   , sizeof(double_complex)) );
      HXT_CHECK( hxtCalloc(&CM,   nG * nN*nN, sizeof(double_complex)) );

      // * --------------------------------------------------------------- * //
      // * CALCULATE MATRIX B                                              * //
      // * --------------------------------------------------------------- * //
      for(e = 0; e < nE; e++)
      {
        for(g = 0; g < nG; g++)
        {
          const double *gxyz = &points[ 3 * (e*nG + g) ];
  
          BM[e*nG + g] = f(gxyz) * fabs(determinants[e*nG + g]);
        }
      }

      // * --------------------------------------------------------------- * //
      // * CALCULATE MATRIX C                                              * //
      // * --------------------------------------------------------------- * //
      for(g = 0; g < nG; g++)
      {
        const  double   gw       =  integrationWeights[g];
  
        double_complex *CMPtr    = &CM            [g*nN*nN];
        const  double  *basisPtr = &basisFunctions[g*nN   ];
  
        for(k = 0; k < nN; k++)
        {
          for(l = 0; l < nN; l++)
          {
            CMPtr[k*nN +l] = gw * basisPtr[k]*
                                  basisPtr[l];
          }
        }
      }
  
      // * --------------------------------------------------------------- * //
      // * CALCULATE MATRIX A                                              * //
      // * A = BETA * A + ALPHA * B * C                                    * //
      // * --------------------------------------------------------------- * //
      const double_complex alpha = coef;
      const double_complex beta  = 1.  ;
      hxtzgemm(BM, CM, alpha, term-> Mats[i][j], beta, nE, nN*nN, nG);
  
      // * --------------------------------------------------------------- * //
      // * ------                                                   ------ * //
      // * --                          DELETE                           -- * //
      // * ------                                                   ------ * //
      // * --------------------------------------------------------------- * //
      HXT_CHECK( hxtFree(&BM) );
      HXT_CHECK( hxtFree(&CM) );
  
      free( integrationPoints );
      free( integrationWeights);
      integrationPoints    = NULL;
      integrationPoints_n  =    0;
      integrationWeights   = NULL;
      integrationWeights_n =    0;
  
      free( basisFunctions );
      basisFunctions   = NULL;
      basisFunctions_n =    0;
      numComponents    =    0;
  
      free( jacobians   );
      free( determinants);
      free( points      );
      jacobians      = NULL;
      jacobians_n    =    0;
      determinants   = NULL;
      determinants_n =    0;
      points         = NULL;
      points_n       =    0;

      free(eleName     );
      free(eleNodeCoord);
      eleName        = NULL;
      eleDim         =    0;
      eleOrder       =    0;
      eleNumNodes    =    0;
      eleNodeCoord   = NULL;
      eleNodeCoord_n =    0;
      eleNumPriNodes =    0;
    }
  }

  return HXT_STATUS_OK;
}
