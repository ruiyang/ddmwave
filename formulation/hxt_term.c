#include "hxt_term.h"

HXTStatus hxtTermCreate(HXTTerm **term)
{
  HXT_CHECK( hxtMalloc(term, sizeof(HXTTerm)) );
  if((*term) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTTerm *ptr = *term;

  ptr->field = NULL;
  ptr-> test = NULL;

  ptr-> Mats = NULL;
  ptr-> Vecs = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtTermDelete(HXTTerm **term)
{
  HXTTerm *ptr = *term;

  HXTGroup *grp = ptr->field;

  size_t i, j;

  for(i = 0; i < grp->entities_n; i++)
  {
    HXTEntity *entity = grp->entities[i];
    for(j = 0; j < entity->elements.types_n; j++)
    {
      HXT_CHECK( hxtFree( &ptr-> Mats[i][j] ) );
      HXT_CHECK( hxtFree( &ptr-> Vecs[i][j] ) );
    }
    HXT_CHECK( hxtFree( &ptr-> Mats[i] ) );
    HXT_CHECK( hxtFree( &ptr-> Vecs[i] ) );
  }
  HXT_CHECK( hxtFree( &ptr-> Mats ) );
  HXT_CHECK( hxtFree( &ptr-> Vecs ) );

  ptr->field = NULL;
  ptr-> test = NULL;

  HXT_CHECK( hxtFree(term) );
  *term = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtTermDummy (HXTTerm  *term)
{
  HXTTerm *ptr = term;

  HXTGroup *grp = ptr->field;

  size_t i, j;

  for(i = 0; i < grp->entities_n; i++)
  {
    HXTEntity *entity = grp->entities[i];
    for(j = 0; j < entity->elements.types_n; j++)
    {
      HXT_CHECK( hxtFree( &ptr-> Mats[i][j] ) );
      HXT_CHECK( hxtFree( &ptr-> Vecs[i][j] ) );
    }
    HXT_CHECK( hxtFree( &ptr-> Mats[i] ) );
    HXT_CHECK( hxtFree( &ptr-> Vecs[i] ) );
  }
  HXT_CHECK( hxtFree( &ptr-> Mats ) );
  HXT_CHECK( hxtFree( &ptr-> Vecs ) );

  ptr->field = NULL;
  ptr-> test = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtTermBuild (HXTTerm  *term,
		        const HXTGroup *field,
			const HXTGroup * test)
{
  if (field->entities_n != test->entities_n)
    return HXT_ERROR_MSG(HXT_STATUS_FORMAT_ERROR, "field != test\n");

  size_t i, j;

  for(i = 0; i < field->entities_n; i++)
  {
    if( !(field->entities[i]->dim == test->entities[i]->dim &&
          field->entities[i]->tag == test->entities[i]->tag ))
    return HXT_ERROR_MSG(HXT_STATUS_FORMAT_ERROR, "field != test\n");
  }
  HXTTerm *ptr = term;

  term->field = (HXTGroup*) field;
  term-> test = (HXTGroup*)  test;

  const HXTGroup *grp = field;

  HXT_CHECK( hxtMalloc(&ptr-> Mats, grp->entities_n * sizeof(double_complex**)) );
  HXT_CHECK( hxtMalloc(&ptr-> Vecs, grp->entities_n * sizeof(double_complex**)) );
  for(i = 0; i < grp->entities_n; i++)
  {
    const HXTEntity *ent = grp->entities[i];

    HXT_CHECK( hxtMalloc(&ptr-> Mats[i], ent->elements.types_n*sizeof(double_complex*)) );
    HXT_CHECK( hxtMalloc(&ptr-> Vecs[i], ent->elements.types_n*sizeof(double_complex*)) );
    for(j = 0; j < ent->elements.types_n; j++)
    {
      const size_t nE = ent->elements.tags_n    [j] ;
      const size_t nN = ent->elements.nodeTags_n[j] /
                        ent->elements.tags_n    [j] ;
      HXT_CHECK( hxtCalloc(&ptr-> Mats[i][j], nE * nN*nN, sizeof(double_complex)) );
      HXT_CHECK( hxtCalloc(&ptr-> Vecs[i][j], nE * nN   , sizeof(double_complex)) );
    }
  }

  return HXT_STATUS_OK;
}
