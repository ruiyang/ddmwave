#define HXT_OMP_CHECK(status)                                           \
do{ HXTStatus s = (status);                                             \
    if(s!=HXT_STATUS_OK){                                               \
      HXT_TRACE_MSG(s, "cannot break OpenMP region -> exiting");        \
      fflush(stdout); fflush(stderr);                                   \
      exit(s);                                                          \
    }                                                                   \
}while(0);                                                              \

#include "hxt_entity.h"
#include "hxt_group.h"
#include "hxt_formulation.h"
#include "hxt_system.h"
#include "hxt_gmres1.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define OMEGA      ( 4. * M_PI * 1)
#define LSP        ( 1.           )
#define N_LAMBDA   ( 20           )

#define theta  (M_PI / 3.)
#define NPade  (8        )
//#define theta  (0.)
//#define NPade  (0 )

#define order  1
#define nrhs   1

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                CONSTANTS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Rx_DOM  1
#define Ry_DOM  1
#define Nx_DOM (Rx_DOM * 2     )
#define Ny_DOM (Ry_DOM         )
#define Nt_DOM (Nx_DOM * Ny_DOM)

#define Pi_SCA 0
#define Pj_SCA 0

#define Lx     (8.00)
#define Ly     (8.00)
#define X_SCA  (1.25)
#define Y_SCA  (1.25)
#define R_SCA  (1.00)

#define LSx (Lx / Rx_DOM)
#define LSy (Ly / Ry_DOM)

#define X0_DOM (-X_SCA - Pi_SCA*LSx)
#define Y0_DOM (-Y_SCA - Pj_SCA*LSy)
#define Z0_DOM  +0

#define WAVENUMBER ( OMEGA      / LSP       )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define FREQ       ( WAVENUMBER / (2*M_PI)  )
#define LC         ( LAMBDA     / N_LAMBDA  )

static double_complex fK2    (const double *xyz);
static double_complex fK     (const double *xyz);
static double_complex fSource(const double *xyz);

int main(int argc, char **argv)
{
  if( Lx/Rx_DOM <= 2*R_SCA ||
      Ly/Ry_DOM <= 2*R_SCA  )
  {
    printf("Size of subdomain too small!\n");
    printf("Lx / Rx_DOM : %f\n", Lx/Rx_DOM );
    printf("Ly / Ry_DOM : %f\n", Ly/Ry_DOM );
    printf("2  *  R_SCA : %f\n", 2 * R_SCA );
    exit(0);
  }
  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ("rectangularDom", &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                             SOURCE                              -- * //
  // * --------------------------------------------------------------------- * //
  int p0 = gmshModelGeoAddPoint(     0, 0, 0, 0, -1, &ierr);
  int p1 = gmshModelGeoAddPoint( R_SCA, 0, 0, 0, -1, &ierr);
  int p2 = gmshModelGeoAddPoint(-R_SCA, 0, 0, 0, -1, &ierr);

  int scaTags[2] = {0};
      scaTags[0] = gmshModelGeoAddCircleArc(p1, p0, p2, -1, 0, 0, 0, &ierr);
      scaTags[1] = gmshModelGeoAddCircleArc(p2, p0, p1, -1, 0, 0, 0, &ierr);
  int scaLlp     = gmshModelGeoAddCurveLoop(scaTags, 2, -1,          &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                           RECTANGLES                            -- * //
  // * --------------------------------------------------------------------- * //
  int ptTags[Rx_DOM+1][Ry_DOM+1] = {{0}};

  for(int i = 0; i < Rx_DOM+1; i++)
  {
    for(int j = 0; j < Ry_DOM+1; j++)
    {
      ptTags[i][j] = gmshModelGeoAddPoint(X0_DOM + Lx*i/Rx_DOM, 
                                          Y0_DOM + Ly*j/Ry_DOM, 
                                          Z0_DOM, 0, -1, &ierr);
    }
  }

  int lxTags[Rx_DOM  ][Ry_DOM+1] = {{0}};
  int lyTags[Rx_DOM+1][Ry_DOM  ] = {{0}};
  int ldTags[Rx_DOM  ][Ry_DOM  ] = {{0}};

  for(int i = 0; i < Rx_DOM+1; i++)
  {
    for(int j = 0; j < Ry_DOM+1; j++)
    {
      if(i < Rx_DOM) lxTags[i][j] = gmshModelGeoAddLine(ptTags[i  ][j], ptTags[i+1][j], -1, &ierr);
      if(j < Ry_DOM) lyTags[i][j] = gmshModelGeoAddLine(ptTags[i  ][j], ptTags[i][j+1], -1, &ierr);
      if(i < Rx_DOM &&
	 j < Ry_DOM) ldTags[i][j] = gmshModelGeoAddLine(ptTags[i+1][j], ptTags[i][j+1], -1, &ierr);
    }
  }

  int recLlps[Nx_DOM][Ny_DOM] = {{0}};
  int recTags[Nx_DOM][Ny_DOM] = {{0}};

  for(int i = 0; i < Rx_DOM; i++)
  {
    for(int j = 0; j < Ry_DOM; j++)
    {
      if(i == Pi_SCA && j == Pj_SCA)
      {
        int LTags[] = { lxTags[i][j],
                        ldTags[i][j],
                       -lyTags[i][j],
                      };

        recLlps[2*i][j]  = gmshModelGeoAddCurveLoop   (LTags   , 3, -1, &ierr);
        int wireTags[ ]  = {recLlps[2*i][j], scaLlp};
        recTags[2*i][j]  = gmshModelGeoAddPlaneSurface(wireTags, 2, -1, &ierr);

        int UTags[] = {-lxTags[i  ][j+1],
                       -ldTags[i  ][j  ],
                        lyTags[i+1][j  ],
                      };

        recLlps[2*i+1][j] = gmshModelGeoAddCurveLoop   ( UTags            , 3, -1, &ierr);
        recTags[2*i+1][j] = gmshModelGeoAddPlaneSurface(&recLlps[2*i+1][j], 1, -1, &ierr);
      }
      else
      {
        int LTags[] = { lxTags[i][j],
                        ldTags[i][j],
                       -lyTags[i][j],
                      };

        recLlps[2*i  ][j] = gmshModelGeoAddCurveLoop   ( LTags            , 3, -1, &ierr);
        recTags[2*i  ][j] = gmshModelGeoAddPlaneSurface(&recLlps[2*i  ][j], 1, -1, &ierr);

        int UTags[] = {-lxTags[i  ][j+1],
                       -ldTags[i  ][j  ],
                        lyTags[i+1][j  ],
                      };

        recLlps[2*i+1][j] = gmshModelGeoAddCurveLoop   ( UTags            , 3, -1, &ierr);
        recTags[2*i+1][j] = gmshModelGeoAddPlaneSurface(&recLlps[2*i+1][j], 1, -1, &ierr);
      }
    }
  }

  // * --------------------------------------------------------------------- * //
  // * --                         GENERATE  MESH                          -- * //
  // * --------------------------------------------------------------------- * //
  gmshModelGeoSynchronize(        &ierr);
  gmshModelMeshGenerate  (2,      &ierr);
  gmshModelMeshSetOrder  (order,  &ierr);
  gmshWrite("rectangularDom.msh", &ierr);
  printf("MESHING FINISHED\n");

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                  DDM                                  * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  omp_set_num_threads(Nt_DOM);

  #pragma omp parallel
  {
    const int threadid = omp_get_thread_num ();

    const int Ix = (threadid / Ny_DOM);
    const int Iy = (threadid % Ny_DOM);
    const int Rx = (Ix       / 2     );

    // * --------------------------------------------------------------------- * //
    // * --                              Group                              -- * //
    // * --------------------------------------------------------------------- * //
    size_t offset = 0;
  
    HXTGroup *domain = NULL;
    HXT_OMP_CHECK( hxtGroupCreate   (&domain   ) );
    HXT_OMP_CHECK( hxtGroupAddEntity( domain, 2, recTags[Ix][Iy]) );
    HXT_OMP_CHECK( hxtGroupBuild    ( domain, offset) ); 

    HXTGroup *source = NULL;
    HXT_OMP_CHECK( hxtGroupCreate   (&source   ) );
    HXT_OMP_CHECK( hxtGroupAddEntity( source, 1, scaTags[0]) );
    HXT_OMP_CHECK( hxtGroupAddEntity( source, 1, scaTags[1]) );
    HXT_OMP_CHECK( hxtGroupBuild    ( source, offset) ); 
  
    HXTGroup *bnd[3];
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[0]) );
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[1]) );
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[2]) );
  
    if( Ix % 2 == 0 )
    {
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[0], 1, lxTags[Rx  ][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[1], 1, ldTags[Rx  ][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[2], 1, lyTags[Rx  ][Iy  ]) );
    }
    else
    {
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[0], 1, lxTags[Rx  ][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[1], 1, ldTags[Rx  ][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[2], 1, lyTags[Rx+1][Iy  ]) );
    }

    HXT_OMP_CHECK( hxtGroupBuild( bnd[0], offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild( bnd[1], offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild( bnd[2], offset) ); 
  
    HXTGroup *aux[3][NPade];
    HXTGroup *cnr[6][NPade];
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupCreate (&aux[0][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&aux[1][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&aux[2][j]) );
  
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[0][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[1][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[2][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[3][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[4][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[5][j]) );
  
      if( (Ix%2) == 0 )
      {
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[0][j], 1,  lxTags[Rx  ][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[1][j], 1,  ldTags[Rx  ][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[2][j], 1,  lyTags[Rx  ][Iy  ]) );

        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[0][j], 0,  ptTags[Rx  ][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[1][j], 0,  ptTags[Rx+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[2][j], 0,  ptTags[Rx+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[3][j], 0,  ptTags[Rx  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[4][j], 0,  ptTags[Rx  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[5][j], 0,  ptTags[Rx  ][Iy  ]) );
      }
      else
      {
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[0][j], 1,  lxTags[Rx  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[1][j], 1,  ldTags[Rx  ][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[2][j], 1,  lyTags[Rx+1][Iy  ]) );

        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[0][j], 0,  ptTags[Rx+1][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[1][j], 0,  ptTags[Rx  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[2][j], 0,  ptTags[Rx  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[3][j], 0,  ptTags[Rx+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[4][j], 0,  ptTags[Rx+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[5][j], 0,  ptTags[Rx+1][Iy+1]) );
      }
      HXT_OMP_CHECK( hxtGroupBuild  ( aux[0][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[0][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[1][j],   offset) ); 
  
      HXT_OMP_CHECK( hxtGroupBuild  ( aux[1][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[2][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[3][j],   offset) ); 
  
      HXT_OMP_CHECK( hxtGroupBuild  ( aux[2][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[4][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[5][j],   offset) ); 
    }

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Wave                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *wave = NULL;
    HXT_OMP_CHECK( hxtFormulationCreate    (&wave        ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( wave, domain));
    HXT_OMP_CHECK( hxtFormulationHelmholtz ( wave, fK2   ));

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation HABC                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *HABC[3];
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[2] ));

    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[0], bnd[0] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[1], bnd[1] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[2], bnd[2] ));
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[0], aux[0][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[1], aux[1][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[2], aux[2][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[0], fK , theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[1], fK , theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[2], fK , theta ));

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Help                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *Help[3];
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[2] ));
  
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[0], bnd[0] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[1], bnd[1] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[2], bnd[2] ));
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Help[0], aux[0][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( Help[1], aux[1][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( Help[2], aux[2][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[0], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[1], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[2], fK2, theta));

    // * --------------------------------------------------------------------- * //
    // * --                   Formulation Treatment                         -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *Trea[6];
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[0]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[1]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[2]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[3]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[4]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[5]) );
  
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[0], cnr[5][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[1], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[2], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[3], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[4], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[5], cnr[0][j]) );
    }
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[0], cnr[0][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[1], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[2], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[3], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[4], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[5], cnr[5][j]) );
    }
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[0], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[1], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[2], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[3], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[4], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[5], fK, theta) );

    // * --------------------------------------------------------------------- * //
    // * --                              System                             -- * //
    // * --------------------------------------------------------------------- * //
    HXTSystem *sys = NULL;
    HXT_OMP_CHECK( hxtSystemCreate (&sys         ) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, wave   ) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[3]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[4]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[5]) );
    HXT_OMP_CHECK( hxtSystemBuild  ( sys, 1      ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( sys         ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( sys, 1      ) );
  
    // * --------------------------------------------------------------------- * //
    // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemCSTR( sys, 0, source, fSource) ); 
  
    // * --------------------------------------------------------------------- * //
    // * --                          PARDISO SOLVER                         -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemSolver( sys              ) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys, -1,   -1, 11) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys, -1,   -1, 22) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys,  0, nrhs, 33) );
  
    // * --------------------------------------------------------------------- * //
    // * --                          WRITE RESULTS                          -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, threadid+1) );

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Update                         -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *EUpd[3];
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[2] ));
  
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[0], bnd[0] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[1], bnd[1] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[2], bnd[2] ));
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[0], aux[0][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[1], aux[1][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[2], aux[2][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[0], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[1], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[2], fK, theta) );

    HXTFormulation *CUpd[6];
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[3]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[4]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[5]) );
  
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[0], cnr[5][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[1], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[2], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[3], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[4], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[5], cnr[0][j]) );
    }
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[0], cnr[0][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[1], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[2], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[3], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[4], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[5], cnr[5][j]) );
    }
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[0], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[1], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[2], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[3], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[4], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[5], fK, theta) );

    // * --------------------------------------------------------------------- * //
    // * --                        Update System                            -- * //
    // * --------------------------------------------------------------------- * //
    const int Nx = Nx_DOM;
    const int Ny = Ny_DOM;

    HXTSystem *sysUpd[3];
    HXT_OMP_CHECK( hxtSystemCreate (&sysUpd[0] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&sysUpd[1] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&sysUpd[2] ) );
    if((Iy   > 0 ) && (Ix%2 == 0))
    {
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[0], EUpd[0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[0], CUpd[0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[0], CUpd[1]) );
    }
    if((Ix   > 0 ) && (Ix%2 == 0))
    {
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[2], EUpd[2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[2], CUpd[4]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[2], CUpd[5]) );
    }
    if((Ix+1 < Nx) && (Ix%2 == 1))
    {
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[2], EUpd[2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[2], CUpd[4]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[2], CUpd[5]) );
    }
    if((Iy+1 < Ny) && (Ix%2 == 1))
    {
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[0], EUpd[0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[0], CUpd[0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[0], CUpd[1]) );
    }
    HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[1], EUpd[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[1], CUpd[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sysUpd[1], CUpd[3]) );

    HXT_OMP_CHECK( hxtSystemBuild  ( sysUpd[0], 1      ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( sysUpd[1], 1      ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( sysUpd[2], 1      ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( sysUpd[0]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( sysUpd[1]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( sysUpd[2]         ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( sysUpd[0], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( sysUpd[1], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( sysUpd[2], 1      ) );

    HXT_OMP_CHECK( hxtSystemSolver ( sysUpd[0] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( sysUpd[1] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( sysUpd[2] ) );
  
    HXT_OMP_CHECK( hxtSystemPhase  ( sysUpd[0], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( sysUpd[1], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( sysUpd[2], -1, -1, 12) );
  
    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                  Krylov                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtGMRES1Triangle ( sys, sysUpd, Ix, Iy, Nx, Ny, 100) );

    // * --------------------------------------------------------------------- * //
    // * --                     DIRICHLET BOUNDARY AGAIN                    -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemCSTR ( sys, 0, source, fSource) ); 
    HXT_OMP_CHECK( hxtSystemPhase( sys, 0, nrhs  , 33     ) );

    // * --------------------------------------------------------------------- * //
    // * --                          WRITE RESULTS                          -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemWriteMSH(sys, domain, "ddm.msh", 0, threadid+1) );

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                 RELEASE                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtSystemPhase ( sys      , -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( sysUpd[0], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( sysUpd[1], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( sysUpd[2], -1,   -1, -1) );

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                 DELETE                                * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtSystemDelete     (&sys      ) );
    HXT_OMP_CHECK( hxtSystemDelete     (&sysUpd[0]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&sysUpd[1]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&sysUpd[2]) );
  
    HXT_OMP_CHECK( hxtFormulationDelete(&wave   ) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[4]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[5]) );
  
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[4]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[5]) );
  
    HXT_OMP_CHECK( hxtGroupDelete(&domain ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[0] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[1] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[2] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&source ) );
  
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupDelete(&aux[0][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&aux[1][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&aux[2][j] ) );
  
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[0][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[1][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[2][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[3][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[4][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[5][j] ) );
    }

  }

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static double fgetK(const double *xyz)
{
  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double_complex fSource(const double *xyz)
{
  const double    k = fgetK(xyz);
  const double    x = xyz[0];

  return -( cos(k * x) + I*sin(k * x) );
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}
