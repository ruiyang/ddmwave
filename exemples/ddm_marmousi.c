#define HXT_OMP_CHECK(status)						\
do{ HXTStatus s = (status);						\
    if(s!=HXT_STATUS_OK){						\
      HXT_TRACE_MSG(s, "cannot break OpenMP region -> exiting");	\
      fflush(stdout); fflush(stderr);					\
      exit(s);								\
    }									\
}while(0);                                                              \

#include "hxt_entity.h"
#include "hxt_group.h"
#include "hxt_formulation.h"
#include "hxt_system.h"
#include "hxt_gmres1.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nx_DOM 9
#define Ny_DOM 3

#define OMEGA      ( 2. * M_PI * 10) // * HERE, OMEGA  =  2 * M_PI *  Frequence.
#define N_LAMBDA   ( 10            ) // * HERE, N_LAMBDA IS NUM OF POINTS PER L.

static const int    order  = 1;
static const double theta  = M_PI / 3.;
static const int    NPade  = 4;
static const int    dim    = 2;
static const int    nrhs   = 1;

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                CONSTANTS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nt_DOM (Nx_DOM * Ny_DOM)
#define Lx     (9000.0)
#define Ly     (2904.0)

#define SCA_X1 (+Lx/8.*1.)
#define SCA_Y1 (-10.     )
#define SCA_X2 (+Lx/8.*7.)
#define SCA_Y2 (-10.     )

#define LSx (Lx / Nx_DOM)
#define LSy (Ly / Ny_DOM)

#define LSPMin     ( 1500                   )
#define WAVENUMBER ( OMEGA      / LSPMin    )
#define FREQ       ( WAVENUMBER / (2*M_PI)  )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define LC         ( LAMBDA     / N_LAMBDA  )

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
double *marmousi_x;
double *marmousi_y;
double *marmousi_vel;
int     marmousi_row;
int     marmousi_col;
        // *  allocate                         *marmousi_x, *marmousi_y, *marmousi_vel
        // *  read data from file and store in *marmousi_x, *marmousi_y, *marmousi_vel
void    marmousiReader(const char   *file);
double biInterpolation(const double *xyz );  // * use marmousi data to retrun a double 

static double_complex fK     (const double *xyz);
static double_complex fK2    (const double *xyz);
static double_complex fSource(const double *xyz);

int main(int argc, char **argv)
{
  marmousiReader("../exemples/rectangularDomMarmousi.dat");

  if(argc != 2)
  {
    printf("Example:\n\t ./main nIters\n");
    return 0;
  }
  const int nIters = atoi(argv[1]);
  printf("Example:\n\t ./main nIters\n"            );
  printf("Usage: %d nIters     [options]\n", nIters);
  printf("LC: %f\n", LC);

  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ("marmousiDom",    &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                             SOURCE                              -- * //
  // * --------------------------------------------------------------------- * //
  int sca_p1 = gmshModelGeoAddPoint(SCA_X1, SCA_Y1, 0, 0, -1, &ierr); 
  int sca_p2 = gmshModelGeoAddPoint(SCA_X2, SCA_Y2, 0, 0, -1, &ierr); 

  // * --------------------------------------------------------------------- * //
  // * --                           RECTANGLES                            -- * //
  // * --------------------------------------------------------------------- * //
  int ptTags[Nx_DOM+1][Ny_DOM+1] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      ptTags[i][j] = gmshModelGeoAddPoint(   +Lx*i/Nx_DOM, 
                                          -Ly+Ly*j/Ny_DOM, 0, 0, -1, &ierr);
    }
  }

  int lxTags[Nx_DOM  ][Ny_DOM+1] = {{0}};
  int lyTags[Nx_DOM+1][Ny_DOM  ] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      if(i < Nx_DOM) lxTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i+1][j], -1, &ierr);
      if(j < Ny_DOM) lyTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i][j+1], -1, &ierr);
    }
  }

  int recLlps[Nx_DOM][Ny_DOM] = {{0}};
  int recTags[Nx_DOM][Ny_DOM] = {{0}};
  int i1InSurface = -1;
  int j1InSurface = -1;
  int i2InSurface = -1;
  int j2InSurface = -1;

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      int curveTags[] = { lxTags[i  ][j  ],
                          lyTags[i+1][j  ],
                         -lxTags[i  ][j+1],
                         -lyTags[i  ][j  ],
                        };

      recLlps[i][j] = gmshModelGeoAddCurveLoop   ( curveTags    , 4, -1, &ierr);
      recTags[i][j] = gmshModelGeoAddPlaneSurface(&recLlps[i][j], 1, -1, &ierr);
    
      if( ( i    * LSx     ) <= SCA_X1 &&
          ((i+1) * LSx     ) >  SCA_X1 &&
          ( j    * LSy - Ly) <= SCA_Y1 &&
          ((j+1) * LSy - Ly) >  SCA_Y1  )
      {
        i1InSurface = i;
        j1InSurface = j;
      }
      if( ( i    * LSx     ) <= SCA_X2 &&
          ((i+1) * LSx     ) >  SCA_X2 &&
          ( j    * LSy - Ly) <= SCA_Y2 &&
          ((j+1) * LSy - Ly) >  SCA_Y2  )
      {
        i2InSurface = i;
        j2InSurface = j;
      }
    }
  }

  // * --------------------------------------------------------------------- * //
  // * --                         GENERATE  MESH                          -- * //
  // * --------------------------------------------------------------------- * //
  gmshModelGeoSynchronize(        &ierr);
  gmshModelMeshEmbed     (0, &sca_p1, 1, 2, recTags[i1InSurface][j1InSurface], &ierr);
  gmshModelMeshEmbed     (0, &sca_p2, 1, 2, recTags[i2InSurface][j2InSurface], &ierr);
  gmshModelMeshGenerate  (2,      &ierr);
  gmshModelMeshSetOrder  (order,  &ierr);
  gmshWrite("rectangularDom.msh", &ierr);
  printf("MESHING FINISHED\n");

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                  DDM                                  * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  omp_set_num_threads(Nt_DOM);

  #pragma omp parallel
  {
    const int threadid = omp_get_thread_num ();

    const int Nx = (Nx_DOM       );
    const int Ny = (Ny_DOM       );
    const int Ix = (threadid / Ny);
    const int Iy = (threadid % Ny);

    // * --------------------------------------------------------------------- * //
    // * --                              Group                              -- * //
    // * --------------------------------------------------------------------- * //
    size_t offset = 0;
  
    HXTGroup *domain = NULL;
    HXT_OMP_CHECK( hxtGroupCreate   (&domain   ) );
    HXT_OMP_CHECK( hxtGroupAddEntity( domain, 2, recTags[Ix][Iy]) );
    HXT_OMP_CHECK( hxtGroupAddEntity( domain, 0, sca_p1         ) );
    HXT_OMP_CHECK( hxtGroupAddEntity( domain, 0, sca_p2         ) );
    HXT_OMP_CHECK( hxtGroupBuild    ( domain, offset) ); 

    HXTGroup *source = NULL;
    HXT_OMP_CHECK( hxtGroupCreate   (&source   ) );
    HXT_OMP_CHECK( hxtGroupAddEntity( source, 0, sca_p1) );
    HXT_OMP_CHECK( hxtGroupAddEntity( source, 0, sca_p2) );
    HXT_OMP_CHECK( hxtGroupBuild    ( source, offset) ); 
  
    HXTGroup *bnd[4];
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[0]) );
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[1]) );
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[2]) );
    HXT_OMP_CHECK( hxtGroupCreate(&bnd[3]) );
  
    HXT_OMP_CHECK( hxtGroupAddEntity( bnd[0], 1, lxTags[Ix  ][Iy  ]) );
    HXT_OMP_CHECK( hxtGroupAddEntity( bnd[1], 1, lyTags[Ix+1][Iy  ]) );
    HXT_OMP_CHECK( hxtGroupAddEntity( bnd[2], 1, lxTags[Ix  ][Iy+1]) );
    HXT_OMP_CHECK( hxtGroupAddEntity( bnd[3], 1, lyTags[Ix  ][Iy  ]) );
  
    HXT_OMP_CHECK( hxtGroupBuild( bnd[0], offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild( bnd[1], offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild( bnd[2], offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild( bnd[3], offset) ); 
  
    HXTGroup *aux[4][NPade];
    HXTGroup *cnr[8][NPade];
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupCreate (&aux[0][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&aux[1][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&aux[2][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&aux[3][j]) );
  
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[0][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[1][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[2][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[3][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[4][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[5][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[6][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&cnr[7][j]) );
  
      HXT_OMP_CHECK( hxtGroupAddEntity( aux[0][j], 1,  lxTags[Ix  ][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( aux[1][j], 1,  lyTags[Ix+1][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( aux[2][j], 1,  lxTags[Ix  ][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( aux[3][j], 1,  lyTags[Ix  ][Iy  ]) );
    
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[0][j], 0,  ptTags[Ix  ][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[1][j], 0,  ptTags[Ix+1][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[2][j], 0,  ptTags[Ix+1][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[3][j], 0,  ptTags[Ix+1][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[4][j], 0,  ptTags[Ix+1][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[5][j], 0,  ptTags[Ix  ][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[6][j], 0,  ptTags[Ix  ][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( cnr[7][j], 0,  ptTags[Ix  ][Iy  ]) );
  
      HXT_OMP_CHECK( hxtGroupBuild  ( aux[0][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[0][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[1][j],   offset) ); 

      HXT_OMP_CHECK( hxtGroupBuild  ( aux[2][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[4][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[5][j],   offset) ); 

      HXT_OMP_CHECK( hxtGroupBuild  ( aux[1][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[2][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[3][j],   offset) ); 

      HXT_OMP_CHECK( hxtGroupBuild  ( aux[3][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[6][j],   offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( cnr[7][j],   offset) ); 
    }

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Wave                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *wave = NULL;
    HXT_OMP_CHECK( hxtFormulationCreate    (&wave        ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( wave, domain));
    HXT_OMP_CHECK( hxtFormulationHelmholtz ( wave, fK2   ));

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation HABC                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *HABC[4];
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[2] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[3] ));

    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[0], bnd[0] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[1], bnd[1] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[2], bnd[2] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[3], bnd[3] ));
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[0], aux[0][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[1], aux[1][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[2], aux[2][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[3], aux[3][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[0], fK , theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[1], fK , theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[2], fK , theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[3], fK , theta ));

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Help                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *Help[4];
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[2] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[3] ));
  
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[0], bnd[0] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[1], bnd[1] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[2], bnd[2] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( Help[3], bnd[3] ));
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Help[0], aux[0][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( Help[1], aux[1][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( Help[2], aux[2][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( Help[3], aux[3][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[0], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[1], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[2], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[3], fK2, theta));

    // * --------------------------------------------------------------------- * //
    // * --                   Formulation Treatment                         -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *Trea[8];
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[0]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[1]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[2]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[3]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[4]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[5]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[6]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&Trea[7]) );
  
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[0], cnr[7][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[1], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[2], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[3], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[4], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[5], cnr[6][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[6], cnr[5][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[7], cnr[0][j]) );
    }
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[0], cnr[0][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[1], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[2], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[3], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[4], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[5], cnr[5][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[6], cnr[6][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( Trea[7], cnr[7][j]) );
    }
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[0], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[1], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[2], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[3], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[4], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[5], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[6], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[7], fK, theta) );

    // * --------------------------------------------------------------------- * //
    // * --                              System                             -- * //
    // * --------------------------------------------------------------------- * //
    HXTSystem *sys = NULL;
    HXT_OMP_CHECK( hxtSystemCreate (&sys         ) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, wave   ) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[3]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[3]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[3]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[4]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[5]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[6]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Trea[7]) );
    HXT_OMP_CHECK( hxtSystemBuild  ( sys, nrhs   ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( sys         ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( sys, 1      ) );
  
    // * --------------------------------------------------------------------- * //
    // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
    // * --------------------------------------------------------------------- * //
    for(size_t l = 0; l < nrhs; l++)
      HXT_OMP_CHECK( hxtSystemCSTR(sys, l, source, fSource) ); 
  
    // * --------------------------------------------------------------------- * //
    // * --                          PARDISO SOLVER                         -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemSolver( sys              ) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys, -1,   -1, 11) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys, -1,   -1, 22) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys,  0, nrhs, 33) );
  
    // * --------------------------------------------------------------------- * //
    // * --                          WRITE RESULTS                          -- * //
    // * --------------------------------------------------------------------- * //
    //HXT_OMP_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, threadid+1) );

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Update                         -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *EUpd[4];
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[2] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[3] ));
  
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[0], bnd[0] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[1], bnd[1] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[2], bnd[2] ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[3], bnd[3] ));
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[0], aux[0][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[1], aux[1][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[2], aux[2][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[3], aux[3][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[0], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[1], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[2], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[3], fK, theta) );

    HXTFormulation *CUpd[8];
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[3]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[4]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[5]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[6]) );
    HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[7]) );
  
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[0], cnr[7][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[1], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[2], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[3], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[4], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[5], cnr[6][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[6], cnr[5][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[7], cnr[0][j]) );
    }
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[0], cnr[0][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[1], cnr[1][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[2], cnr[2][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[3], cnr[3][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[4], cnr[4][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[5], cnr[5][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[6], cnr[6][j]) );
      HXT_OMP_CHECK( hxtFormulationAddField( CUpd[7], cnr[7][j]) );
    }
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[0], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[1], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[2], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[3], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[4], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[5], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[6], fK, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[7], fK, theta) );

    // * --------------------------------------------------------------------- * //
    // * --                        Update System                            -- * //
    // * --------------------------------------------------------------------- * //
    HXTSystem *bcSys[4];
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[0] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[1] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[2] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[3] ) );
    if(Iy     > 0 ){
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[0], EUpd[0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[0], CUpd[0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[0], CUpd[1]) );
    }
    if(Ix + 1 < Nx){
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[1], EUpd[1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[1], CUpd[2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[1], CUpd[3]) );
    }
    if(Iy + 1 < Ny){
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[2], EUpd[2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[2], CUpd[4]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[2], CUpd[5]) );
    }
    if(Ix     > 0 ){
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[3], EUpd[3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[3], CUpd[6]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[3], CUpd[7]) );
    }
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[0], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[1], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[2], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[3], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[0]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[1]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[2]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[3]         ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[0], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[1], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[2], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[3], 1      ) );

    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[0] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[1] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[2] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[3] ) );
  
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[0], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[1], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[2], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[3], -1, -1, 12) );
  
    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                  Krylov                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXTGMRES1 *kry = NULL;
    HXT_OMP_CHECK(hxtGMRES1Create(&kry) );
    HXT_OMP_CHECK(hxtGMRES1Build ( kry, 80, sys, nrhs, bcSys, 4, 
			          &source, 1, Ix, Iy,  Nx, Ny) );
    HXT_OMP_CHECK(hxtGMRES1Solve ( kry) );
    HXT_OMP_CHECK(hxtGMRES1Delete(&kry) );

    // * --------------------------------------------------------------------- * //
    // * --                     DIRICHLET BOUNDARY AGAIN                    -- * //
    // * --------------------------------------------------------------------- * //
    for(size_t l = 0; l < nrhs; l++)
    {
      HXT_OMP_CHECK( hxtSystemCSTR (sys, l, source, fSource) ); 
      HXT_OMP_CHECK( hxtSystemPhase(sys, l, 1     , 33     ) );
    }

    // * --------------------------------------------------------------------- * //
    // * --                          WRITE RESULTS                          -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, threadid+1) );

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                 RELEASE                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtSystemPhase ( sys     , -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[0], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[1], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[2], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[3], -1,   -1, -1) );

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                 DELETE                                * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtSystemDelete     (&sys      ) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[0]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[1]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[2]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[3]) );
  
    HXT_OMP_CHECK( hxtFormulationDelete(&wave   ) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[4]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[5]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[6]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Trea[7]) );
  
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[4]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[5]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[6]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[7]) );
  
    HXT_OMP_CHECK( hxtGroupDelete(&domain ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[0] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[1] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[2] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&bnd[3] ) );
    HXT_OMP_CHECK( hxtGroupDelete(&source ) );
  
    for(int j = 0; j < NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupDelete(&aux[0][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&aux[1][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&aux[2][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&aux[3][j] ) );
  
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[0][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[1][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[2][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[3][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[4][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[5][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[6][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&cnr[7][j] ) );
    }

  }

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static double fgetK(const double *xyz)
{
  const double LSP = biInterpolation(xyz);
  const double Hz  = OMEGA  / (2. * M_PI     );
  const double k   = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double_complex fSource(const double *xyz)
{
//const double    k = fgetK(xyz);
//const double    x = xyz[0];

  return ( cos(M_PI/4) + I*sin(M_PI/4) );
//return 1.;
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}

void marmousiReader(const char *file)
{
  int i = 0;
  int j = 0;
  char buf[BUFSIZ]={""};
  int offset;
  char *data = NULL;

  // * OPEN FILE
  FILE *fp;
  fp = fopen(file,"r");
  rewind (fp);
  fgets(buf, BUFSIZ, fp);
  sscanf(buf, "%d %d", &marmousi_row, &marmousi_col);

  marmousi_x   = malloc(marmousi_row             *sizeof(double));
  marmousi_y   = malloc(marmousi_col             *sizeof(double));
  marmousi_vel = malloc(marmousi_row*marmousi_col*sizeof(double));

  // * ROW
  fgets(buf, BUFSIZ, fp);
  data = buf;
  while (sscanf(data, " %lf%n", &marmousi_x[i], &offset) == 1)
  {
    data += offset;
    i++;
  }
  fgets(buf, BUFSIZ, fp);
  // * COL
  data = buf;
  while (sscanf(data, " %lf%n", &marmousi_y[j], &offset) == 1)
  {
    data += offset;
    j++;
  }

  // * READ VELOCITY
  for(j = 0; j < marmousi_col; j++)
  { 
    i = 0;
    fgets(buf, BUFSIZ, fp);
    char *data = buf;
    while (sscanf(data, " %lf%n", &marmousi_vel[i*marmousi_col+j], &offset) == 1)
    {
      data += offset;
      i++;
    }
  }
  fclose(fp);
}

double biInterpolation(const double *xyz)
{
  const int ws = 24;
  const int hs = 24;

  const double xvalue = xyz[0];
  const double yvalue = xyz[1];

  int x1 = (xvalue - marmousi_x[0]) / ws;
  int y1 = (yvalue - marmousi_y[0]) / hs;
  if( x1 == 383) x1 -= 1;
  if( y1 == 121) y1 -= 1;
  const int x2 = x1 + 1;
  const int y2 = y1 + 1;
  
  double NP1 = marmousi_vel[x1*marmousi_col + y1];
  double NP2 = marmousi_vel[x2*marmousi_col + y1];
  double NP3 = marmousi_vel[x1*marmousi_col + y2];
  double NP4 = marmousi_vel[x2*marmousi_col + y2];

  double PW1 = (marmousi_y[y2] - yvalue)*(marmousi_x[x2] - xvalue);
  double PW2 = (marmousi_y[y2] - yvalue)*(xvalue - marmousi_x[x1]);
  double PW3 = (marmousi_x[x2] - xvalue)*(yvalue - marmousi_y[y1]);
  double PW4 = (yvalue - marmousi_y[y1])*(xvalue - marmousi_x[x1]);
//double DIV = 24 * (x2 - x1) * 24 * (y2 - y1);
  double DIV = 24. * 24.;

  double re = PW1 * NP1 + PW2 * NP2 + PW3 * NP3 + PW4 * NP4;
         re = re / DIV;

  return re;
}
