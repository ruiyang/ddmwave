#define HXT_OMP_CHECK(status)                                           \
do{ HXTStatus s = (status);                                             \
    if(s!=HXT_STATUS_OK){                                               \
      HXT_TRACE_MSG(s, "cannot break OpenMP region -> exiting");        \
      fflush(stdout); fflush(stderr);                                   \
      exit(s);                                                          \
    }                                                                   \
}while(0);                                                              \

#include "hxt_entity.h"
#include "hxt_group.h"
#include "hxt_formulation.h"
#include "hxt_system.h"
#include "hxt_gmres2.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define OMEGA      ( 2. * M_PI * 1)
#define LSP        ( 1.           )
#define N_LAMBDA   ( 3            )

#define theta  (M_PI / 3.)
#define NPade  (8        )

#define order  7
#define nrhs   1

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                CONSTANTS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nx_DOM 2
#define Ny_DOM 3
#define Nt_DOM (Nx_DOM * Ny_DOM)

#define Pi_SCA 0
#define Pj_SCA 0

#define Lx     ( 7.5)
#define Ly     ( 7.5)
#define X_SCA  (1.25)
#define Y_SCA  (1.25)
#define R_SCA  (1.00)

#define LSx (Lx / Nx_DOM)
#define LSy (Ly / Ny_DOM)

#define X0_DOM (-X_SCA - Pi_SCA*LSx)
#define Y0_DOM (-Y_SCA - Pj_SCA*LSy)
#define Z0_DOM  +0

#define WAVENUMBER ( OMEGA      / LSP       )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define FREQ       ( WAVENUMBER / (2*M_PI)  )
#define LC         ( LAMBDA     / N_LAMBDA  )

static double_complex fK2    (const double *xyz);
static double_complex fK     (const double *xyz);
static double_complex fSource(const double *xyz);

int main(int argc, char **argv)
{  
  if( Lx/Nx_DOM <= 2*R_SCA ||
      Ly/Ny_DOM <= 2*R_SCA  )
  {
    printf("Size of subdomain too small!\n");
    printf("Lx / Nx_DOM : %f\n", Lx/Nx_DOM );
    printf("Ly / Ny_DOM : %f\n", Ly/Ny_DOM );
    printf("2  *  R_SCA : %f\n", 2 * R_SCA );
    exit(0);
  }
  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ("rectangularDom", &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                             SOURCE                              -- * //
  // * --------------------------------------------------------------------- * //
  int p0 = gmshModelGeoAddPoint(     0, 0, 0, 0, -1, &ierr);
  int p1 = gmshModelGeoAddPoint( R_SCA, 0, 0, 0, -1, &ierr);
  int p2 = gmshModelGeoAddPoint(-R_SCA, 0, 0, 0, -1, &ierr);

  int scaTags[2] = {0};
      scaTags[0] = gmshModelGeoAddCircleArc(p1, p0, p2, -1, 0, 0, 0, &ierr);
      scaTags[1] = gmshModelGeoAddCircleArc(p2, p0, p1, -1, 0, 0, 0, &ierr);
  int scaLlp     = gmshModelGeoAddCurveLoop(scaTags, 2, -1,          &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                           RECTANGLES                            -- * //
  // * --------------------------------------------------------------------- * //
  int ptTags[Nx_DOM+1][Ny_DOM+1] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      ptTags[i][j] = gmshModelGeoAddPoint(X0_DOM + Lx*i/Nx_DOM, 
                                          Y0_DOM + Ly*j/Ny_DOM, 
                                          Z0_DOM, 0, -1, &ierr);
    }
  }

  int lxTags[Nx_DOM  ][Ny_DOM+1] = {{0}};
  int lyTags[Nx_DOM+1][Ny_DOM  ] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      if(i < Nx_DOM) lxTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i+1][j], -1, &ierr);
      if(j < Ny_DOM) lyTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i][j+1], -1, &ierr);
    }
  }

  int recLlps[Nx_DOM][Ny_DOM] = {{0}};
  int recTags[Nx_DOM][Ny_DOM] = {{0}};

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      if(i == Pi_SCA && j == Pj_SCA)
      {
        int curveTags[] = { lxTags[i  ][j  ],
                            lyTags[i+1][j  ],
                           -lxTags[i  ][j+1],
                           -lyTags[i  ][j  ],
                          };

        recLlps[i][j]  = gmshModelGeoAddCurveLoop   (curveTags, 4, -1, &ierr);
        int wireTags[] = {recLlps[i][j], scaLlp};
        recTags[i][j]  = gmshModelGeoAddPlaneSurface(wireTags , 2, -1, &ierr);
      }
      else
      {
        int curveTags[] = { lxTags[i  ][j  ],
                            lyTags[i+1][j  ],
                           -lxTags[i  ][j+1],
                           -lyTags[i  ][j  ],
                          };

        recLlps[i][j] = gmshModelGeoAddCurveLoop   ( curveTags    , 4, -1, &ierr);
        recTags[i][j] = gmshModelGeoAddPlaneSurface(&recLlps[i][j], 1, -1, &ierr);
      }
    }
  }

  // * --------------------------------------------------------------------- * //
  // * --                         GENERATE  MESH                          -- * //
  // * --------------------------------------------------------------------- * //
  gmshModelGeoSynchronize(        &ierr);
  gmshModelMeshGenerate  (2,      &ierr);
  gmshModelMeshSetOrder  (order,  &ierr);
  gmshWrite("rectangularDom.msh", &ierr);
  printf("MESHING FINISHED\n");

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                  DDM                                  * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  omp_set_num_threads(Ny_DOM);

  #pragma omp parallel
  {
    const int threadid = omp_get_thread_num (); // * equal to Iy

    const int Nx =   Nx_DOM;
    const int Ny =   Ny_DOM;
          int Ix =        0;
    const int Iy = threadid;

    HXTGroup *domain[Nx];
    HXTGroup *source[Nx];
    HXTGroup *bnd[Nx][4];
    HXTGroup *aux[Nx][4][NPade];
    HXTGroup *cnr[Nx][8][NPade];

    HXTFormulation *wave[Nx]   ;
    HXTFormulation *HABC[Nx][4];
    HXTFormulation *Help[Nx][4];
    HXTFormulation *Trea[Nx][8];
    HXTFormulation *EUpd[Nx][4];
    HXTFormulation *CUpd[Nx][8];

    HXTSystem *  sys[  Nx];
    HXTSystem *bcSys[4*Nx];

    for(Ix = 0; Ix < Nx; Ix++)
    {
      // * --------------------------------------------------------------------- * //
      // * --                              Group                              -- * //
      // * --------------------------------------------------------------------- * //
      size_t offset = 0;
  
      HXT_OMP_CHECK( hxtGroupCreate   (&domain[Ix]   ) );
      HXT_OMP_CHECK( hxtGroupAddEntity( domain[Ix], 2, recTags[Ix][Iy]) );
      HXT_OMP_CHECK( hxtGroupBuild    ( domain[Ix], offset) ); 
  
      HXT_OMP_CHECK( hxtGroupCreate   (&source[Ix]   ) );
      HXT_OMP_CHECK( hxtGroupAddEntity( source[Ix], 1, scaTags[0]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( source[Ix], 1, scaTags[1]) );
      HXT_OMP_CHECK( hxtGroupBuild    ( source[Ix], offset) ); 
    
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][0]) );
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][1]) );
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][2]) );
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][3]) );
    
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][0], 1, lxTags[Ix  ][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][1], 1, lyTags[Ix+1][Iy  ]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][2], 1, lxTags[Ix  ][Iy+1]) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][3], 1, lyTags[Ix  ][Iy  ]) );
    
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][0], offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][1], offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][2], offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][3], offset) ); 
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][0][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][1][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][2][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][3][j]) );
    
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][0][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][7][j]) );
    
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][0][j], 1,  lxTags[Ix  ][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][1][j], 1,  lyTags[Ix+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][2][j], 1,  lxTags[Ix  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][3][j], 1,  lyTags[Ix  ][Iy  ]) );
      
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][0][j], 0,  ptTags[Ix  ][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][1][j], 0,  ptTags[Ix+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][2][j], 0,  ptTags[Ix+1][Iy  ]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][3][j], 0,  ptTags[Ix+1][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][4][j], 0,  ptTags[Ix+1][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][5][j], 0,  ptTags[Ix  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][6][j], 0,  ptTags[Ix  ][Iy+1]) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][7][j], 0,  ptTags[Ix  ][Iy  ]) );
    
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][0][j], ++offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][0][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][1][j],   offset) ); 
  
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][2][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][4][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][5][j],   offset) ); 
  
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][1][j], ++offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][2][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][3][j],   offset) ); 
  
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][3][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][6][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][7][j],   offset) ); 
      }

      // * --------------------------------------------------------------------- * //
      // * --                      Formulation Wave                           -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate    (&wave[Ix]             ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( wave[Ix], domain[Ix]));
      HXT_OMP_CHECK( hxtFormulationHelmholtz ( wave[Ix], fK2        ));
  
      // * --------------------------------------------------------------------- * //
      // * --                      Formulation HABC                           -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][3] ));
  
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][0], bnd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][1], bnd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][2], bnd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][3], bnd[Ix][3] ));
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][0], aux[Ix][0][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][1], aux[Ix][1][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][2], aux[Ix][2][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][3], aux[Ix][3][j] ));
      }
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][0], fK , theta ));
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][1], fK , theta ));
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][2], fK , theta ));
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][3], fK , theta ));

      // * --------------------------------------------------------------------- * //
      // * --                      Formulation Help                           -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][3] ));
    
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][0], bnd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][1], bnd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][2], bnd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][3], bnd[Ix][3] ));
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][0], aux[Ix][0][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][1], aux[Ix][1][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][2], aux[Ix][2][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][3], aux[Ix][3][j] ));
      }
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][0], fK2, theta));
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][1], fK2, theta));
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][2], fK2, theta));
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][3], fK2, theta));
  
      // * --------------------------------------------------------------------- * //
      // * --                   Formulation Treatment                         -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][7]) );
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][0], cnr[Ix][7][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][1], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][2], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][3], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][4], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][5], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][6], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][7], cnr[Ix][0][j]) );
      }
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][0], cnr[Ix][0][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][1], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][2], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][3], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][4], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][5], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][6], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][7], cnr[Ix][7][j]) );
      }
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][0], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][1], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][2], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][3], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][4], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][5], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][6], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][7], fK, theta) );

      // * --------------------------------------------------------------------- * //
      // * --                              System                             -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCreate (&sys[Ix]              ) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], wave[Ix]   ) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][4]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][5]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][6]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][7]) );
      HXT_OMP_CHECK( hxtSystemBuild  ( sys[Ix], 1           ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( sys[Ix]              ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( sys[Ix], 1           ) );
    
      // * --------------------------------------------------------------------- * //
      // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCSTR(sys[Ix], 0, source[Ix], fSource) ); 
    
      // * --------------------------------------------------------------------- * //
      // * --                          PARDISO SOLVER                         -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemSolver( sys[Ix]              ) );
      HXT_OMP_CHECK( hxtSystemPhase ( sys[Ix], -1,   -1, 11) );
      HXT_OMP_CHECK( hxtSystemPhase ( sys[Ix], -1,   -1, 22) );

      // * --------------------------------------------------------------------- * //
      // * --                      Formulation Update                         -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][3] ));
    
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][0], bnd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][1], bnd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][2], bnd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][3], bnd[Ix][3] ));
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][0], aux[Ix][0][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][1], aux[Ix][1][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][2], aux[Ix][2][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][3], aux[Ix][3][j] ));
      }
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][0], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][1], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][2], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][3], fK, theta) );
  
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][7]) );
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][0], cnr[Ix][7][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][1], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][2], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][3], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][4], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][5], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][6], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][7], cnr[Ix][0][j]) );
      }
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][0], cnr[Ix][0][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][1], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][2], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][3], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][4], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][5], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][6], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][7], cnr[Ix][7][j]) );
      }
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][0], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][1], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][2], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][3], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][4], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][5], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][6], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][7], fK, theta) );

      // * --------------------------------------------------------------------- * //
      // * --                        Update System                            -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+0] ) );
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+1] ) );
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+2] ) );
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+3] ) );
      if(Iy     > 0 ){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+0], EUpd[Ix][0]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+0], CUpd[Ix][0]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+0], CUpd[Ix][1]) );
      }
      if(Ix + 1 < Nx){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+1], EUpd[Ix][1]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+1], CUpd[Ix][2]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+1], CUpd[Ix][3]) );
      }
      if(Iy + 1 < Ny){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+2], EUpd[Ix][2]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+2], CUpd[Ix][4]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+2], CUpd[Ix][5]) );
      }
      if(Ix     > 0 ){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+3], EUpd[Ix][3]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+3], CUpd[Ix][6]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+3], CUpd[Ix][7]) );
      }
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+0], 1      ) );
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+1], 1      ) );
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+2], 1      ) );
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+3], 1      ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+0]         ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+1]         ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+2]         ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+3]         ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+0], 1      ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+1], 1      ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+2], 1      ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+3], 1      ) );
  
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+0] ) );
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+1] ) );
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+2] ) );
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+3] ) );
    
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+0], -1, -1, 12) );
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+1], -1, -1, 12) );
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+2], -1, -1, 12) );
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+3], -1, -1, 12) );
    }
  
    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                  Krylov                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXTGMRES2 *kry = NULL;
    HXT_OMP_CHECK( hxtGMRES2Create(&kry   ) );
    HXT_OMP_CHECK( hxtGMRES2Build ( kry, 100, sys, nrhs, bcSys, 4, 
			                      source, 1, Nx, Ny) );
    HXT_OMP_CHECK( hxtGMRES2Solve ( kry, 1) );
    HXT_OMP_CHECK( hxtGMRES2Delete(&kry   ) );

    for(Ix = 0; Ix < Nx; Ix++)
    {
      // * --------------------------------------------------------------------- * //
      // * --                     DIRICHLET BOUNDARY AGAIN                    -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCSTR ( sys[Ix], 0, source[Ix], fSource) ); 
      HXT_OMP_CHECK( hxtSystemPhase( sys[Ix], 0, nrhs, 33           ) );
  
      // * --------------------------------------------------------------------- * //
      // * --                          WRITE RESULTS                          -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemWriteMSH(sys[Ix], domain[Ix], "res.msh", 0, Iy+Ix*Ny+1) );
  
      // * ********************************************************************* * //
      // * ******                                                         ****** * //
      // *                                 RELEASE                               * //
      // * ******                                                         ****** * //
      // * ********************************************************************* * //
      HXT_OMP_CHECK( hxtSystemPhase ( sys[Ix]      , -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+0], -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+1], -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+2], -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+3], -1,   -1, -1) );
  
      // * ********************************************************************* * //
      // * ******                                                         ****** * //
      // *                                 DELETE                                * //
      // * ******                                                         ****** * //
      // * ********************************************************************* * //
      HXT_OMP_CHECK( hxtSystemDelete  (&sys[Ix]      ) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+0]) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+1]) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+2]) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+3]) );
    
      HXT_OMP_CHECK( hxtFormulationDelete(&wave[Ix]   ) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][7]) );
    
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][7]) );
    
      HXT_OMP_CHECK( hxtGroupDelete(&domain[Ix] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][0] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][1] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][2] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][3] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&source[Ix] ) );
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][0][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][1][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][2][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][3][j] ) );
    
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][0][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][1][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][2][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][3][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][4][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][5][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][6][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][7][j] ) );
      }
    }
    #pragma omp barrier
  }

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static double fgetK(const double *xyz)
{
  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double_complex fSource(const double *xyz)
{
  const double    k = fgetK(xyz);
  const double    x = xyz[0];

  return -( cos(k * x) + I*sin(k * x) );
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}
