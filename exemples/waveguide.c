#include "hxt_entity.h"
#include "hxt_group.h"
#include "hxt_formulation.h"
#include "hxt_system.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nx_DOM 1
#define Ny_DOM 1

#define OMEGA      ( 8. * M_PI * 1)
#define LSP        ( 1.           )
#define N_LAMBDA   ( 10           )

#define theta  (M_PI / 3.)
#define NPade  (8        )

#define order  1
#define nrhs   1
#define mdim   3

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                CONSTANTS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
static const double  ky = (M_PI / 0.25 *1.);
static const double  kz = (M_PI / 0.25 *1.);

#define Nt_DOM (Nx_DOM * Ny_DOM)
#define Lx     (0.50)
#define Ly     (0.25)
#define Lz     (0.25)

#define LSx (Lx / Nx_DOM)
#define LSy (Ly / Ny_DOM)

#define WAVENUMBER ( OMEGA      / LSP       )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define FREQ       ( WAVENUMBER / (2*M_PI)  )
#define LC         ( LAMBDA     / N_LAMBDA  )

static double_complex fK2    (const double *xyz);
static double_complex fK     (const double *xyz);
static double_complex fSource(const double *xyz);
static double_complex fZero  (const double *xyz);
static double_complex fKInf  (const double *xyz);

int main(int argc, char **argv)
{
  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ( "cuboidDom",     &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                           RECTANGLES                            -- * //
  // * --------------------------------------------------------------------- * //
  int ptTags[Nx_DOM+1][Ny_DOM+1] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      ptTags[i][j] = gmshModelGeoAddPoint(Lx*i /Nx_DOM, 
                                          Ly*j /Ny_DOM, 
                                          0, 0, -1, &ierr);
    }
  }

  int  lxTags[Nx_DOM  ][Ny_DOM+1]    = {{ 0 }};
  int  lyTags[Nx_DOM+1][Ny_DOM  ]    = {{ 0 }};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      if(i < Nx_DOM) lxTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i+1][j], -1, &ierr);
      if(j < Ny_DOM) lyTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i][j+1], -1, &ierr);
    }
  }

  int recLlps[Nx_DOM][Ny_DOM] = {{0}};
  int recTags[Nx_DOM][Ny_DOM] = {{0}};

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      int curveTags[] = { lxTags[i  ][j  ],
                          lyTags[i+1][j  ],
                         -lxTags[i  ][j+1],
                         -lyTags[i  ][j  ],
                        };

      recLlps[i][j] = gmshModelGeoAddCurveLoop   ( curveTags    , 4, -1, &ierr);
      recTags[i][j] = gmshModelGeoAddPlaneSurface(&recLlps[i][j], 1, -1, &ierr);

      gmshModelGeoMeshSetRecombine         (2, recTags[i][j],            0, &ierr);
      gmshModelGeoMeshSetTransfiniteSurface(recTags[i][j], "Left", NULL, 0, &ierr);
    }
  }

  int    *cubDimTags  [Nx_DOM][Ny_DOM];
  size_t  cubDimTags_n[Nx_DOM][Ny_DOM];

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      int numElements = Lz / LAMBDA * N_LAMBDA;

      const double dx =  0;
      const double dy =  0;
      const double dz = Lz;

      int inDimTags[2] = {2, recTags[i][j]};
  
      gmshModelGeoExtrude( inDimTags, 2, dx, dy, dz, 
                          &cubDimTags[i][j], &cubDimTags_n[i][j], 
                          &numElements, 1, NULL, 0,  true, &ierr);
    }
  }
  // * cubDimTags[i][j][ 1] is tag of front face
  // * cubDimTags[i][j][ 3] is tag of back  face
  // * cubDimTags[i][j][ 5] is tag of down  face
  // * cubDimTags[i][j][ 7] is tag of right face
  // * cubDimTags[i][j][ 9] is tag of up    face
  // * cubDimTags[i][j][11] is tag of left  face

  // * --------------------------------------------------------------------- * //
  // * --                         GENERATE  MESH                          -- * //
  // * --------------------------------------------------------------------- * //
  gmshModelGeoSynchronize(        &ierr);
  gmshModelMeshGenerate  (mdim,   &ierr);
  gmshModelMeshSetOrder  (order,  &ierr);
  gmshWrite     ("cuboidDom.msh", &ierr);
  printf("MESHING FINISHED\n");

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                              MONO DOMAIN                              * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  size_t offset = 0;

  // * --------------------------------------------------------------------- * //
  // * --                              Group                              -- * //
  // * --------------------------------------------------------------------- * //
  HXTGroup *domain = NULL;
  HXTGroup *source = NULL;
  HXTGroup *bnd    = NULL;
  HXTGroup *zero   = NULL;

  HXT_CHECK( hxtGroupCreate(&domain) );
  HXT_CHECK( hxtGroupCreate(&source) );
  HXT_CHECK( hxtGroupCreate(&bnd   ) );
  HXT_CHECK( hxtGroupCreate(&zero  ) );

  HXT_CHECK( hxtGroupAddEntity( domain, mdim  , recTags   [0][0]    ) ); // * main  volume
  HXT_CHECK( hxtGroupAddEntity( zero  , mdim-1, cubDimTags[0][0][ 1]) ); // * front   face
  HXT_CHECK( hxtGroupAddEntity( zero  , mdim-1, cubDimTags[0][0][ 3]) ); // * back    face
  HXT_CHECK( hxtGroupAddEntity( zero  , mdim-1, cubDimTags[0][0][ 5]) ); // * down    face
  HXT_CHECK( hxtGroupAddEntity( bnd   , mdim-1, cubDimTags[0][0][ 7]) ); // * right   face
  HXT_CHECK( hxtGroupAddEntity( zero  , mdim-1, cubDimTags[0][0][ 9]) ); // * up      face
  HXT_CHECK( hxtGroupAddEntity( source, mdim-1, cubDimTags[0][0][11]) ); // * left    face

  HXT_CHECK( hxtGroupBuild( domain, offset) ); 
  HXT_CHECK( hxtGroupBuild( source, offset) ); 
  HXT_CHECK( hxtGroupBuild( bnd   , offset) ); 
  HXT_CHECK( hxtGroupBuild( zero  , offset) ); 

  HXTGroup *aux[NPade];
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtGroupCreate   (&aux[j]) );
    HXT_CHECK( hxtGroupAddEntity( aux[j], mdim-1, cubDimTags[0][0][ 7]) );
    HXT_CHECK( hxtGroupBuild    ( aux[j], ++offset) ); 
  }

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation Wave                           -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *wave = NULL;
  HXT_CHECK( hxtFormulationCreate    (&wave        ));
  HXT_CHECK( hxtFormulationAddField  ( wave, domain));
  HXT_CHECK( hxtFormulationHelmholtz ( wave, fK2   ));

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation HABC                           -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *HABC;
  HXT_CHECK( hxtFormulationCreate  (&HABC      ));
  HXT_CHECK( hxtFormulationAddField( HABC, bnd ));
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( HABC, aux[j] ));
  }
  HXT_CHECK( hxtFormulationHABC( HABC, fKInf, theta ));

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation Help                           -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *Help;
  HXT_CHECK( hxtFormulationCreate  (&Help      ));
  HXT_CHECK( hxtFormulationAddField( Help, bnd ));
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( Help, aux[j] ));
  }
  HXT_CHECK( hxtFormulationHABCHelper( Help, fK2, theta));

  // * --------------------------------------------------------------------- * //
  // * --                              System                             -- * //
  // * --------------------------------------------------------------------- * //
  HXTSystem *sys = NULL;
  HXT_CHECK( hxtSystemCreate (&sys      ) );
  HXT_CHECK( hxtSystemAddFML ( sys, wave) );
  HXT_CHECK( hxtSystemAddFML ( sys, HABC) );
  HXT_CHECK( hxtSystemAddFML ( sys, Help) );
  HXT_CHECK( hxtSystemBuild  ( sys, 1   ) );
  HXT_CHECK( hxtSystemCOOMat ( sys      ) );
  HXT_CHECK( hxtSystemCSRMat ( sys, 1   ) );

  // * --------------------------------------------------------------------- * //
  // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
  // * --------------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemCSTR(sys, 0, source, fSource) ); 
  HXT_CHECK( hxtSystemCSTR(sys, 0, zero  , fZero  ) ); 

  // * --------------------------------------------------------------------- * //
  // * --                          PARDISO SOLVER                         -- * //
  // * --------------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemSolver( sys              ) );
  HXT_CHECK( hxtSystemPhase ( sys, -1,   -1, 11) );
  HXT_CHECK( hxtSystemPhase ( sys, -1,   -1, 22) );
  HXT_CHECK( hxtSystemPhase ( sys,  0, nrhs, 33) );
  HXT_CHECK( hxtSystemPhase ( sys, -1,   -1, -1) );

  // * --------------------------------------------------------------------- * //
  // * --                          WRITE RESULTS                          -- * //
  // * --------------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, 0) );

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                 DELETE                                * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  HXT_CHECK( hxtSystemDelete     (&sys ) );
  HXT_CHECK( hxtFormulationDelete(&wave) );
  HXT_CHECK( hxtFormulationDelete(&HABC) );
  HXT_CHECK( hxtFormulationDelete(&Help) );
  HXT_CHECK( hxtGroupDelete   (&domain ) );
  HXT_CHECK( hxtGroupDelete   (&bnd    ) );
  HXT_CHECK( hxtGroupDelete   (&source ) );
  HXT_CHECK( hxtGroupDelete   (&zero   ) );

  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtGroupDelete(&aux[j] ) );
  }

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static double fgetK (const double *xyz)
{
  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double fgetKx(const double *xyz)
{
  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  const double kx = sqrt( (k  * k ) - 
		          (ky * ky) - 
			  (kz * kz) );

  return kx;
}

static double_complex fSource(const double *xyz)
{
  const double    y = xyz[1];
  const double    z = xyz[2];

  return sin(ky * y) * sin(kz * z);
}

static double_complex fKInf  (const double *xyz)
{
//const double    k  = fgetK (xyz);
  const double    kx = fgetKx(xyz);

  return kx;
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}

static double_complex fZero  (const double *xyz)
{
  return 0.;
}
