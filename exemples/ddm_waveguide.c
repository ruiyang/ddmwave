#define HXT_OMP_CHECK(status)                                           \
do{ HXTStatus s = (status);                                             \
    if(s!=HXT_STATUS_OK){                                               \
      HXT_TRACE_MSG(s, "cannot break OpenMP region -> exiting");        \
      fflush(stdout); fflush(stderr);                                   \
      exit(s);                                                          \
    }                                                                   \
}while(0);                                                              \

#include "hxt_entity.h"
#include "hxt_group.h"
#include "hxt_formulation.h"
#include "hxt_system.h"
#include "hxt_gmres1.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nx_DOM 4
#define Ny_DOM 1

#define OMEGA      ( 8. * M_PI * 1)
#define LSP        ( 1.           )
#define N_LAMBDA   ( 3            )
/*
#define theta  (M_PI / 3.)
#define NPade  (4        )
*/
#define theta  (0.)
#define NPade  (0 )

#define order  4
#define nrhs   1
#define mdim   3

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                CONSTANTS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define m 1
#define n 1
static const double  ky = (M_PI / 0.25 *m);
static const double  kz = (M_PI / 0.25 *n);

#define Nt_DOM (Nx_DOM * Ny_DOM)
#define Lx     (0.50)
#define Ly     (0.25)
#define Lz     (0.25)
/*
#define Lx     (4.00)
#define Ly     (1.00)
*/

#define LSx (Lx / Nx_DOM)
#define LSy (Ly / Ny_DOM)

#define WAVENUMBER ( OMEGA      / LSP       )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define FREQ       ( WAVENUMBER / (2*M_PI)  )
#define LC         ( LAMBDA     / N_LAMBDA  )

static double_complex fK2    (const double *xyz);
static double_complex fK     (const double *xyz);
static double_complex fSource(const double *xyz);
static double_complex fZero  (const double *xyz);
static double_complex fKInf  (const double *xyz);

int main(int argc, char **argv)
{
  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ( "cuboidDom",     &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                           RECTANGLES                            -- * //
  // * --------------------------------------------------------------------- * //
  int ptTags[Nx_DOM+1][Ny_DOM+1] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      ptTags[i][j] = gmshModelGeoAddPoint(Lx*i /Nx_DOM, 
                                          Ly*j /Ny_DOM, 
                                          0, 0, -1, &ierr);
    }
  }

  int  lxTags[Nx_DOM  ][Ny_DOM+1]    = {{ 0 }};
  int  lyTags[Nx_DOM+1][Ny_DOM  ]    = {{ 0 }};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      if(i < Nx_DOM) lxTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i+1][j], -1, &ierr);
      if(j < Ny_DOM) lyTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i][j+1], -1, &ierr);
    }
  }

  int recLlps[Nx_DOM][Ny_DOM] = {{0}};
  int recTags[Nx_DOM][Ny_DOM] = {{0}};

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      int curveTags[] = { lxTags[i  ][j  ],
                          lyTags[i+1][j  ],
                         -lxTags[i  ][j+1],
                         -lyTags[i  ][j  ],
                        };

      recLlps[i][j] = gmshModelGeoAddCurveLoop   ( curveTags    , 4, -1, &ierr);
      recTags[i][j] = gmshModelGeoAddPlaneSurface(&recLlps[i][j], 1, -1, &ierr);

      gmshModelGeoMeshSetRecombine         (2, recTags[i][j],            0, &ierr);
      gmshModelGeoMeshSetTransfiniteSurface(recTags[i][j], "Left", NULL, 0, &ierr);
    }
  }

  int    *cubDimTags  [Nx_DOM][Ny_DOM];
  size_t  cubDimTags_n[Nx_DOM][Ny_DOM];

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      int numElements = Lz / LAMBDA * N_LAMBDA;

      const double dx =  0;
      const double dy =  0;
      const double dz = Lz;

      int inDimTags[2] = {2, recTags[i][j]};
  
      gmshModelGeoExtrude( inDimTags, 2, dx, dy, dz, 
                          &cubDimTags[i][j], &cubDimTags_n[i][j], 
                          &numElements, 1, NULL, 0,  true, &ierr);
    }
  }
  // * cubDimTags[i][j][ 1] is tag of front face
  // * cubDimTags[i][j][ 3] is tag of back  face
  // * cubDimTags[i][j][ 5] is tag of down  face
  // * cubDimTags[i][j][ 7] is tag of right face
  // * cubDimTags[i][j][ 9] is tag of up    face
  // * cubDimTags[i][j][11] is tag of left  face

  // * --------------------------------------------------------------------- * //
  // * --                         GENERATE  MESH                          -- * //
  // * --------------------------------------------------------------------- * //
  gmshModelGeoSynchronize(        &ierr);
  gmshModelMeshGenerate  (mdim,   &ierr);
  gmshModelMeshSetOrder  (order,  &ierr);
  gmshWrite     ("cuboidDom.msh", &ierr);
  printf("MESHING FINISHED\n");

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                  DDM                                  * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  omp_set_num_threads(Nt_DOM);

  #pragma omp parallel
  {
    const int threadid = omp_get_thread_num ();

    const int Nx = (Nx_DOM       );
    const int Ny = (Ny_DOM       );
    const int Ix = (threadid / Ny);
    const int Iy = (threadid % Ny);

    // * --------------------------------------------------------------------- * //
    // * --                              Group                              -- * //
    // * --------------------------------------------------------------------- * //
    size_t offset = 0;
  
    HXTGroup *domain = NULL;
    HXT_OMP_CHECK( hxtGroupCreate   (&domain                        ) );
    HXT_OMP_CHECK( hxtGroupAddEntity( domain, mdim, recTags[Ix][Iy] ) );
    HXT_OMP_CHECK( hxtGroupBuild    ( domain, offset                ) ); 

    HXTGroup *diric[2];
    HXT_OMP_CHECK( hxtGroupCreate (&diric[0]) );
    HXT_OMP_CHECK( hxtGroupCreate (&diric[1]) );

                     HXT_OMP_CHECK( hxtGroupAddEntity( diric[1], mdim-1, cubDimTags[Ix][Iy][ 1] ) );
                     HXT_OMP_CHECK( hxtGroupAddEntity( diric[1], mdim-1, cubDimTags[Ix][Iy][ 3] ) );
    if(Iy     == 0 ) HXT_OMP_CHECK( hxtGroupAddEntity( diric[1], mdim-1, cubDimTags[Ix][Iy][ 5] ) );
    if(Iy + 1 == Ny) HXT_OMP_CHECK( hxtGroupAddEntity( diric[1], mdim-1, cubDimTags[Ix][Iy][ 9] ) );
    if(Ix     == 0 ) HXT_OMP_CHECK( hxtGroupAddEntity( diric[0], mdim-1, cubDimTags[Ix][Iy][11] ) );

    HXT_OMP_CHECK( hxtGroupBuild  ( diric[0], offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild  ( diric[1], offset) ); 

    HXTGroup *bnd[6][1+NPade];
    for(int j = 0; j < 1+NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupCreate (&bnd[0][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&bnd[1][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&bnd[2][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&bnd[3][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&bnd[4][j]) );
      HXT_OMP_CHECK( hxtGroupCreate (&bnd[5][j]) );
  
                      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[3][j], mdim-1, cubDimTags[Ix][Iy][ 7] ) );
      if(Iy     > 0 ) HXT_OMP_CHECK( hxtGroupAddEntity( bnd[2][j], mdim-1, cubDimTags[Ix][Iy][ 5] ) );
      if(Iy + 1 < Ny) HXT_OMP_CHECK( hxtGroupAddEntity( bnd[4][j], mdim-1, cubDimTags[Ix][Iy][ 9] ) );
      if(Ix     > 0 ) HXT_OMP_CHECK( hxtGroupAddEntity( bnd[5][j], mdim-1, cubDimTags[Ix][Iy][11] ) );
    }

    HXT_OMP_CHECK( hxtGroupBuild  ( bnd[0][0],   offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild  ( bnd[1][0],   offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild  ( bnd[2][0],   offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild  ( bnd[3][0],   offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild  ( bnd[4][0],   offset) ); 
    HXT_OMP_CHECK( hxtGroupBuild  ( bnd[5][0],   offset) ); 
    for(int j = 1; j < 1+NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupBuild  ( bnd[0][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( bnd[1][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( bnd[2][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( bnd[3][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( bnd[4][j], ++offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild  ( bnd[5][j], ++offset) ); 
    }

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Wave                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *wave = NULL;
    HXT_OMP_CHECK( hxtFormulationCreate    (&wave        ));
    HXT_OMP_CHECK( hxtFormulationAddField  ( wave, domain));
    HXT_OMP_CHECK( hxtFormulationHelmholtz ( wave, fK2   ));
    #pragma omp barrier

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation HABC                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *HABC[4];
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[2] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&HABC[3] ));

    for(int j = 0; j < 1+NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[0], bnd[2][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[1], bnd[3][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[2], bnd[4][j] ));
      HXT_OMP_CHECK( hxtFormulationAddField( HABC[3], bnd[5][j] ));
    }
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[0], fKInf, theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[1], fKInf, theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[2], fKInf, theta ));
    HXT_OMP_CHECK( hxtFormulationHABC( HABC[3], fKInf, theta ));

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Help                           -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *Help[4];
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[2] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&Help[3] ));
  
    for(int j = 0; j < 1+NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( Help[0], bnd[2][j] )); // * down  face
      HXT_OMP_CHECK( hxtFormulationAddField( Help[1], bnd[3][j] )); // * right face
      HXT_OMP_CHECK( hxtFormulationAddField( Help[2], bnd[4][j] )); // * up    face
      HXT_OMP_CHECK( hxtFormulationAddField( Help[3], bnd[5][j] )); // * left  face
    }
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[0], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[1], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[2], fK2, theta));
    HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[3], fK2, theta));
    #pragma omp barrier

    // * --------------------------------------------------------------------- * //
    // * --                              System                             -- * //
    // * --------------------------------------------------------------------- * //
    HXTSystem *sys = NULL;
    HXT_OMP_CHECK( hxtSystemCreate (&sys         ) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, wave   ) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, HABC[3]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[0]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[1]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[2]) );
    HXT_OMP_CHECK( hxtSystemAddFML ( sys, Help[3]) );
    HXT_OMP_CHECK( hxtSystemBuild  ( sys, nrhs   ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( sys         ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( sys, 1      ) );
  
    // * --------------------------------------------------------------------- * //
    // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
    // * --------------------------------------------------------------------- * //
    for(size_t l = 0; l < nrhs; l++)
    {
      HXT_OMP_CHECK( hxtSystemCSTR(sys, l, diric[0], fSource) ); 
      HXT_OMP_CHECK( hxtSystemCSTR(sys, l, diric[1], fZero  ) ); 
    }

    // * --------------------------------------------------------------------- * //
    // * --                          PARDISO SOLVER                         -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemSolver( sys              ) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys, -1,   -1, 11) );
    HXT_OMP_CHECK( hxtSystemPhase ( sys, -1,   -1, 22) );
  //HXT_OMP_CHECK( hxtSystemPhase ( sys,  0, nrhs, 33) );

    // * --------------------------------------------------------------------- * //
    // * --                          WRITE RESULTS                          -- * //
    // * --------------------------------------------------------------------- * //
    //   HXT_OMP_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, threadid+1) );

    // * --------------------------------------------------------------------- * //
    // * --                      Formulation Update                         -- * //
    // * --------------------------------------------------------------------- * //
    HXTFormulation *EUpd[4];
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[0] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[1] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[2] ));
    HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[3] ));
  
    for(int j = 0; j < 1+NPade; j++)
    {
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[0], bnd[2][j] )); // * down  face
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[1], bnd[3][j] )); // * right face
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[2], bnd[4][j] )); // * up    face
      HXT_OMP_CHECK( hxtFormulationAddField( EUpd[3], bnd[5][j] )); // * left  face
    }
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[0], fKInf, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[1], fKInf, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[2], fKInf, theta) );
    HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[3], fKInf, theta) );

    // * --------------------------------------------------------------------- * //
    // * --                        Update System                            -- * //
    // * --------------------------------------------------------------------- * //
    HXTSystem *bcSys[4];
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[0] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[1] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[2] ) );
    HXT_OMP_CHECK( hxtSystemCreate (&bcSys[3] ) );
    if(Iy     > 0 ) HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[0], EUpd[0]) );
    if(Ix + 1 < Nx) HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[1], EUpd[1]) );
    if(Iy + 1 < Ny) HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[2], EUpd[2]) );
    if(Ix     > 0 ) HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[3], EUpd[3]) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[0], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[1], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[2], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[3], nrhs   ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[0]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[1]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[2]         ) );
    HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[3]         ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[0], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[1], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[2], 1      ) );
    HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[3], 1      ) );

    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[0] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[1] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[2] ) );
    HXT_OMP_CHECK( hxtSystemSolver ( bcSys[3] ) );
  
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[0], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[1], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[2], -1, -1, 12) );
    HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[3], -1, -1, 12) );
  
    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                  Krylov                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXTGMRES1 *kry = NULL;
    HXT_OMP_CHECK(hxtGMRES1Create(&kry) );
    HXT_OMP_CHECK(hxtGMRES1Build ( kry, 80, sys, nrhs, bcSys, 4, 
			           diric, 2, Ix, Iy,   Nx, Ny) );
    HXT_OMP_CHECK(hxtGMRES1Solve ( kry) );
    HXT_OMP_CHECK(hxtGMRES1Delete(&kry) );

    // * --------------------------------------------------------------------- * //
    // * --                     DIRICHLET BOUNDARY AGAIN                    -- * //
    // * --------------------------------------------------------------------- * //
    for(size_t l = 0; l < nrhs; l++)
    {
      HXT_OMP_CHECK( hxtSystemCSTR(sys, l, diric[0], fSource) ); 
      HXT_OMP_CHECK( hxtSystemCSTR(sys, l, diric[1], fZero  ) ); 
    }
    HXT_OMP_CHECK( hxtSystemPhase ( sys,  0, nrhs, 33) );

    // * --------------------------------------------------------------------- * //
    // * --                          WRITE RESULTS                          -- * //
    // * --------------------------------------------------------------------- * //
    HXT_OMP_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, threadid+1) );

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                 RELEASE                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtSystemPhase ( sys     , -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[0], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[1], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[2], -1,   -1, -1) );
    HXT_OMP_CHECK( hxtSystemPhase ( bcSys[3], -1,   -1, -1) );

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                 DELETE                                * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXT_OMP_CHECK( hxtSystemDelete     (&sys      ) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[0]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[1]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[2]) );
    HXT_OMP_CHECK( hxtSystemDelete     (&bcSys[3]) );
  
    HXT_OMP_CHECK( hxtFormulationDelete(&wave   ) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&HABC[3]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&Help[3]) );
  
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[0]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[1]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[2]) );
    HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[3]) );
  
    HXT_OMP_CHECK( hxtGroupDelete(&domain  ) );
    HXT_OMP_CHECK( hxtGroupDelete(&diric[0]) );
    HXT_OMP_CHECK( hxtGroupDelete(&diric[1]) );
  
    for(int j = 0; j < 1+NPade; j++)
    {
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[0][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[1][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[2][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[3][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[4][j] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[5][j] ) );
    }

  }

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static double fgetK (const double *xyz)
{
//const double   y = xyz[1];
//const double lsp = 1.25  * (1. - 0.4 * exp( -32.*(y-0.5)*(y-0.5) ) );

  const double Hz  = OMEGA  / (2. * M_PI     );
  const double k   = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double fgetKx(const double *xyz)
{
//const double   y = xyz[1];
//const double lsp = 1.25  * (1. - 0.4 * exp( -32.*(y-0.5)*(y-0.5) ) );

  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  const double kx = sqrt( (k  * k ) - 
		          (ky * ky) - 
			  (kz * kz) );

  return kx;
}

static double_complex fSource(const double *xyz)
{
  const double    y = xyz[1];
  const double    z = xyz[2];

  return sin(ky * y) * sin(kz * z);
}

static double_complex fKInf  (const double *xyz)
{
//const double    k  = fgetK (xyz);
  const double    kx = fgetKx(xyz);

  return kx;
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}

static double_complex fZero  (const double *xyz)
{
  return 0.;
}
