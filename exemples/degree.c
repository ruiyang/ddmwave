#include "hxt_dofManager.h"

#define num 10

int main(int argc, char **argv)
{
  size_t i;

  HXTDofs* dofs = NULL;
  HXT_CHECK( hxtDofsCreate(&dofs            ) );
  HXT_CHECK( hxtDofsBuild ( dofs, num, false) );

  for(i = 0; i < num; i++)
  {
    dofs->entity[i] = rand() % 100;
    dofs->type  [i] = rand() % 10 ;
  }

  HXTDofManager *dofM = NULL;
  HXT_CHECK( hxtDofManagerCreate(&dofM) );

  HXT_CHECK( hxtDofManagerDofsAdd (dofM, dofs->entity, dofs->type, num) );
  HXT_CHECK( hxtDofManagerDofsSort(dofM) );
  HXT_CHECK( hxtDofManagerDofsPrt (dofM) );

  int e = dofs->entity[5];
  int t = dofs->type  [5];

  HXT_CHECK( hxtDofManagerGnrGlobalId(dofM, true) );
  HXT_CHECK( hxtDofManagerPrtGlobalId(dofM      ) );
  int id   = hxtDofManagerGetGlobalId(dofM, e, t)  ;

  printf("id of [%d, %d] is %d\n", e, t, id);

  HXT_CHECK( hxtDofManagerDelete(&dofM) );
  HXT_CHECK( hxtDofsDelete      (&dofs) );

  return 0;
}
