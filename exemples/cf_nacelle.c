#define HXT_OMP_CHECK(status)						\
do{ HXTStatus s = (status);						\
    if(s!=HXT_STATUS_OK){						\
      HXT_TRACE_MSG(s, "cannot break OpenMP region -> exiting");	\
      fflush(stdout); fflush(stderr);					\
      exit(s);								\
    }									\
}while(0);                                                              \

#include <gmshc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#include "hxt_system.h"
#include "hxt_gmres2.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nx_DOM  8
#define Ny_DOM  6
#define Nt_DOM (Nx_DOM * Ny_DOM)

#define dim  2
#define nrhs 1

#define theta  (M_PI / 3.)
#define NPade  (4        )

#define OMEGA      ( 2. * M_PI * 10)
#define LSP        ( 1.            )
#define N_LAMBDA   ( 20            )
#define order      ( 1             )

#define WAVENUMBER ( OMEGA      / LSP       )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define LC         ( LAMBDA     / N_LAMBDA  )

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                FUNCTIONS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
static void meshGenerator ();

static double_complex fK     (const double *xyz);
static double_complex fK2    (const double *xyz);
static double_complex fSource(const double *xyz);
static double_complex fZero  (const double *xyz);

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    printf("Example:\n\t ./main nIters\n");
    return 0;
  }
  const int nIters = atoi(argv[1]);
  printf("Example:\n\t ./main nIters\n"            );
  printf("Usage: %d nIters     [options]\n", nIters);
  printf("LC: %f\n", LC);

  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ("Nacelle",        &ierr);

  gmshOptionSetNumber("General.Terminal", 1, &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  meshGenerator();

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                  DDM                                  * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  double tol = 1e-6;

  int    *domains  ;
  size_t  domains_n;
  gmshModelGetEntities(&domains, &domains_n, 2, &ierr);

  int domTags[Nx_DOM][Ny_DOM] = {{0}};

  for(int dom = 0; dom < domains_n/2; dom++)
  {
    const int domTag = domains[2*dom+1];
    const int Ix_DOM = (domTag / 100)-1;
    const int Iy_DOM = (domTag % 100)-1;

    domTags[Ix_DOM][Iy_DOM] = domTag;
  }

  omp_set_num_threads(Ny_DOM);

  #pragma omp parallel
  {
    const int threadid = omp_get_thread_num (); // * equal to Iy

    const int Nx =   Nx_DOM;
    const int Ny =   Ny_DOM;
          int Ix =        0;
    const int Iy = threadid;

    HXTGroup *domain[Nx];
    HXTGroup *source[Nx];
    HXTGroup *bnd[Nx][4];
    HXTGroup *aux[Nx][4][NPade];
    HXTGroup *cnr[Nx][8][NPade];

    HXTFormulation *wave[Nx]   ;
    HXTFormulation *HABC[Nx][4];
    HXTFormulation *Help[Nx][4];
    HXTFormulation *Trea[Nx][8];
    HXTFormulation *EUpd[Nx][4];
    HXTFormulation *CUpd[Nx][8];

    HXTSystem *  sys[  Nx];
    HXTSystem *bcSys[4*Nx];

    // * --------------------------------------------------------------------- * //
    // * --                              CHECK                              -- * //
    // * --------------------------------------------------------------------- * //
    for(Ix = 0; Ix < Nx; Ix++)
    {
      int surDimTag[2] =  {2, domTags[Ix][Iy]};
  
      int bndTags[4] = {0};
      int corTags[8] = {0};
      int srcTags[8] = {0};
      int wllTags[8] = {0};
  
      int srcTags_n  = 0;
      int wllTags_n  = 0;
  
      if(surDimTag[1] != 0)
      {
        int   *boundary   = NULL;
        size_t boundary_n =    0;
        gmshModelGetBoundary(&surDimTag[0], 2,
                             &boundary, &boundary_n, false, true, false, &ierr);
    
        for(size_t bnd = 0; bnd < boundary_n/2; bnd++) 
        {
          int   *physicalTags   = NULL;
          size_t physicalTags_n =    0;
          gmshModelGetPhysicalGroupsForEntity( boundary[2*bnd+0], abs(boundary[2*bnd+1]),
                                              &physicalTags, &physicalTags_n, &ierr);
    
          if(physicalTags_n == 1)
          {
            switch(physicalTags[0]) 
            {
             //case 1000: bndTags[0]           = boundary[2*bnd+1]; break;
  	       case 1000: break;
               case 2000: bndTags[1]           = boundary[2*bnd+1]; break;
               case 3000: bndTags[2]           = boundary[2*bnd+1]; break;
               case 4000: bndTags[3]           = boundary[2*bnd+1]; break;
               case 5000: wllTags[wllTags_n++] = boundary[2*bnd+1]; break;
               case 6000: srcTags[srcTags_n++] = boundary[2*bnd+1]; break;
               default  : printf("   Invalid physical tag\n");
            } 
          } 
          else if (physicalTags_n == 0)
          {
            double xmin, xmax, ymin, ymax, zmin, zmax;
            gmshModelGetBoundingBox(1, abs(boundary[2*bnd+1]), 
                                    &xmin, &ymin, &zmin, 
                                    &xmax, &ymax, &zmax, &ierr);
            if(fabs(xmin - xmax) < tol)
            {
              // *    vertical   boundary
              if     (boundary[2*bnd+1] < 0) bndTags[1] = boundary[2*bnd+1];
              else if(boundary[2*bnd+1] > 0) bndTags[3] = boundary[2*bnd+1];
            }
            if(fabs(ymin - ymax) < tol)
            {
              // *    horizontal boundary
              if     (boundary[2*bnd+1] < 0) bndTags[2] = boundary[2*bnd+1];
              else if(boundary[2*bnd+1] > 0) bndTags[0] = boundary[2*bnd+1];
            }
          }
          else printf(" Invalid physicalTags_n = %lu\n", physicalTags_n);
        }
  
        for(int i = 0; i < 4; i++)
        {
          if(bndTags[i] != 0)
          {
            int bndDimTag[2] = {1, bndTags[i]};
      
            int   *linEnd  ;
            size_t linEnd_n;
            gmshModelGetBoundary(&bndDimTag[0], 2,
                                 &linEnd, &linEnd_n, false, true, false, &ierr);
      
            corTags[2*i+0] = linEnd[1];
            corTags[2*i+1] = linEnd[3];
          }
        }
        if(corTags[0] != corTags[7]) { corTags[0] = 0; corTags[7] = 0; } 
        if(corTags[2] != corTags[1]) { corTags[2] = 0; corTags[1] = 0; } 
        if(corTags[4] != corTags[3]) { corTags[4] = 0; corTags[3] = 0; } 
        if(corTags[6] != corTags[5]) { corTags[6] = 0; corTags[5] = 0; } 
      }

      // * --------------------------------------------------------------------- * //
      // * --                              Group                              -- * //
      // * --------------------------------------------------------------------- * //
      size_t offset = 0;
  
      HXT_OMP_CHECK( hxtGroupCreate   (&domain[Ix]   ) );
      HXT_OMP_CHECK( hxtGroupAddEntity( domain[Ix], 2, domTags[Ix][Iy]) );
      HXT_OMP_CHECK( hxtGroupBuild    ( domain[Ix], offset) ); 
  
      HXT_OMP_CHECK( hxtGroupCreate   (&source[Ix]   ) );
      for(int i = 0; i < srcTags_n; i++)
        HXT_OMP_CHECK( hxtGroupAddEntity( source[Ix], 1, srcTags[i]) );

      HXT_OMP_CHECK( hxtGroupBuild    ( source[Ix], offset) ); 
    
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][0]) );
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][1]) );
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][2]) );
      HXT_OMP_CHECK( hxtGroupCreate(&bnd[Ix][3]) );
    
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][0], 1, abs(bndTags[0]) ) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][1], 1, abs(bndTags[1]) ) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][2], 1, abs(bndTags[2]) ) );
      HXT_OMP_CHECK( hxtGroupAddEntity( bnd[Ix][3], 1, abs(bndTags[3]) ) );
    
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][0], offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][1], offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][2], offset) ); 
      HXT_OMP_CHECK( hxtGroupBuild( bnd[Ix][3], offset) ); 
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][0][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][1][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][2][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&aux[Ix][3][j]) );
    
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][0][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtGroupCreate (&cnr[Ix][7][j]) );
    
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][0][j], 1, abs(bndTags[0]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][1][j], 1, abs(bndTags[1]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][2][j], 1, abs(bndTags[2]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( aux[Ix][3][j], 1, abs(bndTags[3]) ) );
      
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][0][j], 0, abs(corTags[0]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][1][j], 0, abs(corTags[1]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][2][j], 0, abs(corTags[2]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][3][j], 0, abs(corTags[3]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][4][j], 0, abs(corTags[4]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][5][j], 0, abs(corTags[5]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][6][j], 0, abs(corTags[6]) ) );
        HXT_OMP_CHECK( hxtGroupAddEntity( cnr[Ix][7][j], 0, abs(corTags[7]) ) );
    
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][0][j], ++offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][0][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][1][j],   offset) ); 
  
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][2][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][4][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][5][j],   offset) ); 
  
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][1][j], ++offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][2][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][3][j],   offset) ); 
  
        HXT_OMP_CHECK( hxtGroupBuild  ( aux[Ix][3][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][6][j],   offset) ); 
        HXT_OMP_CHECK( hxtGroupBuild  ( cnr[Ix][7][j],   offset) ); 
      }

      // * --------------------------------------------------------------------- * //
      // * --                      Formulation Wave                           -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate    (&wave[Ix]             ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( wave[Ix], domain[Ix]));
      HXT_OMP_CHECK( hxtFormulationHelmholtz ( wave[Ix], fK2        ));
  
      // * --------------------------------------------------------------------- * //
      // * --                      Formulation HABC                           -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&HABC[Ix][3] ));
  
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][0], bnd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][1], bnd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][2], bnd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( HABC[Ix][3], bnd[Ix][3] ));
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][0], aux[Ix][0][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][1], aux[Ix][1][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][2], aux[Ix][2][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( HABC[Ix][3], aux[Ix][3][j] ));
      }
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][0], fK , theta ));
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][1], fK , theta ));
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][2], fK , theta ));
      HXT_OMP_CHECK( hxtFormulationHABC( HABC[Ix][3], fK , theta ));

      // * --------------------------------------------------------------------- * //
      // * --                      Formulation Help                           -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&Help[Ix][3] ));
    
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][0], bnd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][1], bnd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][2], bnd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( Help[Ix][3], bnd[Ix][3] ));
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][0], aux[Ix][0][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][1], aux[Ix][1][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][2], aux[Ix][2][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( Help[Ix][3], aux[Ix][3][j] ));
      }
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][0], fK2, theta));
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][1], fK2, theta));
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][2], fK2, theta));
      HXT_OMP_CHECK( hxtFormulationHABCHelper( Help[Ix][3], fK2, theta));
  
      // * --------------------------------------------------------------------- * //
      // * --                   Formulation Treatment                         -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&Trea[Ix][7]) );
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][0], cnr[Ix][7][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][1], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][2], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][3], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][4], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][5], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][6], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][7], cnr[Ix][0][j]) );
      }
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][0], cnr[Ix][0][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][1], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][2], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][3], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][4], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][5], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][6], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( Trea[Ix][7], cnr[Ix][7][j]) );
      }
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][0], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][1], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][2], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][3], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][4], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][5], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][6], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCorner( Trea[Ix][7], fK, theta) );

      // * --------------------------------------------------------------------- * //
      // * --                              System                             -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCreate (&sys[Ix]              ) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], wave[Ix]   ) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], HABC[Ix][3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Help[Ix][3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][0]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][1]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][2]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][3]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][4]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][5]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][6]) );
      HXT_OMP_CHECK( hxtSystemAddFML ( sys[Ix], Trea[Ix][7]) );
      HXT_OMP_CHECK( hxtSystemBuild  ( sys[Ix], 1           ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( sys[Ix]              ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( sys[Ix], 1           ) );
    
      // * --------------------------------------------------------------------- * //
      // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCSTR(sys[Ix], 0, source[Ix], fSource) ); 
    
      // * --------------------------------------------------------------------- * //
      // * --                          PARDISO SOLVER                         -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemSolver( sys[Ix]              ) );
      HXT_OMP_CHECK( hxtSystemPhase ( sys[Ix], -1,   -1, 11) );
      HXT_OMP_CHECK( hxtSystemPhase ( sys[Ix], -1,   -1, 22) );

      // * --------------------------------------------------------------------- * //
      // * --                      Formulation Update                         -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationCreate(&EUpd[Ix][3] ));
    
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][0], bnd[Ix][0] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][1], bnd[Ix][1] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][2], bnd[Ix][2] ));
      HXT_OMP_CHECK( hxtFormulationAddField  ( EUpd[Ix][3], bnd[Ix][3] ));
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][0], aux[Ix][0][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][1], aux[Ix][1][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][2], aux[Ix][2][j] ));
        HXT_OMP_CHECK( hxtFormulationAddField( EUpd[Ix][3], aux[Ix][3][j] ));
      }
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][0], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][1], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][2], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCUpd   ( EUpd[Ix][3], fK, theta) );
  
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationCreate(&CUpd[Ix][7]) );
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][0], cnr[Ix][7][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][1], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][2], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][3], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][4], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][5], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][6], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][7], cnr[Ix][0][j]) );
      }
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][0], cnr[Ix][0][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][1], cnr[Ix][1][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][2], cnr[Ix][2][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][3], cnr[Ix][3][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][4], cnr[Ix][4][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][5], cnr[Ix][5][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][6], cnr[Ix][6][j]) );
        HXT_OMP_CHECK( hxtFormulationAddField( CUpd[Ix][7], cnr[Ix][7][j]) );
      }
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][0], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][1], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][2], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][3], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][4], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][5], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][6], fK, theta) );
      HXT_OMP_CHECK( hxtFormulationHABCCnrUpd( CUpd[Ix][7], fK, theta) );

      // * --------------------------------------------------------------------- * //
      // * --                        Update System                            -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+0] ) );
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+1] ) );
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+2] ) );
      HXT_OMP_CHECK( hxtSystemCreate (&bcSys[4*Ix+3] ) );
      if(Iy     > 0 ){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+0], EUpd[Ix][0]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+0], CUpd[Ix][0]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+0], CUpd[Ix][1]) );
      }
      if(Ix + 1 < Nx){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+1], EUpd[Ix][1]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+1], CUpd[Ix][2]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+1], CUpd[Ix][3]) );
      }
      if(Iy + 1 < Ny){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+2], EUpd[Ix][2]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+2], CUpd[Ix][4]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+2], CUpd[Ix][5]) );
      }
      if(Ix     > 0 ){
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+3], EUpd[Ix][3]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+3], CUpd[Ix][6]) );
        HXT_OMP_CHECK( hxtSystemAddFML ( bcSys[4*Ix+3], CUpd[Ix][7]) );
      }
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+0], 1      ) );
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+1], 1      ) );
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+2], 1      ) );
      HXT_OMP_CHECK( hxtSystemBuild  ( bcSys[4*Ix+3], 1      ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+0]         ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+1]         ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+2]         ) );
      HXT_OMP_CHECK( hxtSystemCOOMat ( bcSys[4*Ix+3]         ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+0], 1      ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+1], 1      ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+2], 1      ) );
      HXT_OMP_CHECK( hxtSystemCSRMat ( bcSys[4*Ix+3], 1      ) );
  
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+0] ) );
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+1] ) );
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+2] ) );
      HXT_OMP_CHECK( hxtSystemSolver ( bcSys[4*Ix+3] ) );
    
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+0], -1, -1, 12) );
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+1], -1, -1, 12) );
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+2], -1, -1, 12) );
      HXT_OMP_CHECK( hxtSystemPhase  ( bcSys[4*Ix+3], -1, -1, 12) );
    }

    // * ********************************************************************* * //
    // * ******                                                         ****** * //
    // *                                  Krylov                               * //
    // * ******                                                         ****** * //
    // * ********************************************************************* * //
    HXTGMRES2 *kry = NULL;
    HXT_OMP_CHECK( hxtGMRES2Create(&kry   ) );
    HXT_OMP_CHECK( hxtGMRES2Build ( kry, 80, sys, nrhs, bcSys, 4, 
			                     source, 1, Nx, Ny) );
    HXT_OMP_CHECK( hxtGMRES2Solve ( kry, 1) );
    HXT_OMP_CHECK( hxtGMRES2Delete(&kry   ) );

    for(Ix = 0; Ix < Nx; Ix++)
    {
      // * --------------------------------------------------------------------- * //
      // * --                     DIRICHLET BOUNDARY AGAIN                    -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemCSTR ( sys[Ix], 0, source[Ix], fSource) ); 
      HXT_OMP_CHECK( hxtSystemPhase( sys[Ix], 0, nrhs, 33           ) );
  
      // * --------------------------------------------------------------------- * //
      // * --                          WRITE RESULTS                          -- * //
      // * --------------------------------------------------------------------- * //
      HXT_OMP_CHECK( hxtSystemWriteMSH(sys[Ix], domain[Ix], "res.msh", 0, Iy+Ix*Ny+1) );
  
      // * ********************************************************************* * //
      // * ******                                                         ****** * //
      // *                                 RELEASE                               * //
      // * ******                                                         ****** * //
      // * ********************************************************************* * //
      HXT_OMP_CHECK( hxtSystemPhase ( sys[Ix]      , -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+0], -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+1], -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+2], -1,   -1, -1) );
      HXT_OMP_CHECK( hxtSystemPhase ( bcSys[4*Ix+3], -1,   -1, -1) );
  
      // * ********************************************************************* * //
      // * ******                                                         ****** * //
      // *                                 DELETE                                * //
      // * ******                                                         ****** * //
      // * ********************************************************************* * //
      HXT_OMP_CHECK( hxtSystemDelete  (&sys[Ix]      ) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+0]) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+1]) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+2]) );
      HXT_OMP_CHECK( hxtSystemDelete  (&bcSys[4*Ix+3]) );
    
      HXT_OMP_CHECK( hxtFormulationDelete(&wave[Ix]   ) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&HABC[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Help[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&Trea[Ix][7]) );
    
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&EUpd[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][0]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][1]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][2]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][3]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][4]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][5]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][6]) );
      HXT_OMP_CHECK( hxtFormulationDelete(&CUpd[Ix][7]) );
    
      HXT_OMP_CHECK( hxtGroupDelete(&domain[Ix] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][0] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][1] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][2] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&bnd[Ix][3] ) );
      HXT_OMP_CHECK( hxtGroupDelete(&source[Ix] ) );
    
      for(int j = 0; j < NPade; j++)
      {
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][0][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][1][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][2][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&aux[Ix][3][j] ) );
    
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][0][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][1][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][2][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][3][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][4][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][5][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][6][j] ) );
        HXT_OMP_CHECK( hxtGroupDelete(&cnr[Ix][7][j] ) );
      }
    }
    #pragma omp barrier
  } 

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static void meshGenerator()
{
  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * Default parameters
  const double h_near = 1.0;
  const double h_far  = 1.0;
  const double L_duct = 0.3;
  const double Lx     = 3.0;
  const double Ly     = 2.5;

  // * Nacelle (points)
  gmshModelOccAddPoint(-0.3000000000000000, 1.5000000000000000, 0, h_near,    1, &ierr);
  gmshModelOccAddPoint(-0.2987304221999528, 1.4999980867903138, 0, h_near,    2, &ierr);
  gmshModelOccAddPoint(-0.2954590219195146, 1.4999894098722784, 0, h_near,    3, &ierr);
  gmshModelOccAddPoint(-0.2902590561329333, 1.4999695633124288, 0, h_near,    4, &ierr);
  gmshModelOccAddPoint(-0.2832037818144567, 1.4999341411772997, 0, h_near,    5, &ierr);
  gmshModelOccAddPoint(-0.2743664559383324, 1.4998787375334264, 0, h_near,    6, &ierr);
  gmshModelOccAddPoint(-0.2638203354788085, 1.4997989464473434, 0, h_near,    7, &ierr);
  gmshModelOccAddPoint(-0.2516386774101325, 1.4996903619855857, 0, h_near,    8, &ierr);
  gmshModelOccAddPoint(-0.2378947387065523, 1.4995485782146885, 0, h_near,    9, &ierr);
  gmshModelOccAddPoint(-0.2226617763423157, 1.4993691892011862, 0, h_near,   10, &ierr);
  gmshModelOccAddPoint(-0.2060130472916705, 1.4991477890116141, 0, h_near,   11, &ierr);
  gmshModelOccAddPoint(-0.1880218085288645, 1.4988799717125068, 0, h_near,   12, &ierr);
  gmshModelOccAddPoint(-0.1687613170281455, 1.4985613313703996, 0, h_near,   13, &ierr);
  gmshModelOccAddPoint(-0.1483048297637612, 1.4981874620518270, 0, h_near,   14, &ierr);
  gmshModelOccAddPoint(-0.1267256037099594, 1.4977539578233241, 0, h_near,   15, &ierr);
  gmshModelOccAddPoint(-0.1040968958409880, 1.4972564127514256, 0, h_near,   16, &ierr);
  gmshModelOccAddPoint(-0.0804919631310948, 1.4966904209026668, 0, h_near,   17, &ierr);
  gmshModelOccAddPoint(-0.0559840625545274, 1.4960515763435822, 0, h_near,   18, &ierr);
  gmshModelOccAddPoint(-0.0306464510855338, 1.4953354731407069, 0, h_near,   19, &ierr);
  gmshModelOccAddPoint(-0.0045523856983616, 1.4945377053605760, 0, h_near,   20, &ierr);
  gmshModelOccAddPoint( 0.0222248766327412, 1.4936543132180056, 0, h_near,   21, &ierr);
  gmshModelOccAddPoint( 0.0496120789335270, 1.4926865234015851, 0, h_near,   22, &ierr);
  gmshModelOccAddPoint( 0.0775359642297478, 1.4916389087120159, 0, h_near,   23, &ierr);
  gmshModelOccAddPoint( 0.1059232755471561, 1.4905160977185339, 0, h_near,   24, &ierr);
  gmshModelOccAddPoint( 0.1347007559115039, 1.4893227189903755, 0, h_near,   25, &ierr);
  gmshModelOccAddPoint( 0.1637951483485434, 1.4880634010967773, 0, h_near,   26, &ierr);
  gmshModelOccAddPoint( 0.1931331958840269, 1.4867427726069748, 0, h_near,   27, &ierr);
  gmshModelOccAddPoint( 0.2226416415437066, 1.4853654620902048, 0, h_near,   28, &ierr);
  gmshModelOccAddPoint( 0.2522472283533346, 1.4839360981157035, 0, h_near,   29, &ierr);
  gmshModelOccAddPoint( 0.2818766993386633, 1.4824593092527072, 0, h_near,   30, &ierr);
  gmshModelOccAddPoint( 0.3114567975254448, 1.4809397240704518, 0, h_near,   31, &ierr);
  gmshModelOccAddPoint( 0.3409142659394311, 1.4793819711381737, 0, h_near,   32, &ierr);
  gmshModelOccAddPoint( 0.3701758476063748, 1.4777906790251094, 0, h_near,   33, &ierr);
  gmshModelOccAddPoint( 0.3991682855520280, 1.4761704763004948, 0, h_near,   34, &ierr);
  gmshModelOccAddPoint( 0.4278183228021426, 1.4745259915335662, 0, h_near,   35, &ierr);
  gmshModelOccAddPoint( 0.4560527023824713, 1.4728618532935602, 0, h_near,   36, &ierr);
  gmshModelOccAddPoint( 0.4837981673187660, 1.4711826901497127, 0, h_near,   37, &ierr);
  gmshModelOccAddPoint( 0.5109814606367789, 1.4694931306712600, 0, h_near,   38, &ierr);
  gmshModelOccAddPoint( 0.5375293253622622, 1.4677978034274384, 0, h_near,   39, &ierr);
  gmshModelOccAddPoint( 0.5633692282590527, 1.4661012625652645, 0, h_near,   40, &ierr);
  gmshModelOccAddPoint( 0.5884720603760486, 1.4644035968985738, 0, h_near,   41, &ierr);
  gmshModelOccAddPoint( 0.6128760204039941, 1.4626979739747705, 0, h_near,   42, &ierr);
  gmshModelOccAddPoint( 0.6366250969383089, 1.4609769659635023, 0, h_near,   43, &ierr);
  gmshModelOccAddPoint( 0.6597632785744117, 1.4592331450344158, 0, h_near,   44, &ierr);
  gmshModelOccAddPoint( 0.6823345539077219, 1.4574590833571586, 0, h_near,   45, &ierr);
  gmshModelOccAddPoint( 0.7043829115336588, 1.4556473531013774, 0, h_near,   46, &ierr);
  gmshModelOccAddPoint( 0.7259523400476415, 1.4537905264367195, 0, h_near,   47, &ierr);
  gmshModelOccAddPoint( 0.7470868280450892, 1.4518811755328322, 0, h_near,   48, &ierr);
  gmshModelOccAddPoint( 0.7678303641214211, 1.4499118725593625, 0, h_near,   49, &ierr);
  gmshModelOccAddPoint( 0.7882269368720566, 1.4478751896859574, 0, h_near,   50, &ierr);
  gmshModelOccAddPoint( 0.8083205348924145, 1.4457636990822642, 0, h_near,   51, &ierr);
  gmshModelOccAddPoint( 0.8281551467779145, 1.4435699729179299, 0, h_near,   52, &ierr);
  gmshModelOccAddPoint( 0.8477747611239757, 1.4412865833626016, 0, h_near,   53, &ierr);
  gmshModelOccAddPoint( 0.8672233665260169, 1.4389061025859267, 0, h_near,   54, &ierr);
  gmshModelOccAddPoint( 0.8865449515794576, 1.4364211027575522, 0, h_near,   55, &ierr);
  gmshModelOccAddPoint( 0.9057835048797171, 1.4338241560471250, 0, h_near,   56, &ierr);
  gmshModelOccAddPoint( 0.9249830150222146, 1.4311078346242925, 0, h_near,   57, &ierr);
  gmshModelOccAddPoint( 0.9441874706023690, 1.4282647106587019, 0, h_near,   58, &ierr);
  gmshModelOccAddPoint( 0.9634408602155999, 1.4252873563199999, 0, h_near,   59, &ierr);
  gmshModelOccAddPoint( 0.9827722478565070, 1.4221712172508760, 0, h_near,   60, &ierr);
  gmshModelOccAddPoint( 1.0021509991164135, 1.4189232329861872, 0, h_near,   61, &ierr);
  gmshModelOccAddPoint( 1.0215315549858226, 1.4155532165338327, 0, h_near,   62, &ierr);
  gmshModelOccAddPoint( 1.0408683564552379, 1.4120709809017116, 0, h_near,   63, &ierr);
  gmshModelOccAddPoint( 1.0601158445151637, 1.4084863390977229, 0, h_near,   64, &ierr);
  gmshModelOccAddPoint( 1.0792284601561029, 1.4048091041297661, 0, h_near,   65, &ierr);
  gmshModelOccAddPoint( 1.0981606443685594, 1.4010490890057399, 0, h_near,   66, &ierr);
  gmshModelOccAddPoint( 1.1168668381430371, 1.3972161067335436, 0, h_near,   67, &ierr);
  gmshModelOccAddPoint( 1.1353014824700389, 1.3933199703210764, 0, h_near,   68, &ierr);
  gmshModelOccAddPoint( 1.1534190183400692, 1.3893704927762371, 0, h_near,   69, &ierr);
  gmshModelOccAddPoint( 1.1711738867436312, 1.3853774871069253, 0, h_near,   70, &ierr);
  gmshModelOccAddPoint( 1.1885205286712286, 1.3813507663210400, 0, h_near,   71, &ierr);
  gmshModelOccAddPoint( 1.2054133851133650, 1.3773001434264802, 0, h_near,   72, &ierr);
  gmshModelOccAddPoint( 1.2218068970605445, 1.3732354314311450, 0, h_near,   73, &ierr);
  gmshModelOccAddPoint( 1.2376555055032699, 1.3691664433429336, 0, h_near,   74, &ierr);
  gmshModelOccAddPoint( 1.2529136514320454, 1.3651029921697451, 0, h_near,   75, &ierr);
  gmshModelOccAddPoint( 1.2675357758373744, 1.3610548909194788, 0, h_near,   76, &ierr);
  gmshModelOccAddPoint( 1.2814763197097605, 1.3570319526000336, 0, h_near,   77, &ierr);
  gmshModelOccAddPoint( 1.2946897240397075, 1.3530439902193088, 0, h_near,   78, &ierr);
  gmshModelOccAddPoint( 1.3071340397340678, 1.3491002687496183, 0, h_near,   79, &ierr);
  gmshModelOccAddPoint( 1.3188092829772511, 1.3452036822495976, 0, h_near,   80, &ierr);
  gmshModelOccAddPoint( 1.3297425443262842, 1.3413530145109940, 0, h_near,   81, &ierr);
  gmshModelOccAddPoint( 1.3399613655777376, 1.3375469808211058, 0, h_near,   82, &ierr);
  gmshModelOccAddPoint( 1.3494932885281816, 1.3337842964672317, 0, h_near,   83, &ierr);
  gmshModelOccAddPoint( 1.3583658549741870, 1.3300636767366700, 0, h_near,   84, &ierr);
  gmshModelOccAddPoint( 1.3666066067123239, 1.3263838369167200, 0, h_near,   85, &ierr);
  gmshModelOccAddPoint( 1.3742430855391632, 1.3227434922946795, 0, h_near,   86, &ierr);
  gmshModelOccAddPoint( 1.3813028332512749, 1.3191413581578477, 0, h_near,   87, &ierr);
  gmshModelOccAddPoint( 1.3878133916452300, 1.3155761497935230, 0, h_near,   88, &ierr);
  gmshModelOccAddPoint( 1.3938023025175985, 1.3120465824890037, 0, h_near,   89, &ierr);
  gmshModelOccAddPoint( 1.3992971076649514, 1.3085513715315891, 0, h_near,   90, &ierr);
  gmshModelOccAddPoint( 1.4043253488838587, 1.3050892322085772, 0, h_near,   91, &ierr);
  gmshModelOccAddPoint( 1.4089145679708910, 1.3016588798072668, 0, h_near,   92, &ierr);
  gmshModelOccAddPoint( 1.4130923067226191, 1.2982590296149565, 0, h_near,   93, &ierr);
  gmshModelOccAddPoint( 1.4168861069356131, 1.2948883969189451, 0, h_near,   94, &ierr);
  gmshModelOccAddPoint( 1.4203235104064436, 1.2915456970065309, 0, h_near,   95, &ierr);
  gmshModelOccAddPoint( 1.4234320589316809, 1.2882296451650126, 0, h_near,   96, &ierr);
  gmshModelOccAddPoint( 1.4262392943078961, 1.2849389566816889, 0, h_near,   97, &ierr);
  gmshModelOccAddPoint( 1.4287726303873629, 1.2816723494857822, 0, h_near,   98, &ierr);
  gmshModelOccAddPoint( 1.4310518043646006, 1.2784287000219530, 0, h_near,   99, &ierr);
  gmshModelOccAddPoint( 1.4330846546146057, 1.2752071304337900, 0, h_near,  100, &ierr);
  gmshModelOccAddPoint( 1.4348779959580076, 1.2720067840002738, 0, h_near,  101, &ierr);
  gmshModelOccAddPoint( 1.4364386432154359, 1.2688268040003843, 0, h_near,  102, &ierr);
  gmshModelOccAddPoint( 1.4377734112075198, 1.2656663337131024, 0, h_near,  103, &ierr);
  gmshModelOccAddPoint( 1.4388891147548886, 1.2625245164174086, 0, h_near,  104, &ierr);
  gmshModelOccAddPoint( 1.4397925686781721, 1.2594004953922833, 0, h_near,  105, &ierr);
  gmshModelOccAddPoint( 1.4404905877979992, 1.2562934139167068, 0, h_near,  106, &ierr);
  gmshModelOccAddPoint( 1.4409899869349998, 1.2532024152696595, 0, h_near,  107, &ierr);
  gmshModelOccAddPoint( 1.4412975809098028, 1.2501266427301221, 0, h_near,  108, &ierr);
  gmshModelOccAddPoint( 1.4414201845430381, 1.2470652395770749, 0, h_near,  109, &ierr);
  gmshModelOccAddPoint( 1.4413646126553346, 1.2440173490894983, 0, h_near,  110, &ierr);
  gmshModelOccAddPoint( 1.4411376800673219, 1.2409821145463731, 0, h_near,  111, &ierr);
  gmshModelOccAddPoint( 1.4407462015996295, 1.2379586792266792, 0, h_near,  112, &ierr);
  gmshModelOccAddPoint( 1.4401969920728868, 1.2349461864093974, 0, h_near,  113, &ierr);
  gmshModelOccAddPoint( 1.4394968663077232, 1.2319437793735082, 0, h_near,  114, &ierr);
  gmshModelOccAddPoint( 1.4386526391247678, 1.2289506013979918, 0, h_near,  115, &ierr);
  gmshModelOccAddPoint( 1.4376711253446504, 1.2259657957618291, 0, h_near,  116, &ierr);
  gmshModelOccAddPoint( 1.4365591397880000, 1.2229885057440000, 0, h_near,  117, &ierr);
  gmshModelOccAddPoint( 1.4353209654502130, 1.2200181664371075, 0, h_near,  118, &ierr);
  gmshModelOccAddPoint( 1.4339507580257536, 1.2170553801882427, 0, h_near,  119, &ierr);
  gmshModelOccAddPoint( 1.4324401413838517, 1.2141010411581195, 0, h_near,  120, &ierr);
  gmshModelOccAddPoint( 1.4307807393937382, 1.2111560435074511, 0, h_near,  121, &ierr);
  gmshModelOccAddPoint( 1.4289641759246439, 1.2082212813969513, 0, h_near,  122, &ierr);
  gmshModelOccAddPoint( 1.4269820748457991, 1.2052976489873333, 0, h_near,  123, &ierr);
  gmshModelOccAddPoint( 1.4248260600264344, 1.2023860404393112, 0, h_near,  124, &ierr);
  gmshModelOccAddPoint( 1.4224877553357806, 1.1994873499135981, 0, h_near,  125, &ierr);
  gmshModelOccAddPoint( 1.4199587846430684, 1.1966024715709076, 0, h_near,  126, &ierr);
  gmshModelOccAddPoint( 1.4172307718175279, 1.1937322995719533, 0, h_near,  127, &ierr);
  gmshModelOccAddPoint( 1.4142953407283902, 1.1908777280774490, 0, h_near,  128, &ierr);
  gmshModelOccAddPoint( 1.4111441152448858, 1.1880396512481077, 0, h_near,  129, &ierr);
  gmshModelOccAddPoint( 1.4077687192362451, 1.1852189632446435, 0, h_near,  130, &ierr);
  gmshModelOccAddPoint( 1.4041607765716990, 1.1824165582277697, 0, h_near,  131, &ierr);
  gmshModelOccAddPoint( 1.4003119111204778, 1.1796333303581996, 0, h_near,  132, &ierr);
  gmshModelOccAddPoint( 1.3962137467518123, 1.1768701737966474, 0, h_near,  133, &ierr);
  gmshModelOccAddPoint( 1.3918579073349331, 1.1741279827038260, 0, h_near,  134, &ierr);
  gmshModelOccAddPoint( 1.3872360167390707, 1.1714076512404492, 0, h_near,  135, &ierr);
  gmshModelOccAddPoint( 1.3823396988334558, 1.1687100735672304, 0, h_near,  136, &ierr);
  gmshModelOccAddPoint( 1.3771617800741638, 1.1660363424023938, 0, h_near,  137, &ierr);
  gmshModelOccAddPoint( 1.3717090669893410, 1.1633898586952205, 0, h_near,  138, &ierr);
  gmshModelOccAddPoint( 1.3659973855084711, 1.1607755125763191, 0, h_near,  139, &ierr);
  gmshModelOccAddPoint( 1.3600427118843919, 1.1581982189959870, 0, h_near,  140, &ierr);
  gmshModelOccAddPoint( 1.3538610223699419, 1.1556628929045218, 0, h_near,  141, &ierr);
  gmshModelOccAddPoint( 1.3474682932179600, 1.1531744492522209, 0, h_near,  142, &ierr);
  gmshModelOccAddPoint( 1.3408805006812845, 1.1507378029893816, 0, h_near,  143, &ierr);
  gmshModelOccAddPoint( 1.3341136210127535, 1.1483578690663012, 0, h_near,  144, &ierr);
  gmshModelOccAddPoint( 1.3271836304652063, 1.1460395624332775, 0, h_near,  145, &ierr);
  gmshModelOccAddPoint( 1.3201065052914809, 1.1437877980406079, 0, h_near,  146, &ierr);
  gmshModelOccAddPoint( 1.3128982217444156, 1.1416074908385898, 0, h_near,  147, &ierr);
  gmshModelOccAddPoint( 1.3055747560768494, 1.1395035557775204, 0, h_near,  148, &ierr);
  gmshModelOccAddPoint( 1.2981520845416201, 1.1374809078076973, 0, h_near,  149, &ierr);
  gmshModelOccAddPoint( 1.2906461833915670, 1.1355444618794182, 0, h_near,  150, &ierr);
  gmshModelOccAddPoint( 1.2830730288795280, 1.1336991329429800, 0, h_near,  151, &ierr);
  gmshModelOccAddPoint( 1.2754485972583420, 1.1319498359486806, 0, h_near,  152, &ierr);
  gmshModelOccAddPoint( 1.2677888647808473, 1.1303014858468172, 0, h_near,  153, &ierr);
  gmshModelOccAddPoint( 1.2601098076998822, 1.1287589975876873, 0, h_near,  154, &ierr);
  gmshModelOccAddPoint( 1.2524274022682853, 1.1273272861215882, 0, h_near,  155, &ierr);
  gmshModelOccAddPoint( 1.2447574699229378, 1.1260112034409910, 0, h_near,  156, &ierr);
  gmshModelOccAddPoint( 1.2371065431432720, 1.1248118240687639, 0, h_near,  157, &ierr);
  gmshModelOccAddPoint( 1.2294667565246746, 1.1237243674498927, 0, h_near,  158, &ierr);
  gmshModelOccAddPoint( 1.2218290061348731, 1.1227435493667490, 0, h_near,  159, &ierr);
  gmshModelOccAddPoint( 1.2141841880415942, 1.1218640856017048, 0, h_near,  160, &ierr);
  gmshModelOccAddPoint( 1.2065231983125653, 1.1210806919371317, 0, h_near,  161, &ierr);
  gmshModelOccAddPoint( 1.1988369330155138, 1.1203880841554017, 0, h_near,  162, &ierr);
  gmshModelOccAddPoint( 1.1911162882181663, 1.1197809780388865, 0, h_near,  163, &ierr);
  gmshModelOccAddPoint( 1.1833521599882502, 1.1192540893699578, 0, h_near,  164, &ierr);
  gmshModelOccAddPoint( 1.1760000000000002, 1.1188021339309877, 0, h_near,  165, &ierr);
  gmshModelOccAddPoint( 1.1676570375016206, 1.1184198275043478, 0, h_near,  166, &ierr);
  gmshModelOccAddPoint( 1.1597078353803612, 1.1181018858724101, 0, h_near,  167, &ierr);
  gmshModelOccAddPoint( 1.1516787340974417, 1.1178430248175462, 0, h_near,  168, &ierr);
  gmshModelOccAddPoint( 1.1435606297205891, 1.1176379601221278, 0, h_near,  169, &ierr);
  gmshModelOccAddPoint( 1.1353444183175307, 1.1174814075685273, 0, h_near,  170, &ierr);
  gmshModelOccAddPoint( 1.1270209959559938, 1.1173680829391159, 0, h_near,  171, &ierr);
  gmshModelOccAddPoint( 1.1185812587037050, 1.1172927020162657, 0, h_near,  172, &ierr);
  gmshModelOccAddPoint( 1.1100161026283915, 1.1172499805823484, 0, h_near,  173, &ierr);
  gmshModelOccAddPoint( 1.1013164237977808, 1.1172346344197359, 0, h_near,  174, &ierr);
  gmshModelOccAddPoint( 1.0924731182795999, 1.1172413793108000, 0, h_near,  175, &ierr);
  gmshModelOccAddPoint( 1.0834748637432567, 1.1172672429155304, 0, h_near,  176, &ierr);
  gmshModelOccAddPoint( 1.0743014642648825, 1.1173185004043873, 0, h_near,  177, &ierr);
  gmshModelOccAddPoint( 1.0649305055222895, 1.1174037388254490, 0, h_near,  178, &ierr);
  gmshModelOccAddPoint( 1.0553395731932902, 1.1175315452267940, 0, h_near,  179, &ierr);
  gmshModelOccAddPoint( 1.0455062529556964, 1.1177105066565003, 0, h_near,  180, &ierr);
  gmshModelOccAddPoint( 1.0354081304873206, 1.1179492101626463, 0, h_near,  181, &ierr);
  gmshModelOccAddPoint( 1.0250227914659749, 1.1182562427933103, 0, h_near,  182, &ierr);
  gmshModelOccAddPoint( 1.0143278215694715, 1.1186401915965702, 0, h_near,  183, &ierr);
  gmshModelOccAddPoint( 1.0033008064756224, 1.1191096436205046, 0, h_near,  184, &ierr);
  gmshModelOccAddPoint( 0.9919193318622402, 1.1196731859131916, 0, h_near,  185, &ierr);
  gmshModelOccAddPoint( 0.9801609834071370, 1.1203394055227096, 0, h_near,  186, &ierr);
  gmshModelOccAddPoint( 0.9680033467881249, 1.1211168894971368, 0, h_near,  187, &ierr);
  gmshModelOccAddPoint( 0.9554240076830162, 1.1220142248845513, 0, h_near,  188, &ierr);
  gmshModelOccAddPoint( 0.9424005517696229, 1.1230399987330313, 0, h_near,  189, &ierr);
  gmshModelOccAddPoint( 0.9289105647257571, 1.1242027980906553, 0, h_near,  190, &ierr);
  gmshModelOccAddPoint( 0.9149316322292316, 1.1255112100055016, 0, h_near,  191, &ierr);
  gmshModelOccAddPoint( 0.9004413399578581, 1.1269738215256480, 0, h_near,  192, &ierr);
  gmshModelOccAddPoint( 0.8854172735894490, 1.1285992196991732, 0, h_near,  193, &ierr);
  gmshModelOccAddPoint( 0.8698370188018166, 1.1303959915741550, 0, h_near,  194, &ierr);
  gmshModelOccAddPoint( 0.8536802654824547, 1.1323714304349906, 0, h_near,  195, &ierr);
  gmshModelOccAddPoint( 0.8369511649564076, 1.1345177895632750, 0, h_near,  196, &ierr);
  gmshModelOccAddPoint( 0.8196696501213343, 1.1368176190129913, 0, h_near,  197, &ierr);
  gmshModelOccAddPoint( 0.8018559169011027, 1.1392533071176614, 0, h_near,  198, &ierr);
  gmshModelOccAddPoint( 0.7835301612195810, 1.1418072422108076, 0, h_near,  199, &ierr);
  gmshModelOccAddPoint( 0.7647125790006376, 1.1444618126259525, 0, h_near,  200, &ierr);
  gmshModelOccAddPoint( 0.7454233661681409, 1.1471994066966180, 0, h_near,  201, &ierr);
  gmshModelOccAddPoint( 0.7256827186459585, 1.1500024127563266, 0, h_near,  202, &ierr);
  gmshModelOccAddPoint( 0.7055108323579597, 1.1528532191386007, 0, h_near,  203, &ierr);
  gmshModelOccAddPoint( 0.6849279032280126, 1.1557342141769620, 0, h_near,  204, &ierr);
  gmshModelOccAddPoint( 0.6639541271799853, 1.1586277862049335, 0, h_near,  205, &ierr);
  gmshModelOccAddPoint( 0.6426097001377460, 1.1615163235560368, 0, h_near,  206, &ierr);
  gmshModelOccAddPoint( 0.6209148180251627, 1.1643822145637948, 0, h_near,  207, &ierr);
  gmshModelOccAddPoint( 0.5988896767661047, 1.1672078475617294, 0, h_near,  208, &ierr);
  gmshModelOccAddPoint( 0.5765544722844398, 1.1699756108833628, 0, h_near,  209, &ierr);
  gmshModelOccAddPoint( 0.5539294005040362, 1.1726678928622174, 0, h_near,  210, &ierr);
  gmshModelOccAddPoint( 0.5310346573487623, 1.1752670818318154, 0, h_near,  211, &ierr);
  gmshModelOccAddPoint( 0.5078904387424863, 1.1777555661256793, 0, h_near,  212, &ierr);
  gmshModelOccAddPoint( 0.4845169406090762, 1.1801157340773314, 0, h_near,  213, &ierr);
  gmshModelOccAddPoint( 0.4609343588724011, 1.1823301111174744, 0, h_near,  214, &ierr);
  gmshModelOccAddPoint( 0.4371628894563291, 1.1843894485076563, 0, h_near,  215, &ierr);
  gmshModelOccAddPoint( 0.4132227282847282, 1.1862972475472342, 0, h_near,  216, &ierr);
  gmshModelOccAddPoint( 0.3891340712814669, 1.1880581063130116, 0, h_near,  217, &ierr);
  gmshModelOccAddPoint( 0.3649171143704134, 1.1896766228817914, 0, h_near,  218, &ierr);
  gmshModelOccAddPoint( 0.3405920534754354, 1.1911573953303773, 0, h_near,  219, &ierr);
  gmshModelOccAddPoint( 0.3161790845204026, 1.1925050217355722, 0, h_near,  220, &ierr);
  gmshModelOccAddPoint( 0.2916984034291824, 1.1937241001741792, 0, h_near,  221, &ierr);
  gmshModelOccAddPoint( 0.2671702061256434, 1.1948192287230019, 0, h_near,  222, &ierr);
  gmshModelOccAddPoint( 0.2426146885336538, 1.1957950054588433, 0, h_near,  223, &ierr);
  gmshModelOccAddPoint( 0.2180520465770818, 1.1966560284585066, 0, h_near,  224, &ierr);
  gmshModelOccAddPoint( 0.1935024761797952, 1.1974068957987953, 0, h_near,  225, &ierr);
  gmshModelOccAddPoint( 0.1689861732656636, 1.1980522055565124, 0, h_near,  226, &ierr);
  gmshModelOccAddPoint( 0.1445233337585545, 1.1985965558084610, 0, h_near,  227, &ierr);
  gmshModelOccAddPoint( 0.1201341535823364, 1.1990445446314448, 0, h_near,  228, &ierr);
  gmshModelOccAddPoint( 0.0960000000000000, 1.1994007701022666, 0, h_near,  229, &ierr);
  gmshModelOccAddPoint( 0.0716575549180462, 1.1996698302977298, 0, h_near,  230, &ierr);
  gmshModelOccAddPoint( 0.0476105282777101, 1.1998563232946375, 0, h_near,  231, &ierr);
  gmshModelOccAddPoint( 0.0237179446637388, 1.1999648471697932, 0, h_near,  232, &ierr);
  gmshModelOccAddPoint( 0.0000000000000000, 1.2000000000000000, 0, h_near,  233, &ierr);

  // * Fan duct (points)
  gmshModelOccAddPoint( -L_duct, 1.2000000000000000, 0, h_near, 234, &ierr);
  gmshModelOccAddPoint( -L_duct, 0.3586206896556000, 0, h_near, 235, &ierr);

  // * Spinner  (points)
  gmshModelOccAddPoint( 0.0000000000000000, 0.3586206896556000, 0, h_near,  236, &ierr);
  gmshModelOccAddPoint( 0.0094369356474370, 0.3585833677801259, 0, h_near,  237, &ierr);
  gmshModelOccAddPoint( 0.0194167690446929, 0.3584636577634160, 0, h_near,  238, &ierr);
  gmshModelOccAddPoint( 0.0299032528379802, 0.3582499430200392, 0, h_near,  239, &ierr);
  gmshModelOccAddPoint( 0.0408601396735112, 0.3579306069645641, 0, h_near,  240, &ierr);
  gmshModelOccAddPoint( 0.0522511821974983, 0.3574940330115595, 0, h_near,  241, &ierr);
  gmshModelOccAddPoint( 0.0640401330561540, 0.3569286045755942, 0, h_near,  242, &ierr);
  gmshModelOccAddPoint( 0.0761907448956906, 0.3562227050712369, 0, h_near,  243, &ierr);
  gmshModelOccAddPoint( 0.0886667703623206, 0.3553647179130562, 0, h_near,  244, &ierr);
  gmshModelOccAddPoint( 0.1014319621022565, 0.3543430265156212, 0, h_near,  245, &ierr);
  gmshModelOccAddPoint( 0.1144500727617104, 0.3531460192702524, 0, h_near,  246, &ierr);
  gmshModelOccAddPoint( 0.1276848549868951, 0.3517631396397190, 0, h_near,  247, &ierr);
  gmshModelOccAddPoint( 0.1411000614240227, 0.3501861850905401, 0, h_near,  248, &ierr);
  gmshModelOccAddPoint( 0.1546594447193058, 0.3484072716013699, 0, h_near,  249, &ierr);
  gmshModelOccAddPoint( 0.1683267575189567, 0.3464185151508629, 0, h_near,  250, &ierr);
  gmshModelOccAddPoint( 0.1820657524691879, 0.3442120317176733, 0, h_near,  251, &ierr);
  gmshModelOccAddPoint( 0.1958401822162117, 0.3417799372804556, 0, h_near,  252, &ierr);
  gmshModelOccAddPoint( 0.2096137994062407, 0.3391143478178640, 0, h_near,  253, &ierr);
  gmshModelOccAddPoint( 0.2233503566854871, 0.3362073793085530, 0, h_near,  254, &ierr);
  gmshModelOccAddPoint( 0.2370136067001635, 0.3330511477311768, 0, h_near,  255, &ierr);
  gmshModelOccAddPoint( 0.2505686505145814, 0.3296374883012909, 0, h_near,  256, &ierr);
  gmshModelOccAddPoint( 0.2640330089466618, 0.3259473215689799, 0, h_near,  257, &ierr);
  gmshModelOccAddPoint( 0.2774922979283390, 0.3219473895478331, 0, h_near,  258, &ierr);
  gmshModelOccAddPoint( 0.2910366843026320, 0.3176034866759807, 0, h_near,  259, &ierr);
  gmshModelOccAddPoint( 0.3047563349125602, 0.3128814073915527, 0, h_near,  260, &ierr);
  gmshModelOccAddPoint( 0.3187414166011429, 0.3077469461326795, 0, h_near,  261, &ierr);
  gmshModelOccAddPoint( 0.3330820962113991, 0.3021658973374913, 0, h_near,  262, &ierr);
  gmshModelOccAddPoint( 0.3478685405863481, 0.2961040554441182, 0, h_near,  263, &ierr);
  gmshModelOccAddPoint( 0.3631909165690094, 0.2895272148906906, 0, h_near,  264, &ierr);
  gmshModelOccAddPoint( 0.3791393910024019, 0.2824011701153385, 0, h_near,  265, &ierr);
  gmshModelOccAddPoint( 0.3958015194316177, 0.2746925523854085, 0, h_near,  266, &ierr);
  gmshModelOccAddPoint( 0.4132257846475764, 0.2663805144128162, 0, h_near,  267, &ierr);
  gmshModelOccAddPoint( 0.4314305911576642, 0.2574538479423018, 0, h_near,  268, &ierr);
  gmshModelOccAddPoint( 0.4504335697513631, 0.2479015926680023, 0, h_near,  269, &ierr);
  gmshModelOccAddPoint( 0.4702523512181548, 0.2377127882840553, 0, h_near,  270, &ierr);
  gmshModelOccAddPoint( 0.4909045663475211, 0.2268764744845982, 0, h_near,  271, &ierr);
  gmshModelOccAddPoint( 0.5124078459289438, 0.2153816909637684, 0, h_near,  272, &ierr);
  gmshModelOccAddPoint( 0.5347798207519048, 0.2032174774157031, 0, h_near,  273, &ierr);
  gmshModelOccAddPoint( 0.5580381216058857, 0.1903728735345399, 0, h_near,  274, &ierr);
  gmshModelOccAddPoint( 0.5822003792803687, 0.1768369190144160, 0, h_near,  275, &ierr);
  gmshModelOccAddPoint( 0.6072842245648352, 0.1625980868079891, 0, h_near,  276, &ierr);
  gmshModelOccAddPoint( 0.6333072882487674, 0.1476406612941676, 0, h_near,  277, &ierr);
  gmshModelOccAddPoint( 0.6602872011216466, 0.1319470495207083, 0, h_near,  278, &ierr);
  gmshModelOccAddPoint( 0.6882415939729553, 0.1154996496800321, 0, h_near,  279, &ierr);
  gmshModelOccAddPoint( 0.7171880975921747, 0.0982808599645601, 0, h_near,  280, &ierr);
  gmshModelOccAddPoint( 0.7471443427687871, 0.0802730785667134, 0, h_near,  281, &ierr);
  gmshModelOccAddPoint( 0.7781279602922739, 0.0614587036789129, 0, h_near,  282, &ierr);
  gmshModelOccAddPoint( 0.8101565809521170, 0.0418201334935799, 0, h_near,  283, &ierr);
  gmshModelOccAddPoint( 0.8432478355377984, 0.0213397662031352, 0, h_near,  284, &ierr);
  gmshModelOccAddPoint( 0.8774193548388000, 0.0000000000000000, 0, h_near,  285, &ierr);

  // * Surrounding boundary (points)
  gmshModelOccAddPoint(Lx  , 0  , 0, h_far, 290, &ierr);
  gmshModelOccAddPoint(Lx  , Ly , 0, h_far, 291, &ierr);
  gmshModelOccAddPoint(-0.3, Ly , 0, h_far, 292, &ierr);

  gmshModelOccAddLine(290, 285, 17, &ierr);
  gmshModelOccAddLine(290, 291, 18, &ierr);
  gmshModelOccAddLine(291, 292, 19, &ierr);
  gmshModelOccAddLine(292, 1  , 20, &ierr);

  int pt[285];
  for(int i = 0; i < 285; i++) pt[i] = i+1;

  gmshModelOccAddSpline(&pt[0]  , 165, 1, &ierr);  // * Nacelle surface (hard wall)
  gmshModelOccAddSpline(&pt[164],  65, 2, &ierr);  // * Nacelle surface (lined    )
  gmshModelOccAddSpline(&pt[228],   5, 3, &ierr);  // * Nacelle surface (hard wall)

  gmshModelOccAddLine(233, 234, 4, &ierr);  // * Fan duct outer wall
  gmshModelOccAddLine(234, 235, 5, &ierr);  // * Fan Face
  gmshModelOccAddLine(235, 236, 6, &ierr);  // * Fan duct inner wall

  // * Spinner surface
  gmshModelOccAddSpline(&pt[235], 50, 7, &ierr);

  int curveTags[11] = {17, -18, -19, 20, 1, 2, 3, 4, 5, 6, 7};
  int  wireTags[1]  = {1};
  gmshModelOccAddCurveLoop   (curveTags, 11, 1, &ierr);
  gmshModelOccAddPlaneSurface( wireTags,  1, 1, &ierr);
  gmshModelOccSynchronize(&ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                PARTITION                              * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  double xmin, xmax, ymin, ymax, zmin, zmax;

  gmshModelGetBoundingBox (2, 1,  &xmin, &ymin, &zmin,
                           &xmax, &ymax, &zmax, &ierr);
  gmshModelOccAddRectangle(xmin, ymin, 0, xmax-xmin, ymax-ymin, 2, 0, &ierr);

  int    *inside  ;
  size_t  inside_n;
  int    **ovvs   ;
  size_t  *ovvs_n ;
  size_t   ovvs_nn;
  int  objDimTags[2] = {2, 2};
  int toolDimTags[2] = {2, 1};
  gmshModelOccCut( objDimTags, 2,
                  toolDimTags, 2,
                  &inside, &inside_n,
                  &ovvs, &ovvs_n, &ovvs_nn,
                   -1, -1,  1, &ierr);

  int doms[2*Nx_DOM*Ny_DOM] = {0};

  const double dx = (xmax - xmin) / Nx_DOM;
  const double dy = (ymax - ymin) / Ny_DOM;
  for(int i = 0; i < Nx_DOM; i++) 
  {
    for(int j = 0; j < Ny_DOM; j++) 
    {
      int tag = 100 * (i+1) + (j+1);
      gmshModelOccAddRectangle(-0.3 + i*dx, 
                                0.0 + j*dy, 0, dx, dy, tag, 0, &ierr);
      doms[2*(i*Ny_DOM+j) + 0] =   2;
      doms[2*(i*Ny_DOM+j) + 1] = tag;
    }
  }

  int   *dom  ;
  size_t dom_n;
  gmshModelOccCut(  doms,  2*Nx_DOM*Ny_DOM,
                  inside, inside_n,
                  &dom  , &dom_n  ,
                  &ovvs, &ovvs_n, &ovvs_nn,
                   -1, -1,  1, &ierr);

  int   *frag  ;
  size_t frag_n;
  gmshModelOccFragment(   dom,   dom_n,
                         NULL,       0,
                       &frag , &frag_n,
                       &ovvs, &ovvs_n, &ovvs_nn,
                        -1, -1,  1, &ierr);

  gmshModelOccSynchronize(       &ierr);
  gmshModelMeshGenerate  (2    , &ierr);
  gmshModelMeshSetOrder  (order, &ierr);
  gmshWrite("part_metis.msh"   , &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                              PHYSICAL TAGS                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
 
  // * ADD PHYSICAL TAGS TO HARDWALL
//const double xmin1 = -0.31, ymin1 =  1.11, zmin1 = -0.1;
//const double xmax1 =  1.45, ymax1 =  1.51, zmax1 =  0.1;
  const double xmin1 = -0.31, ymin1 =  1.19, zmin1 = -0.1;
  const double xmax1 =  0.10, ymax1 =  1.21, zmax1 =  0.1;
  int    *wl1Tags   = NULL;
  size_t  wl1Tags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin1, ymin1, zmin1,
                                    xmax1, ymax1, zmax1,
                                    &wl1Tags, &wl1Tags_n, 1, &ierr);

//const double xmin2 = -0.31, ymin2 = -0.10, zmin2 = -0.1;
//const double xmax2 =  0.88, ymax2 =  0.36, zmax2 =  0.1;
  const double xmin2 = -0.31, ymin2 =  0.34, zmin2 = -0.1;
  const double xmax2 =  0.01, ymax2 =  0.36, zmax2 =  0.1;
  int    *wl2Tags   = NULL;
  size_t  wl2Tags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin2, ymin2, zmin2,
                                    xmax2, ymax2, zmax2,
                                    &wl2Tags, &wl2Tags_n, 1, &ierr);

  int    *wlTags   = malloc( (wl1Tags_n + wl2Tags_n)* sizeof(int) );
  size_t  wlTags_n = wl1Tags_n + wl2Tags_n;
  memcpy(&wlTags[0]        , &wl1Tags[0], wl1Tags_n * sizeof(int) );
  memcpy(&wlTags[wl1Tags_n], &wl2Tags[0], wl2Tags_n * sizeof(int) );

  gmshModelAddPhysicalGroup(1, wlTags, wlTags_n, 5000, &ierr);
  gmshModelSetPhysicalName (1, 5000, "niceBC", &ierr);

  // * ADD PHYSICAL TAGS TO SOURCE
  const double xmin3 = -0.31, ymin3 =  0.34, zmin3 = -0.1;
  const double xmax3 = -0.29, ymax3 =  1.21, zmax3 =  0.1;
  int    *srcTags   = NULL;
  size_t  srcTags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin3, ymin3, zmin3,
                                    xmax3, ymax3, zmax3,
                                    &srcTags, &srcTags_n, 1, &ierr);

  gmshModelAddPhysicalGroup(1, srcTags, srcTags_n, 6000, &ierr);
  gmshModelSetPhysicalName (1, 6000, "niceSRC", &ierr);

  // * ADD PHYSICAL TAGS TO INF
  const double xmin4 = 2.99, ymin4 = -0.1, zmin4 = -0.1;
  const double xmax4 = 3.01, ymax4 = +2.6, zmax4 =  0.1;
  int    *inf1Tags   = NULL;
  size_t  inf1Tags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin4, ymin4, zmin4,
                                    xmax4, ymax4, zmax4,
                                    &inf1Tags, &inf1Tags_n, 1, &ierr);

  gmshModelAddPhysicalGroup(1, inf1Tags, inf1Tags_n, 2000, &ierr);
  gmshModelSetPhysicalName (1, 2000, "rightInf", &ierr);

  // * ADD PHYSICAL TAGS TO INF
  const double xmin5 = -0.31, ymin5 =  2.49, zmin5 = -0.1;
  const double xmax5 = +3.01, ymax5 =  2.51, zmax5 =  0.1;
  int    *inf2Tags   = NULL;
  size_t  inf2Tags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin5, ymin5, zmin5,
                                    xmax5, ymax5, zmax5,
                                    &inf2Tags, &inf2Tags_n, 1, &ierr);

  gmshModelAddPhysicalGroup(1, inf2Tags, inf2Tags_n, 3000, &ierr);
  gmshModelSetPhysicalName (1, 3000, "topInf", &ierr);

  // * ADD PHYSICAL TAGS TO ABC
  const double xmin6 = -0.31, ymin6 =  1.49, zmin6 = -0.1;
  const double xmax6 = -0.29, ymax6 =  2.51, zmax6 =  0.1;
  int    *abc1Tags   = NULL;
  size_t  abc1Tags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin6, ymin6, zmin6,
                                    xmax6, ymax6, zmax6,
                                    &abc1Tags, &abc1Tags_n, 1, &ierr);

  gmshModelAddPhysicalGroup(1, abc1Tags, abc1Tags_n, 4000, &ierr);
  gmshModelSetPhysicalName (1, 4000, "leftABC", &ierr);

  // * ADD PHYSICAL TAGS TO ABC
  const double xmin7 =  0.87, ymin7 = -0.01, zmin7 = -0.1;
  const double xmax7 =  3.01, ymax7 =  0.01, zmax7 =  0.1;
  int    *abc2Tags   = NULL;
  size_t  abc2Tags_n =    0;
  gmshModelGetEntitiesInBoundingBox(xmin7, ymin7, zmin7,
                                    xmax7, ymax7, zmax7,
                                    &abc2Tags, &abc2Tags_n, 1, &ierr);

  gmshModelAddPhysicalGroup(1, abc2Tags, abc2Tags_n, 1000, &ierr);
  gmshModelSetPhysicalName (1, 1000, "bottomABC", &ierr);
}

static double fgetK(const double *xyz)
{
  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double_complex fSource(const double *xyz)
{
  const  double  y = xyz[1];

//return  sin((M_PI / 0.50) * y) * 10.;
  return -1.;
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}

static double_complex fZero  (const double *xyz)
{
  double complex re = 0.;

  return re;
}
