#include "hxt_entity.h"
#include "hxt_group.h"
#include "hxt_formulation.h"
#include "hxt_system.h"

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                VARIABLES                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nx_DOM 1
#define Ny_DOM 1

#define Pi_SCA 0
#define Pj_SCA 0

#define OMEGA      ( 2. * M_PI * 1)
#define LSP        ( 1.           )
#define N_LAMBDA   ( 20           )

#define theta  (M_PI / 3.)
#define NPade  (8        )

#define order  1
#define nrhs   1

// * ********************************************************************* * //
// * ******                                                         ****** * //
// *                                CONSTANTS                              * //
// * ******                                                         ****** * //
// * ********************************************************************* * //
#define Nt_DOM (Nx_DOM * Ny_DOM)
#define Lx     ( 7.5)
#define Ly     ( 7.5)
#define X_SCA  (1.25)
#define Y_SCA  (1.25)
#define R_SCA  (1.00)

#define LSx (Lx / Nx_DOM)
#define LSy (Ly / Ny_DOM)

#define X0_DOM (-X_SCA - Pi_SCA*LSx)
#define Y0_DOM (-Y_SCA - Pj_SCA*LSy)
#define Z0_DOM  +0

#define WAVENUMBER ( OMEGA      / LSP       )
#define LAMBDA     ( (2*M_PI)   / WAVENUMBER)
#define FREQ       ( WAVENUMBER / (2*M_PI)  )
#define LC         ( LAMBDA     / N_LAMBDA  )

static double_complex fK2    (const double *xyz);
static double_complex fK     (const double *xyz);
static double_complex fSource(const double *xyz);

int main(int argc, char **argv)
{
  if( Lx/Nx_DOM <= 2*R_SCA ||
      Ly/Ny_DOM <= 2*R_SCA  )
  {
    printf("Size of subdomain too small!\n");
    printf("Lx / Nx_DOM : %f\n", Lx/Nx_DOM );
    printf("Ly / Ny_DOM : %f\n", Ly/Ny_DOM );
    printf("2  *  R_SCA : %f\n", 2 * R_SCA );
    exit(0);
  }
  int ierr = 0;

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            GMSH INITIALIZE                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshInitialize( argc,  argv,  1, &ierr);
  gmshModelAdd  ("rectangularDom", &ierr);

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                            MESH GENERATION                            * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  printf("MESHING...\n");
  gmshOptionSetNumber("Mesh.CharacteristicLengthMin", LC, &ierr);
  gmshOptionSetNumber("Mesh.CharacteristicLengthMax", LC, &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                             SOURCE                              -- * //
  // * --------------------------------------------------------------------- * //
  int p0 = gmshModelGeoAddPoint(     0, 0, 0, 0, -1, &ierr);
  int p1 = gmshModelGeoAddPoint( R_SCA, 0, 0, 0, -1, &ierr);
  int p2 = gmshModelGeoAddPoint(-R_SCA, 0, 0, 0, -1, &ierr);

  int scaTags[2] = {0};
      scaTags[0] = gmshModelGeoAddCircleArc(p1, p0, p2, -1, 0, 0, 0, &ierr);
      scaTags[1] = gmshModelGeoAddCircleArc(p2, p0, p1, -1, 0, 0, 0, &ierr);
  int scaLlp     = gmshModelGeoAddCurveLoop(scaTags, 2, -1,          &ierr);

  // * --------------------------------------------------------------------- * //
  // * --                           RECTANGLES                            -- * //
  // * --------------------------------------------------------------------- * //
  int ptTags[Nx_DOM+1][Ny_DOM+1] = {{0}};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      ptTags[i][j] = gmshModelGeoAddPoint(X0_DOM + Lx*i/Nx_DOM, 
                                          Y0_DOM + Ly*j/Ny_DOM, 
                                          Z0_DOM, 0, -1, &ierr);
    }
  }

  int  lxTags[Nx_DOM  ][Ny_DOM+1]    = {{ 0 }};
  int  lyTags[Nx_DOM+1][Ny_DOM  ]    = {{ 0 }};

  for(int i = 0; i < Nx_DOM+1; i++)
  {
    for(int j = 0; j < Ny_DOM+1; j++)
    {
      if(i < Nx_DOM) lxTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i+1][j], -1, &ierr);
      if(j < Ny_DOM) lyTags[i][j] = gmshModelGeoAddLine(ptTags[i][j], ptTags[i][j+1], -1, &ierr);
    }
  }

  int recLlps[Nx_DOM][Ny_DOM] = {{0}};
  int recTags[Nx_DOM][Ny_DOM] = {{0}};

  for(int i = 0; i < Nx_DOM; i++)
  {
    for(int j = 0; j < Ny_DOM; j++)
    {
      if(i == 0 && j == 0)
      {
        int curveTags[] = { lxTags[i  ][j  ],
                            lyTags[i+1][j  ],
                           -lxTags[i  ][j+1],
                           -lyTags[i  ][j  ],
                          };

        recLlps[i][j]  = gmshModelGeoAddCurveLoop   (curveTags, 4, -1, &ierr);
        int wireTags[] = {recLlps[i][j], scaLlp};
        recTags[i][j]  = gmshModelGeoAddPlaneSurface(wireTags , 2, -1, &ierr);
      }
      else
      {
        int curveTags[] = { lxTags[i  ][j  ],
                            lyTags[i+1][j  ],
                           -lxTags[i  ][j+1],
                           -lyTags[i  ][j  ],
                          };

        recLlps[i][j] = gmshModelGeoAddCurveLoop   ( curveTags    , 4, -1, &ierr);
        recTags[i][j] = gmshModelGeoAddPlaneSurface(&recLlps[i][j], 1, -1, &ierr);
      }
    }
  }

  // * --------------------------------------------------------------------- * //
  // * --                         GENERATE  MESH                          -- * //
  // * --------------------------------------------------------------------- * //
  gmshModelGeoSynchronize(        &ierr);
  gmshModelMeshGenerate  (2,      &ierr);
  gmshModelMeshSetOrder  (order,  &ierr);
  gmshWrite("rectangularDom.msh", &ierr);
  printf("MESHING FINISHED\n");

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                              MONO DOMAIN                              * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  size_t offset = 0;

  // * --------------------------------------------------------------------- * //
  // * --                              Group                              -- * //
  // * --------------------------------------------------------------------- * //
  HXTGroup *domain = NULL;
  HXT_CHECK( hxtGroupCreate   (&domain   ) );
  HXT_CHECK( hxtGroupAddEntity( domain, 2, recTags[0][0]) );
  HXT_CHECK( hxtGroupBuild    ( domain, offset) ); 

  HXTGroup *source = NULL;
  HXT_CHECK( hxtGroupCreate   (&source   ) );
  HXT_CHECK( hxtGroupAddEntity( source, 1, scaTags[0   ]) );
  HXT_CHECK( hxtGroupAddEntity( source, 1, scaTags[1   ]) );
  HXT_CHECK( hxtGroupBuild    ( source, offset) ); 

  HXTGroup *bnd[4];
  HXT_CHECK( hxtGroupCreate(&bnd[0]) );
  HXT_CHECK( hxtGroupCreate(&bnd[1]) );
  HXT_CHECK( hxtGroupCreate(&bnd[2]) );
  HXT_CHECK( hxtGroupCreate(&bnd[3]) );

  HXT_CHECK( hxtGroupAddEntity( bnd[0], 1, lxTags[0][0]) );
  HXT_CHECK( hxtGroupAddEntity( bnd[1], 1, lyTags[1][0]) );
  HXT_CHECK( hxtGroupAddEntity( bnd[2], 1, lxTags[0][1]) );
  HXT_CHECK( hxtGroupAddEntity( bnd[3], 1, lyTags[0][0]) );

  HXT_CHECK( hxtGroupBuild( bnd[0], offset) ); 
  HXT_CHECK( hxtGroupBuild( bnd[1], offset) ); 
  HXT_CHECK( hxtGroupBuild( bnd[2], offset) ); 
  HXT_CHECK( hxtGroupBuild( bnd[3], offset) ); 

  HXTGroup *aux[4][NPade];
  HXTGroup *cnr[8][NPade];
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtGroupCreate (&aux[0][j]) );
    HXT_CHECK( hxtGroupCreate (&aux[1][j]) );
    HXT_CHECK( hxtGroupCreate (&aux[2][j]) );
    HXT_CHECK( hxtGroupCreate (&aux[3][j]) );

    HXT_CHECK( hxtGroupCreate (&cnr[0][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[1][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[2][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[3][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[4][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[5][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[6][j]) );
    HXT_CHECK( hxtGroupCreate (&cnr[7][j]) );

    HXT_CHECK( hxtGroupAddEntity( aux[0][j], 1,  lxTags[0][0]) );
    HXT_CHECK( hxtGroupAddEntity( aux[1][j], 1,  lyTags[1][0]) );
    HXT_CHECK( hxtGroupAddEntity( aux[2][j], 1,  lxTags[0][1]) );
    HXT_CHECK( hxtGroupAddEntity( aux[3][j], 1,  lyTags[0][0]) );

    HXT_CHECK( hxtGroupAddEntity( cnr[0][j], 0,  ptTags[0][0]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[1][j], 0,  ptTags[1][0]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[2][j], 0,  ptTags[1][0]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[3][j], 0,  ptTags[1][1]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[4][j], 0,  ptTags[1][1]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[5][j], 0,  ptTags[0][1]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[6][j], 0,  ptTags[0][1]) );
    HXT_CHECK( hxtGroupAddEntity( cnr[7][j], 0,  ptTags[0][0]) );

    HXT_CHECK( hxtGroupBuild  ( aux[0][j], ++offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[0][j],   offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[1][j],   offset) ); 

    HXT_CHECK( hxtGroupBuild  ( aux[1][j], ++offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[2][j],   offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[3][j],   offset) ); 

    HXT_CHECK( hxtGroupBuild  ( aux[2][j], ++offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[4][j],   offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[5][j],   offset) ); 

    HXT_CHECK( hxtGroupBuild  ( aux[3][j], ++offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[6][j],   offset) ); 
    HXT_CHECK( hxtGroupBuild  ( cnr[7][j],   offset) ); 
  }

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation Wave                           -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *wave = NULL;
  HXT_CHECK( hxtFormulationCreate    (&wave        ));
  HXT_CHECK( hxtFormulationAddField  ( wave, domain));
  HXT_CHECK( hxtFormulationHelmholtz ( wave, fK2   ));

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation HABC                           -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *HABC[4];
  HXT_CHECK( hxtFormulationCreate(&HABC[0] ));
  HXT_CHECK( hxtFormulationCreate(&HABC[1] ));
  HXT_CHECK( hxtFormulationCreate(&HABC[2] ));
  HXT_CHECK( hxtFormulationCreate(&HABC[3] ));

  HXT_CHECK( hxtFormulationAddField  ( HABC[0], bnd[0] ));
  HXT_CHECK( hxtFormulationAddField  ( HABC[1], bnd[1] ));
  HXT_CHECK( hxtFormulationAddField  ( HABC[2], bnd[2] ));
  HXT_CHECK( hxtFormulationAddField  ( HABC[3], bnd[3] ));
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( HABC[0], aux[0][j] ));
    HXT_CHECK( hxtFormulationAddField( HABC[1], aux[1][j] ));
    HXT_CHECK( hxtFormulationAddField( HABC[2], aux[2][j] ));
    HXT_CHECK( hxtFormulationAddField( HABC[3], aux[3][j] ));
  }
  HXT_CHECK( hxtFormulationHABC( HABC[0], fK , theta ));
  HXT_CHECK( hxtFormulationHABC( HABC[1], fK , theta ));
  HXT_CHECK( hxtFormulationHABC( HABC[2], fK , theta ));
  HXT_CHECK( hxtFormulationHABC( HABC[3], fK , theta ));

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation Help                           -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *Help[4];
  HXT_CHECK( hxtFormulationCreate(&Help[0] ));
  HXT_CHECK( hxtFormulationCreate(&Help[1] ));
  HXT_CHECK( hxtFormulationCreate(&Help[2] ));
  HXT_CHECK( hxtFormulationCreate(&Help[3] ));

  HXT_CHECK( hxtFormulationAddField  ( Help[0], bnd[0] ));
  HXT_CHECK( hxtFormulationAddField  ( Help[1], bnd[1] ));
  HXT_CHECK( hxtFormulationAddField  ( Help[2], bnd[2] ));
  HXT_CHECK( hxtFormulationAddField  ( Help[3], bnd[3] ));
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( Help[0], aux[0][j] ));
    HXT_CHECK( hxtFormulationAddField( Help[1], aux[1][j] ));
    HXT_CHECK( hxtFormulationAddField( Help[2], aux[2][j] ));
    HXT_CHECK( hxtFormulationAddField( Help[3], aux[3][j] ));
  }
  HXT_CHECK( hxtFormulationHABCHelper( Help[0], fK2, theta));
  HXT_CHECK( hxtFormulationHABCHelper( Help[1], fK2, theta));
  HXT_CHECK( hxtFormulationHABCHelper( Help[2], fK2, theta));
  HXT_CHECK( hxtFormulationHABCHelper( Help[3], fK2, theta));

  // * --------------------------------------------------------------------- * //
  // * --                   Formulation Treatment                         -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *Trea[8];
  HXT_CHECK( hxtFormulationCreate(&Trea[0]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[1]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[2]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[3]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[4]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[5]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[6]) );
  HXT_CHECK( hxtFormulationCreate(&Trea[7]) );

  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( Trea[0], cnr[7][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[1], cnr[2][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[2], cnr[1][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[3], cnr[4][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[4], cnr[3][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[5], cnr[6][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[6], cnr[5][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[7], cnr[0][j]) );
  }
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( Trea[0], cnr[0][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[1], cnr[1][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[2], cnr[2][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[3], cnr[3][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[4], cnr[4][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[5], cnr[5][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[6], cnr[6][j]) );
    HXT_CHECK( hxtFormulationAddField( Trea[7], cnr[7][j]) );
  }
  HXT_CHECK( hxtFormulationHABCCorner( Trea[0], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[1], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[2], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[3], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[4], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[5], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[6], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCorner( Trea[7], fK, theta) );

  // * --------------------------------------------------------------------- * //
  // * --                      Formulation Update                         -- * //
  // * --------------------------------------------------------------------- * //
  HXTFormulation *EUpd[4];
  HXT_CHECK( hxtFormulationCreate(&EUpd[0] ));
  HXT_CHECK( hxtFormulationCreate(&EUpd[1] ));
  HXT_CHECK( hxtFormulationCreate(&EUpd[2] ));
  HXT_CHECK( hxtFormulationCreate(&EUpd[3] ));

  HXT_CHECK( hxtFormulationAddField  ( EUpd[0], bnd[0] ));
  HXT_CHECK( hxtFormulationAddField  ( EUpd[1], bnd[1] ));
  HXT_CHECK( hxtFormulationAddField  ( EUpd[2], bnd[2] ));
  HXT_CHECK( hxtFormulationAddField  ( EUpd[3], bnd[3] ));
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( EUpd[0], aux[0][j] ));
    HXT_CHECK( hxtFormulationAddField( EUpd[1], aux[1][j] ));
    HXT_CHECK( hxtFormulationAddField( EUpd[2], aux[2][j] ));
    HXT_CHECK( hxtFormulationAddField( EUpd[3], aux[3][j] ));
  }
  HXT_CHECK( hxtFormulationHABCUpd   ( EUpd[0], fK, theta) );
  HXT_CHECK( hxtFormulationHABCUpd   ( EUpd[1], fK, theta) );
  HXT_CHECK( hxtFormulationHABCUpd   ( EUpd[2], fK, theta) );
  HXT_CHECK( hxtFormulationHABCUpd   ( EUpd[3], fK, theta) );

  HXTFormulation *CUpd[8];
  HXT_CHECK( hxtFormulationCreate(&CUpd[0]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[1]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[2]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[3]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[4]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[5]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[6]) );
  HXT_CHECK( hxtFormulationCreate(&CUpd[7]) );

  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( CUpd[0], cnr[7][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[1], cnr[2][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[2], cnr[1][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[3], cnr[4][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[4], cnr[3][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[5], cnr[6][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[6], cnr[5][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[7], cnr[0][j]) );
  }
  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtFormulationAddField( CUpd[0], cnr[0][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[1], cnr[1][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[2], cnr[2][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[3], cnr[3][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[4], cnr[4][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[5], cnr[5][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[6], cnr[6][j]) );
    HXT_CHECK( hxtFormulationAddField( CUpd[7], cnr[7][j]) );
  }
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[0], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[1], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[2], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[3], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[4], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[5], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[6], fK, theta) );
  HXT_CHECK( hxtFormulationHABCCnrUpd( CUpd[7], fK, theta) );

  HXTSystem *sysUpd[4];
  HXT_CHECK( hxtSystemCreate (&sysUpd[0] ) );
  HXT_CHECK( hxtSystemCreate (&sysUpd[1] ) );
  HXT_CHECK( hxtSystemCreate (&sysUpd[2] ) );
  HXT_CHECK( hxtSystemCreate (&sysUpd[3] ) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[0], EUpd[0]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[0], CUpd[0]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[0], CUpd[1]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[1], EUpd[1]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[1], CUpd[2]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[1], CUpd[3]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[2], EUpd[2]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[2], CUpd[4]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[2], CUpd[5]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[3], EUpd[3]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[3], CUpd[6]) );
  HXT_CHECK( hxtSystemAddFML ( sysUpd[3], CUpd[7]) );
  HXT_CHECK( hxtSystemBuild  ( sysUpd[0], 1      ) );
  HXT_CHECK( hxtSystemBuild  ( sysUpd[1], 1      ) );
  HXT_CHECK( hxtSystemBuild  ( sysUpd[2], 1      ) );
  HXT_CHECK( hxtSystemBuild  ( sysUpd[3], 1      ) );
  HXT_CHECK( hxtSystemCOOMat ( sysUpd[0]         ) );
  HXT_CHECK( hxtSystemCOOMat ( sysUpd[1]         ) );
  HXT_CHECK( hxtSystemCOOMat ( sysUpd[2]         ) );
  HXT_CHECK( hxtSystemCOOMat ( sysUpd[3]         ) );
  HXT_CHECK( hxtSystemCSRMat ( sysUpd[0], 1      ) );
  HXT_CHECK( hxtSystemCSRMat ( sysUpd[1], 1      ) );
  HXT_CHECK( hxtSystemCSRMat ( sysUpd[2], 1      ) );
  HXT_CHECK( hxtSystemCSRMat ( sysUpd[3], 1      ) );

  HXT_CHECK( hxtSystemSolver ( sysUpd[0]              ) );
  HXT_CHECK( hxtSystemSolver ( sysUpd[1]              ) );
  HXT_CHECK( hxtSystemSolver ( sysUpd[2]              ) );
  HXT_CHECK( hxtSystemSolver ( sysUpd[3]              ) );

  HXT_CHECK( hxtSystemPhase  ( sysUpd[0],  0, nrhs, 13) );
  HXT_CHECK( hxtSystemPhase  ( sysUpd[1],  0, nrhs, 13) );
  HXT_CHECK( hxtSystemPhase  ( sysUpd[2],  0, nrhs, 13) );
  HXT_CHECK( hxtSystemPhase  ( sysUpd[3],  0, nrhs, 13) );

  HXT_CHECK( hxtSystemPhase  ( sysUpd[0], -1,   -1, -1) );
  HXT_CHECK( hxtSystemPhase  ( sysUpd[1], -1,   -1, -1) );
  HXT_CHECK( hxtSystemPhase  ( sysUpd[2], -1,   -1, -1) );
  HXT_CHECK( hxtSystemPhase  ( sysUpd[3], -1,   -1, -1) );

  // * --------------------------------------------------------------------- * //
  // * --                              System                             -- * //
  // * --------------------------------------------------------------------- * //
  HXTSystem *sys = NULL;
  HXT_CHECK( hxtSystemCreate (&sys         ) );
  HXT_CHECK( hxtSystemAddFML ( sys, wave   ) );
  HXT_CHECK( hxtSystemAddFML ( sys, HABC[0]) );
  HXT_CHECK( hxtSystemAddFML ( sys, HABC[1]) );
  HXT_CHECK( hxtSystemAddFML ( sys, HABC[2]) );
  HXT_CHECK( hxtSystemAddFML ( sys, HABC[3]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Help[0]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Help[1]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Help[2]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Help[3]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[0]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[1]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[2]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[3]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[4]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[5]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[6]) );
  HXT_CHECK( hxtSystemAddFML ( sys, Trea[7]) );
  HXT_CHECK( hxtSystemBuild  ( sys, 1      ) );
  HXT_CHECK( hxtSystemCOOMat ( sys         ) );
  HXT_CHECK( hxtSystemCSRMat ( sys, 1      ) );

  // * --------------------------------------------------------------------- * //
  // * --                  DIRICHLET BOUNDARY CONDITION                   -- * //
  // * --------------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemCSTR(sys, 0, source, fSource) ); 

  // * --------------------------------------------------------------------- * //
  // * --                          PARDISO SOLVER                         -- * //
  // * --------------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemSolver( sys              ) );
  HXT_CHECK( hxtSystemPhase ( sys, -1,   -1, 11) );
  HXT_CHECK( hxtSystemPhase ( sys, -1,   -1, 22) );
  HXT_CHECK( hxtSystemPhase ( sys,  0, nrhs, 33) );
  HXT_CHECK( hxtSystemPhase ( sys, -1,   -1, -1) );

  // * --------------------------------------------------------------------- * //
  // * --                          WRITE RESULTS                          -- * //
  // * --------------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemWriteMSH(sys, domain, "res.msh", 0, 0) );

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                                 DELETE                                * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  HXT_CHECK( hxtSystemDelete     (&sys      ) );
  HXT_CHECK( hxtSystemDelete     (&sysUpd[0]) );
  HXT_CHECK( hxtSystemDelete     (&sysUpd[1]) );
  HXT_CHECK( hxtSystemDelete     (&sysUpd[2]) );
  HXT_CHECK( hxtSystemDelete     (&sysUpd[3]) );

  HXT_CHECK( hxtFormulationDelete(&wave   ) );
  HXT_CHECK( hxtFormulationDelete(&HABC[0]) );
  HXT_CHECK( hxtFormulationDelete(&HABC[1]) );
  HXT_CHECK( hxtFormulationDelete(&HABC[2]) );
  HXT_CHECK( hxtFormulationDelete(&HABC[3]) );
  HXT_CHECK( hxtFormulationDelete(&Help[0]) );
  HXT_CHECK( hxtFormulationDelete(&Help[1]) );
  HXT_CHECK( hxtFormulationDelete(&Help[2]) );
  HXT_CHECK( hxtFormulationDelete(&Help[3]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[0]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[1]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[2]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[3]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[4]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[5]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[6]) );
  HXT_CHECK( hxtFormulationDelete(&Trea[7]) );

  HXT_CHECK( hxtFormulationDelete(&EUpd[0]) );
  HXT_CHECK( hxtFormulationDelete(&EUpd[1]) );
  HXT_CHECK( hxtFormulationDelete(&EUpd[2]) );
  HXT_CHECK( hxtFormulationDelete(&EUpd[3]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[0]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[1]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[2]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[3]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[4]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[5]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[6]) );
  HXT_CHECK( hxtFormulationDelete(&CUpd[7]) );

  HXT_CHECK( hxtGroupDelete(&domain ) );
  HXT_CHECK( hxtGroupDelete(&bnd[0] ) );
  HXT_CHECK( hxtGroupDelete(&bnd[1] ) );
  HXT_CHECK( hxtGroupDelete(&bnd[2] ) );
  HXT_CHECK( hxtGroupDelete(&bnd[3] ) );
  HXT_CHECK( hxtGroupDelete(&source ) );

  for(int j = 0; j < NPade; j++)
  {
    HXT_CHECK( hxtGroupDelete(&aux[0][j] ) );
    HXT_CHECK( hxtGroupDelete(&aux[1][j] ) );
    HXT_CHECK( hxtGroupDelete(&aux[2][j] ) );
    HXT_CHECK( hxtGroupDelete(&aux[3][j] ) );

    HXT_CHECK( hxtGroupDelete(&cnr[0][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[1][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[2][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[3][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[4][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[5][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[6][j] ) );
    HXT_CHECK( hxtGroupDelete(&cnr[7][j] ) );
  }

  // * ********************************************************************* * //
  // * ******                                                         ****** * //
  // *                             FINALIZE GMSH                             * //
  // * ******                                                         ****** * //
  // * ********************************************************************* * //
  gmshFinalize( &ierr );

  return 0;
}

static double fgetK(const double *xyz)
{
  const double Hz = OMEGA  / (2. * M_PI     );
  const double k  = 1./LSP * (2. * M_PI * Hz);

  return k;
}

static double_complex fSource(const double *xyz)
{
  const double    k = fgetK(xyz);
  const double    x = xyz[0];

  return -( cos(k * x) + I*sin(k * x) );
}

static double_complex fK     (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k;

  return re;
}

static double_complex fK2    (const double *xyz)
{
  const double    k = fgetK(xyz);
  double_complex re = k*k;

  return re;
}
