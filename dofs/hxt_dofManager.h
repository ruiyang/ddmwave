#ifndef _HXTREME_DOFMANAGER_H_
#define _HXTREME_DOFMANAGER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <complex.h>

#include "hxt_types.h"
#include "hxt_tools.h"
#include "hxt_dofs.h"
#include "hxt_sort_array.h"
#include "hxt_search_array.h"

/* *
  @struct HXTDofManager
  This struct manages degree of freedom

  Variables represent in the following
    
    * nTUGD    number of total unfixed global dofs
    * first    first entity of dofs
    * last     last  entity of dofs 
* */

typedef struct HXTDofManagerStruct
{
  bool generated;

  HXTDofs* globalDofs;
  HXTDofs* globalFixs;

  size_t nTUGD;
  int    first;
  int    last ;

  struct {
    size_t  num;
    int **state;
  } globalIdV;

} HXTDofManager;

HXTStatus hxtDofManagerCreate  (HXTDofManager** dofM);
HXTStatus hxtDofManagerDelete  (HXTDofManager** dofM);

// * DOFS FUNCTIONS * //
HXTStatus hxtDofManagerDofsAdd (HXTDofManager*  dofM, int *entity, int *type, size_t num);
HXTStatus hxtDofManagerDofsSort(HXTDofManager*  dofM);
HXTStatus hxtDofManagerDofsPrt (HXTDofManager*  dofM);

HXTStatus hxtDofManagerFixsAdd (HXTDofManager*  dofM, int *entity, int *type, size_t          num);
HXTStatus hxtDofManagerFixsSet (HXTDofManager*  dofM, int  entity, int  type, double_complex* val);
HXTStatus hxtDofManagerFixsGet (HXTDofManager*  dofM, int  entity, int  type, double_complex* val);
HXTStatus hxtDofManagerFixsSort(HXTDofManager*  dofM);
HXTStatus hxtDofManagerFixsPrt (HXTDofManager*  dofM);

// * GLOBALIDV FUNCTIONTS * //
HXTStatus hxtDofManagerGnrGlobalId(HXTDofManager* dofM, const bool reserve);
int       hxtDofManagerGetGlobalId(HXTDofManager* dofM, const int   entity, const int type);
HXTStatus hxtDofManagerPrtGlobalId(HXTDofManager* dofM);

#endif
