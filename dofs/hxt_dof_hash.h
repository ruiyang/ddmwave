#ifndef HEXTREME_DOF_HASH_H
#define HEXTREME_DOF_HASH_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "hxt_types.h"
#include "hxt_tools.h"
#include "uthash.h"

typedef struct
{
  int entity;
  int   type;

} dof_key_t;

typedef struct
{
  dof_key_t key;

  int              index;
  int              state;
  bool            valued;
  double_complex   value;

  UT_hash_handle  hh;

} HXTDofHash;

HXTStatus hxtDofHashCreate(HXTDofHash **hashs);
HXTStatus hxtDofHashBuild (HXTDofHash  *hashs);
HXTStatus hxtDofHashDelete(HXTDofHash **hashs);
HXTStatus hxtDofHashAdd   (HXTDofHash **hashs, 
		           const int   entity, 
			   const int     type, 
			   const int    index);

int       hxtDofHashFind  (const HXTDofHash  *hashs, 
		           const int         entity, 
			   const int           type);
int       hxtDofHashCounts(const HXTDofHash  *hashs);

#endif
