#include "hxt_dof_hash.h"

HXTStatus hxtDofHashCreate(HXTDofHash **hashs)
{
  (*hashs) = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtDofHashBuild (HXTDofHash  *hashs)
{
  ( hashs) = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtDofHashDelete(HXTDofHash **hashs)
{
  HXTDofHash *curr  =   NULL;
  HXTDofHash *tmp   =   NULL;

  HASH_ITER(hh, *hashs, curr, tmp) 
  {
    HASH_DEL(*hashs, curr);  // * delete; users advances to next
    free(curr);              // * optional- if you want to free 
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtDofHashAdd   (HXTDofHash **hashs,
		           const int   entity,
			   const int     type,
			   const int    index)
{
  HXTDofHash *obj = calloc( 1, sizeof(HXTDofHash));
  obj->key.entity = entity;
  obj->key.type   = type  ;
  obj->index      = index ;

  HASH_ADD (hh, *hashs, key, sizeof(dof_key_t), obj );

  return HXT_STATUS_OK;
}

int       hxtDofHashFind  (const HXTDofHash  *hashs,
		           const int         entity,
			   const int           type)
{
  HXTDofHash *sch = calloc( 1, sizeof(HXTDofHash));
  HXTDofHash *ptr = NULL;

  sch->key.entity = entity;
  sch->key.type   = type  ;

  HASH_FIND(hh,  hashs, &sch->key, sizeof(dof_key_t), ptr);

  free(sch);

  if (ptr) return ptr->index;
  else     return -1;
}

int       hxtDofHashCounts(const HXTDofHash  *hashs)
{
  int num_users = 0;

  num_users = HASH_COUNT( hashs);

  return num_users;
}

