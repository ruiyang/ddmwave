#ifndef HEXTREME_DOFS_H
#define HEXTREME_DOFS_H

#include <limits.h>
#include <complex.h>
#include <stdint.h>
#include <stdbool.h>
#include "hxt_types.h"
#include "hxt_tools.h"
#include "hxt_sort_array.h"

/* *
  @struct HXTDofs
  This struct represents some degree of freedom

  A Dof is defined by a pair of two integers called (entity, type).

  Variable state represents the state of this Dof
    
    * isFixed         this Dof is fixed
    * isUndef         this Dof is not defined
    * positive number this Dof is defined 
  
  @explication
  Object of HXTDofs contains a set of Dofs, for instance:

    HXTDofs dofs;
    dofs.num  = 8;
    dofs.comp = 1; // * there are 8*1 dofs

    dofs.entity = { 0,  1,  2,  3,  4,  5,  6,  7};
    dofs.type   = { 0,  0,  0,  0,  0,  0,  0,  0};
    dofs.state  = {-1, -1, -1, -1, -1, -1, -1, -1};

    dofs.valued = false; // * a dof has no value representing
    dofs.value  =  NULL;

    dofs.valued =  true; 
    dofs.comp   =     1; // * a dof represents scalar degree
    dofs.value  = {0., 0., 0., 0., 0., 0., 0., 0.};

    dofs.valued =  true; 
    dofs.comp   =     3; // * a dof represents vector degree
    dofs.value  = {
                   0., 0., 0., 
                   0., 0., 0., 
                   0., 0., 0.,
                   0., 0., 0.,
                   0., 0., 0.,
                   0., 0., 0.,
                   0., 0., 0.,
                   0., 0., 0.
                  };

  @application

    HXTDof* dofs;
    HXT_CHECK( hxtDofsCreate(&dofs              ) );
    HXT_CHECK( hxtDofsBuild ( dofs, num, 1, true) );

    dofs->entity[i] = entity;
    dofs->type  [i] =   type;

    HXT_CHECK( hxtDofsDelete(&dofs) );
* */

static const int isFixed = -2;
static const int isUndef = -1;

typedef struct HXTDofsStruct
{
  size_t  num;  // * number of dofs
  int *entity;  // * entity of dofs
  int *type  ;  // * type   of dofs
  int *state ;  // * state  of dofs

  bool            valued;  // * if there is vector of value
  double_complex*  value;  // * num * sizeof(double_complex) 

} HXTDofs;

HXTStatus hxtDofsCreate(HXTDofs **dofs);
HXTStatus hxtDofsDelete(HXTDofs **dofs);
HXTStatus hxtDofsBuild (HXTDofs  *dofs, size_t num, bool valued);
HXTStatus hxtDofsResize(HXTDofs  *dofs, size_t num);
HXTStatus hxtDofsSort  (HXTDofs  *dofs);

/* *
 @function  hxtDofsCreate
 @parametre HXTDofs** dofs

 Create a new Object HXTDofs
 **
 
 @function  hxtDofsDelete
 @parametre HXTDofs** dofs

 Delete this Object HXTDofs
 **

 @function  hxtDofsBuild
 @parametre HXTDofs* dofs
            size_t    num
	    bool   valued

 Initialize a new Object HXTDofs
 **
 
 @function  hxtDofsResize
 @parametre HXTDofs* dofs
            size_t    num

 Realloc this Object HXTDofs
 **
* */

#endif
