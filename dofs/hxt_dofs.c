#include "hxt_dofs.h"

// * -------------------------------------------------------------------- * //
// *                                 DOFS                                 * //
// * -------------------------------------------------------------------- * //
HXTStatus hxtDofsCreate(HXTDofs **dofs)
{
  HXT_CHECK( hxtMalloc(dofs, sizeof(HXTDofs)) );
  if((*dofs) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  (*dofs)->num    =     0;
  (*dofs)->valued = false;

  (*dofs)->entity =  NULL;
  (*dofs)->type   =  NULL;
  (*dofs)->state  =  NULL;

  (*dofs)->value  =  NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtDofsDelete(HXTDofs **dofs)
{
  if( (*dofs) != NULL)
  {
    HXTDofs *ptr = *dofs;

    HXT_CHECK( hxtFree(&(ptr->type  ) ) );
    HXT_CHECK( hxtFree(&(ptr->entity) ) );
    HXT_CHECK( hxtFree(&(ptr->state ) ) );
    HXT_CHECK( hxtFree(&(ptr->value ) ) );

    ptr->num    =     0;
    ptr->valued = false;

    ptr->entity =  NULL;
    ptr->type   =  NULL;
    ptr->state  =  NULL;

    ptr->value  =  NULL;

    HXT_CHECK( hxtFree(dofs) );
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtDofsBuild (HXTDofs  *dofs, size_t num, bool valued)
{
  dofs->entity = NULL;
  dofs->type   = NULL;
  dofs->state  = NULL;
  dofs->value  = NULL;

  dofs->valued = valued;
  dofs->num    =    num;
  if(num)
  {
    HXT_CHECK( hxtMalloc(&dofs->entity, num*sizeof(int)) );
    HXT_CHECK( hxtMalloc(&dofs->type  , num*sizeof(int)) );
    HXT_CHECK( hxtMalloc(&dofs->state , num*sizeof(int)) );
    memset(dofs->state, isUndef, num*sizeof(int));

    if(valued)
    {
      HXT_CHECK( hxtMalloc(&dofs->value,    num*sizeof(double_complex)) );
                    memset( dofs->value, 0, num*sizeof(double_complex))  ;
    }
    else dofs->value = NULL;
  }
  else{
    dofs->entity = NULL;
    dofs->type   = NULL;
    dofs->state  = NULL;
    dofs->value  = NULL;
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtDofsResize(HXTDofs  *dofs, size_t num)
{
  dofs->num = num;
  if(num){
    HXT_CHECK( hxtRealloc(&dofs->entity, num*sizeof( int            )) );
    HXT_CHECK( hxtRealloc(&dofs->type  , num*sizeof( int            )) );
    HXT_CHECK( hxtRealloc(&dofs->state , num*sizeof( int            )) );
    if(dofs->valued)
    HXT_CHECK( hxtRealloc(&dofs->value , num*sizeof( double_complex )) );
  }
  else{
    HXT_CHECK( hxtFree(&(dofs->entity) ) );
    HXT_CHECK( hxtFree(&(dofs->type  ) ) );
    HXT_CHECK( hxtFree(&(dofs->state ) ) );
    HXT_CHECK( hxtFree(&(dofs->value ) ) );
    dofs->type   = NULL;
    dofs->entity = NULL;
    dofs->state  = NULL;
    dofs->value  = NULL;
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtDofsSort  (HXTDofs  *dofs)
{
  if(dofs->num == 0 ) return HXT_STATUS_OK;
  size_t   num  = dofs->num;
  
  hxtSortArrayHierarchy(int, dofs->entity, 
                        int, dofs->type  ,
                        int, dofs->state , 1, num, INT_MAX);

  int   *c_entity =       malloc(num*sizeof(int));
  int   *c_type   =       malloc(num*sizeof(int));
  int   *c_state  =       malloc(num*sizeof(int));
  memcpy(c_entity, dofs->entity, num*sizeof(int));
  memcpy(c_type  , dofs->type  , num*sizeof(int));
  memcpy(c_state , dofs->state , num*sizeof(int));

  size_t size = 1;

  for(size_t i = 1; i < num; i++)
  {
    if( !(c_entity[i] == c_entity[i-1] && 
	  c_type  [i] == c_type  [i-1])  )
    {
      dofs->entity[size] = c_entity[i];
      dofs->type  [size] = c_type  [i];
      dofs->state [size] = c_state [i];
      size++;
    }
  }
  HXT_CHECK( hxtDofsResize(dofs, size) );

  free(c_entity);
  free(c_type  );
  free(c_state );

  return HXT_STATUS_OK;
}
