#include "hxt_dofManager.h"

HXTStatus hxtDofManagerCreate(HXTDofManager** dofM)
{
  HXT_CHECK( hxtMalloc(dofM, sizeof(HXTDofManager)) );
  if(*dofM == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  (*dofM)->generated  = false;

  size_t num = 0;

  (*dofM)->globalDofs = NULL;
  (*dofM)->globalFixs = NULL;
  HXT_CHECK( hxtDofsCreate(&(*dofM)->globalDofs) );
  HXT_CHECK( hxtDofsCreate(&(*dofM)->globalFixs) );
  HXT_CHECK( hxtDofsBuild ( (*dofM)->globalDofs, num, false) );
  HXT_CHECK( hxtDofsBuild ( (*dofM)->globalFixs, num, true ) );

  (*dofM)->nTUGD = 0;
  (*dofM)->first = 0;
  (*dofM)->last  = 0;

  (*dofM)->globalIdV.state = NULL;
  (*dofM)->globalIdV.num   =    0;

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerDelete(HXTDofManager** dofM)
{
  HXT_CHECK( hxtDofsDelete(&(*dofM)->globalDofs) );
  HXT_CHECK( hxtDofsDelete(&(*dofM)->globalFixs) );

  for(size_t i = 0; i < (*dofM)->globalIdV.num; i++)
    HXT_CHECK( hxtFree(&(*dofM)->globalIdV.state[i]) );

  HXT_CHECK( hxtFree(&(*dofM)->globalIdV.state) );

  HXT_CHECK( hxtFree(dofM) );

  return HXT_STATUS_OK;
}

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                                   DOFS                                  * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
HXTStatus hxtDofManagerDofsAdd(HXTDofManager* dofM, int* entity, int* type, size_t num)
{
  size_t oldSize = (dofM->globalDofs)->num;
  size_t newSize = (dofM->globalDofs)->num + num;

  HXT_CHECK( hxtDofsResize(dofM->globalDofs, newSize) );

  for(size_t i = oldSize; i < newSize; i++)
  {
    (dofM->globalDofs)->entity[i] = entity[i-oldSize];
    (dofM->globalDofs)->type  [i] = type  [i-oldSize];
    (dofM->globalDofs)->state [i] = isUndef;
    if(entity[i-oldSize] > dofM->last)
    { 
      dofM->last = entity[i-oldSize];
    }
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerDofsSort (HXTDofManager* dofM)
{
  if( !(dofM->globalDofs)->num ) return HXT_STATUS_OK;

  HXTDofs* dofs =  dofM->globalDofs;
  size_t   num  = (dofM->globalDofs)->num;

  hxtSortArrayHierarchy(int, dofs->entity, 
                        int, dofs->type  ,
                        int, dofs->state , 1, num, dofM->last);

  int* c_entity =         malloc(num*sizeof(int));
  int* c_type   =         malloc(num*sizeof(int));
  int* c_state  =         malloc(num*sizeof(int));
  memcpy(c_entity, dofs->entity, num*sizeof(int));
  memcpy(c_type  , dofs->type  , num*sizeof(int));
  memcpy(c_state , dofs->state , num*sizeof(int));

  size_t size = 1;

  for(size_t i = 1; i < num; i++)
  {
    if( !(c_entity[i] == c_entity[i-1] && 
	  c_type  [i] == c_type  [i-1])  )
    {
      dofs->entity[size] = c_entity[i];
      dofs->type  [size] = c_type  [i];
      dofs->state [size] = c_state [i];
      size++;
    }
  }
  HXT_CHECK( hxtDofsResize(dofs, size) );

  free(c_entity);
  free(c_type  );
  free(c_state );

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerDofsPrt(HXTDofManager* dofM)
{
  printf("  /* ----------------------------------------------- */\n");

  if(dofM->generated)
  { 
    printf("  dofM has been generated\n");
    printf("  number of total unfixed global dofs == %lu\n\n", dofM->nTUGD);
  }
  else 
    printf("  dofM hasn't been generated\n\n");

  if(dofM->globalDofs == NULL) return HXT_INFO("globalDofs has been deleted!\n");

  size_t i;
  HXTDofs* dofs = dofM->globalDofs;

  for(i = 0; i < dofs->num; i++) 
  {
    printf("  (%u, %u): ", dofs->entity[i], dofs->type[i]);
    printf("%d -- Global ID\t", dofs->state[i]);

    if(dofs->valued == false)
    {
      printf(" -- Without Value\n");
    }
    else
    {
      //if     (dofs->comp == 1)
      //{
        printf("(%f, %f) -- Value\n", creal(dofs->value[i]),
                                      cimag(dofs->value[i]));
      //}
      //else if(dofs->comp == 2)
      //{
      //  printf("(%f, %f), (%f, %f) -- Value\n", 
      //                         creal(dofs->value[2*i+0]),
      //                         cimag(dofs->value[2*i+0]),
      //                         creal(dofs->value[2*i+1]),
      //                         cimag(dofs->value[2*i+1]));
      //}
      //else if(dofs->comp == 3)
      //{
      //  printf("(%f, %f), (%f, %f), (%f, %f) -- Value\n", 
      //                         creal(dofs->value[3*i+0]),
      //                         cimag(dofs->value[3*i+0]),
      //                         creal(dofs->value[3*i+1]),
      //                         cimag(dofs->value[3*i+1]),
      //                         creal(dofs->value[3*i+2]),
      //                         cimag(dofs->value[3*i+2]));
      //}
    }
  }
  printf("  /* ----------------------------------------------- */\n");

  return HXT_STATUS_OK;
}

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                                   FIXS                                  * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
HXTStatus hxtDofManagerFixsAdd(HXTDofManager* dofM, int* entity, int* type, size_t num)
{
  size_t i;

  size_t oldSize = (dofM->globalFixs)->num;
  size_t newSize = (dofM->globalFixs)->num + num;

  HXT_CHECK( hxtDofsResize(dofM->globalFixs, newSize) );

  for(i = oldSize; i < newSize; i++)
  {
    (dofM->globalFixs)->entity[i] = entity[i-oldSize];
    (dofM->globalFixs)->type  [i] = type  [i-oldSize];
  //(dofM->globalFixs)->state [i] = isFixed;
    (dofM->globalFixs)->state [i] = -2;
  }

  // * ADD FIXED POINTS INTO dofM->globalDofs
  if( !(dofM->globalDofs)->num ) return HXT_STATUS_OK;

  HXTDofs* GDofs = dofM->globalDofs;
  int  key0, key1;
  ptrdiff_t index;
  for(i = 0; i < num; i++)
  {
     key0  = entity[i];
     key1  = type  [i];
     index = hxtSearchArrayPair(int, &key0, GDofs->entity, sizeof(int), 
                                int, &key1, GDofs->type  , sizeof(int), GDofs->num);
     if(index != -1) GDofs->state[index] = isFixed; 
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerFixsSort (HXTDofManager* dofM)
{
  if( !(dofM->globalFixs)->num ) return HXT_STATUS_OK;

  HXTDofs* dofs =  dofM->globalFixs;
  size_t   num  = (dofM->globalFixs)->num;

  hxtSortArrayHierarchy(int           , dofs->entity, 
                        int           , dofs->type  ,
                        double_complex, dofs->value , 1, num, dofM->last);

  int             *c_entity = malloc(num*sizeof(int           ));
  int             *c_type   = malloc(num*sizeof(int           ));
  double_complex  *c_value  = malloc(num*sizeof(double_complex));
  memcpy(c_entity, dofs->entity,     num*sizeof(int           ));
  memcpy(c_type  , dofs->type  ,     num*sizeof(int           ));
  memcpy(c_value , dofs->value ,     num*sizeof(double_complex));

  size_t size = 1;

  for(size_t i = 1; i < num; i++)
  {
    if( !(c_entity[i] == c_entity[i-1] &&
          c_type  [i] == c_type  [i-1])  )
    {
      dofs->entity[size] = c_entity[i];
      dofs->type  [size] = c_type  [i];
      dofs->value [size] = c_value [i];
      size++;
    }
  }
  HXT_CHECK( hxtDofsResize(dofs, size) );

  free(c_entity);
  free(c_type  );
  free(c_value );

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerFixsPrt(HXTDofManager* dofM)
{
  printf("  /* ----------------------------------------------- */\n");

  if(dofM->generated)
  { 
    printf("  dofM has been generated\n");
    printf("  number of total unfixed global dofs == %lu\n\n", dofM->nTUGD);
  }
  else 
    printf("  dofM hasn't been generated\n\n");

  if( (dofM->globalFixs->num) == 0)
    return HXT_INFO("globalFixs is empty.\n");

  size_t i;
  HXTDofs* dofs = dofM->globalFixs;

  for(i = 0; i < dofs->num; i++) 
  {
    printf("  (%u, %u): ", dofs->entity[i], dofs->type[i]);
    printf("%d -- Global ID\t", dofs->state[i]);

    if(dofs->valued == false)
    {
      printf(" -- Without Value\n");
    }
    else
    {
      //if(dofs->comp == 1)
      //{
        printf("(%f, %f) -- Value\n", creal(dofs->value[i]),
                                      cimag(dofs->value[i]));
      //}
      //else if(dofs->comp == 2)
      //{
      //  printf("(%f, %f), (%f, %f) -- Value\n", 
      //                         creal(dofs->value[2*i+0]),
      //                         cimag(dofs->value[2*i+0]),
      //                         creal(dofs->value[2*i+1]),
      //                         cimag(dofs->value[2*i+1]));
      //}
      //else if(dofs->comp == 3)
      //{
      //  printf("(%f, %f), (%f, %f), (%f, %f) -- Value\n", 
      //                         creal(dofs->value[3*i+0]),
      //                         cimag(dofs->value[3*i+0]),
      //                         creal(dofs->value[3*i+1]),
      //                         cimag(dofs->value[3*i+1]),
      //                         creal(dofs->value[3*i+2]),
      //                         cimag(dofs->value[3*i+2]));
      //}
    }
  }
  printf("  /* ----------------------------------------------- */\n");

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerFixsGet(HXTDofManager* dofM, int entity, int type, double_complex *val)
{
  // * ADD FIXED POINTS INTO dofM->globalDofs
  //if( !(dofM->globalFixs)->num ) return HXT_STATUS_OK;

  HXTDofs* FDofs = dofM->globalFixs;
  ptrdiff_t index;
  index = hxtSearchArrayPair(int, &entity, FDofs->entity, sizeof(int),
                             int, &type  , FDofs->type  , sizeof(int), FDofs->num);

  if(index != -1) val[0] = FDofs->value[index]; 
  else return HXT_ERROR_MSG(HXT_STATUS_ERROR, "Dof(%u, %u) not fixed\n", entity, type);

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerFixsSet(HXTDofManager* dofM, int entity, int type, double_complex* val)
{
  HXTDofs* FDofs = dofM->globalFixs;
  ptrdiff_t index;
  index = hxtSearchArrayPair(int, &entity, FDofs->entity, sizeof(int),
                             int, &type  , FDofs->type  , sizeof(int), FDofs->num);

  if(index != -1) FDofs->value[index] = val[0]; 
  else return HXT_ERROR_MSG(HXT_STATUS_ERROR, "Dof(%u, %u) not fixed\n", entity, type);

  return HXT_STATUS_OK;
}

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                                GLOBALIDV                                * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
static inline HXTStatus count(HXTDofManager* dofM)
{
  HXTDofs* globalDofs = dofM->globalDofs;

  int id = 0;

  for(size_t i = 0; i < globalDofs->num; i++) 
  {
    if(globalDofs->state[i] != isFixed)
    {
       globalDofs->state[i] = id;
       id++;
    }
  }
  dofM->nTUGD = id;

  return HXT_STATUS_OK;
}

static inline HXTStatus vectorize(HXTDofManager* dofM)
{
  size_t i;

  HXTDofs* globalDofs = dofM->globalDofs;
  dofM->first = globalDofs->entity[0];

  size_t first = dofM->first;
  size_t last  = dofM->last;

  dofM->globalIdV.num = last - first + 1;
  size_t sizeV        = last - first + 1;

  // * alloc
  HXT_CHECK( hxtMalloc(&(dofM->globalIdV.state), sizeV * sizeof(int*)) );
  if(dofM->globalIdV.state == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  // * count Terms and get max type
  int* countTerms = NULL;
  int* maxType    = NULL;
  HXT_CHECK( hxtCalloc(&countTerms, sizeV + first, sizeof(int)) );
  HXT_CHECK( hxtCalloc(&maxType   , sizeV + first, sizeof(int)) );

  for(i = 0; i < globalDofs->num; i++) 
  {
    countTerms[globalDofs->entity[i]]++;
    if(        globalDofs->type  [i]  > maxType[globalDofs->entity[i]] )
      maxType [globalDofs->entity[i]] =         globalDofs->type  [i];
  }

  // * alloc globalIdV
  for(i = first; i < sizeV + first; i++)
  {
    dofM->globalIdV.state[i-first] = NULL;
  //dofM->globalIdV[i-first].size  =    0;
    if(countTerms[i])
    {
      HXT_CHECK( hxtMalloc(&(dofM->globalIdV.state[i-first]), (maxType[i]+1)*sizeof(int)) );
    //dofM->globalIdV[i - first].size = maxType[i] + 1;
      memset(dofM->globalIdV.state[i-first], -1, (maxType[i]+1)*sizeof(int));
    }
    else
    {
      HXT_CHECK( hxtMalloc(&(dofM->globalIdV.state[i-first]), (           1)*sizeof(int)) );
      memset(dofM->globalIdV.state[i-first], -1, (           1)*sizeof(int));
    }
  }

  for(i = 0; i < globalDofs->num; i++) 
  {
    dofM->globalIdV.state[globalDofs->entity[i]-first]
                         [globalDofs->type[i]        ] = globalDofs->state[i];
  }

  HXT_CHECK( hxtFree(&countTerms) );
  HXT_CHECK( hxtFree(&maxType   ) );

  return HXT_STATUS_OK;
}

HXTStatus hxtDofManagerGnrGlobalId(HXTDofManager* dofM, bool reserve)
{
  if( (dofM->globalDofs)->num == 0) return HXT_STATUS_OK;
    //return HXT_ERROR_MSG(HXT_STATUS_ERROR, 
    //  "Object globalDofs is empty!\n");

  HXT_CHECK(     count(dofM) );
  HXT_CHECK( vectorize(dofM) );
  if(reserve == false) HXT_CHECK( hxtDofsDelete(&dofM->globalDofs) );

  dofM->generated = true;

  return HXT_STATUS_OK;
}

int hxtDofManagerGetGlobalId(HXTDofManager* dofM, 
                                const int entity, 
                                const int type  )
{
  if(entity < dofM->first) return -1;
  if(entity > dofM-> last) return -1;

  return dofM->globalIdV.state[entity - dofM->first][type];
}

HXTStatus hxtDofManagerPrtGlobalId(HXTDofManager* dofM)
{
  if(dofM->globalDofs == NULL)
    return HXT_INFO("globalDofs has been deleted!\n");

  printf("  /* ----------------------------------------------- */\n");

  if(dofM->generated)
  { 
    printf("  dofM has been generated\n");
    printf("  number of total unfixed global dofs == %lu\n\n", dofM->nTUGD);
  }
  else printf("  dofM hasn't been generated\n\n");

  size_t i;
  for(i = 0; i < (dofM->globalDofs)->num; i++)
  {
    int entity = (dofM->globalDofs)->entity[i];
    int type   = (dofM->globalDofs)->type  [i];

    int globalId;
    globalId = hxtDofManagerGetGlobalId(dofM, entity, type); 

    if     (globalId == isUndef) printf("  dof(%u, %u) is not defined   \n", entity, type);
    else if(globalId == isFixed) printf("  dof(%u, %u) is fixed point   \n", entity, type);
    else                         printf("  globalId of dof(%u, %u) is %u\n", entity, type, globalId);
  }
  printf("  /* ----------------------------------------------- */\n");

  return HXT_STATUS_OK;
}
