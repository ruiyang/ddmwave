#include "hxt_system.h"
  
static int coo_to_crs(int *ia , int    *ja , double_complex   *a,
                      int *row, int    *col, double_complex *coo,
                      int  nnz, int  rowMax, const int      base);

HXTStatus hxtSystemCreate(HXTSystem **sys)
{
  HXT_CHECK( hxtMalloc(sys, sizeof(HXTSystem)) );
  if((*sys) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTSystem *ptr = *sys;

  ptr->base = 0;

  size_t i;

  for(i = 0; i < fmls_m; i++) ptr->formulations[i] = NULL;
  ptr->formulations_n = 0;

  ptr->nUnknowns = 0;
  ptr->nrhs      = 0;
  ptr->nnz       = 0;

  ptr->a    = NULL;
  ptr->irn  = NULL;
  ptr->jcn  = NULL;
  ptr->rhs  = NULL;
  ptr->x    = NULL;

  ptr->lineptr  = NULL;
  ptr->colindex = NULL;
  ptr->value    = NULL;

//ptr->dofM   =  NULL;
//HXT_CHECK( hxtDofManagerCreate(&ptr->dofM) );
  ptr->hashs  =  NULL;
  HXT_CHECK( hxtDofHashCreate  (&ptr->hashs) );
  HXT_CHECK( hxtDofHashBuild   ( ptr->hashs) );

  ptr->solver = false;
  ptr->ls     =  NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemDelete(HXTSystem **sys)
{
  HXTSystem *ptr = *sys;

  ptr->base = 0;

  size_t i;

  for(i = 0; i < fmls_m; i++) ptr->formulations[i] = NULL;
  ptr->formulations_n = 0;

  ptr->nUnknowns = 0;
  ptr->nrhs      = 0;
  ptr->nnz       = 0;

  HXT_CHECK( hxtFree(&ptr->a  ) );
  HXT_CHECK( hxtFree(&ptr->irn) );
  HXT_CHECK( hxtFree(&ptr->jcn) );
  HXT_CHECK( hxtFree(&ptr->rhs) );
  HXT_CHECK( hxtFree(&ptr->x  ) );
  ptr->a    = NULL;
  ptr->irn  = NULL;
  ptr->jcn  = NULL;
  ptr->rhs  = NULL;
  ptr->x    = NULL;

  HXT_CHECK( hxtFree(&ptr->lineptr ) );
  HXT_CHECK( hxtFree(&ptr->colindex) );
  HXT_CHECK( hxtFree(&ptr->value   ) );
  ptr->lineptr  = NULL;
  ptr->colindex = NULL;
  ptr->value    = NULL;

//HXT_CHECK( hxtDofManagerDelete(&ptr->dofM ) );
  HXT_CHECK( hxtDofHashDelete   (&ptr->hashs) );

  linear_solver_delete(&ptr->ls);
  ptr->solver = false;

  HXT_CHECK( hxtFree(sys) );

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemBuild(HXTSystem  *sys, const int  nrhs)
{
  sys->nrhs = nrhs;

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemAddFML(HXTSystem *sys, HXTFormulation *fml)
{
  if(fml == NULL) return HXT_ERROR(HXT_STATUS_POINTER_ERROR);

  const size_t      NF  = sys->formulations_n++;
  sys->formulations[NF] = fml;

  if(sys->formulations_n > fmls_m)
      return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemCOOMat(HXTSystem *sys)
{
  size_t i, j, e, k, im, ie, it, et;

  const size_t numFormulations = sys->formulations_n;
  // * ----------------------------------------------------------------------- * //
  // * ------                                                           ------ * //
  // * --                               FIND nnz                            -- * //
  // * ------                                                           ------ * //
  // * ----------------------------------------------------------------------- * //
  int nnz = 0;
  for(im = 0; im < numFormulations; im++)
  {
    const HXTFormulation   *formulation = sys->formulations[im];
    const size_t ATerms_n = formulation->ATerms_n;
    for(it = 0; it < ATerms_n; it++)
    {
      const HXTTerm  * term = formulation->ATerms[it];
      const HXTGroup *group = term->field;

      const size_t entities_n = group->entities_n;
      for(ie = 0; ie < entities_n; ie++)
      {
        const HXTEntity *ett = group->entities[ie];

        const size_t   elementTypes_n = ett->elements.types_n   ;
        const size_t  *elementTags_n  = ett->elements.tags_n    ;
        const size_t  *nodeTags_n     = ett->elements.nodeTags_n;

        const size_t   numElementTypes = elementTypes_n;
        for(j = 0; j < numElementTypes; j++)
	{
          const size_t nE =                 elementTags_n[j];
          const size_t nN = nodeTags_n[j] / elementTags_n[j];

          nnz += nE * nN*nN;
	}
      }
    }
  }
  sys->nnz = nnz;

  // * ----------------------------------------------------------------------- * //
  // * ------                                                           ------ * //
  // * --                            Create DOFs                            -- * //
  // * ------                                                           ------ * //
  // * ----------------------------------------------------------------------- * //
  HXTDofs* dofs = NULL;
  HXT_CHECK( hxtDofsCreate(&dofs          ) );
  HXT_CHECK( hxtDofsBuild ( dofs, 0, false) );

  for(im = 0; im < numFormulations; im++)
  {
    const HXTFormulation   *formulation = sys->formulations[im];
    const size_t ATerms_n = formulation->ATerms_n;

    for(it = 0; it < ATerms_n; it++)
    {
      const HXTTerm  * term = formulation->ATerms[it];

      const HXTGroup *field =  term-> field;
      const size_t offset_f = field->offset;
      for(ie = 0; ie < field->entities_n; ie++)
      {
        const HXTEntity *ett = field->entities[ie];

	const size_t  nodeTags_n = ett->nodes.tags_n;
        const size_t *nodeTags 	 = ett->nodes.tags  ;

	const size_t origin = dofs->num;
        HXT_CHECK( hxtDofsResize( dofs, origin+nodeTags_n) );

        for(i = 0; i < nodeTags_n; i++)
        {
          dofs->entity[i+origin] = nodeTags[i];
          dofs->type  [i+origin] = offset_f;
        }
      }

      const HXTGroup  *test =  term->  test;
      const size_t offset_t =  test->offset;
      for(ie = 0; ie <  test->entities_n; ie++)
      {
        const HXTEntity *ett =  test->entities[ie];

	const size_t  nodeTags_n = ett->nodes.tags_n;
        const size_t *nodeTags 	 = ett->nodes.tags  ;

	const size_t origin = dofs->num;
        HXT_CHECK( hxtDofsResize( dofs, origin+nodeTags_n) );

        for(i = 0; i < nodeTags_n; i++)
        {
          dofs->entity[i+origin] = nodeTags[i];
          dofs->type  [i+origin] = offset_t;
        }
      }
    }
  }
//HXT_CHECK( hxtDofManagerDofsAdd    (sys->dofM, dofs->entity, dofs->type, dofs->num) );
//HXT_CHECK( hxtDofManagerDofsSort   (sys->dofM      ) );
//HXT_CHECK( hxtDofManagerGnrGlobalId(sys->dofM, true) );

  HXT_CHECK( hxtDofsSort  ( dofs) );
  for(int index = 0; index < dofs->num; index++)
  {
    const int entity = dofs->entity[index];
    const int type   = dofs->type  [index];
    HXT_CHECK( hxtDofHashAdd(&sys->hashs, entity, type, index) );
  }
  HXT_CHECK( hxtDofsDelete(&dofs) );

  // * ----------------------------------------------------------------------- * //
  // * --                              nUnknowns                            -- * //
  // * ----------------------------------------------------------------------- * //
//sys->nUnknowns  = (sys->dofM)->nTUGD;
//const int nUnknowns = sys->nUnknowns;
  sys->nUnknowns      = hxtDofHashCounts(sys->hashs);
  const int nUnknowns = sys->nUnknowns;

  // * ----------------------------------------------------------------------- * //
  // * ------                                                           ------ * //
  // * --                          FORM COO MATRIX                          -- * //
  // * ------                                                           ------ * //
  // * ----------------------------------------------------------------------- * //
  const int nrhs = sys->nrhs;
  HXT_CHECK( hxtMalloc(&sys->a  , (nnz            )*sizeof(double_complex)) );
  HXT_CHECK( hxtMalloc(&sys->irn, (nnz            )*sizeof(int           )) );
  HXT_CHECK( hxtMalloc(&sys->jcn, (nnz            )*sizeof(int           )) );
  HXT_CHECK( hxtCalloc(&sys->rhs, (nUnknowns *nrhs),sizeof(double_complex)) );
  HXT_CHECK( hxtCalloc(&sys->x  , (nUnknowns *nrhs),sizeof(double_complex)) );

  nnz = 0;
  for(im = 0; im < numFormulations; im++)
  {
    const HXTFormulation   *formulation = sys->formulations[im];
    const size_t ATerms_n = formulation->ATerms_n;
    for(it = 0; it < ATerms_n; it++)
    {
      const HXTTerm  * term = formulation->ATerms[it];
      const HXTGroup *group = term->field;

      const int offset_f = (term->field)->offset;
      const int offset_t = (term-> test)->offset;

      const size_t entities_n = group->entities_n;
      for(ie = 0; ie < entities_n; ie++)
      {
        const HXTEntity *ett = group->entities[ie];

        const size_t   elementTypes_n = (const size_t  ) ett->elements.types_n   ;
        const size_t  *elementTags_n  = (const size_t* ) ett->elements.tags_n    ;
        const size_t  *nodeTags_n     = (const size_t* ) ett->elements.nodeTags_n;
        const size_t **nodeTags       = (const size_t**) ett->elements.nodeTags  ;
        for(et = 0; et < elementTypes_n; et++)
	{
          const size_t nE =                  elementTags_n[et];
          const size_t nN = nodeTags_n[et] / elementTags_n[et];
	  const double_complex *AM = term-> Mats[ie][et];
	  memcpy(&sys->a[nnz], &AM[0], nE*nN*nN*sizeof(double_complex));

          for(e = 0; e < nE; e++)
          {
	    const size_t *eNodeTags = &nodeTags[et][e*nN];
	    for(j = 0; j < nN; j++)
            {
              for(k = 0; k < nN; k++)
              {
              //int row = hxtDofManagerGetGlobalId(sys->dofM, eNodeTags[j], offset_t);
              //int col = hxtDofManagerGetGlobalId(sys->dofM, eNodeTags[k], offset_f);
                int row = hxtDofHashFind(sys->hashs, eNodeTags[j], offset_t);
                int col = hxtDofHashFind(sys->hashs, eNodeTags[k], offset_f);
                sys->irn[nnz + e*nN*nN + j*nN +k] = row;
                sys->jcn[nnz + e*nN*nN + j*nN +k] = col;
    	      }
  	    }
	  }
          nnz += nE * nN*nN; 
	}
      }
    }
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemCSRMat(HXTSystem *sys, const int base)
{
  if(sys->nUnknowns == 0) return HXT_STATUS_OK;

  sys->base = base;

  const int nUnknowns = sys->nUnknowns;

  HXT_CHECK( hxtCalloc(&sys->lineptr , (nUnknowns+1),sizeof(int           )) );
  HXT_CHECK( hxtMalloc(&sys->colindex, (sys->nnz   )*sizeof(int           )) );
  HXT_CHECK( hxtMalloc(&sys->value   , (sys->nnz   )*sizeof(double_complex)) );
  if((sys->lineptr) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  sys->nnz = coo_to_crs(sys->lineptr, sys->colindex , sys->value,
                        sys->irn    , sys->jcn      , sys->a    ,
                        sys->nnz    , sys->nUnknowns, 0         );

  HXT_CHECK( hxtRealloc(&sys->colindex, sys->nnz*sizeof(int           )) );
  HXT_CHECK( hxtRealloc(&sys->value   , sys->nnz*sizeof(double_complex)) );

  int i;
  for(i = 0; i < nUnknowns+1; i++) sys->lineptr [i] += base;
  for(i = 0; i < sys->nnz   ; i++) sys->colindex[i] += base;

  HXT_CHECK( hxtFree(&sys->a  ) );
  HXT_CHECK( hxtFree(&sys->irn) );
  HXT_CHECK( hxtFree(&sys->jcn) );
  sys->a    = NULL;
  sys->irn  = NULL;
  sys->jcn  = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemRHSZero  (HXTSystem  *sys, const int irhs)
{
  const int nUnknowns =  sys->nUnknowns;
  double_complex *rhs = &sys->rhs[irhs * nUnknowns ];

  memset( rhs, 0, nUnknowns*sizeof(double_complex) );

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemSolZero(HXTSystem  *sys, const int irhs)
{
  const int nUnknowns =  sys->nUnknowns;
  double_complex *x   = &sys->x[irhs * nUnknowns ];

  memset( x, 0, nUnknowns*sizeof(double_complex) );

  return HXT_STATUS_OK;
}

static int coo_to_crs(int *ia , int    *ja , double_complex   *a,
                      int *row, int    *col, double_complex *coo,
                      int  nnz, int  rowMax, const int      base)
{
  int *lineptr        = ia;
  int *colindex       = ja;
  double_complex *crs =  a;

  // * SORT VALUES
  parallel_rsort(row, col, coo, nnz, rowMax);

  // * TRANSFORMER COO EN CRS
  int count_nnz = 0;
  int i0        = 0;
  lineptr[0]    = 0 + base;

  // * THE FIRST LOOP
  crs     [0] = coo[0];
  colindex[0] = col[0];
  int ik  = row[0] - base;
  if( ik != i0)
  {
    lineptr[ik] =  0 + base;
    i0          = ik;
  }
  count_nnz++;

  int k;
  for(k = 1; k < nnz; k++)
  {
    if( row[k-1] == row[k] &&
        col[k-1] == col[k] )
    {
      crs[count_nnz-1] += coo[k];
    }
    else
    {
      crs     [count_nnz] = coo[k];
      colindex[count_nnz] = col[k];
      int i  = row[k] - base;
      if( i != i0)
      {
        lineptr[i] = count_nnz + base;
        i0 = i;
      }
      count_nnz++;
    }
  }
  lineptr[rowMax] = count_nnz + base;

  return count_nnz;
}

HXTStatus hxtSystemSolver(HXTSystem *sys)
{
  linear_solver_create(&sys->ls);
  sys->solver = false;

  if(sys->nUnknowns == 0) return HXT_STATUS_OK;

  const int            nnz  = sys->nnz      ;
  const int              n  = sys->nUnknowns;
        int            *ia  = sys->lineptr  ;
        int            *ja  = sys->colindex ;
        double_complex * a  = sys->value    ;
  const int            nrhs = sys->nrhs     ;

  linear_solver_build ( sys->ls, ia, ja, a, nnz, n, nrhs);

  sys->solver = true;

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemPhase(HXTSystem *sys  ,
                         const int  irhs ,
                         const int  nrhs ,
                         const int  phase)
{
  if(sys->solver == false) return HXT_STATUS_OK;

  if( phase == 33  || phase == 13)
  {
    double_complex *b  = sys->rhs ;
    linear_solver  *ls = sys->ls  ;
    linear_solver_phase ( ls, phase, b   , nrhs,  irhs);
    linear_solver_cpy_x ( ls,   sys->x   , nrhs,  irhs);
  }
  else
  {
    linear_solver  *ls = sys->ls  ;
    linear_solver_phase ( ls, phase, NULL, -1  ,    -1);
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemCSTR(HXTSystem        *sys,
		        const int        irhs, 
			const HXTGroup *group,
       double_complex (*f)(const double *xyz))
{
  if(sys->nUnknowns == 0) return HXT_STATUS_OK;

  const int base = sys->base;

  size_t k, ie, in;

  int            *irn = sys->lineptr  ;
  int            *jcn = sys->colindex ;
  double_complex *a   = sys->value    ;
  double_complex *rhs = sys->rhs      ;
  const int nUnknowns = sys->nUnknowns;

  const int offset = group->offset;

  const size_t numEtts  = group->entities_n;
  for(ie = 0; ie < numEtts; ie++)
  {
    const HXTEntity *ett = group->entities[ie];

    const size_t *nodeTags   = ett->nodes.tags  ;
    const size_t  nodeTags_n = ett->nodes.tags_n;
    const double *coord      = ett->nodes.coord ;

    for(in = 0; in < nodeTags_n; in++)
    {
      const double xyz[3] = { coord[3*in + 0],
                              coord[3*in + 1],
                              coord[3*in + 2]
                            };

    //int lptr = hxtDofManagerGetGlobalId(sys->dofM, nodeTags[in], offset);
      int lptr = hxtDofHashFind(sys->hashs, nodeTags[in], offset);

      if(lptr < 0) continue;

      for(k = irn[lptr] - base; k < irn[lptr+1] - base; k++)
      {
        a[k] = 0.0;
        if( (jcn[k] - base) == lptr)
        {
          a[k]  = 1.0;
          rhs[lptr + irhs*nUnknowns] = f(xyz);
        }
      }
    }

  }
  return HXT_STATUS_OK;
}

HXTStatus hxtSystemWriteMSH(const HXTSystem *sys,
                            const HXTGroup  *grp,
                            const char      *fln,
                            const int       irhs,
                            const int     partid)
{
  const int rowMax =  sys->nUnknowns;
  if( rowMax == 0) return HXT_STATUS_OK;

  const double_complex *x = &sys->x[irhs*rowMax];

  size_t et, e, ie, j;
  // * ------------------------------------------------------------- * //
  // * --                     GET OUTPUT NAME                     -- * //
  // * ------------------------------------------------------------- * //
  const char s  [ 2] = ".";
        char str[32] = "\0";

  strcpy(str, fln);

  char *first, *second;

  first  = strtok(str , s); // * get the first  token
  second = strtok(NULL, s); // * get the second token

  int ret  = strncmp(second, "msh", 3*sizeof(char));
  if( ret != 0) return HXT_ERROR_MSG(HXT_STATUS_ERROR, 
                       "OUTPUT FILE NAME WRONG: %s!\n", fln);

  char output[32] = "\0";
  if(partid > 0)
  {
    char queue [32] = "\0";
    strcpy  (output, first);
    snprintf(queue , sizeof(char)*32, "_%u.msh", partid);
    strcat  (output, queue);
  }
  else strcpy(output, fln);

  // * ------------------------------------------------------------- * //
  // * --                          PRINTF                         -- * //
  // * ------------------------------------------------------------- * //
  FILE *fp = fopen(output,"w");
  fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");

  fprintf(fp, "$ElementNodeData\n" );
//fprintf(fp, "2\n\"%s\"\n", output);
  fprintf(fp, "2\n\"%s\"\n", fln   );
  fprintf(fp, "1\n%.16g \n", 0.    );
  fprintf(fp, "4\n%d\n%d\n", 0, 1  );

  size_t numElesTotal = 0;

  const size_t numEtts  = grp->entities_n;
  for(ie = 0; ie < numEtts; ie++)
  {
    const HXTEntity *ett = grp->entities[ie];

    const size_t     elementTypes_n = ett->elements.types_n;
    const size_t    *elementTags_n  = ett->elements.tags_n ;
    for(et = 0; et < elementTypes_n; et++)
    {
      size_t nE = elementTags_n[et];
      numElesTotal += nE;
    }
  }

  fprintf(fp, "%lu\n", numElesTotal);
  if(partid > 0) fprintf(fp, "%d\n" , partid-1    );
  else           fprintf(fp, "%d\n" , 0           );
  
  const int offset = grp->offset;

  for(ie = 0; ie < numEtts; ie++)
  {
    const HXTEntity *ett = grp->entities[ie];

    size_t   elementTypes_n = ett->elements.types_n    ;
    size_t **elementTags    = ett->elements.tags       ;
    size_t  *elementTags_n  = ett->elements.tags_n     ;
    size_t **nodeTags       = ett->elements.nodeTags   ;
    size_t  *nodeTags_n     = ett->elements.nodeTags_n ;

    for(et = 0; et < elementTypes_n; et++)
    {
      size_t nE =                  elementTags_n[et];
      size_t nN = nodeTags_n[et] / elementTags_n[et]; 
      for(e = 0; e < nE; e++)
      {
        fprintf(fp, "%lu ", elementTags[et][e]);
        fprintf(fp, "%lu ", nN);
    
        size_t *eNodeTags = &nodeTags[et][e*nN];
        for(j = 0; j < nN; j++)
        {
        //const int row = hxtDofManagerGetGlobalId(sys->dofM, eNodeTags[j], offset);
          const int row = hxtDofHashFind(sys->hashs, eNodeTags[j], offset);

          fprintf(fp, "%.8g ", creal( x[row] ) );
        }
        fprintf(fp, "\n");
      }
    }

  }
  fprintf(fp, "$EndElementNodeData\n");

  fclose(fp);

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemBMxToRHS (HXTSystem *SRV, const double alpha,
                       const HXTSystem *SBM, const double  beta,
                       const HXTSystem *SXV, const size_t  lrhs)
{
  if(SRV->nUnknowns == 0) return HXT_STATUS_OK;
  if(SBM->nUnknowns == 0) return HXT_STATUS_OK;
  if(SXV->nUnknowns == 0) return HXT_STATUS_OK;

  size_t e, j, k, et, it, im, ie;

  const int  xvNUnknowns = SXV->nUnknowns;
  const int  rvNUnknowns = SRV->nUnknowns;

  const double_complex *x   = &SXV->x  [ lrhs * xvNUnknowns ];
        double_complex *rhs = &SRV->rhs[ lrhs * rvNUnknowns ];

  const size_t numFormulations = SBM->formulations_n;

  for(im = 0; im < numFormulations; im++)
  {
    const HXTFormulation   *formulation = SBM->formulations[im];
    const size_t BTerms_n = formulation->BTerms_n;
    for(it = 0; it < BTerms_n; it++)
    {
      const HXTTerm  * term = formulation->BTerms[it];
      const HXTGroup *group = term->field;

      const int offset_f = (term->field)->offset;
      const int offset_t = (term-> test)->offset;

      const size_t entities_n = group->entities_n;
      for(ie = 0; ie < entities_n; ie++)
      {
        const HXTEntity *ett = group->entities[ie];

        const size_t   elementTypes_n = (const size_t  ) ett->elements.types_n   ;
        const size_t  *elementTags_n  = (const size_t* ) ett->elements.tags_n    ;
        const size_t  *nodeTags_n     = (const size_t* ) ett->elements.nodeTags_n;
        const size_t **nodeTags       = (const size_t**) ett->elements.nodeTags  ;
        for(et = 0; et < elementTypes_n; et++)
	{
          const size_t nE =                  elementTags_n[et];
          const size_t nN = nodeTags_n[et] / elementTags_n[et];

          const double_complex *BM = term-> Mats[ie][et];

          for(e = 0; e < nE; e++)
          {
	    const size_t *eNodeTags = &nodeTags[et][e*nN];
	    for(j = 0; j < nN; j++)
            {
              double_complex sum = 0.;
              for(k = 0; k < nN; k++)
              {
              //int col = hxtDofManagerGetGlobalId(SXV->dofM, eNodeTags[k], offset_f);
                int col = hxtDofHashFind(SXV->hashs, eNodeTags[k], offset_f);

                sum += BM[e*nN*nN + j*nN + k] * x[ col ];
    	      }
            //int row   = hxtDofManagerGetGlobalId(SRV->dofM, eNodeTags[j], offset_t);
              int row   = hxtDofHashFind(SRV->hashs, eNodeTags[j], offset_t);
              rhs[row] += sum * beta;
  	    }
	  }
	}
      }
    }
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemCMxToRHS (HXTSystem *SRV, const double alpha,
                       const HXTSystem *SCM, const double  beta,
                       const HXTSystem *SXV, const size_t  lrhs)
{
  if(SRV->nUnknowns == 0) return HXT_STATUS_OK;
  if(SCM->nUnknowns == 0) return HXT_STATUS_OK;
  if(SXV->nUnknowns == 0) return HXT_STATUS_OK;

  size_t e, j, k, et, it, im, ie;

  const int  xvNUnknowns = SXV->nUnknowns;
  const int  rvNUnknowns = SRV->nUnknowns;

  const double_complex *x   = &SXV->x  [ lrhs * xvNUnknowns ];
        double_complex *rhs = &SRV->rhs[ lrhs * rvNUnknowns ];

  const size_t numFormulations = SCM->formulations_n;

  for(im = 0; im < numFormulations; im++)
  {
    const HXTFormulation   *formulation = SCM->formulations[im];
    const size_t CTerms_n = formulation->CTerms_n;
    for(it = 0; it < CTerms_n; it++)
    {
      const HXTTerm  * term = formulation->CTerms[it];
      const HXTGroup *group = term->field;

      const int offset_f = (term->field)->offset;
      const int offset_t = (term-> test)->offset;

      const size_t entities_n = group->entities_n;
      for(ie = 0; ie < entities_n; ie++)
      {
        const HXTEntity *ett = group->entities[ie];

        const size_t   elementTypes_n = (const size_t  ) ett->elements.types_n   ;
        const size_t  *elementTags_n  = (const size_t* ) ett->elements.tags_n    ;
        const size_t  *nodeTags_n     = (const size_t* ) ett->elements.nodeTags_n;
        const size_t **nodeTags       = (const size_t**) ett->elements.nodeTags  ;
        for(et = 0; et < elementTypes_n; et++)
	{
          const size_t nE =                  elementTags_n[et];
          const size_t nN = nodeTags_n[et] / elementTags_n[et];

          const double_complex *CM = term-> Mats[ie][et];

          for(e = 0; e < nE; e++)
          {
	    const size_t *eNodeTags = &nodeTags[et][e*nN];
	    for(j = 0; j < nN; j++)
            {
              double_complex sum = 0.;
              for(k = 0; k < nN; k++)
              {
              //int col = hxtDofManagerGetGlobalId(SXV->dofM, eNodeTags[k], offset_f);
                int col = hxtDofHashFind(SXV->hashs, eNodeTags[k], offset_f);

                sum += CM[e*nN*nN + j*nN + k] * x[ col ];
    	      }
            //int row   = hxtDofManagerGetGlobalId(SRV->dofM, eNodeTags[j], offset_t);
              int row   = hxtDofHashFind(SRV->hashs, eNodeTags[j], offset_t);
              rhs[row] += sum * beta;
  	    }
	  }
	}
      }
    }
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemSolToArr(HXTSystem *sys     , const size_t  lRHS,
                            double_complex *arr, const size_t arr_n,
                                                 const double alpha)
{
  if(sys->nUnknowns == 0) return HXT_STATUS_OK;

  const int nUnknowns = sys->nUnknowns  ;

  const double_complex *x = &sys->x[ lRHS * nUnknowns ];
        double_complex *y = &arr   [ lRHS * arr_n     ];

  for(size_t i = 0; i < nUnknowns; i++)
  {
    y[i] = x[i] + alpha * y[i];
  }

  return HXT_STATUS_OK;
}

HXTStatus hxtSystemArrToSol(HXTSystem *sys     , const size_t  lRHS,
                            double_complex *arr, const size_t arr_n,
                                                 const double alpha)
{
  if(sys->nUnknowns == 0) return HXT_STATUS_OK;

  const int nUnknowns = sys->nUnknowns  ;

        double_complex *x = &sys->x[ lRHS * nUnknowns ];
  const double_complex *y = &arr   [ lRHS * arr_n     ];

  for(size_t i = 0; i < nUnknowns; i++)
  {
    x[i] = y[i] + alpha * x[i];
  }

  return HXT_STATUS_OK;
}
