#ifndef HXTWAVE_SYSTEM_H
#define HXTWAVE_SYSTEM_H

#include  "hxt_linear_algebra.h"

#include "linear_solver.h"
#include "radix_sort.h"

#include "hxt_formulation.h"
#include "hxt_dofManager.h"
#include "hxt_dof_hash.h"

#define fmls_m 50

typedef struct HXTSystemStruct
{
  size_t offsets_n;

  HXTFormulation  *formulations[fmls_m];
  size_t           formulations_n ;

  int base;

  int nUnknowns;
  int nrhs;
  int nnz ;
  double_complex *a  ;
  int            *irn;
  int            *jcn;
  double_complex *rhs;
  double_complex *x  ;

  int            *lineptr ;
  int            *colindex;
  double_complex *value   ;

//HXTDofManager *dofM;
  HXTDofHash   *hashs;

  bool   solver;
  linear_solver  *ls;

} HXTSystem;

HXTStatus hxtSystemCreate (HXTSystem **sys);
HXTStatus hxtSystemAddFML (HXTSystem  *sys, HXTFormulation *fml);
HXTStatus hxtSystemBuild  (HXTSystem  *sys, const int nrhs);
HXTStatus hxtSystemDelete (HXTSystem **sys);
HXTStatus hxtSystemCOOMat (HXTSystem  *sys);
HXTStatus hxtSystemCSRMat (HXTSystem  *sys, const int base);
HXTStatus hxtSystemRHSZero(HXTSystem  *sys, const int irhs);
HXTStatus hxtSystemSolZero(HXTSystem  *sys, const int irhs);

HXTStatus hxtSystemCSTR(HXTSystem *sys, const int irhs,  const HXTGroup *group, 
                                        double_complex (*f)(const double *xyz));

HXTStatus hxtSystemSolver (HXTSystem  *sys);
HXTStatus hxtSystemPhase  (HXTSystem  *sys, 
                           const int  irhs, 
                           const int  nrhs, 
                           const int phase);

HXTStatus hxtSystemWriteMSH(const HXTSystem *sys, 
                            const HXTGroup  *grp, 
                            const char      *fln, 
                            const int       irhs, 
                            const int     partid);


HXTStatus hxtSystemBMxToRHS (HXTSystem *SRV, const double alpha, 
                       const HXTSystem *SBM, const double  beta, 
                       const HXTSystem *SXV, const size_t  lrhs);
HXTStatus hxtSystemCMxToRHS (HXTSystem *SRV, const double alpha, 
                       const HXTSystem *SCM, const double  beta, 
                       const HXTSystem *SXV, const size_t  lrhs);


HXTStatus hxtSystemSolToArr(HXTSystem *sys     , const size_t  lRHS, 
                            double_complex *arr, const size_t arr_n, 
                                                 const double alpha);
HXTStatus hxtSystemArrToSol(HXTSystem *sys     , const size_t  lRHS, 
                            double_complex *arr, const size_t arr_n, 
                                                 const double alpha);

#endif
