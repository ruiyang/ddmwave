/* -------------------------------------------------------------------- */
/*      Example program to show the use of the "PARDISO" routine        */
/*      for a complex unsymmetric linear pstems                         */
/* -------------------------------------------------------------------- */
/*      This program can be downloaded from the following site:         */
/*      http://www.pardiso-project.org                                  */
/*                                                                      */
/*  (C) Olaf Schenk, Institute of Computational Science                 */
/*      Universita della Svizzera italiana, Lugano, Switzerland.        */
/*      Email: olaf.schenk@usi.ch                                       */
/* -------------------------------------------------------------------- */
#if defined HXTWAVE_HAVE_PARDISO

#include <omp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "linear_solver.h"
#include "radix_sort.h"

// * PARDISO prototype.
void pardisoinit(void *, int *, int *, int *, double *, int *);
void pardiso    (void *, int *, int *, int *,    int *, int *, double_complex *, 
                 int  *, int *, int *, int *,    int *, int *, double_complex *, 
                 double_complex *, int *, double *);
void pardiso_chkmatrix_z (int *, int *, double_complex *, int *, int *, int *);
void pardiso_chkvec_z    (int *, int *, double_complex *, int *);
void pardiso_printstats_z(int *, int *, double_complex *, int *, 
                          int *, int *, double_complex *, int *);

static inline void linear_solver_malloc (void *ptrToPtr, size_t size);
static inline void linear_solver_realloc(void* ptrToPtr, size_t size);
static inline void linear_solver_free   (void *ptrToPtr);

struct pardiso_linear_solver_struct
{
  int    mtype;        // * Real complex unsymmetric matrix.
  int    nrhs;         // * Number of right hand sides.
  void  *pt[64];       // * Internal solver memory pointer pt,                  
                       // * 32-bit: int pt[64]; 64-bit: long int pt[64]         
                       // * or void *pt[64] should be OK on both architectures   
  int    iparm[64];
  int    maxfct, mnum, phase, error, msglvl, solver;
  double dparm[64];    // * Pardiso control parameters. 
  
  double_complex ddum; // * Double  dummy.
  int	         idum; // * Integer dummy.

  int nnz;
  int   n;             // * number of rows
  int *ia;             // * pointer
  int *ja;             // * pointer
  double_complex *a;   // * pointer
  double_complex *x; 
};

void linear_solver_create(linear_solver **ps)
{
  linear_solver_malloc(ps, sizeof(linear_solver));
  (*ps)->ia   = NULL;
  (*ps)->ja   = NULL;
  (*ps)->nnz  = 0;
  (*ps)->n    = 0;
  (*ps)->nrhs = 0;
  (*ps)->a    = NULL;
  (*ps)->x    = NULL;
}

void linear_solver_delete(linear_solver **ps)
{
  if((*ps)->n == 0)
  {
    linear_solver_free(ps);
    return;
  }

  linear_solver_free(&(*ps)-> x);
  (*ps)->ia   = NULL;
  (*ps)->ja   = NULL;
  (*ps)->a    = NULL;
  (*ps)->x    = NULL;
  (*ps)->nnz  = 0;
  (*ps)->n    = 0;
  (*ps)->nrhs = 0;

  linear_solver_free(ps);
}

int linear_solver_build(linear_solver *ps, int *ia, int   *ja, 
                                           double_complex  *a, 
                                           const int      nnz, 
                                           const int        n, 
                                           const int     nrhs)
{
  if (n == 0) return 1; 

  ps->ia = ia;
  ps->ja = ja;
  ps-> a =  a;
  linear_solver_malloc (&ps-> x, (n*nrhs)*sizeof(double_complex));

  ps->nnz   =  nnz;
  ps->n     =    n;
  ps->mtype =   13; // * Real complex unsymmetric matrix.
  ps->nrhs  = nrhs; // * Number of max right hand  sides.

  int i;
  for(i = 0; i < 64; i++) ps->iparm[i] = 0; 
  for(i = 0; i < 64; i++) ps->dparm[i] = 0; 

  int   num_procs;  // * Number of processors.
  char *var;        // * Auxiliary  variables. 

  /* -------------------------------------------------------------------- */
  /* ..  Setup Pardiso control parameters und initialize the solvers      */
  /*     internal adress pointers. This is only necessary for the FIRST   */
  /*     call of the PARDISO solver.                                      */
  /* ---------------------------------------------------------------------*/      
  // * Numbers of processors, value of OMP_NUM_THREADS
  var = getenv("OMP_NUM_THREADS");
  if(var != NULL) sscanf( var, "%d", &num_procs );
  else 
  {
    printf("[PARDISO]: Set environment OMP_NUM_THREADS to 1\n");
    exit(1);
  }
  ps->iparm[2] = num_procs;
   
  ps->maxfct = 1; // * Maximum number of numerical factorizations. 
  ps->mnum   = 1; // * Which factorization to use.
  ps->msglvl = 0; // * No Print statistical information  
  ps->error  = 0; // * Initialize error flag 

  /* -------------------------------------------------------------------- */    
  /* ..  Check license file pardiso.lic                                   */
  /* -------------------------------------------------------------------- */ 
  ps->error  = 0;
  ps->solver = 0; // * use sparse direct solver
  pardisoinit(ps->pt, &ps->mtype, &ps->solver, ps->iparm, ps->dparm, &ps->error);

  if(ps->error != 0)
  {
    if(ps->error == -10 ) printf("[PARDISO]: No license file found       \n");
    if(ps->error == -11 ) printf("[PARDISO]: License is expired          \n");
    if(ps->error == -12 ) printf("[PARDISO]: Wrong username or hostname  \n");
    return 1;
  }
  else
  {
    if(ps->msglvl ==  1 ) printf("[PARDISO]: License check was successful\n");
  }

  /* -------------------------------------------------------------------- */
  /*  .. pardiso_chk_matrix_z(...)                                        */
  /*     Checks the consistency of the given matrix.                      */
  /*     Use this functionality only for debugging purposes               */
  /* -------------------------------------------------------------------- */
//pardiso_chkmatrix_z (&ps->mtype, &ps->n, ps->a, ps->ia, ps->ja, &ps->error);
//if(ps->error != 0) 
//{
//  printf("\n[PARDISO]: ERROR in consistency of matrix: %d", ps->error);
//  exit(1);
//}

  return 1;
}

int linear_solver_phase(linear_solver *ps, const int    phase, 
                                           double_complex *bs,
                                           const int     crhs, 
                                           const int     irhs)
{
  if(ps->n == 0) return 1; 

  int *ia           =  ps->ia;
  int *ja           =  ps->ja;
  double_complex *a =  ps-> a;
  int  n            =  ps-> n;
//int  nnz          =  ps-> nnz;

  double_complex *x = &ps-> x[irhs*n];
  double_complex *b = &    bs[irhs*n];
  int          nrhs = crhs;

  /* -------------------------------------------------------------------- */
  /* ..  pardiso_chkvec_z(...)                                            */
  /*     Checks the given vectors for infinite and NaN values             */
  /*     Input parameters (see PARDISO user manual for a description):    */
  /*     Use this functionality only for debugging purposes               */
  /* -------------------------------------------------------------------- */
  pardiso_chkvec_z (&n, &nrhs, b, &ps->error);
  if(ps->error != 0) 
  {
    printf("\n[PARDISO]: ERROR in right hand side: %d", ps->error);
    exit(1);
  }

  ps->phase = phase;
  /* -------------------------------------------------------------------- */
  /* ..  Analysis, Numerical Factorization,                               */
  /*     Solve ,   Iterative Refinement                                   */
  /* -------------------------------------------------------------------- */
  if(phase == 13)
  {    
    ps->iparm[7] = 1; 
    pardiso(ps->pt, &ps->maxfct, &ps->mnum, &ps->mtype, &ps->phase,
            &n, a, ia, ja, &ps->idum, &nrhs,
            ps->iparm, &ps->msglvl, b, x, &ps->error, ps->dparm);
    if(ps->error != 0) 
    {
      printf("\n[PARDISO]: ERROR as phase == 13: %d", ps->error);
      exit(1);
    }
    if(ps->msglvl == 1) printf("\n[PARDISO]: Reordering completed ... ");
    if(ps->msglvl == 1) printf("\n[PARDISO]: Number of nonzeros in factors  = %d", ps->iparm[17]);
    if(ps->msglvl == 1) printf("\n[PARDISO]: Number of factorization MFLOPS = %d", ps->iparm[18]);
    if(ps->msglvl == 1) printf("\n[PARDISO]: Factorization completed ...");
    if(ps->msglvl == 1) printf("\n[PARDISO]: Solution completed ...\n");
  }

  /* -------------------------------------------------------------------- */
  /* ..  Analysis, Numerical Factorization,                               */
  /* -------------------------------------------------------------------- */
  if(phase == 12)
  {    
    pardiso(ps->pt, &ps->maxfct, &ps->mnum, &ps->mtype, &ps->phase,
            &n, a, ia, ja, &ps->idum, &ps->nrhs,
            ps->iparm, &ps->msglvl, &ps->ddum, &ps->ddum, &ps->error, ps->dparm);
    if(ps->error != 0) 
    {
      printf("\n[PARDISO]: ERROR during symbolic factor: %d", ps->error);
      exit(1);
    }
    if(ps->msglvl == 1) printf("\n[PARDISO]: Reordering completed ... ");
    if(ps->msglvl == 1) printf("\n[PARDISO]: Number of nonzeros in factors  = %d", ps->iparm[17]);
    if(ps->msglvl == 1) printf("\n[PARDISO]: Number of factorization MFLOPS = %d", ps->iparm[18]);
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Reordering and Symbolic Factorization.  This step also allocates */
  /*     all memory that is necessary for the factorization.              */
  /* -------------------------------------------------------------------- */ 
  if(phase == 11)
  {    
    pardiso(ps->pt, &ps->maxfct, &ps->mnum, &ps->mtype, &ps->phase,
            &n, a, ia, ja, &ps->idum, &ps->nrhs,
            ps->iparm, &ps->msglvl, &ps->ddum, &ps->ddum, &ps->error, ps->dparm);
    if(ps->error != 0) 
    {
      printf("\n[PARDISO]: ERROR during symbolic factor: %d", ps->error);
      exit(1);
    }
    if(ps->msglvl == 1) printf("\n[PARDISO]: Reordering completed ... ");
    if(ps->msglvl == 1) printf("\n[PARDISO]: Number of nonzeros in factors  = %d", ps->iparm[17]);
    if(ps->msglvl == 1) printf("\n[PARDISO]: Number of factorization MFLOPS = %d", ps->iparm[18]);
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Numerical factorization.                                         */
  /* -------------------------------------------------------------------- */ 
  if(phase == 22)
  {    
    pardiso(ps->pt, &ps->maxfct, &ps->mnum, &ps->mtype, &ps->phase,
            &n, a, ia, ja, &ps->idum, &ps->nrhs,
            ps->iparm, &ps->msglvl, &ps->ddum, &ps->ddum, &ps->error, ps->dparm);
    if(ps->error != 0) 
    {
      printf("\n[PARDISO]: ERROR during numerical factor: %d", ps->error);
      exit(2);
    }
    if(ps->msglvl == 1) printf("\n[PARDISO]: Factorization completed ...\n");
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Back substitution and iterative refinement.                      */
  /* -------------------------------------------------------------------- */    
  if(phase == 33)
  {    
    // * Max numbers of iterative refinement steps.
    ps->iparm[7] = 1; 
    pardiso(ps->pt, &ps->maxfct, &ps->mnum, &ps->mtype, &ps->phase,
            &n, a, ia, ja, &ps->idum, &nrhs,
            ps->iparm, &ps->msglvl, b, x, &ps->error, ps->dparm);
    if(ps->error != 0) 
    { 
      printf("\n[PARDISO]: ERROR during solution: %d", ps->error);
      exit(3);
    }
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Termination and release of memory.                               */
  /* -------------------------------------------------------------------- */ 
  if(phase == -1)
  {    
    pardiso(ps->pt, &ps->maxfct, &ps->mnum, &ps->mtype, &ps->phase,
            &n, a, ia, ja, &ps->idum, &ps->nrhs,
            ps->iparm, &ps->msglvl, &ps->ddum, &ps->ddum, &ps->error, ps->dparm);
  }
  return 1;
}

int linear_solver_cpy_x(linear_solver *ps, double_complex *c, 
                                           const int    crhs,
                                           const int    irhs)
{    
  if(ps->n == 0) return 1; 

  double_complex *x   = &ps->x[irhs* ps->n];
  double_complex *ptr = &    c[irhs* ps->n];
  memcpy(ptr, x, crhs*(ps->n)*sizeof(double_complex));

  return 1;
}

static inline void linear_solver_malloc(void *ptrToPtr, size_t size)
{
  void **p = (void**) ptrToPtr;
  *p = malloc(size);
  if(*p==NULL)
  {
    printf("linear solver malloc failed at ");
   fprintf(stderr, "%s, line %d.\n",__FILE__, __LINE__);
    exit(1);
  }
}

static inline void linear_solver_realloc(void* ptrToPtr, size_t size)
{
  void** p = (void**)ptrToPtr;
  void* newptr = realloc(*p, size);
  if (newptr==NULL && *p!=NULL && size!=0)
  {
    printf("linear solver realloc failed at ");
   fprintf(stderr, "%s, line %d.\n",__FILE__, __LINE__);
    exit(1);
  }
  *p = newptr;
}

static inline void linear_solver_free(void *ptrToPtr)
{
  void **p = (void**) ptrToPtr;
  free(*p);
  *p=NULL;
}

#endif
