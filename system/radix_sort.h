#ifndef CRADIX_SORT_H
#define CRADIX_SORT_H

#include <complex.h>
#include <stdint.h>
#include <math.h>

typedef int            RADIX_KEY ;
typedef double complex RADIX_DATA;

void parallel_rsort(RADIX_KEY     *row, 
                    RADIX_KEY     *col, 
                    RADIX_DATA    *arr, 
                    const uint64_t len, 
                    const uint64_t max);

#endif
