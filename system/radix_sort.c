#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "radix_sort.h"

#ifdef _OPENMP
#include <omp.h>
#else
#include <time.h>
static inline int    omp_get_max_threads (void ) {return 1;}
static inline int    omp_get_thread_num  (void ) {return 0;}
static inline int    omp_get_num_threads (void ) {return 1;}
static inline int    omp_get_num_procs   (void ) {return 1;}
static inline int    omp_get_thread_limit(void ) {return 1;}
static inline void   omp_set_num_threads (int n) {        ;}

static inline double omp_get_wtime(void) 
{
  clock_t t = clock();
  return (double)t/CLOCKS_PER_SEC;
}
#endif

/* define SIMD ALIGNMENT */
#ifndef SIMD_ALIGN // must imperatively be a power of two, greater than 2*sizeof(unsigned)
#ifdef __AVX512F__ // 64 bytes for AVX-512
#define SIMD_ALIGN 64
#elif defined(__AVX2__)
#define SIMD_ALIGN 32
#else
#define SIMD_ALIGN 16
#endif
#endif

#define SHIFT 11 //shift must be at least 3 with double of 8 bytes to ensure 64 bytes alignement
#define ONESHIFT (1<<SHIFT)

typedef uint64_t* __restrict__ __attribute__((aligned(SIMD_ALIGN))) fast_uptr;

static void scan(fast_uptr h)
{
  uint64_t   i = 0;
  uint64_t sum = 0;
  for(; i < ONESHIFT; i++) 
  {
    uint64_t tsum = h[i] + sum;
    h[i]  = sum; // sum of the preceding, but not with itself !!
    sum  = tsum;
  }
}

/*****************************************************************
 *  SORTING FUNCTIONS WITHOUT STRUCT
 *****************************************************************/

static inline void lsb_ws(RADIX_KEY*  __restrict__     row,
                          RADIX_KEY*  __restrict__     col, 
                          RADIX_DATA* __restrict__   array, 
		          RADIX_KEY*  __restrict__   b_row, 
                          RADIX_KEY*  __restrict__   b_col, 
                          RADIX_DATA* __restrict__ b_array, 
		                   fast_uptr             h, 
                                   const uint64_t   length, 
                                   const uint64_t ncolpass, 
                                   const uint64_t nrowpass)
{
  uint64_t pass;
  for(pass = 0; pass < ncolpass; pass++)
  {
    memset(h, 0, ONESHIFT*sizeof(uint64_t));
    // 1.  parallel histogramming pass
    uint64_t i;
    for(i = 0; i < length; i++)
      h[col[i] >> (SHIFT*pass) & (ONESHIFT-1)]++;

		
    // 2.  Sum the histograms -- 
    //     each histogram entry records 
    //     the number of values preceding itself.
    scan(h);

    // byte 0: read/write histogram
    for(i = 0; i < length; i++)
    {
      uint64_t ye  = h[col[i] >> (SHIFT*pass) & (ONESHIFT-1)]++;
      b_row   [ye] =   row[i];
      b_col   [ye] =   col[i];
      b_array [ye] = array[i];
    }
    RADIX_KEY* t_row = NULL;
    t_row =   row;
      row = b_row;
    b_row = t_row;
	
    RADIX_KEY* t_col = NULL;
    t_col =   col;
      col = b_col;
    b_col = t_col;
	
    RADIX_DATA* t_array = NULL;
    t_array =   array;
      array = b_array;
    b_array = t_array;
  }

  for(pass = 0; pass < nrowpass; pass++)
  {
    memset(h, 0, ONESHIFT*sizeof(uint64_t));
    // 1.  parallel histogramming pass
    uint64_t i;
    for(i = 0; i < length; i++)
      h[row[i] >> (SHIFT*pass) & (ONESHIFT-1)]++;

    // 2.  Sum the histograms -- 
    //     each histogram entry records 
    //     the number of values preceding itself.
    scan(h);

    // byte 0: read/write histogram
    for(i = 0; i < length; i++)
    {
      uint64_t ye  = h[row[i] >> (SHIFT*pass) & (ONESHIFT-1)]++;
      b_row   [ye] =   row[i];
      b_col   [ye] =   col[i];
      b_array [ye] = array[i];
    }
    RADIX_KEY* t_row = NULL;
    t_row =   row;
      row = b_row;
    b_row = t_row;
	
    RADIX_KEY* t_col = NULL;
    t_col =   col;
      col = b_col;
    b_col = t_col;
	
    RADIX_DATA* t_array = NULL;
    t_array =   array;
      array = b_array;
    b_array = t_array;
  }
}

void parallel_rsort(RADIX_KEY * const __restrict__   row, 
                    RADIX_KEY * const __restrict__   col, 
                    RADIX_DATA* const __restrict__ array, 
                                const uint64_t    length, 
                                const uint64_t         n)
{
  RADIX_KEY * const __restrict__ b_row   = malloc(length*sizeof(RADIX_KEY ));
  RADIX_KEY * const __restrict__ b_col   = malloc(length*sizeof(RADIX_KEY ));
  RADIX_DATA* const __restrict__ b_array = malloc(length*sizeof(RADIX_DATA));
  uint64_t  nthreads;
  void*     base_address;
  fast_uptr h_all, h_tot;

  // calculate number of bits
  uint64_t nbits = 0;
  while(n>>nbits) nbits++;

  const uint64_t bits_rem = (nbits>SHIFT)?nbits-SHIFT:0;
  const uint64_t nrowpass = (bits_rem+SHIFT-1)/ SHIFT;
  const uint64_t ncolpass = (nbits   +SHIFT-1)/ SHIFT;

  // 1.  parallel histogramming pass
  #pragma omp parallel
  {
    #pragma omp single
    {
      nthreads = omp_get_num_threads();
      base_address = malloc((nthreads*ONESHIFT+ONESHIFT+1)*sizeof(uint64_t) + SIMD_ALIGN);
      h_all = (fast_uptr) (((size_t)base_address + SIMD_ALIGN -1) & ~(SIMD_ALIGN - 1));
      h_tot = h_all + nthreads*ONESHIFT;
      h_tot[ONESHIFT] = length;
    }
    const uint64_t threadID = omp_get_thread_num();

    fast_uptr h_this = h_all + threadID*ONESHIFT;
    memset(h_this, 0, ONESHIFT*sizeof(uint64_t));

    uint64_t i;

    // store the row information in the array h_this, chque liagne est son no. de ligne
    // example:
    // row = {1, 2, 9, 8, 1, 0, 1, 3, 4, 7};
    // No.    1, 1, 1, 1, 2, 1, 3, 1, 1, 1
    #pragma omp for schedule(static)
    for(i = 0; i < length; i++) h_this[row[i]>>bits_rem]++; 

    #pragma omp for schedule(static)
    for (i=0; i<ONESHIFT; i++)
    {
      uint64_t sum = 0;
      uint64_t j;
      for(j = 0; j < nthreads+1; j++)
      {
        uint64_t tsum = h_all[j*ONESHIFT + i] + sum;
                        h_all[j*ONESHIFT + i] = sum;
        sum = tsum;
      }
    }
	
    // calculate total sums
    #pragma omp single
    scan(h_tot);

    // now every thread may calculate all it's starting indexes
    #pragma vector aligned
    #pragma omp simd aligned(h_this, h_tot:SIMD_ALIGN)
    for(i = 0; i < ONESHIFT; i++) h_this[i] += h_tot[i];

    // byte 2: read/write histogram, copy
    #pragma omp for schedule(static)
    for (i = 0; i < length; i++) 
    {
      uint64_t yo  = h_this[row[i]>>bits_rem]++;
      b_row   [yo] = row   [i];
      b_col   [yo] = col   [i];
      b_array [yo] = array [i];
    }

    #pragma omp for schedule(static)
    for (i=0; i<ONESHIFT; i++)
    {
      uint64_t start = h_tot[i], end = h_tot[i+1];
      lsb_ws(b_row + start, b_col + start, b_array + start, 
               row + start,   col + start,   array + start,
	       h_this     ,  end-start, ncolpass, nrowpass);
    }
  }

  // * copy buffer to array if needed
  if(((nrowpass+ncolpass)&1)==0)
  {
    memcpy(row  , b_row  , n*sizeof(RADIX_KEY ));
    memcpy(col  , b_col  , n*sizeof(RADIX_KEY ));
    memcpy(array, b_array, n*sizeof(RADIX_DATA));
  }
  free(base_address);
  free(b_row  );
  free(b_col  );
  free(b_array);
}
