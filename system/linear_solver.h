#ifndef LINEAR_SOLVER_H
#define LINEAR_SOLVER_H

#if defined HXTWAVE_HAVE_PARDISO

#include <complex.h>
typedef double complex double_complex;

typedef struct pardiso_linear_solver_struct linear_solver;

void linear_solver_create(linear_solver **ls);
void linear_solver_delete(linear_solver **ls);
int  linear_solver_build (linear_solver  *ls, int *ia, int   *ja, 
                                              double_complex  *a, 
                                              const int      nnz, 
                                              const int        n, 
                                              const int     nrhs);
int  linear_solver_phase (linear_solver  *ls, const int    phase, 
                                              double_complex  *b,
                                              const int     crhs, 
                                              const int     irhs);
int  linear_solver_cpy_x (linear_solver  *ls, double_complex  *c, 
                                              const int     crhs,
                                              const int     irhs);

#elif defined HXTWAVE_HAVE_MKL

#define MKL_INT       int
#define MKL_Complex16 complex

#include <complex.h>
typedef double complex double_complex;
#include "mkl.h"

typedef struct mkl_linear_solver_struct linear_solver;

void linear_solver_create(linear_solver **ls);
void linear_solver_delete(linear_solver **ls);
int  linear_solver_build (linear_solver  *ls, int *ia, int   *ja, 
                                              double_complex  *a, 
                                              const int      nnz, 
                                              const int        n, 
                                              const int     nrhs);
int  linear_solver_phase (linear_solver  *ls, const int    phase, 
                                              double_complex  *b,
                                              const int     crhs, 
                                              const int     irhs);
int  linear_solver_cpy_x (linear_solver  *ls, double_complex  *c, 
                                              const int     crhs,
                                              const int     irhs);

#endif

#endif
