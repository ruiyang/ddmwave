#if defined HXTWAVE_HAVE_MKL

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "linear_solver.h"

static inline void linear_solver_malloc (void *ptrToPtr, size_t size);
static inline void linear_solver_realloc(void* ptrToPtr, size_t size);
static inline void linear_solver_free   (void *ptrToPtr);

struct mkl_linear_solver_struct
{
  int    mtype;        // * Real complex unsymmetric matrix.
  int    nrhs;         // * Number of right hand sides.
  void  *pt[64];       // * Internal solver memory pointer pt,                  
                       // * 32-bit: int pt[64]; 64-bit: long int pt[64]         
                       // * or void *pt[64] should be OK on both architectures   
  int    iparm[64];
  int    maxfct, mnum, phase, error, msglvl;
  
//double_complex ddum; // * Double  dummy.
  double         ddum; // * Double  dummy.
  int	         idum; // * Integer dummy.

  int nnz;
  int n  ;           // * number of rows
  int *ia;           // * pointer
  int *ja;           // * pointer
  double_complex *a; // * pointer
  double_complex *x;
};

void linear_solver_create(linear_solver **ms)
{
  linear_solver_malloc(ms, sizeof(linear_solver));
  (*ms)->ia   = NULL;
  (*ms)->ja   = NULL;
  (*ms)->nnz  = 0;
  (*ms)->n    = 0;
  (*ms)->nrhs = 0;
  (*ms)->a    = NULL;
  (*ms)->x    = NULL;
}

void linear_solver_delete(linear_solver **ms)
{
  if((*ms)->n == 0)
  {
    linear_solver_free(ms);
    return;
  }

  linear_solver_free(&(*ms)-> x);
  (*ms)->ia   = NULL;
  (*ms)->ja   = NULL;
  (*ms)->a    = NULL;
  (*ms)->x    = NULL;
  (*ms)->nnz  = 0;
  (*ms)->n    = 0;
  (*ms)->nrhs = 0;

  linear_solver_free(ms);
}

int linear_solver_build(linear_solver *ms, int *ia, int  *ja, 
                                           double_complex *a, 
                                           const int     nnz,
                                           const int       n,
                                           const int    nrhs)
{
  if(n == 0) return 1; 

  ms->ia = ia;
  ms->ja = ja;
  ms->a  = a;
  linear_solver_malloc (&ms-> x, (n*nrhs)*sizeof(double_complex));

  ms->nnz = nnz;
  ms->n   = n;

  int i;
  for(i = 0; i < 64; i++) ms->pt[i]    = 0; 
  for(i = 0; i < 64; i++) ms->iparm[i] = 0; 

  ms->mtype = 13;   // * Real complex unsymmetric matrix.
  ms->nrhs  = nrhs; // * Number of right hand sides.

  /* -------------------------------------------------------------------- */
  /* ..  Setup Pardiso control parameters.                                */
  /* ---------------------------------------------------------------------*/    
  ms->iparm[0 ] =  1;       // * No solver default 
  ms->iparm[1 ] =  3;       // * Fill-in reordering from OPENMP 
  ms->iparm[3 ] =  0;       // * No iterative-direct algorithm 
  ms->iparm[4 ] =  0;       // * No user fill-in reducing permutation 
  ms->iparm[5 ] =  0;       // * Write solution into x 
  ms->iparm[6 ] =  0;       // * Not in use 
  ms->iparm[7 ] =  2;       // * Max numbers of iterative refinement steps 
  ms->iparm[8 ] =  0;       // * Not in use 
  ms->iparm[9 ] = 13;       // * Perturb the pivot elements with 1E-13 
  ms->iparm[10] =  1;       // * Use nonsymmetric permutation and scaling MPS 
  ms->iparm[11] =  0;       // * Conjugate/transpose solve 
  ms->iparm[12] =  1;       // * Maximum weighted matching algorithm is switched-on (default for non-symmetric) 
  ms->iparm[13] =  0;       // * Output: Number of perturbed pivots 
  ms->iparm[14] =  0;       // * Not in use 
  ms->iparm[15] =  0;       // * Not in use 
  ms->iparm[16] =  0;       // * Not in use 
  ms->iparm[17] = -1;       // * Output: Number of nonzeros in the factor LU 
  ms->iparm[18] = -1;       // * Output: Mflops for LU factorization 
  ms->iparm[19] =  0;       // * Output: Numbers of CG Iterations 
   
  ms->maxfct = 1; // * Maximum number of numerical factorizations. 
  ms->mnum   = 1; // * Which factorization to use.
  ms->msglvl = 0; // * No Print statistical information  
  ms->error  = 0; // * Initialize error flag 

  return 1;
}

int linear_solver_phase(linear_solver  *ms, const int    phase, 
                                            double_complex *bs,
                                            const int     crhs, 
                                            const int     irhs)
{
  if(ms->n == 0) return 1; 

  int *ia           = ms->ia;
  int *ja           = ms->ja;
  double_complex *a = ms-> a;
  int  n            = ms-> n;

  double_complex *x = &ms-> x[irhs*n];
  double_complex *b = &    bs[irhs*n];
  int          nrhs = crhs;

  ms->phase = phase;
  /* -------------------------------------------------------------------- */    
  /* ..  Analysis, Numerical Factorization,                               */ 
  /*     Solve ,   Iterative Refinement                                   */
  /* -------------------------------------------------------------------- */ 
  if(phase == 13)
  {    
    ms->iparm[7] = 1; 
    PARDISO (ms->pt, &ms->maxfct, &ms->mnum, &ms->mtype, &ms->phase,
             &n, a, ia, ja, &ms->idum, &nrhs,
             ms->iparm, &ms->msglvl, b, x, &ms->error);
    if(ms->error != 0) 
    {
      printf("\n[PARDISO]: ERROR as phase == 13: %d", ms->error);
      exit(1);
    }
    if(ms->msglvl == 1) printf("\n[PARDISO]: Reordering completed ... ");
    if(ms->msglvl == 1) printf("\n[PARDISO]: Number of nonzeros in factors  = %d", ms->iparm[17]);
    if(ms->msglvl == 1) printf("\n[PARDISO]: Number of factorization MFLOPS = %d", ms->iparm[18]);
    if(ms->msglvl == 1) printf("\n[PARDISO]: Factorization completed ...");
    if(ms->msglvl == 1) printf("\n[PARDISO]: Solution completed ...\n");
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Reordering and Symbolic Factorization.  This step also allocates */
  /*     all memory that is necessary for the factorization.              */
  /* -------------------------------------------------------------------- */ 
  if(phase == 11)
  {    
    PARDISO (ms->pt, &ms->maxfct, &ms->mnum, &ms->mtype, &ms->phase,
             &n, a, ia, ja, &ms->idum, &nrhs,
             ms->iparm, &ms->msglvl, &ms->ddum, &ms->ddum, &ms->error);
    if(ms->error != 0) 
    {
      printf("\n[PARDISO]: ERROR during symbolic factor: %d", ms->error);
      exit(1);
    }
    if(ms->msglvl == 1) printf("\n[PARDISO]: Reordering completed ... ");
    if(ms->msglvl == 1) printf("\n[PARDISO]: Number of nonzeros in factors  = %d", ms->iparm[17]);
    if(ms->msglvl == 1) printf("\n[PARDISO]: Number of factorization MFLOPS = %d", ms->iparm[18]);
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Numerical factorization.                                         */
  /* -------------------------------------------------------------------- */ 
  if(phase == 22)
  {    
    PARDISO (ms->pt, &ms->maxfct, &ms->mnum, &ms->mtype, &ms->phase,
             &n, a, ia, ja, &ms->idum, &nrhs,
             ms->iparm, &ms->msglvl, &ms->ddum, &ms->ddum, &ms->error);
    if(ms->error != 0) 
    {
      printf("\n[PARDISO]: ERROR during numerical factor: %d", ms->error);
      exit(2);
    }
    if(ms->msglvl == 1) printf("\n[PARDISO]: Factorization completed ...\n");
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Back substitution and iterative refinement.                      */
  /* -------------------------------------------------------------------- */    
  if(phase == 33)
  {    
    // * Max numbers of iterative refinement stems.
    ms->iparm[7] = 1; 
    PARDISO (ms->pt, &ms->maxfct, &ms->mnum, &ms->mtype, &ms->phase,
             &n, a, ia, ja, &ms->idum, &nrhs,
             ms->iparm, &ms->msglvl, b, x, &ms->error);
    if(ms->error != 0) 
    { 
      printf("\n[PARDISO]: ERROR during solution: %d", ms->error);
      exit(3);
    }
  }

  /* -------------------------------------------------------------------- */    
  /* ..  Termination and release of memory.                               */
  /* -------------------------------------------------------------------- */ 
  if(phase == -1)
  {    
    PARDISO (ms->pt, &ms->maxfct, &ms->mnum, &ms->mtype, &ms->phase,
             &n, a, ia, ja, &ms->idum, &nrhs,
             ms->iparm, &ms->msglvl, &ms->ddum, &ms->ddum, &ms->error);
  }
  return 1;
}

int linear_solver_cpy_x(linear_solver *ms, double_complex *c, 
                                           const int    crhs,
                                           const int    irhs)
{    
  if(ms->n == 0) return 1; 

  double_complex *x   = &ms->x[irhs* ms->n];
  double_complex *ptr = &    c[irhs* ms->n];
  memcpy(ptr, x, crhs*(ms->n)*sizeof(double_complex));

  return 1;
}

static inline void linear_solver_malloc(void *ptrToPtr, size_t size)
{
  void **p = (void**) ptrToPtr;
  *p = malloc(size);
  if(*p==NULL)
  {
    printf("linear solver malloc failed at ");
   fprintf(stderr, "%s, line %d.\n",__FILE__, __LINE__);
    exit(1);
  }
}

static inline void linear_solver_realloc(void* ptrToPtr, size_t size)
{
  void** p = (void**)ptrToPtr;
  void* newptr = realloc(*p, size);
  if (newptr==NULL && *p!=NULL && size!=0)
  {
    printf("linear solver realloc failed at ");
   fprintf(stderr, "%s, line %d.\n",__FILE__, __LINE__);
    exit(1);
  }
  *p = newptr;
}

static inline void linear_solver_free(void *ptrToPtr)
{
  void **p = (void**) ptrToPtr;
  free(*p);
  *p=NULL;
}

#endif
