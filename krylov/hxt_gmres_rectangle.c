#include "hxt_gmres1.h"

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                          GMRES DATA                        == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static double_complex **   mptrs;
static double         *   norm_b;
static double         *   norm_r;
static double         * residual;
static double_complex *ss_origin;
static double_complex *cs_origin;
static double_complex *sn_origin;
static double_complex *hh_origin; // * hessenburg matrix

#if !defined(OMP_GMRES_SWEEP_NO_MACROS)
  #define SS_ORIGIN(l, a)    (ss_origin[(l)*(niters+1)            + (a)                 ])
  #define CS_ORIGIN(l, a)    (cs_origin[(l)*(niters+1)            + (a)                 ])
  #define SN_ORIGIN(l, a)    (sn_origin[(l)*(niters+1)            + (a)                 ])
  #define HH_ORIGIN(l, a, b) (hh_origin[(l)*(niters+1)*(niters+1) + (a)*(niters+1) + (b)])
#endif

typedef struct exchangeStruct
{
  bool me;
  double_complex  **your;
  double_complex  **ptrs;
  double_complex ***ptps;
} exchange;

static HXTStatus gmres_exchange_set(exchange *ex)
{
  const int mthreads = omp_get_max_threads();
  const int threadid = omp_get_thread_num ();
  int mt;

  for(mt = 0; mt < mthreads; mt++)
  {
    void **px = (void**)  &ex->ptrs[mt];
    mptrs[threadid*mthreads + mt] = *px;
  }
  #pragma omp barrier
  for(mt = 0; mt < mthreads; mt++) 
  {
    void **px = (void**)  &ex->your[mt];
    *px = mptrs[mt*mthreads + threadid];
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

static HXTStatus gmres_exchange_do (exchange *ex)
{
  const int mthreads = omp_get_max_threads();
  int mt;

  #pragma omp barrier
  if(ex->me == true)
  {
     for(mt = 0; mt < mthreads; mt++) 
       if(ex->ptps[mt] != NULL) *ex->ptps[mt] = ex->your[mt];

     ex->me = false;
  }
  else
  {
     for(mt = 0; mt < mthreads; mt++)
       if(ex->ptps[mt] != NULL) *ex->ptps[mt] = ex->ptrs[mt];

     ex->me =  true;
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                       GMRES FUNCTION                       == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static inline int            decomposed_gmres_zaxpy (const int             irhs,
                                                           double_complex    *y,
                                                     const size_t           y_n,
                                                     const double_complex    *x,
                                                     const size_t           x_n,
                                                     const double_complex alpha);
static inline double         decomposed_gmres_dznrm2(const int             irhs,
                                                     const double_complex    *x,
                                                     const size_t           x_n);
static inline int            decomposed_gmres_zscal (const int             irhs,
                                                           double_complex    *s,
                                                     const size_t           s_n,
                                                     const double_complex alpha);
static inline double_complex decomposed_gmres_zdotc (const int             irhs,
                                                     const double_complex  *x  ,
                                                     const size_t           x_n,
                                                     const double_complex  *y  ,
                                                     const size_t           y_n);

static void decomposed_gmres_update(double_complex **grs, const size_t *grs_n, const size_t grs_nn,
                                    double_complex **vrs, const size_t *vrs_n, const size_t vrs_nn,
                                    const size_t    irhs, const size_t  nrhs     ,
                                    const size_t    i   , const int    niters)
{
  size_t et;

  for(et = 0; et < vrs_nn; et++)
    decomposed_gmres_zscal(irhs, grs[et], grs_n[et], 0.);

  double_complex *y  =  malloc ( (niters+1)*sizeof(double_complex) );
  memcpy(y, &SS_ORIGIN(irhs, 0), (niters+1)*sizeof(double_complex) );

  int  k, m;
  for (k = i; k >= 0; k--)
  {
    y[k] /= HH_ORIGIN(irhs, k, k);
    for(m = k - 1; m >= 0; m--)
    {
      y[m] -= HH_ORIGIN(irhs, m, k) * y[k];
    }
  }

  for(k = 0; k <= i; k++)
  {
    for(et = 0; et < vrs_nn; et++)
    {
      double_complex *ivrs = &vrs[et][ k * vrs_n[et] * nrhs ];
      decomposed_gmres_zaxpy(irhs, grs[et], grs_n[et], ivrs, vrs_n[et], y[k]);
    }
  }
  free(y);
}

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                         FREE MATRIX                        == * //
// * ======                                                    ====== * //
// * ================================================================ * //
/*
static double_complex fZero(const double *xyz)
{
  double_complex re = 0.;
  return re;
}
*/
static HXTStatus multMatrix(HXTSystem    *sys, HXTSystem   **bcSys,
                            const size_t  itx, double_complex **xs, const size_t *xs_n,
                            const size_t  ity, double_complex **ys, const size_t *ys_n,
                            exchange *ex)
{
  const size_t nrhs = sys->nrhs;

  size_t l, m;

  for(l = 0; l < nrhs; l++)
  {
    HXT_CHECK( hxtSystemRHSZero(sys, l) );
    for(m = 0; m < 4; m++)
    {
      const size_t    ins_n =  xs_n[m];
      double_complex *ins   = &xs  [m][ itx * ins_n * nrhs ];

      HXT_CHECK( hxtSystemArrToSol(bcSys[m], l , ins, ins_n, 0.             ) );
      HXT_CHECK( hxtSystemBMxToRHS(sys     , 0., bcSys[m],   1., bcSys[m], l) );
    }
  //HXT_CHECK( hxtSystemCSTR (sys, l, source, fZero) ); 
    HXT_CHECK( hxtSystemPhase(sys, l, 1, 33        ) );

    for(m = 0; m < 4; m++)
    {
      const size_t    ins_n =  xs_n[m];
      double_complex *ins   = &xs  [m][ itx * ins_n * nrhs ];

      HXT_CHECK( hxtSystemRHSZero (bcSys[m], l) );

      HXT_CHECK( hxtSystemCMxToRHS(bcSys[m], 0., bcSys[m],   2., sys     , l) );
      HXT_CHECK( hxtSystemArrToSol(bcSys[m], l , ins, ins_n, 0.             ) );
      HXT_CHECK( hxtSystemBMxToRHS(bcSys[m], 0., bcSys[m],  -1., bcSys[m], l) );

      HXT_CHECK( hxtSystemPhase(bcSys[m], l, 1, 33) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- exchange functions                                         -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( gmres_exchange_do ( ex) );

  for(l = 0; l < nrhs; l++)
  {
    for(m = 0; m < 4; m++)
    {
      const size_t    outs_n =  ys_n[m];
      double_complex *outs   = &ys  [m][ ity * outs_n * nrhs ];

      HXT_CHECK( hxtSystemSolToArr(bcSys[m], l, outs, outs_n, 0.) );
    }
  }

  return HXT_STATUS_OK;
}



HXTStatus hxtGMRESRectangle(HXTSystem *sys, HXTSystem **bcSys,
		            const int   Ix, const int      Iy,
			    const int   Nx, const int      Ny,
			                    const int  niters)
{
  if(niters == 0) return HXT_STATUS_OK;

  int i = 0;

  size_t j, l, m;

  const int threadid = omp_get_thread_num ();
  const int mthreads = omp_get_max_threads();

  const size_t nrhs = sys->nrhs;

  // * ---------------------------------------------------------------- * //
  // * ------                                                    ------ * //
  // * --                         EXCHANGE DATA                      -- * //
  // * ------                                                    ------ * //
  // * ---------------------------------------------------------------- * //
  #pragma omp single
  {
    mptrs = malloc( mthreads*mthreads*sizeof(double_complex*) );
  }
  #pragma omp barrier

  exchange ex;

  ex.me   = true;
  ex.your = NULL;
  ex.ptrs = NULL;
  ex.ptps = NULL;
  ex.your = malloc( mthreads*sizeof(double_complex* ) );
  ex.ptrs = malloc( mthreads*sizeof(double_complex* ) );
  ex.ptps = malloc( mthreads*sizeof(double_complex**) );

  for(int mt = 0; mt < mthreads; mt++) ex.your[mt] = NULL;
  for(int mt = 0; mt < mthreads; mt++) ex.ptrs[mt] = NULL;
  for(int mt = 0; mt < mthreads; mt++) ex.ptps[mt] = NULL;

  // * ---------------------------------------------------------------- * //
  // * ------                                                    ------ * //
  // * --                   STORE EXCHANGE POINTERS                  -- * //
  // * ------                                                    ------ * //
  // * ---------------------------------------------------------------- * //
  const int Ix_DOM = Ix;
  const int Iy_DOM = Iy;
  const int Nx_DOM = Nx;
  const int Ny_DOM = Ny;

  if(Iy_DOM     > 0      )
  {
    ex.ptrs[ threadid-1      ] =  bcSys[0]->x;
    ex.ptps[ threadid-1      ] = &bcSys[0]->x;
  }
  if(Ix_DOM + 1 < Nx_DOM )
  {
    ex.ptrs[ threadid+Ny_DOM ] =  bcSys[1]->x;
    ex.ptps[ threadid+Ny_DOM ] = &bcSys[1]->x;
  }
  if(Iy_DOM + 1 < Ny_DOM )
  {
    ex.ptrs[ threadid+1      ] =  bcSys[2]->x;
    ex.ptps[ threadid+1      ] = &bcSys[2]->x;
  }
  if(Ix_DOM     > 0      )
  {
    ex.ptrs[ threadid-Ny_DOM ] =  bcSys[3]->x;
    ex.ptps[ threadid-Ny_DOM ] = &bcSys[3]->x;
  }
  HXT_CHECK( gmres_exchange_set(&ex) );

  // * ---------------------------------------------------------------- * //
  // * ------                                                    ------ * //
  // * --                       GMRES PARAMETERS                     -- * //
  // * ------                                                    ------ * //
  // * ---------------------------------------------------------------- * //
  #pragma omp single
  {
    norm_b    = calloc(nrhs, sizeof(double));
    norm_r    = calloc(nrhs, sizeof(double));
    residual  = calloc(nrhs, sizeof(double));

    ss_origin = calloc(nrhs*(niters+1)            ,sizeof(double_complex) );
    cs_origin = calloc(nrhs*(niters+1)            ,sizeof(double_complex) );
    sn_origin = calloc(nrhs*(niters+1)            ,sizeof(double_complex) );
    hh_origin = calloc(nrhs*(niters+1)*(niters+1) ,sizeof(double_complex) );
  }
  #pragma omp barrier
  double tolerance = 10e-7;

  double_complex *grs[4];
  double_complex *brs[4];
  double_complex *xrs[4];
  double_complex *rrs[4];
  double_complex *vrs[4];
  double_complex *zrs[4];
  double_complex *wrs[4];

  size_t grs_n[4];
  size_t brs_n[4];
  size_t xrs_n[4];
  size_t rrs_n[4];
  size_t vrs_n[4];
  size_t zrs_n[4];
  size_t wrs_n[4];

  for(m = 0; m < 4; m++)
  {
    const int nUnknowns = bcSys[m]->nUnknowns;

    grs  [m] = NULL;
    brs  [m] = NULL;
    xrs  [m] = NULL;
    rrs  [m] = NULL;
    vrs  [m] = NULL;
    zrs  [m] = NULL;
    wrs  [m] = NULL;

    grs  [m] = calloc(nUnknowns * nrhs             , sizeof(double_complex) );
    brs  [m] = calloc(nUnknowns * nrhs             , sizeof(double_complex) );
    xrs  [m] = calloc(nUnknowns * nrhs             , sizeof(double_complex) );
    rrs  [m] = calloc(nUnknowns * nrhs             , sizeof(double_complex) );
    vrs  [m] = calloc(nUnknowns * nrhs * (niters+1), sizeof(double_complex) );
    zrs  [m] = calloc(nUnknowns * nrhs * (niters+1), sizeof(double_complex) );
    wrs  [m] = calloc(nUnknowns * nrhs             , sizeof(double_complex) );

    grs_n[m] = nUnknowns ;
    brs_n[m] = nUnknowns ;
    xrs_n[m] = nUnknowns ;
    rrs_n[m] = nUnknowns ;
    vrs_n[m] = nUnknowns ;
    zrs_n[m] = nUnknowns ;
    wrs_n[m] = nUnknowns ;
  }

  // * ---------------------------------------------------------------- * //
  // * ------                                                    ------ * //
  // * --                             START                          -- * //
  // * ------                                                    ------ * //
  // * ---------------------------------------------------------------- * //

  // * ---------------------------------------------------------------- * //
  // * -- get w_{n+1}                                                -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemPhase(sys, 0, nrhs,  33 ) );

  // * ---------------------------------------------------------------- * //
  // * -- update law g_{n+1} = -g_{n} + 2 * S(v_{n+1})               -- * //
  // * -- here       g_{n  } =  0                                    -- * //
  // * -- thus       b       =  0     + 2 * S(w_{n+1})               -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(m = 0; m < 4; m++)
    {
      HXT_CHECK( hxtSystemRHSZero (bcSys[m], l) );
      HXT_CHECK( hxtSystemCMxToRHS(bcSys[m], 0., bcSys[m], 2., sys, l) );
      HXT_CHECK( hxtSystemPhase   (bcSys[m], l, 1, 33) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- exchange functions                                         -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( gmres_exchange_do (&ex) );

  // * ---------------------------------------------------------------- * //
  // * -- SET B VEC VALUES                                           -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(m = 0; m < 4; m++)
    {
      HXT_CHECK( hxtSystemSolToArr(bcSys[m], l, brs[m], brs_n[m], 0.) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- SET RHS ZERO                                               -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++) HXT_CHECK( hxtSystemRHSZero(sys, l) );

  // * ---------------------------------------------------------------- * //
  // * -- r0 = A * x0                                                -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( multMatrix(sys, bcSys, 0, xrs, xrs_n, 0, rrs, rrs_n, &ex) );

  // * ---------------------------------------------------------------- * //
  // * -- r0 = b - (I - A) * x0 ---> r0 = b - x0 + A * x0            -- * //
  // * ---------------------------------------------------------------- * //
  const double_complex cone   =  1.;
  const double_complex cminus = -1.;

  for(l = 0; l < nrhs; l++)
  {
    for(m = 0; m < 4; m++)
    {
      decomposed_gmres_zaxpy(l, rrs[m], rrs_n[m], brs[m], brs_n[m], cone  );
      decomposed_gmres_zaxpy(l, rrs[m], rrs_n[m], xrs[m], xrs_n[m], cminus);
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- calculate norm_r and norm_b                                -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(m = 0; m < 4; m++)
    {
      double tmp_b = 0.;
      double tmp_r = 0.;
      tmp_b += decomposed_gmres_dznrm2(l, brs[m], brs_n[m])*
               decomposed_gmres_dznrm2(l, brs[m], brs_n[m]);
      tmp_r += decomposed_gmres_dznrm2(l, rrs[m], rrs_n[m])*
               decomposed_gmres_dznrm2(l, rrs[m], rrs_n[m]);
      #pragma omp atomic update
      norm_b[l] += tmp_b;
      #pragma omp atomic update
      norm_r[l] += tmp_r;
    }
  }
  #pragma omp barrier

  // * ---------------------------------------------------------------- * //
  // * -- ss[0] = norm_r                                             -- * //
  // * ---------------------------------------------------------------- * //
  #pragma omp single
  {
    for(l = 0; l < nrhs; l++)
    {
      norm_b[l] = sqrt(norm_b[l]);
      norm_r[l] = sqrt(norm_r[l]);

      if(norm_b[l] == 0.) norm_b[l] = 1.;

      residual [l] = norm_r[l] / norm_b[l];

      SS_ORIGIN(l, 0) = norm_r[l];
    }
    printf("norm_b[0]     : %f\n",    norm_b[0]);
    printf("norm_r[0]     : %f\n",    norm_r[0]);
  }
  #pragma omp barrier

  // * ---------------------------------------------------------------- * //
  // * -- LOOP OF GMRES                                              -- * //
  // * ---------------------------------------------------------------- * //
  size_t k  = 0;
         j  = 1;
  while( j <= niters)
  {
    // * ---------------------------------------------------------------- * //
    // * -- v[0] = v[0] + r / norm_r                                   -- * //
    // * ---------------------------------------------------------------- * //
    for(l = 0; l < nrhs; l++)
    {
      for(m = 0; m < 4; m++)
      {
        const double_complex rec_r = 1./norm_r[l];
        decomposed_gmres_zaxpy(l, &vrs[m][0], vrs_n[m], rrs[m], rrs_n[m], rec_r );
      }
    }
    #pragma omp barrier

    for(i = 0; i < niters && j <= niters; i++, j++)
    {
      // * ---------------------------------------------------------------- * //
      // * -- z[i] = M^{-1} v[i]                                         -- * //
      // * ---------------------------------------------------------------- * //
      for( l = 0; l < nrhs; l++)
      {
        for(m = 0; m < 4; m++)
        {
          double_complex *ivrs = &vrs[m][i * vrs_n[m] * nrhs];
          double_complex *izrs = &zrs[m][i * zrs_n[m] * nrhs];
          decomposed_gmres_zaxpy(l, izrs, zrs_n[m], ivrs, vrs_n[m], 1. );
        }
      }

      // * ---------------------------------------------------------------- * //
      // * -- w = A v[i]                                                 -- * //
      // * ---------------------------------------------------------------- * //
      HXT_CHECK( multMatrix(sys, bcSys, i, zrs, zrs_n, 0, wrs, wrs_n, &ex) );

      // * ---------------------------------------------------------------- * //
      // * --         w = v[i] - w                                       -- * //
      // * -- that is w =  (I- A)v[i]                                    -- * //
      // * ---------------------------------------------------------------- * //
      for( l = 0; l < nrhs; l++)
      {
        for(m = 0; m < 4; m++)
        {
          double_complex *izrs = &zrs[m][i * zrs_n[m] * nrhs];

          decomposed_gmres_zscal(l, wrs[m], wrs_n[m],                 cminus);
          decomposed_gmres_zaxpy(l, wrs[m], wrs_n[m], izrs, zrs_n[m], cone  );
        }
      }

      for(l = 0; l < nrhs; l++)
      {
        for(k = 0; k <= i; k++)
        {
          // * ---------------------------------------------------------------- * //
          // * -- h[k][i] = zdotc(v[k], w)                                   -- * //
          // * ---------------------------------------------------------------- * //
          for(m = 0; m < 4; m++)
          {
            double_complex *ivrs = &vrs[m][k * vrs_n[m] * nrhs];

            double_complex tmp = decomposed_gmres_zdotc(l, ivrs, vrs_n[m], wrs[m], wrs_n[m]);

            double* complex_h = (double*)(&HH_ORIGIN(l, k, i));
            #pragma omp atomic update
            complex_h[0] += creal(tmp);
            #pragma omp atomic update
            complex_h[1] += cimag(tmp);
          }
          #pragma omp barrier

          // * ---------------------------------------------------------------- * //
          // * -- w = w - v[k] * h[k][i];                                    -- * //
          // * ---------------------------------------------------------------- * //
          for(m = 0; m < 4; m++)
          {
            double_complex *ivrs = &vrs[m][k * vrs_n[m] * nrhs];

            double_complex  tmp_hh = - HH_ORIGIN(l, k, i);

            decomposed_gmres_zaxpy(l, wrs[m], wrs_n[m], ivrs, vrs_n[m], tmp_hh);
          }
          #pragma omp barrier
        }
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- h[i+1][i] = dznrm2(w)                                      -- * //
      // * ---------------------------------------------------------------- * //
      for(l = 0; l < nrhs; l++)
      {
        for(m = 0; m < 4; m++)
        {
          double sum = decomposed_gmres_dznrm2(l, wrs[m], wrs_n[m]) *
                       decomposed_gmres_dznrm2(l, wrs[m], wrs_n[m]) ;

          double* complex_h = (double*)(&HH_ORIGIN(l, i+1, i));
          #pragma omp atomic update
          complex_h[0] += sum;
        }
        #pragma omp barrier

        #pragma omp single
        HH_ORIGIN(l, i+1, i) = csqrt( HH_ORIGIN(l, i+1, i) );
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- v[i+1] = v[i+1] + w / h[i+1][i]                            -- * //
      // * ---------------------------------------------------------------- * //
      for(l = 0; l < nrhs; l++)
      {
        for(m = 0; m < 4; m++)
        {
          double_complex *ivrs = &vrs[m][ (i+1) * vrs_n[m] * nrhs];

          double_complex  rec_hh = 1. / HH_ORIGIN(l, i+1, i);

          decomposed_gmres_zaxpy(l, ivrs, vrs_n[m], wrs[m], wrs_n[m], rec_hh);
        }
        #pragma omp barrier
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- GIVEN ROTATION                                             -- * //
      // * ---------------------------------------------------------------- * //
      #pragma omp single
      {
        printf("    iteration %lu, ", j);
        for(l = 0; l < nrhs; l++)
        {
          for(k = 0; k < i; k++)
          {
            hxtzarot(&HH_ORIGIN(l, k, i), &HH_ORIGIN(l, k+1, i), &CS_ORIGIN(l, k), &SN_ORIGIN(l, k));
          }
          hxtzgrot(&HH_ORIGIN(l, i, i), &HH_ORIGIN(l, i+1, i), &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));
          hxtzarot(&HH_ORIGIN(l, i, i), &HH_ORIGIN(l, i+1, i), &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));
          hxtzarot(&SS_ORIGIN(l, i)   , &SS_ORIGIN(l, i+1)   , &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));

          // * residual = cabs(ss[i+1]) / norm_b
          residual[l] = cabs ( SS_ORIGIN(l, i+1) ) / norm_b[l];
          printf("res %lu: %.3e  ", l, residual[l]);
        }
        printf("\n");
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- UPDATE                                                     -- * //
      // * ---------------------------------------------------------------- * //
      if(residual[0] < tolerance)
      {
        for(l = 0; l < nrhs; l++)
        {
          HXT_CHECK( hxtSystemRHSZero(sys, l) );

          decomposed_gmres_update(grs, grs_n, 4,
                                  zrs, zrs_n, 4,
                                  l, nrhs, i, niters);

          for(m = 0; m < 4; m++)
          {
            HXT_CHECK( hxtSystemArrToSol(bcSys[m], l, grs  [m],     grs_n[m], 0.) );
            HXT_CHECK( hxtSystemBMxToRHS(sys     , 0, bcSys[m], 1., bcSys[m], l ) );
          }
        }

        // * ---------------------------------------------------------------- * //
        // * -- DELETE                                                     -- * //
        // * ---------------------------------------------------------------- * //
        #pragma omp barrier
        for(m = 0; m < 4; m++)
        {
	  free(grs[m]);
	  free(brs[m]);
	  free(xrs[m]);
	  free(rrs[m]);
	  free(vrs[m]);
	  free(zrs[m]);
	  free(wrs[m]);

          grs  [m] = NULL;
          brs  [m] = NULL;
          xrs  [m] = NULL;
          rrs  [m] = NULL;
          vrs  [m] = NULL;
          zrs  [m] = NULL;
          wrs  [m] = NULL;
    
          grs_n[m] =    0;
          brs_n[m] =    0;
          xrs_n[m] =    0;
          rrs_n[m] =    0;
          vrs_n[m] =    0;
          zrs_n[m] =    0;
          wrs_n[m] =    0;
        }
        free(ex.ptrs);
        free(ex.ptps);
        free(ex.your);
        ex.me =  true;

        #pragma omp single
        {
          free(mptrs    );

	  free(norm_b   );
	  free(norm_r   );
	  free(residual );

	  free(ss_origin);
	  free(cs_origin);
	  free(sn_origin);
	  free(hh_origin);
        }
        #pragma omp barrier

        return HXT_STATUS_OK;
      }


    }
  }
  // * ---------------------------------------------------------------- * //
  // * -- UPDATE                                                     -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    HXT_CHECK( hxtSystemRHSZero(sys, l) );

    decomposed_gmres_update(grs, grs_n, 4,
                            zrs, zrs_n, 4,
                            l, nrhs, i-1, niters);

    for(m = 0; m < 4; m++)
    {
      HXT_CHECK( hxtSystemArrToSol(bcSys[m], l, grs  [m],     grs_n[m], 0.) );
      HXT_CHECK( hxtSystemBMxToRHS(sys     , 0, bcSys[m], 1., bcSys[m], l ) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- DELETE                                                     -- * //
  // * ---------------------------------------------------------------- * //
  #pragma omp barrier
  for(m = 0; m < 4; m++)
  {
    free(grs[m]);
    free(brs[m]);
    free(xrs[m]);
    free(rrs[m]);
    free(vrs[m]);
    free(zrs[m]);
    free(wrs[m]);

    grs  [m] = NULL;
    brs  [m] = NULL;
    xrs  [m] = NULL;
    rrs  [m] = NULL;
    vrs  [m] = NULL;
    zrs  [m] = NULL;
    wrs  [m] = NULL;
    
    grs_n[m] =    0;
    brs_n[m] =    0;
    xrs_n[m] =    0;
    rrs_n[m] =    0;
    vrs_n[m] =    0;
    zrs_n[m] =    0;
    wrs_n[m] =    0;
  }
  free(ex.ptrs);
  free(ex.ptps);
  free(ex.your);
  ex.me =  true;

  #pragma omp single
  {
    free(mptrs    );
  
    free(norm_b   );
    free(norm_r   );
    free(residual );
  
    free(ss_origin);
    free(cs_origin);
    free(sn_origin);
    free(hh_origin);
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

static inline int decomposed_gmres_zaxpy(const int             irhs,
                                               double_complex    *y,
                                         const size_t           y_n,
                                         const double_complex    *x,
                                         const size_t           x_n,
                                         const double_complex alpha)
{
  if(y_n == 0) return 1;
  hxtzaxpy(&y[irhs*y_n], &x[irhs*y_n], alpha, y_n);
  return 0;
}

static inline double decomposed_gmres_dznrm2(const int             irhs,
                                             const double_complex    *x,
                                             const size_t           x_n)
{
  if(x_n == 0) return 0.;
  else return hxtdznrm2(&x[irhs*x_n], x_n);
}

static inline int    decomposed_gmres_zscal (const int             irhs,
                                                   double_complex    *s,
                                             const size_t           s_n,
                                             const double_complex alpha)
{
  if(s_n == 0) return 1;
  hxtzscal(&s[irhs*s_n], alpha, s_n);
  return 0;
}

static inline double_complex decomposed_gmres_zdotc(const int             irhs,
                                                    const double_complex  *x  ,
                                                    const size_t           x_n,
                                                    const double_complex  *y  ,
                                                    const size_t           y_n)
{
  double_complex zero = 0.;
  if(x_n == 0) return zero;
  else return hxtzdotc(&x[irhs*x_n], &y[irhs*x_n], x_n);
}
