#ifndef HXTWAVE_GMRES2_H
#define HXTWAVE_GMRES2_H

#include "hxt_system.h"

typedef struct HXTGMRES2Struct
{
  int threadid;  // * equal to Iy
  int mthreads;  // * equal to Ny

  int Nx;
  int Ny;

  HXTSystem **  sys;
  HXTSystem **bcSys;

  int iters_n;

  size_t edges_n;
  size_t    nrhs;

  bool me;
  double_complex  **your;
  double_complex  **ptrs;
  double_complex ***ptps;

  double_complex **grs;
  double_complex **brs;
  double_complex **xrs;
  double_complex **rrs;
  double_complex **vrs;
  double_complex **zrs;
  double_complex **wrs;

  size_t *grs_n;
  size_t *brs_n;
  size_t *xrs_n;
  size_t *rrs_n;
  size_t *vrs_n;
  size_t *zrs_n;
  size_t *wrs_n;

  HXTGroup **diric  ;
  size_t     diric_n;

} HXTGMRES2;

HXTStatus hxtGMRES2Create(HXTGMRES2 **kry               );
HXTStatus hxtGMRES2Solve (HXTGMRES2  *kry, const int pre);
HXTStatus hxtGMRES2Delete(HXTGMRES2 **kry               );

HXTStatus hxtGMRES2Build (HXTGMRES2  *  kry, size_t iters_n, 
                          HXTSystem **  sys, size_t    nrhs, 
			  HXTSystem **bcSys, size_t edges_n, 
			  HXTGroup  **diric, size_t diric_n, 
		  	  const int   Nx, const int      Ny);

HXTStatus SGSFwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);
HXTStatus SGSBwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);
HXTStatus SWPFwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);
HXTStatus SWPBwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);

#endif
