#include "hxt_gmres2.h"

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                          GMRES DATA                        == * //
// * ======                                                    ====== * //
// * ================================================================ * //
#define restart 20

static double_complex **   mptrs;
static double         *   norm_b;
static double         *   norm_r;
static double         * residual;
static double_complex *ss_origin;
static double_complex *cs_origin;
static double_complex *sn_origin;
static double_complex *hh_origin; // * hessenburg matrix

#if !defined(OMP_GMRES_SWEEP_NO_MACROS)
  #define SS_ORIGIN(l, a)    (ss_origin[(l)*(restart+1)             + (a)                  ])
  #define CS_ORIGIN(l, a)    (cs_origin[(l)*(restart+1)             + (a)                  ])
  #define SN_ORIGIN(l, a)    (sn_origin[(l)*(restart+1)             + (a)                  ])
  #define HH_ORIGIN(l, a, b) (hh_origin[(l)*(restart+1)*(restart  ) + (a)*(restart  ) + (b)])
#endif

static HXTStatus gmres_exchange_set(HXTGMRES2 *kry)
{
  const int Nx = kry->Nx;
  const int Ny = kry->Ny;
  const int Nt = Nx * Ny;
        int Ix =       0;
  const int Iy = omp_get_thread_num();

  int j; 

  for(Ix = 0; Ix < Nx; Ix++)
  {
    const int It = Ix*Ny + Iy;

    for(j = 0; j < Nt; j++)
    {
      void **px = (void**)  &kry->ptrs[j + Nt*Ix];
      mptrs[It*Nt + j ] = *px;
    }
  }
  #pragma omp barrier
  for(Ix = 0; Ix < Nx; Ix++)
  {
    const int It = Ix*Ny + Iy;

    for(j = 0; j < Nt; j++) 
    {
      void **px = (void**)  &kry->your[j + Nt*Ix];
      *px = mptrs[j*Nt + It ];
    }
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

static HXTStatus gmres_exchange_do (HXTGMRES2 *kry)
{
  const int Nx = kry->Nx;
  const int Ny = kry->Ny;
  const int Nt = Nx * Ny;

  int j;

  #pragma omp barrier
  if(kry->me == true)
  {
     for(j = 0; j < Nt * Nx; j++) 
       if(kry->ptps[j] != NULL) *kry->ptps[j] = kry->your[j];

     kry->me = false;
  }
  else
  {
     for(j = 0; j < Nt * Nx; j++) 
       if(kry->ptps[j] != NULL) *kry->ptps[j] = kry->ptrs[j];

     kry->me =  true;
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                       GMRES2 FUNCTION                      == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static inline int            decomposed_gmres_zaxpy (const int             irhs,
                                                           double_complex    *y,
                                                     const size_t           y_n,
                                                     const double_complex    *x,
                                                     const size_t           x_n,
                                                     const double_complex alpha);
static inline double         decomposed_gmres_dznrm2(const int             irhs,
                                                     const double_complex    *x,
                                                     const size_t           x_n);
static inline int            decomposed_gmres_zscal (const int             irhs,
                                                           double_complex    *s,
                                                     const size_t           s_n,
                                                     const double_complex alpha);
static inline double_complex decomposed_gmres_zdotc (const int             irhs,
                                                     const double_complex  *x  ,
                                                     const size_t           x_n,
                                                     const double_complex  *y  ,
                                                     const size_t           y_n);

static void decomposed_gmres_update(double_complex **grs, const size_t *grs_n, const size_t grs_nn,
                                    double_complex **vrs, const size_t *vrs_n, const size_t vrs_nn,
                                    const size_t    irhs, const size_t   nrhs,
                                    const size_t    i   , const int    length)
{
  size_t et;

  //for(et = 0; et < vrs_nn; et++)
  //  decomposed_gmres_zscal(irhs, grs[et], grs_n[et], 0.);

  double_complex *y  =  malloc ( (restart+1)*sizeof(double_complex) );
  memcpy(y, &SS_ORIGIN(irhs, 0), (restart+1)*sizeof(double_complex) );

  int  k, m;
  for (k = i; k >= 0; k--)
  {
    y[k] /= HH_ORIGIN(irhs, k, k);
    for(m = k - 1; m >= 0; m--)
    {
      y[m] -= HH_ORIGIN(irhs, m, k) * y[k];
    }
  }

  for(k = 0; k <= i; k++)
  {
    for(et = 0; et < vrs_nn; et++)
    {
      double_complex *ivrs = &vrs[et ][ k * vrs_n[et] * nrhs ];
      decomposed_gmres_zaxpy(irhs, grs[et], grs_n[et], ivrs, vrs_n[et], y[k]);
    }
  }
  free(y);
}

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                         FREE MATRIX                        == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static double_complex fZero(const double *xyz)
{
  double_complex re = 0.;
  return re;
}

static HXTStatus multMatrix(HXTGMRES2   *kry,
                            const size_t itx, double_complex **xs, const size_t *xs_n,
                            const size_t ity, double_complex **ys, const size_t *ys_n)
{
  HXTSystem **  sys = kry->  sys;
  HXTSystem **bcSys = kry->bcSys;

  const size_t nrhs    = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  const int Nx = kry->Nx;
        int Ix =       0;

  size_t l, m, i;
  

  for(l = 0; l < nrhs; l++)
  {
    for(Ix = 0; Ix < Nx; Ix++)
    {
      HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );
      for(m = 0; m < edges_n; m++)
      {
        const size_t p = Ix*edges_n + m;

        const size_t    ins_n =  xs_n[p];
        double_complex *ins   = &xs  [p][ itx * ins_n * nrhs ];
  
        HXT_CHECK( hxtSystemArrToSol(bcSys[p] , l , ins, ins_n, 0.             ) );
        HXT_CHECK( hxtSystemBMxToRHS(  sys[Ix], 0., bcSys[p],   1., bcSys[p], l) );
      }
      for(i = 0; i < kry->diric_n; i++) 
        HXT_CHECK( hxtSystemCSTR (sys[Ix], l, kry->diric[i*Nx+Ix], fZero) ); 

      HXT_CHECK( hxtSystemPhase(sys[Ix], l, 1, 33 ) );
  
      for(m = 0; m < 4; m++)
      {
        const size_t p = Ix*edges_n + m;

        const size_t    ins_n =  xs_n[p];
        double_complex *ins   = &xs  [p][ itx * ins_n * nrhs ];
  
        HXT_CHECK( hxtSystemRHSZero (bcSys[p], l) );
  
        HXT_CHECK( hxtSystemCMxToRHS(bcSys[p], 0., bcSys[p],   2.,  sys[Ix], l) );
        HXT_CHECK( hxtSystemArrToSol(bcSys[p], l , ins, ins_n, 0.             ) );
        HXT_CHECK( hxtSystemBMxToRHS(bcSys[p], 0., bcSys[p],  -1., bcSys[p], l) );
  
        HXT_CHECK( hxtSystemPhase(bcSys[p], l, 1, 33) );
      }
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- exchange functions                                         -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( gmres_exchange_do (kry) );

  for(l = 0; l < nrhs; l++)
  {
    for(Ix = 0; Ix < Nx; Ix++)
    {
      for(m = 0; m < edges_n; m++)
      {
        const size_t p = Ix*edges_n + m;

        const size_t    outs_n =  ys_n[p];
        double_complex *outs   = &ys  [p][ ity * outs_n * nrhs ];
  
        HXT_CHECK( hxtSystemSolToArr(bcSys[p], l, outs, outs_n, 0.) );
      }
    }
  }

  return HXT_STATUS_OK;
}



HXTStatus hxtGMRES2Create(HXTGMRES2 **kry)
{
  HXT_CHECK( hxtMalloc(kry, sizeof(HXTGMRES2)) );
  if((*kry) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTGMRES2 *ptr = *kry;

  ptr->threadid = -1;
  ptr->mthreads = -1;

  ptr->edges_n = 0;

  ptr->me   = true;
  ptr->your = NULL;
  ptr->ptrs = NULL;
  ptr->ptps = NULL;

  ptr->grs = NULL;
  ptr->brs = NULL;
  ptr->xrs = NULL;
  ptr->rrs = NULL;
  ptr->vrs = NULL;
  ptr->zrs = NULL;
  ptr->wrs = NULL;

  ptr->grs_n = NULL;
  ptr->brs_n = NULL;
  ptr->xrs_n = NULL;
  ptr->rrs_n = NULL;
  ptr->vrs_n = NULL;
  ptr->zrs_n = NULL;
  ptr->wrs_n = NULL;

  ptr->  sys = NULL;
  ptr->bcSys = NULL;

  ptr->Nx = 0;
  ptr->Ny = 0;

  ptr->iters_n = 0;

  ptr->diric   = NULL;
  ptr->diric_n =    0;
 
  return HXT_STATUS_OK;
}

HXTStatus hxtGMRES2Build (HXTGMRES2  *  kry, size_t iters_n,
                          HXTSystem **  sys, size_t    nrhs,
			  HXTSystem **bcSys, size_t edges_n,
			  HXTGroup  **diric, size_t diric_n,
		  	  const int   Nx, const int      Ny)
{
  const int threadid = omp_get_thread_num ();
  const int mthreads = omp_get_max_threads();

  HXTGMRES2 *ptr = kry;

  ptr->threadid = threadid; // * equal to Iy
  ptr->mthreads = mthreads; // * equal to Ny
  ptr->edges_n  =  edges_n;
  ptr->iters_n  =  iters_n;
  ptr->nrhs     =     nrhs;

  ptr->  sys =   sys;
  ptr->bcSys = bcSys;

  ptr->Nx = Nx;
  ptr->Ny = Ny;
  
        int Ix =        0;
  const int Iy = threadid;
  const int Nt = Nx *  Ny;

  #pragma omp single
  {
    mptrs = malloc( Nt*Nt*sizeof(double_complex*) );

    norm_b    = calloc(nrhs, sizeof(double));
    norm_r    = calloc(nrhs, sizeof(double));
    residual  = calloc(nrhs, sizeof(double));

    ss_origin = calloc(nrhs*(restart+1)             ,sizeof(double_complex) );
    cs_origin = calloc(nrhs*(restart+1)             ,sizeof(double_complex) );
    sn_origin = calloc(nrhs*(restart+1)             ,sizeof(double_complex) );
    hh_origin = calloc(nrhs*(restart+1)*(restart  ) ,sizeof(double_complex) );
  }
  #pragma omp barrier

  ptr->grs   = malloc( edges_n*Nx *sizeof(double_complex*) );
  ptr->brs   = malloc( edges_n*Nx *sizeof(double_complex*) );
  ptr->xrs   = malloc( edges_n*Nx *sizeof(double_complex*) );
  ptr->rrs   = malloc( edges_n*Nx *sizeof(double_complex*) );
  ptr->vrs   = malloc( edges_n*Nx *sizeof(double_complex*) );
  ptr->zrs   = malloc( edges_n*Nx *sizeof(double_complex*) );
  ptr->wrs   = malloc( edges_n*Nx *sizeof(double_complex*) );

  ptr->grs_n = malloc( edges_n*Nx *sizeof(double_complex ) );
  ptr->brs_n = malloc( edges_n*Nx *sizeof(double_complex ) );
  ptr->xrs_n = malloc( edges_n*Nx *sizeof(double_complex ) );
  ptr->rrs_n = malloc( edges_n*Nx *sizeof(double_complex ) );
  ptr->vrs_n = malloc( edges_n*Nx *sizeof(double_complex ) );
  ptr->zrs_n = malloc( edges_n*Nx *sizeof(double_complex ) );
  ptr->wrs_n = malloc( edges_n*Nx *sizeof(double_complex ) );

  size_t m = 0;

  for(m = 0; m < edges_n*Nx; m++)
  {
    const int nUnknowns = bcSys[m]->nUnknowns;

    ptr->grs  [m] = NULL;
    ptr->brs  [m] = NULL;
    ptr->xrs  [m] = NULL;
    ptr->rrs  [m] = NULL;
    ptr->vrs  [m] = NULL;
    ptr->zrs  [m] = NULL;
    ptr->wrs  [m] = NULL;

    ptr->grs  [m] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->brs  [m] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->xrs  [m] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->rrs  [m] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->vrs  [m] = calloc(nUnknowns * nrhs * (restart+1), sizeof(double_complex) );
    ptr->zrs  [m] = calloc(nUnknowns * nrhs * (restart+1), sizeof(double_complex) );
    ptr->wrs  [m] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );

    ptr->grs_n[m] = nUnknowns ;
    ptr->brs_n[m] = nUnknowns ;
    ptr->xrs_n[m] = nUnknowns ;
    ptr->rrs_n[m] = nUnknowns ;
    ptr->vrs_n[m] = nUnknowns ;
    ptr->zrs_n[m] = nUnknowns ;
    ptr->wrs_n[m] = nUnknowns ;
  }

  ptr->me   = true;
  ptr->your = NULL;
  ptr->ptrs = NULL;
  ptr->ptps = NULL;
  ptr->your = malloc( Nt*Nx * sizeof(double_complex* ) );
  ptr->ptrs = malloc( Nt*Nx * sizeof(double_complex* ) );
  ptr->ptps = malloc( Nt*Nx * sizeof(double_complex**) );

  for(int mt = 0; mt < Nt*Nx; mt++) {
    ptr->your[mt] = NULL;
    ptr->ptrs[mt] = NULL;
    ptr->ptps[mt] = NULL;
  }

  for(Ix = 0; Ix < Nx; Ix++)
  {
    if(Iy   > 0 )
    {
      ptr->ptrs[ Ny*Ix + threadid-1  + Nt*Ix ] =  bcSys[edges_n*Ix+0]->x;
      ptr->ptps[ Ny*Ix + threadid-1  + Nt*Ix ] = &bcSys[edges_n*Ix+0]->x;
    }
    if(Ix+1 < Nx)
    {
      ptr->ptrs[ Ny*Ix + threadid+Ny + Nt*Ix ] =  bcSys[edges_n*Ix+1]->x;
      ptr->ptps[ Ny*Ix + threadid+Ny + Nt*Ix ] = &bcSys[edges_n*Ix+1]->x;
    }
    if(Iy+1 < Ny)
    {
      ptr->ptrs[ Ny*Ix + threadid+1  + Nt*Ix ] =  bcSys[edges_n*Ix+2]->x;
      ptr->ptps[ Ny*Ix + threadid+1  + Nt*Ix ] = &bcSys[edges_n*Ix+2]->x;

    }
    if(Ix   > 0 )
    {
      ptr->ptrs[ Ny*Ix + threadid-Ny + Nt*Ix ] =  bcSys[edges_n*Ix+3]->x;
      ptr->ptps[ Ny*Ix + threadid-Ny + Nt*Ix ] = &bcSys[edges_n*Ix+3]->x;
    }
  }
  HXT_CHECK( gmres_exchange_set(ptr) );

  ptr->diric   = diric  ;
  ptr->diric_n = diric_n;

  return HXT_STATUS_OK;
}

HXTStatus hxtGMRES2Delete(HXTGMRES2 **kry)
{
  HXTGMRES2 *ptr = *kry;

  const size_t Nx      = ptr->Nx;
  const size_t edges_n = ptr->edges_n;

  #pragma omp barrier
  for(size_t m = 0; m < edges_n*Nx; m++)
  {
    free(ptr->grs[m]);
    free(ptr->brs[m]);
    free(ptr->xrs[m]);
    free(ptr->rrs[m]);
    free(ptr->vrs[m]);
    free(ptr->zrs[m]);
    free(ptr->wrs[m]);

    ptr->grs  [m] = NULL;
    ptr->brs  [m] = NULL;
    ptr->xrs  [m] = NULL;
    ptr->rrs  [m] = NULL;
    ptr->vrs  [m] = NULL;
    ptr->zrs  [m] = NULL;
    ptr->wrs  [m] = NULL;
    
    ptr->grs_n[m] =    0;
    ptr->brs_n[m] =    0;
    ptr->xrs_n[m] =    0;
    ptr->rrs_n[m] =    0;
    ptr->vrs_n[m] =    0;
    ptr->zrs_n[m] =    0;
    ptr->wrs_n[m] =    0;
  }
  free(ptr->grs  );
  free(ptr->brs  );
  free(ptr->xrs  );
  free(ptr->rrs  );
  free(ptr->vrs  );
  free(ptr->zrs  );
  free(ptr->wrs  );
  free(ptr->grs_n);
  free(ptr->brs_n);
  free(ptr->xrs_n);
  free(ptr->rrs_n);
  free(ptr->vrs_n);
  free(ptr->zrs_n);
  free(ptr->wrs_n);

  free(ptr->ptrs);
  free(ptr->ptps);
  free(ptr->your);
  ptr->me =  true;

  #pragma omp single
  {
    free(mptrs    );
  
    free(norm_b   );
    free(norm_r   );
    free(residual );
  
    free(ss_origin);
    free(cs_origin);
    free(sn_origin);
    free(hh_origin);
  }
  #pragma omp barrier

  ptr->threadid = -1;
  ptr->mthreads = -1;

  ptr->edges_n  =  0;
  ptr->   nrhs  =  0;

  ptr->  sys = NULL;
  ptr->bcSys = NULL;

  ptr->Nx = 0;
  ptr->Ny = 0;

  ptr->iters_n = 0;

  ptr->diric   = NULL;
  ptr->diric_n =    0;

  HXT_CHECK( hxtFree(kry) );

  return HXT_STATUS_OK;
}

HXTStatus hxtGMRES2Solve(HXTGMRES2  *kry, const int pre)
{
  // * ---------------------------------------------------------------- * //
  // * ------                                                    ------ * //
  // * --                             START                          -- * //
  // * ------                                                    ------ * //
  // * ---------------------------------------------------------------- * //
  double tolerance = 10e-7;

  HXTGMRES2 *ptr = kry;

  const size_t iters_n = ptr->iters_n;
  const size_t edges_n = ptr->edges_n;
  const size_t    nrhs = ptr->   nrhs;

  HXTSystem **  sys = ptr->  sys;
  HXTSystem **bcSys = ptr->bcSys;

  double_complex **grs =  ptr->grs;
  double_complex **brs =  ptr->brs;
  double_complex **xrs =  ptr->xrs;
  double_complex **rrs =  ptr->rrs;
  double_complex **vrs =  ptr->vrs;
  double_complex **zrs =  ptr->zrs;
  double_complex **wrs =  ptr->wrs;

  size_t *grs_n = ptr->grs_n;
  size_t *brs_n = ptr->brs_n;
  size_t *xrs_n = ptr->xrs_n;
  size_t *rrs_n = ptr->rrs_n;
  size_t *vrs_n = ptr->vrs_n;
  size_t *zrs_n = ptr->zrs_n;
  size_t *wrs_n = ptr->wrs_n;

  const int Nx = ptr->Nx;
        int Ix =       0;

  int i = 0;

  size_t l, m;

  // * ---------------------------------------------------------------- * //
  // * -- get w_{n+1}                                                -- * //
  // * ---------------------------------------------------------------- * //
  for(Ix = 0; Ix < Nx; Ix++)
    HXT_CHECK( hxtSystemPhase(sys[Ix], 0, nrhs,  33 ) );

  // * ---------------------------------------------------------------- * //
  // * -- update law g_{n+1} = -g_{n} + 2 * S(v_{n+1})               -- * //
  // * -- here       g_{n  } =  0                                    -- * //
  // * -- thus       b       =  0     + 2 * S(w_{n+1})               -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(Ix = 0; Ix < Nx; Ix++)
    {
      for(m = 0; m < edges_n; m++)
      {
        const size_t p = Ix*edges_n + m;

        HXT_CHECK( hxtSystemRHSZero (bcSys[p], l) );
        HXT_CHECK( hxtSystemCMxToRHS(bcSys[p], 0., bcSys[p], 2., sys[Ix], l) );
        HXT_CHECK( hxtSystemPhase   (bcSys[p], l, 1, 33) );
      }
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- exchange functions                                         -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( gmres_exchange_do (ptr) );

  // * ---------------------------------------------------------------- * //
  // * -- SET B VEC VALUES                                           -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(Ix = 0; Ix < Nx; Ix++)
    {
      for(m = 0; m < edges_n; m++)
      {
        const size_t p = Ix*edges_n + m;

        HXT_CHECK( hxtSystemSolToArr(bcSys[p], l, brs[p], brs_n[p], 0.) );
      }
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- LOOP OF GMRES2                                             -- * //
  // * ---------------------------------------------------------------- * //
  size_t j  = 1;
  while( j <= iters_n)
  {
    size_t k  = 0;
    // * ---------------------------------------------------------------- * //
    // * -- SET RHS ZERO                                               -- * //
    // * ---------------------------------------------------------------- * //
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++) HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );
    }

    // * ---------------------------------------------------------------- * //
    // * -- r0 = A * x0                                                -- * //
    // * ---------------------------------------------------------------- * //
    HXT_CHECK( multMatrix(ptr, 0, xrs, xrs_n, 0, rrs, rrs_n) );
  
    // * ---------------------------------------------------------------- * //
    // * -- r0 = b - (I - A) * x0 ---> r0 = b - x0 + A * x0            -- * //
    // * ---------------------------------------------------------------- * //
    const double_complex cone   =  1.;
    const double_complex cminus = -1.;
  
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        for(m = 0; m < edges_n; m++)
        {
          const size_t p = Ix*edges_n + m;
  
          decomposed_gmres_zaxpy(l, rrs[p], rrs_n[p], brs[p], brs_n[p], cone  );
          decomposed_gmres_zaxpy(l, rrs[p], rrs_n[p], xrs[p], xrs_n[p], cminus);
        }
      }
    }
  
    // * ---------------------------------------------------------------- * //
    // * -- calculate norm_r and norm_b                                -- * //
    // * ---------------------------------------------------------------- * //
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        for(m = 0; m < edges_n; m++)
        {
          const size_t p = Ix*edges_n + m;
  
          double tmp_b = 0.;
          double tmp_r = 0.;
          tmp_b += decomposed_gmres_dznrm2(l, brs[p], brs_n[p])*
                   decomposed_gmres_dznrm2(l, brs[p], brs_n[p]);
          tmp_r += decomposed_gmres_dznrm2(l, rrs[p], rrs_n[p])*
                   decomposed_gmres_dznrm2(l, rrs[p], rrs_n[p]);
          #pragma omp atomic update
          norm_b[l] += tmp_b;
          #pragma omp atomic update
          norm_r[l] += tmp_r;
        }
      }
    }
    #pragma omp barrier
  
    // * ---------------------------------------------------------------- * //
    // * -- ss[0] = norm_r                                             -- * //
    // * ---------------------------------------------------------------- * //
    #pragma omp single
    {
      for(l = 0; l < nrhs; l++)
      {
        norm_b[l] = sqrt(norm_b[l]);
        norm_r[l] = sqrt(norm_r[l]);
  
        if(norm_b[l] == 0.) norm_b[l] = 1.;
  
        residual [l] = norm_r[l] / norm_b[l];
  
        SS_ORIGIN(l, 0) = norm_r[l];
      }
      printf("norm_b[0]     : %f\n",    norm_b[0]);
      printf("norm_r[0]     : %f\n",    norm_r[0]);
    }
    #pragma omp barrier

    // * ---------------------------------------------------------------- * //
    // * -- v[0] = v[0] + r / norm_r                                   -- * //
    // * ---------------------------------------------------------------- * //
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        for(m = 0; m < edges_n; m++)
        {
          const size_t p = Ix*edges_n + m;

          const double_complex rec_r = 1./norm_r[l];
          decomposed_gmres_zaxpy(l, &vrs[p][0], vrs_n[p], rrs[p], rrs_n[p], rec_r );
        }
      }
    }
    #pragma omp barrier

    for(i = 0; i < restart && j <= iters_n; i++, j++)
    {
      // * ---------------------------------------------------------------- * //
      // * -- z[i] = M^{-1} v[i]                                         -- * //
      // * ---------------------------------------------------------------- * //
      for( l = 0; l < nrhs; l++)
      {
        for(Ix = 0; Ix < Nx; Ix++)
        {
          for(m = 0; m < edges_n; m++)
          {
            const size_t p = Ix*edges_n + m;

            double_complex *ivrs = &vrs[p][i * vrs_n[p] * nrhs];
            double_complex *izrs = &zrs[p][i * zrs_n[p] * nrhs];
            decomposed_gmres_zaxpy(l, izrs, zrs_n[p], ivrs, vrs_n[p], 1. );
          }
        }
      }
      switch(pre) 
      {
        case 0 :
	  break;
        case 1 :
          HXT_CHECK( SGSFwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
          HXT_CHECK( SGSBwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
	  break;
        case 2 :
          HXT_CHECK( SGSFwd2(ptr, "BH", i, zrs, zrs_n, i, zrs, zrs_n) );
          HXT_CHECK( SGSBwd2(ptr, "BH", i, zrs, zrs_n, i, zrs, zrs_n) );
	  break;
        case 3 :
          if(i%2 == 0) HXT_CHECK( SGSFwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
          if(i%2 == 0) HXT_CHECK( SGSBwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
          if(i%2 == 1) HXT_CHECK( SGSFwd2(ptr, "L2", i, zrs, zrs_n, i, zrs, zrs_n) );
          if(i%2 == 1) HXT_CHECK( SGSBwd2(ptr, "L2", i, zrs, zrs_n, i, zrs, zrs_n) );
	  break;

        case 4 :
          HXT_CHECK( SWPFwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
          HXT_CHECK( SWPBwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
	  break;
        case 5 :
          HXT_CHECK( SWPFwd2(ptr, "BH", i, zrs, zrs_n, i, zrs, zrs_n) );
          HXT_CHECK( SWPBwd2(ptr, "BH", i, zrs, zrs_n, i, zrs, zrs_n) );
	  break;
        case 6 :
          if(i%2 == 0) HXT_CHECK( SWPFwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
          if(i%2 == 0) HXT_CHECK( SWPBwd2(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
          if(i%2 == 1) HXT_CHECK( SWPFwd2(ptr, "L2", i, zrs, zrs_n, i, zrs, zrs_n) );
          if(i%2 == 1) HXT_CHECK( SWPBwd2(ptr, "L2", i, zrs, zrs_n, i, zrs, zrs_n) );
	  break;
        default: printf("pre == 1-6\n");
      } 

      // * ---------------------------------------------------------------- * //
      // * -- w = A v[i]                                                 -- * //
      // * ---------------------------------------------------------------- * //
      HXT_CHECK( multMatrix(ptr, i, zrs, zrs_n, 0, wrs, wrs_n) );

      // * ---------------------------------------------------------------- * //
      // * --         w = v[i] - w                                       -- * //
      // * -- that is w =  (I- A)v[i]                                    -- * //
      // * ---------------------------------------------------------------- * //
      for( l = 0; l < nrhs; l++)
      {
        for(Ix = 0; Ix < Nx; Ix++)
        {
          for(m = 0; m < edges_n; m++)
          {
            const size_t p = Ix*edges_n + m;

            double_complex *izrs = &zrs[p][i * zrs_n[p] * nrhs];
  
            decomposed_gmres_zscal(l, wrs[p], wrs_n[p],                 cminus);
            decomposed_gmres_zaxpy(l, wrs[p], wrs_n[p], izrs, zrs_n[m], cone  );
          }
        }
      }

      for(l = 0; l < nrhs; l++)
      {
        for(k = 0; k <= i; k++)
        {
          // * ---------------------------------------------------------------- * //
          // * -- h[k][i] = zdotc(v[k], w)                                   -- * //
          // * ---------------------------------------------------------------- * //
          for(Ix = 0; Ix < Nx; Ix++)
          {
            for(m = 0; m < edges_n; m++)
            {
              const size_t p = Ix*edges_n + m;

              double_complex *ivrs = &vrs[p][k * vrs_n[p] * nrhs];
  
              double_complex tmp = decomposed_gmres_zdotc(l, ivrs, vrs_n[p], wrs[p], wrs_n[p]);
  
              double* complex_h = (double*)(&HH_ORIGIN(l, k, i));
              #pragma omp atomic update
              complex_h[0] += creal(tmp);
              #pragma omp atomic update
              complex_h[1] += cimag(tmp);
            }
          }
          #pragma omp barrier

          // * ---------------------------------------------------------------- * //
          // * -- w = w - v[k] * h[k][i];                                    -- * //
          // * ---------------------------------------------------------------- * //
          for(Ix = 0; Ix < Nx; Ix++)
          {
            for(m = 0; m < edges_n; m++)
            {
              const size_t p = Ix*edges_n + m;

              double_complex *ivrs = &vrs[p][k * vrs_n[p] * nrhs];
  
              double_complex  tmp_hh = - HH_ORIGIN(l, k, i);
  
              decomposed_gmres_zaxpy(l, wrs[p], wrs_n[p], ivrs, vrs_n[p], tmp_hh);
            }
          }
          #pragma omp barrier
        }
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- h[i+1][i] = dznrm2(w)                                      -- * //
      // * ---------------------------------------------------------------- * //
      for(l = 0; l < nrhs; l++)
      {
        double tot = 0.;
        for(Ix = 0; Ix < Nx; Ix++)
        {
          for(m = 0; m < edges_n; m++)
          {
            const size_t p = Ix*edges_n + m;

            double sum = decomposed_gmres_dznrm2(l, wrs[p], wrs_n[p]) *
                         decomposed_gmres_dznrm2(l, wrs[p], wrs_n[p]) ;

            tot += sum;
          }
        }
        double* complex_h = (double*)(&HH_ORIGIN(l, i+1, i));
        #pragma omp atomic update
        complex_h[0] += tot;
        #pragma omp barrier

        #pragma omp single
        HH_ORIGIN(l, i+1, i) = csqrt( HH_ORIGIN(l, i+1, i) );
      }
      #pragma omp barrier


      // * ---------------------------------------------------------------- * //
      // * -- v[i+1] = v[i+1] + w / h[i+1][i]                            -- * //
      // * ---------------------------------------------------------------- * //
      for(l = 0; l < nrhs; l++)
      {
        for(Ix = 0; Ix < Nx; Ix++)
        {
          for(m = 0; m < edges_n; m++)
          {
            const size_t p = Ix*edges_n + m;

            double_complex *ivrs = &vrs[p][ (i+1) * vrs_n[p] * nrhs];
  
            double_complex  rec_hh = 1. / HH_ORIGIN(l, i+1, i);
  
            decomposed_gmres_zaxpy(l, ivrs, vrs_n[p], wrs[p], wrs_n[p], rec_hh);
          }
        }
        #pragma omp barrier
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- GIVEN ROTATION                                             -- * //
      // * ---------------------------------------------------------------- * //
      #pragma omp single
      {
        printf("    iteration %lu, ", j);
        for(l = 0; l < nrhs; l++)
        {
          for(k = 0; k < i; k++)
          {
            hxtzarot(&HH_ORIGIN(l, k, i), &HH_ORIGIN(l, k+1, i), &CS_ORIGIN(l, k), &SN_ORIGIN(l, k));
          }
          hxtzgrot(&HH_ORIGIN(l, i, i), &HH_ORIGIN(l, i+1, i), &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));
          hxtzarot(&HH_ORIGIN(l, i, i), &HH_ORIGIN(l, i+1, i), &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));
          hxtzarot(&SS_ORIGIN(l, i)   , &SS_ORIGIN(l, i+1)   , &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));

          // * residual = cabs(ss[i+1]) / norm_b
          residual[l] = cabs ( SS_ORIGIN(l, i+1) ) / norm_b[l];
          printf("res %lu: %.3e  ", l, residual[l]);
        }
        printf("\n");
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- UPDATE                                                     -- * //
      // * ---------------------------------------------------------------- * //
      if(residual[0] < tolerance)
      {
        for(l = 0; l < nrhs; l++)
        {
          for(Ix = 0; Ix < Nx; Ix++)
          {
            const size_t os = Ix*edges_n;

            HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );
  
            decomposed_gmres_update(&grs[os], &grs_n[os], edges_n,
                                    &zrs[os], &zrs_n[os], edges_n,
                                    l, nrhs, i, restart);
  
            for(m = 0; m < edges_n; m++)
            {
              HXT_CHECK( hxtSystemArrToSol(bcSys[os+m], l, grs  [os+m],     grs_n[os+m], 0.) );
              HXT_CHECK( hxtSystemBMxToRHS(sys[Ix]    , 0, bcSys[os+m], 1., bcSys[os+m], l ) );
            }
          }
        }
        return HXT_STATUS_OK;
      }
      #pragma omp barrier
    }
    // * ---------------------------------------------------------------- * //
    // * -- x(i) to x(0)                                               -- * //
    // * ---------------------------------------------------------------- * //
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = Ix*edges_n;
  
        decomposed_gmres_update(&xrs[os], &xrs_n[os], edges_n,
                                &zrs[os], &zrs_n[os], edges_n,
                                l, nrhs, i-1, restart);
        decomposed_gmres_update(&grs[os], &grs_n[os], edges_n,
                                &zrs[os], &zrs_n[os], edges_n,
                                l, nrhs, i-1, restart);
      }
    }
    #pragma omp barrier
    // * ---------------------------------------------------------------- * //
    // * -- reset                                                      -- * //
    // * ---------------------------------------------------------------- * //
    for(m = 0; m < edges_n*Nx; m++)
    {
      const int nUnknowns = bcSys[m]->nUnknowns;
  
      memset( rrs[m], 0, nUnknowns * nrhs *               sizeof(double_complex) );
      memset( vrs[m], 0, nUnknowns * nrhs * (restart+1) * sizeof(double_complex) );
      memset( zrs[m], 0, nUnknowns * nrhs * (restart+1) * sizeof(double_complex) );
      memset( wrs[m], 0, nUnknowns * nrhs *               sizeof(double_complex) );
    }
    #pragma omp single
    {
      memset( norm_b  , 0, nrhs * sizeof(double) );
      memset( norm_r  , 0, nrhs * sizeof(double) );
      memset( residual, 0, nrhs * sizeof(double) );
  
      memset( ss_origin, 0, nrhs*(restart+1)             * sizeof(double_complex) );
      memset( cs_origin, 0, nrhs*(restart+1)             * sizeof(double_complex) );
      memset( sn_origin, 0, nrhs*(restart+1)             * sizeof(double_complex) );
      memset( hh_origin, 0, nrhs*(restart+1)*(restart  ) * sizeof(double_complex) );
    }
    #pragma omp barrier
  }
  /*
  // * ---------------------------------------------------------------- * //
  // * -- UPDATE                                                     -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(Ix = 0; Ix < Nx; Ix++)
    {
      const size_t os = Ix*edges_n;

      HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );
  
      decomposed_gmres_update(&grs[os], &grs_n[os], edges_n,
                              &zrs[os], &zrs_n[os], edges_n,
                              l, nrhs, i-1, restart);
  
      for(m = 0; m < edges_n; m++)
      {
        HXT_CHECK( hxtSystemArrToSol(bcSys[os+m], l, grs  [os+m],     grs_n[os+m], 0.) );
        HXT_CHECK( hxtSystemBMxToRHS(sys[Ix]    , 0, bcSys[os+m], 1., bcSys[os+m], l ) );
      }
    }
  }
  */
  return HXT_STATUS_OK;
}

static inline int decomposed_gmres_zaxpy(const int             irhs,
                                               double_complex    *y,
                                         const size_t           y_n,
                                         const double_complex    *x,
                                         const size_t           x_n,
                                         const double_complex alpha)
{
  if(y_n == 0) return 1;
  hxtzaxpy(&y[irhs*y_n], &x[irhs*y_n], alpha, y_n);
  return 0;
}

static inline double decomposed_gmres_dznrm2(const int             irhs,
                                             const double_complex    *x,
                                             const size_t           x_n)
{
  if(x_n == 0) return 0.;
  else return hxtdznrm2(&x[irhs*x_n], x_n);
}

static inline int    decomposed_gmres_zscal (const int             irhs,
                                                   double_complex    *s,
                                             const size_t           s_n,
                                             const double_complex alpha)
{
  if(s_n == 0) return 1;
  hxtzscal(&s[irhs*s_n], alpha, s_n);
  return 0;
}

static inline double_complex decomposed_gmres_zdotc(const int             irhs,
                                                    const double_complex  *x  ,
                                                    const size_t           x_n,
                                                    const double_complex  *y  ,
                                                    const size_t           y_n)
{
  double_complex zero = 0.;
  if(x_n == 0) return zero;
  else return hxtzdotc(&x[irhs*x_n], &y[irhs*x_n], x_n);
}



// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                       PRECONDITIONER                       == * //
// * ======                                                    ====== * //
// * ================================================================ * //
HXTStatus SGSFwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  const int Nx = kry->Nx;
  const int Ny = kry->Ny;
//const int Nt = Nx * Ny;
        int Ix =       0;
  const int Iy = omp_get_thread_num();

  HXTSystem    **  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  int Loop = 0;
  int *Ic = malloc(Nx * sizeof(int));

  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;
       if(strcmp( type, "L1" ) == 0 )
  {
    arrs[0] = 1;
    arrs[1] = 2;
    tmls[0] = 0;
    tmls[1] = 3;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix + Iy;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    arrs[0] = 0;
    arrs[1] = 1;
    tmls[0] = 2;
    tmls[1] = 3;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix - Iy + Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    arrs[0] = 1;
    tmls[0] = 3;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    arrs[0] = 2;
    tmls[0] = 0;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

  size_t l, m, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                      FORWARD OPERATOR                      -- * //
  // * ---------------------------------------------------------------- * //
  for(k = 0; k < Loop; k++)
  {
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );  
        HXT_CHECK( hxtSystemSolZero(sys[Ix], l) );  
        if( Ic[Ix] == k)
        {
          for(m = 0; m < edges_n; m++)
          {
            c = m;
  
            const size_t    ins_n =  xs_n[os+c];
            double_complex *ins   = &xs  [os+c][ itx * ins_n * nrhs ];
  
            HXT_CHECK( hxtSystemArrToSol(bcSys[os+c], l , ins,  ins_n, 0.                ) );
            HXT_CHECK( hxtSystemBMxToRHS(sys[Ix]    , 0., bcSys[os+c], 1., bcSys[os+c], l) );
          }
          for(i = 0; i < kry->diric_n; i++) 
            HXT_CHECK( hxtSystemCSTR (sys[Ix], l, kry->diric[i*Nx+Ix], fZero) ); 

          HXT_CHECK( hxtSystemPhase(sys[Ix], l, 1, 33 ) );
    
          for(m = 0; m < arrs_n; m++)
          {
            c = arrs[m];
  
            const size_t    ins_n =  xs_n[os+c];
            double_complex *ins   = &xs  [os+c][ itx * ins_n * nrhs ];
        
            HXT_CHECK( hxtSystemRHSZero (bcSys[os+c], l) );
            HXT_CHECK( hxtSystemSolZero (bcSys[os+c], l) );
      
            HXT_CHECK( hxtSystemArrToSol(bcSys[os+c], l , ins, ins_n,   0.                ) );
            HXT_CHECK( hxtSystemBMxToRHS(bcSys[os+c], 0., bcSys[os+c], -1., bcSys[os+c], l) );
            HXT_CHECK( hxtSystemCMxToRHS(bcSys[os+c], 0., bcSys[os+c],  2., sys[Ix]    , l) );
      
            HXT_CHECK( hxtSystemPhase(bcSys[os+c], l, 1, 33) );
          }
        }
      }
      #pragma omp barrier
    }

    // *--- exchange functions 
    HXT_CHECK( gmres_exchange_do (kry) );
    
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        if( Ic[Ix] == ((k+1)%Loop) )
        {
          for(m = 0; m < tmls_n; m++)
          {
            c = tmls[m];
  
            const size_t    outs_n =  ys_n[os+c];
            double_complex *outs   = &ys  [os+c][ ity * outs_n * nrhs ];
      
            HXT_CHECK( hxtSystemSolToArr(bcSys[os+c], l, outs, outs_n, 1.) );
          }
        }
      }
    }
    #pragma omp barrier
  }
  free(Ic);

  return HXT_STATUS_OK;
}

HXTStatus SGSBwd2(HXTGMRES2    *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  const int Nx = kry->Nx;
  const int Ny = kry->Ny;
//const int Nt = Nx * Ny;
        int Ix =       0;
  const int Iy = omp_get_thread_num();

  HXTSystem    **  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  int Loop = 0;
  int *Ic = malloc(Nx * sizeof(int));

  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;
       if(strcmp( type, "L1" ) == 0 )
  {
    arrs[0] = 0;
    arrs[1] = 3;
    tmls[0] = 1;
    tmls[1] = 2;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix + Iy;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    arrs[0] = 2;
    arrs[1] = 3;
    tmls[0] = 0;
    tmls[1] = 1;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix - Iy + Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    arrs[0] = 3;
    tmls[0] = 1;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    arrs[0] = 0;
    tmls[0] = 2;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

  size_t l, m, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                     BACKWARD OPERATOR                      -- * //
  // * ---------------------------------------------------------------- * //
  for(k = Loop-1; k >= 0; k--)
  {
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );  
        HXT_CHECK( hxtSystemSolZero(sys[Ix], l) );  
        if( Ic[Ix] == k)
        {
          for(m = 0; m < edges_n; m++)
          {
            c = m;
  
            const size_t    ins_n =  xs_n[os+c];
            double_complex *ins   = &xs  [os+c][ itx * ins_n * nrhs ];
      
            HXT_CHECK( hxtSystemArrToSol(bcSys[os+c], l , ins,  ins_n, 0.                ) );
            HXT_CHECK( hxtSystemBMxToRHS(sys[Ix]    , 0., bcSys[os+c], 1., bcSys[os+c], l) );
          }
          for(i = 0; i < kry->diric_n; i++) 
            HXT_CHECK( hxtSystemCSTR (sys[Ix], l, kry->diric[i*Nx+Ix], fZero) ); 

          HXT_CHECK( hxtSystemPhase(sys[Ix], l, 1, 33 ) );
    
          for(m = 0; m < arrs_n; m++)
          {
            c = arrs[m];
  
            const size_t    ins_n =  xs_n[os+c];
            double_complex *ins   = &xs  [os+c][ itx * ins_n * nrhs ];
      
            HXT_CHECK( hxtSystemRHSZero (bcSys[os+c], l) );
            HXT_CHECK( hxtSystemSolZero (bcSys[os+c], l) );
      
            HXT_CHECK( hxtSystemArrToSol(bcSys[os+c], l , ins, ins_n,   0.                ) );
            HXT_CHECK( hxtSystemBMxToRHS(bcSys[os+c], 0., bcSys[os+c], -1., bcSys[os+c], l) );
            HXT_CHECK( hxtSystemCMxToRHS(bcSys[os+c], 0., bcSys[os+c],  2., sys[Ix]    , l) );
  
            HXT_CHECK( hxtSystemPhase(bcSys[os+c], l, 1, 33) );
          }
        }
      }
      #pragma omp barrier
    }

    // *--- exchange functions 
    HXT_CHECK( gmres_exchange_do (kry) );
    
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        if( Ic[Ix] == ((k-1+Loop)%Loop) )
        {
          for(m = 0; m < tmls_n; m++)
          {
            c = tmls[m];
  
            const size_t    outs_n =  ys_n[os+c];
            double_complex *outs   = &ys  [os+c][ ity * outs_n * nrhs ];
    
            HXT_CHECK( hxtSystemSolToArr(bcSys[os+c], l, outs, outs_n, 1.) );
          }
        }
      }
    }
    #pragma omp barrier
  }
  free(Ic);

  return HXT_STATUS_OK;
}

HXTStatus SWPFwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  const int Nx = kry->Nx;
  const int Ny = kry->Ny;
//const int Nt = Nx * Ny;
        int Ix =       0;
  const int Iy = omp_get_thread_num();

  HXTSystem    **  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  int Loop = 0;
  int *Ic = malloc(Nx * sizeof(int));

  int deps[4] = {0}; size_t deps_n = 0;
  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;
       if(strcmp( type, "L1" ) == 0 )
  {
    deps[0] = 0;
    deps[1] = 3;
    arrs[0] = 1;
    arrs[1] = 2;
    tmls[0] = 0;
    tmls[1] = 3;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix + Iy;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    deps[0] = 2;
    deps[1] = 3;
    arrs[0] = 0;
    arrs[1] = 1;
    tmls[0] = 2;
    tmls[1] = 3;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix - Iy + Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    deps[0] = 0;
    deps[1] = 2;
    deps[2] = 3;
    arrs[0] = 1;
    tmls[0] = 3;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    deps[0] = 0;
    deps[1] = 1;
    deps[2] = 3;
    arrs[0] = 2;
    tmls[0] = 0;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

  size_t l, m, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                   FORWARD TRANSFER OPERATOR                -- * //
  // * ---------------------------------------------------------------- * //
  for(k = 0; k < Loop; k++)
  {
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        if( Ic[Ix] == k)
        {
          HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );  
          HXT_CHECK( hxtSystemSolZero(sys[Ix], l) );  
          for(m = 0; m < deps_n; m++)
          {
            c = deps[m];
  
            const size_t    ins_n =  xs_n[os+c];
            double_complex *ins   = &xs  [os+c][ itx * ins_n * nrhs ];
        
            HXT_CHECK( hxtSystemArrToSol(bcSys[os+c], l , ins,  ins_n, 0.                ) );
            HXT_CHECK( hxtSystemBMxToRHS(sys[Ix]    , 0., bcSys[os+c], 1., bcSys[os+c], l) );
          }
          for(i = 0; i < kry->diric_n; i++) 
            HXT_CHECK( hxtSystemCSTR (sys[Ix], l, kry->diric[i*Nx+Ix], fZero) ); 

          HXT_CHECK( hxtSystemPhase(sys[Ix], l, 1, 33 ) );
    
          for(m = 0; m < arrs_n; m++)
          {
            c = arrs[m];
      
            HXT_CHECK( hxtSystemRHSZero (bcSys[os+c], l) );
            HXT_CHECK( hxtSystemSolZero (bcSys[os+c], l) );
            HXT_CHECK( hxtSystemCMxToRHS(bcSys[os+c], 0., bcSys[os+c], 2., sys[Ix], l) );
            HXT_CHECK( hxtSystemPhase   (bcSys[os+c], l, 1, 33) );
          }
        }
      }
      #pragma omp barrier
  
      // *--- exchange functions 
      HXT_CHECK( gmres_exchange_do (kry) );
      
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        if( Ic[Ix] == ((k+1)%Loop) )
        {
          for(m = 0; m < tmls_n; m++)
          {
            c = tmls[m];
  
            const size_t    outs_n =  ys_n[os+c];
            double_complex *outs   = &ys  [os+c][ ity * outs_n * nrhs ];
            HXT_CHECK( hxtSystemSolToArr(bcSys[os+c], l, outs, outs_n, 1.) );
          }
        }
      }
      #pragma omp barrier
    }
    #pragma omp barrier
  }
  free(Ic);

  return HXT_STATUS_OK;
}

HXTStatus SWPBwd2(HXTGMRES2   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  const int Nx = kry->Nx;
  const int Ny = kry->Ny;
//const int Nt = Nx * Ny;
        int Ix =       0;
  const int Iy = omp_get_thread_num();

  HXTSystem    **  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  int Loop = 0;
  int *Ic = malloc(Nx * sizeof(int));

  int deps[4] = {0}; size_t deps_n = 0;
  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;

       if(strcmp( type, "L1" ) == 0 )
  {
    deps[0] = 1;
    deps[1] = 2;
    arrs[0] = 0;
    arrs[1] = 3;
    tmls[0] = 1;
    tmls[1] = 2;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix + Iy;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    deps[0] = 0;
    deps[1] = 1;
    arrs[0] = 2;
    arrs[1] = 3;
    tmls[0] = 0;
    tmls[1] = 1;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix - Iy + Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    deps[0] = 1;
    deps[1] = 0;
    deps[2] = 2;
    arrs[0] = 3;
    tmls[0] = 1;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    deps[0] = 2;
    deps[1] = 1;
    deps[2] = 3;
    arrs[0] = 0;
    tmls[0] = 2;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;

    for( Ix = 0; Ix < Nx; Ix++) Ic[Ix] = Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

  size_t l, m, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                  BACKWARD TRANSFER OPERATOR                -- * //
  // * ---------------------------------------------------------------- * //
  for(k = Loop-1; k >= 0; k--)
  {
    for(l = 0; l < nrhs; l++)
    {
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        if( Ic[Ix] == k)
        {
          HXT_CHECK( hxtSystemRHSZero(sys[Ix], l) );  
          HXT_CHECK( hxtSystemSolZero(sys[Ix], l) );  
          for(m = 0; m < deps_n; m++)
          {
            c = deps[m];
  
            const size_t    ins_n =  xs_n[os+c];
            double_complex *ins   = &xs  [os+c][ itx * ins_n * nrhs ];
      
            HXT_CHECK( hxtSystemArrToSol(bcSys[os+c], l , ins,  ins_n, 0.                ) );
            HXT_CHECK( hxtSystemBMxToRHS(sys[Ix]    , 0., bcSys[os+c], 1., bcSys[os+c], l) );
          }
          for(i = 0; i < kry->diric_n; i++) 
            HXT_CHECK( hxtSystemCSTR (sys[Ix], l, kry->diric[i*Nx+Ix], fZero) ); 

          HXT_CHECK( hxtSystemPhase(sys[Ix], l, 1, 33 ) );
    
          for(m = 0; m < arrs_n; m++)
          {
            c = arrs[m];
      
            HXT_CHECK( hxtSystemRHSZero (bcSys[os+c], l) );
            HXT_CHECK( hxtSystemSolZero (bcSys[os+c], l) );
            HXT_CHECK( hxtSystemCMxToRHS(bcSys[os+c], 0., bcSys[os+c], 2., sys[Ix], l) );
            HXT_CHECK( hxtSystemPhase   (bcSys[os+c], l, 1, 33) );
          }
        }
      }
      #pragma omp barrier

      // *--- exchange functions 
      HXT_CHECK( gmres_exchange_do (kry) );
    
      for(Ix = 0; Ix < Nx; Ix++)
      {
        const size_t os = edges_n * Ix;

        if( Ic[Ix] == ((k-1+Loop)%Loop) )
        {
          for(m = 0; m < tmls_n; m++)
          {
            c = tmls[m];
  
            const size_t    outs_n =  ys_n[os+c];
            double_complex *outs   = &ys  [os+c][ ity * outs_n * nrhs ];
    
            HXT_CHECK( hxtSystemSolToArr(bcSys[os+c], l, outs, outs_n, 1.) );
          }
        }
      }
      #pragma omp barrier
    }
    #pragma omp barrier
  }
  free(Ic);

  return HXT_STATUS_OK;
}
