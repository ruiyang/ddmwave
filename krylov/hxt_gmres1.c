#include "hxt_gmres1.h"

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                          GMRES DATA                        == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static double_complex **   mptrs;
static double         *   norm_b;
static double         *   norm_r;
static double         * residual;
static double_complex *ss_origin;
static double_complex *cs_origin;
static double_complex *sn_origin;
static double_complex *hh_origin; // * hessenburg matrix

#if !defined(OMP_GMRES_SWEEP_NO_MACROS)
  #define SS_ORIGIN(l, a)    (ss_origin[(l)*(iters_n+1)             + (a)                  ])
  #define CS_ORIGIN(l, a)    (cs_origin[(l)*(iters_n+1)             + (a)                  ])
  #define SN_ORIGIN(l, a)    (sn_origin[(l)*(iters_n+1)             + (a)                  ])
  #define HH_ORIGIN(l, a, b) (hh_origin[(l)*(iters_n+1)*(iters_n+1) + (a)*(iters_n+1) + (b)])
#endif

static HXTStatus gmres_exchange_set(HXTGMRES1 *kry)
{
  const int Nt = omp_get_max_threads(); // * equal to mthreads
  const int It = omp_get_thread_num (); // * equal to threadid
  int t;

  for(t = 0; t < Nt; t++)
  {
    void **px = (void**)  &kry->ptrs[t];
    mptrs[It*Nt + t ] = *px;
  }
  #pragma omp barrier
  for(t = 0; t < Nt; t++) 
  {
    void **px = (void**)  &kry->your[t];
    *px = mptrs[t*Nt + It ];
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

static HXTStatus gmres_exchange_do (HXTGMRES1 *kry)
{
  const int Nt = omp_get_max_threads(); // * equal to mthreads
  int t;

  #pragma omp barrier
  if(kry->me == true)
  {
     for(t = 0; t < Nt; t++) 
       if(kry->ptps[t] != NULL) *kry->ptps[t] = kry->your[t];

     kry->me = false;
  }
  else
  {
     for(t = 0; t < Nt; t++)
       if(kry->ptps[t] != NULL) *kry->ptps[t] = kry->ptrs[t];

     kry->me =  true;
  }
  #pragma omp barrier

  return HXT_STATUS_OK;
}

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                       GMRES FUNCTION                       == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static inline int            decomposed_gmres_zaxpy (const int             irhs,
                                                           double_complex    *y,
                                                     const size_t           y_n,
                                                     const double_complex    *x,
                                                     const size_t           x_n,
                                                     const double_complex alpha);
static inline double         decomposed_gmres_dznrm2(const int             irhs,
                                                     const double_complex    *x,
                                                     const size_t           x_n);
static inline int            decomposed_gmres_zscal (const int             irhs,
                                                           double_complex    *s,
                                                     const size_t           s_n,
                                                     const double_complex alpha);
static inline double_complex decomposed_gmres_zdotc (const int             irhs,
                                                     const double_complex  *x  ,
                                                     const size_t           x_n,
                                                     const double_complex  *y  ,
                                                     const size_t           y_n);

static void decomposed_gmres_update(double_complex **grs, const size_t *grs_n, const size_t grs_nn,
                                    double_complex **vrs, const size_t *vrs_n, const size_t vrs_nn,
                                    const size_t    irhs, const size_t   nrhs,
                                    const size_t    i   , const int   iters_n)
{
  size_t et;

  for(et = 0; et < vrs_nn; et++)
    decomposed_gmres_zscal(irhs, grs[et], grs_n[et], 0.);

  double_complex *y  =  malloc ( (iters_n+1)*sizeof(double_complex) );
  memcpy(y, &SS_ORIGIN(irhs, 0), (iters_n+1)*sizeof(double_complex) );

  int  k, e;
  for (k = i; k >= 0; k--)
  {
    y[k] /= HH_ORIGIN(irhs, k, k);
    for(e = k - 1; e >= 0; e--)
    {
      y[e] -= HH_ORIGIN(irhs, e, k) * y[k];
    }
  }

  for(k = 0; k <= i; k++)
  {
    for(et = 0; et < vrs_nn; et++)
    {
      double_complex *ivrs = &vrs[et][ k * vrs_n[et] * nrhs ];
      decomposed_gmres_zaxpy(irhs, grs[et], grs_n[et], ivrs, vrs_n[et], y[k]);
    }
  }
  free(y);
}

// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                         FREE MATRIX                        == * //
// * ======                                                    ====== * //
// * ================================================================ * //
static double_complex fZero(const double *xyz)
{
  double_complex re = 0.;
  return re;
}

static HXTStatus multMatrix(HXTGMRES1   *kry,
                            const size_t itx, double_complex **xs, const size_t *xs_n,
                            const size_t ity, double_complex **ys, const size_t *ys_n)
{
  HXTSystem  *  sys = kry->  sys;
  HXTSystem **bcSys = kry->bcSys;

  const size_t nrhs = sys->nrhs;

  size_t l, e, k;

  for(l = 0; l < nrhs; l++)
  {
    HXT_CHECK( hxtSystemRHSZero(sys, l) );
    for(e = 0; e < 4; e++)
    {
      const size_t    ins_n =  xs_n[e];
      double_complex *ins   = &xs  [e][ itx * ins_n * nrhs ];

      HXT_CHECK( hxtSystemArrToSol(bcSys[e], l , ins, ins_n, 0.             ) );
      HXT_CHECK( hxtSystemBMxToRHS(sys     , 0., bcSys[e],   1., bcSys[e], l) );
    }
    for(k = 0; k < kry->diric_n; k++) 
      HXT_CHECK( hxtSystemCSTR (sys, l, kry->diric[k], fZero) ); 

    HXT_CHECK( hxtSystemPhase(sys, l, 1, 33 ) );

    for(e = 0; e < 4; e++)
    {
      const size_t    ins_n =  xs_n[e];
      double_complex *ins   = &xs  [e][ itx * ins_n * nrhs ];

      HXT_CHECK( hxtSystemRHSZero (bcSys[e], l) );

      HXT_CHECK( hxtSystemCMxToRHS(bcSys[e], 0., bcSys[e],   2., sys     , l) );
      HXT_CHECK( hxtSystemArrToSol(bcSys[e], l , ins, ins_n, 0.             ) );
      HXT_CHECK( hxtSystemBMxToRHS(bcSys[e], 0., bcSys[e],  -1., bcSys[e], l) );

      HXT_CHECK( hxtSystemPhase(bcSys[e], l, 1, 33) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- exchange functions                                         -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( gmres_exchange_do (kry) );

  for(l = 0; l < nrhs; l++)
  {
    for(e = 0; e < 4; e++)
    {
      const size_t    outs_n =  ys_n[e];
      double_complex *outs   = &ys  [e][ ity * outs_n * nrhs ];

      HXT_CHECK( hxtSystemSolToArr(bcSys[e], l, outs, outs_n, 0.) );
    }
  }

  return HXT_STATUS_OK;
}



HXTStatus hxtGMRES1Create(HXTGMRES1 **kry)
{
  HXT_CHECK( hxtMalloc(kry, sizeof(HXTGMRES1)) );
  if((*kry) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTGMRES1 *ptr = *kry;

  ptr->threadid = -1;
  ptr->mthreads = -1;

  ptr->edges_n = 0;

  ptr->me   = true;
  ptr->your = NULL;
  ptr->ptrs = NULL;
  ptr->ptps = NULL;

  ptr->grs = NULL;
  ptr->brs = NULL;
  ptr->xrs = NULL;
  ptr->rrs = NULL;
  ptr->vrs = NULL;
  ptr->zrs = NULL;
  ptr->wrs = NULL;

  ptr->grs_n = NULL;
  ptr->brs_n = NULL;
  ptr->xrs_n = NULL;
  ptr->rrs_n = NULL;
  ptr->vrs_n = NULL;
  ptr->zrs_n = NULL;
  ptr->wrs_n = NULL;

  ptr->  sys = NULL;
  ptr->bcSys = NULL;

  ptr->Ix = 0;
  ptr->Iy = 0;
  ptr->Nx = 0;
  ptr->Ny = 0;

  ptr->iters_n = 0;

  ptr->diric   = NULL;
  ptr->diric_n =    0;
 
  return HXT_STATUS_OK;
}

HXTStatus hxtGMRES1Build (HXTGMRES1    *kry, size_t iters_n,
		          HXTSystem    *sys, size_t    nrhs,
                          HXTSystem **bcSys, size_t edges_n,
			  HXTGroup  **diric, size_t diric_n,
		          const int   Ix, const int      Iy,
			  const int   Nx, const int      Ny)
{
  const int threadid = omp_get_thread_num ();
  const int mthreads = omp_get_max_threads();

  HXTGMRES1 *ptr = kry;

  ptr->threadid = threadid;
  ptr->mthreads = mthreads;
  ptr->edges_n  =  edges_n;
  ptr->iters_n  =  iters_n;
  ptr->nrhs     =     nrhs;

  ptr->  sys =   sys;
  ptr->bcSys = bcSys;

  ptr->Ix = Ix;
  ptr->Iy = Iy;
  ptr->Nx = Nx;
  ptr->Ny = Ny;

  #pragma omp single
  {
    mptrs = malloc( mthreads*mthreads*sizeof(double_complex*) );

    norm_b    = calloc(nrhs, sizeof(double));
    norm_r    = calloc(nrhs, sizeof(double));
    residual  = calloc(nrhs, sizeof(double));

    ss_origin = calloc(nrhs*(iters_n+1)             ,sizeof(double_complex) );
    cs_origin = calloc(nrhs*(iters_n+1)             ,sizeof(double_complex) );
    sn_origin = calloc(nrhs*(iters_n+1)             ,sizeof(double_complex) );
    hh_origin = calloc(nrhs*(iters_n+1)*(iters_n+1) ,sizeof(double_complex) );
  }
  #pragma omp barrier

  ptr->grs   = malloc( edges_n*sizeof(double_complex*) );
  ptr->brs   = malloc( edges_n*sizeof(double_complex*) );
  ptr->xrs   = malloc( edges_n*sizeof(double_complex*) );
  ptr->rrs   = malloc( edges_n*sizeof(double_complex*) );
  ptr->vrs   = malloc( edges_n*sizeof(double_complex*) );
  ptr->zrs   = malloc( edges_n*sizeof(double_complex*) );
  ptr->wrs   = malloc( edges_n*sizeof(double_complex*) );

  ptr->grs_n = malloc( edges_n*sizeof(double_complex ) );
  ptr->brs_n = malloc( edges_n*sizeof(double_complex ) );
  ptr->xrs_n = malloc( edges_n*sizeof(double_complex ) );
  ptr->rrs_n = malloc( edges_n*sizeof(double_complex ) );
  ptr->vrs_n = malloc( edges_n*sizeof(double_complex ) );
  ptr->zrs_n = malloc( edges_n*sizeof(double_complex ) );
  ptr->wrs_n = malloc( edges_n*sizeof(double_complex ) );

  size_t e = 0;

  for(e = 0; e < edges_n; e++)
  {
    const int nUnknowns = bcSys[e]->nUnknowns;

    ptr->grs  [e] = NULL;
    ptr->brs  [e] = NULL;
    ptr->xrs  [e] = NULL;
    ptr->rrs  [e] = NULL;
    ptr->vrs  [e] = NULL;
    ptr->zrs  [e] = NULL;
    ptr->wrs  [e] = NULL;

    ptr->grs  [e] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->brs  [e] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->xrs  [e] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->rrs  [e] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );
    ptr->vrs  [e] = calloc(nUnknowns * nrhs * (iters_n+1), sizeof(double_complex) );
    ptr->zrs  [e] = calloc(nUnknowns * nrhs * (iters_n+1), sizeof(double_complex) );
    ptr->wrs  [e] = calloc(nUnknowns * nrhs              , sizeof(double_complex) );

    ptr->grs_n[e] = nUnknowns ;
    ptr->brs_n[e] = nUnknowns ;
    ptr->xrs_n[e] = nUnknowns ;
    ptr->rrs_n[e] = nUnknowns ;
    ptr->vrs_n[e] = nUnknowns ;
    ptr->zrs_n[e] = nUnknowns ;
    ptr->wrs_n[e] = nUnknowns ;
  }

  ptr->me   = true;
  ptr->your = NULL;
  ptr->ptrs = NULL;
  ptr->ptps = NULL;
  ptr->your = malloc( mthreads*sizeof(double_complex* ) );
  ptr->ptrs = malloc( mthreads*sizeof(double_complex* ) );
  ptr->ptps = malloc( mthreads*sizeof(double_complex**) );

  for(int t = 0; t < mthreads; t++) ptr->your[t] = NULL;
  for(int t = 0; t < mthreads; t++) ptr->ptrs[t] = NULL;
  for(int t = 0; t < mthreads; t++) ptr->ptps[t] = NULL;

  const int It = threadid;
//const int Nt = mthreads;
  if(Iy   > 0 )
  {
    ptr->ptrs[ It-1  ] =  bcSys[0]->x;
    ptr->ptps[ It-1  ] = &bcSys[0]->x;
  }
  if(Ix+1 < Nx)
  {
    ptr->ptrs[ It+Ny ] =  bcSys[1]->x;
    ptr->ptps[ It+Ny ] = &bcSys[1]->x;
  }
  if(Iy+1 < Ny)
  {
    ptr->ptrs[ It+1  ] =  bcSys[2]->x;
    ptr->ptps[ It+1  ] = &bcSys[2]->x;
  }
  if(Ix   > 0 )
  {
    ptr->ptrs[ It-Ny ] =  bcSys[3]->x;
    ptr->ptps[ It-Ny ] = &bcSys[3]->x;
  }
  HXT_CHECK( gmres_exchange_set(ptr) );

  ptr->diric   = diric  ;
  ptr->diric_n = diric_n;

  return HXT_STATUS_OK;
}

HXTStatus hxtGMRES1Delete(HXTGMRES1 **kry)
{
  HXTGMRES1 *ptr = *kry;

  const size_t edges_n = ptr->edges_n;

  #pragma omp barrier
  for(size_t e = 0; e < edges_n; e++)
  {
    free(ptr->grs[e]);
    free(ptr->brs[e]);
    free(ptr->xrs[e]);
    free(ptr->rrs[e]);
    free(ptr->vrs[e]);
    free(ptr->zrs[e]);
    free(ptr->wrs[e]);

    ptr->grs  [e] = NULL;
    ptr->brs  [e] = NULL;
    ptr->xrs  [e] = NULL;
    ptr->rrs  [e] = NULL;
    ptr->vrs  [e] = NULL;
    ptr->zrs  [e] = NULL;
    ptr->wrs  [e] = NULL;
    
    ptr->grs_n[e] =    0;
    ptr->brs_n[e] =    0;
    ptr->xrs_n[e] =    0;
    ptr->rrs_n[e] =    0;
    ptr->vrs_n[e] =    0;
    ptr->zrs_n[e] =    0;
    ptr->wrs_n[e] =    0;
  }
  free(ptr->grs  );
  free(ptr->brs  );
  free(ptr->xrs  );
  free(ptr->rrs  );
  free(ptr->vrs  );
  free(ptr->zrs  );
  free(ptr->wrs  );
  free(ptr->grs_n);
  free(ptr->brs_n);
  free(ptr->xrs_n);
  free(ptr->rrs_n);
  free(ptr->vrs_n);
  free(ptr->zrs_n);
  free(ptr->wrs_n);

  free(ptr->ptrs);
  free(ptr->ptps);
  free(ptr->your);
  ptr->me =  true;

  #pragma omp single
  {
    free(mptrs    );
  
    free(norm_b   );
    free(norm_r   );
    free(residual );
  
    free(ss_origin);
    free(cs_origin);
    free(sn_origin);
    free(hh_origin);
  }
  #pragma omp barrier

  ptr->threadid = -1;
  ptr->mthreads = -1;

  ptr->edges_n  =  0;
  ptr->   nrhs  =  0;

  ptr->  sys = NULL;
  ptr->bcSys = NULL;

  ptr->Ix = 0;
  ptr->Iy = 0;
  ptr->Nx = 0;
  ptr->Ny = 0;

  ptr->iters_n = 0;

  ptr->diric   = NULL;
  ptr->diric_n =    0;

  HXT_CHECK( hxtFree(kry) );

  return HXT_STATUS_OK;
}

HXTStatus hxtGMRES1Solve(HXTGMRES1 *kry)
{
  // * ---------------------------------------------------------------- * //
  // * ------                                                    ------ * //
  // * --                             START                          -- * //
  // * ------                                                    ------ * //
  // * ---------------------------------------------------------------- * //
  double tolerance = 10e-7;

  HXTGMRES1 *ptr = kry;

  const size_t iters_n = ptr->iters_n;
  const size_t edges_n = ptr->edges_n;
  const size_t    nrhs = ptr->   nrhs;

  HXTSystem  *  sys = ptr->  sys;
  HXTSystem **bcSys = ptr->bcSys;

  double_complex **grs =  ptr->grs;
  double_complex **brs =  ptr->brs;
  double_complex **xrs =  ptr->xrs;
  double_complex **rrs =  ptr->rrs;
  double_complex **vrs =  ptr->vrs;
  double_complex **zrs =  ptr->zrs;
  double_complex **wrs =  ptr->wrs;

  size_t *grs_n = ptr->grs_n;
  size_t *brs_n = ptr->brs_n;
  size_t *xrs_n = ptr->xrs_n;
  size_t *rrs_n = ptr->rrs_n;
  size_t *vrs_n = ptr->vrs_n;
  size_t *zrs_n = ptr->zrs_n;
  size_t *wrs_n = ptr->wrs_n;

  int i = 0;

  size_t j, l, e;

  // * ---------------------------------------------------------------- * //
  // * -- get w_{n+1}                                                -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( hxtSystemPhase(sys, 0, nrhs,  33 ) );

  // * ---------------------------------------------------------------- * //
  // * -- update law g_{n+1} = -g_{n} + 2 * S(v_{n+1})               -- * //
  // * -- here       g_{n  } =  0                                    -- * //
  // * -- thus       b       =  0     + 2 * S(w_{n+1})               -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(e = 0; e < edges_n; e++)
    {
      HXT_CHECK( hxtSystemRHSZero (bcSys[e], l) );
      HXT_CHECK( hxtSystemCMxToRHS(bcSys[e], 0., bcSys[e], 2., sys, l) );
      HXT_CHECK( hxtSystemPhase   (bcSys[e], l, 1, 33) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- exchange functions                                         -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( gmres_exchange_do (ptr) );

  // * ---------------------------------------------------------------- * //
  // * -- SET B VEC VALUES                                           -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(e = 0; e < edges_n; e++)
    {
      HXT_CHECK( hxtSystemSolToArr(bcSys[e], l, brs[e], brs_n[e], 0.) );
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- SET RHS ZERO                                               -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++) HXT_CHECK( hxtSystemRHSZero(sys, l) );

  // * ---------------------------------------------------------------- * //
  // * -- r0 = A * x0                                                -- * //
  // * ---------------------------------------------------------------- * //
  HXT_CHECK( multMatrix(ptr, 0, xrs, xrs_n, 0, rrs, rrs_n) );

  // * ---------------------------------------------------------------- * //
  // * -- r0 = b - (I - A) * x0 ---> r0 = b - x0 + A * x0            -- * //
  // * ---------------------------------------------------------------- * //
  const double_complex cone   =  1.;
  const double_complex cminus = -1.;

  for(l = 0; l < nrhs; l++)
  {
    for(e = 0; e < edges_n; e++)
    {
      decomposed_gmres_zaxpy(l, rrs[e], rrs_n[e], brs[e], brs_n[e], cone  );
      decomposed_gmres_zaxpy(l, rrs[e], rrs_n[e], xrs[e], xrs_n[e], cminus);
    }
  }

  // * ---------------------------------------------------------------- * //
  // * -- calculate norm_r and norm_b                                -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    for(e = 0; e < edges_n; e++)
    {
      double tmp_b = 0.;
      double tmp_r = 0.;
      tmp_b += decomposed_gmres_dznrm2(l, brs[e], brs_n[e])*
               decomposed_gmres_dznrm2(l, brs[e], brs_n[e]);
      tmp_r += decomposed_gmres_dznrm2(l, rrs[e], rrs_n[e])*
               decomposed_gmres_dznrm2(l, rrs[e], rrs_n[e]);
      #pragma omp atomic update
      norm_b[l] += tmp_b;
      #pragma omp atomic update
      norm_r[l] += tmp_r;
    }
  }
  #pragma omp barrier

  // * ---------------------------------------------------------------- * //
  // * -- ss[0] = norm_r                                             -- * //
  // * ---------------------------------------------------------------- * //
  #pragma omp single
  {
    for(l = 0; l < nrhs; l++)
    {
      norm_b[l] = sqrt(norm_b[l]);
      norm_r[l] = sqrt(norm_r[l]);

      if(norm_b[l] == 0.) norm_b[l] = 1.;

      residual [l] = norm_r[l] / norm_b[l];

      SS_ORIGIN(l, 0) = norm_r[l];
    }
    printf("norm_b[0]     : %f\n",    norm_b[0]);
    printf("norm_r[0]     : %f\n",    norm_r[0]);
  }
  #pragma omp barrier

  // * ---------------------------------------------------------------- * //
  // * -- LOOP OF GMRES                                              -- * //
  // * ---------------------------------------------------------------- * //
  size_t k  = 0;
         j  = 1;
  while( j <= iters_n)
  {
    // * ---------------------------------------------------------------- * //
    // * -- v[0] = v[0] + r / norm_r                                   -- * //
    // * ---------------------------------------------------------------- * //
    for(l = 0; l < nrhs; l++)
    {
      for(e = 0; e < edges_n; e++)
      {
        const double_complex rec_r = 1./norm_r[l];
        decomposed_gmres_zaxpy(l, &vrs[e][0], vrs_n[e], rrs[e], rrs_n[e], rec_r );
      }
    }
    #pragma omp barrier

    for(i = 0; i < iters_n && j <= iters_n; i++, j++)
    {
      // * ---------------------------------------------------------------- * //
      // * -- z[i] = M^{-1} v[i]                                         -- * //
      // * ---------------------------------------------------------------- * //
      for( l = 0; l < nrhs; l++)
      {
        for(e = 0; e < edges_n; e++)
        {
          double_complex *ivrs = &vrs[e][i * vrs_n[e] * nrhs];
          double_complex *izrs = &zrs[e][i * zrs_n[e] * nrhs];
          decomposed_gmres_zaxpy(l, izrs, zrs_n[e], ivrs, vrs_n[e], 1. );
        }
      }
      //if(i%2 ==0) HXT_CHECK( SGSFwd1(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
      //if(i%2 ==0) HXT_CHECK( SGSBwd1(ptr, "L1", i, zrs, zrs_n, i, zrs, zrs_n) );
      //if(i%2 ==1) HXT_CHECK( SGSFwd1(ptr, "L2", i, zrs, zrs_n, i, zrs, zrs_n) );
      //if(i%2 ==1) HXT_CHECK( SGSBwd1(ptr, "L2", i, zrs, zrs_n, i, zrs, zrs_n) );
      
      // * ---------------------------------------------------------------- * //
      // * -- w = A v[i]                                                 -- * //
      // * ---------------------------------------------------------------- * //
      HXT_CHECK( multMatrix(ptr, i, zrs, zrs_n, 0, wrs, wrs_n) );

      // * ---------------------------------------------------------------- * //
      // * --         w = v[i] - w                                       -- * //
      // * -- that is w =  (I- A)v[i]                                    -- * //
      // * ---------------------------------------------------------------- * //
      for( l = 0; l < nrhs; l++)
      {
        for(e = 0; e < edges_n; e++)
        {
          double_complex *izrs = &zrs[e][i * zrs_n[e] * nrhs];

          decomposed_gmres_zscal(l, wrs[e], wrs_n[e],                 cminus);
          decomposed_gmres_zaxpy(l, wrs[e], wrs_n[e], izrs, zrs_n[e], cone  );
        }
      }

      for(l = 0; l < nrhs; l++)
      {
        for(k = 0; k <= i; k++)
        {
          // * ---------------------------------------------------------------- * //
          // * -- h[k][i] = zdotc(v[k], w)                                   -- * //
          // * ---------------------------------------------------------------- * //
          for(e = 0; e < edges_n; e++)
          {
            double_complex *ivrs = &vrs[e][k * vrs_n[e] * nrhs];

            double_complex tmp = decomposed_gmres_zdotc(l, ivrs, vrs_n[e], wrs[e], wrs_n[e]);

            double* complex_h = (double*)(&HH_ORIGIN(l, k, i));
            #pragma omp atomic update
            complex_h[0] += creal(tmp);
            #pragma omp atomic update
            complex_h[1] += cimag(tmp);
          }
          #pragma omp barrier

          // * ---------------------------------------------------------------- * //
          // * -- w = w - v[k] * h[k][i];                                    -- * //
          // * ---------------------------------------------------------------- * //
          for(e = 0; e < edges_n; e++)
          {
            double_complex *ivrs = &vrs[e][k * vrs_n[e] * nrhs];

            double_complex  tmp_hh = - HH_ORIGIN(l, k, i);

            decomposed_gmres_zaxpy(l, wrs[e], wrs_n[e], ivrs, vrs_n[e], tmp_hh);
          }
          #pragma omp barrier
        }
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- h[i+1][i] = dznrm2(w)                                      -- * //
      // * ---------------------------------------------------------------- * //
      for(l = 0; l < nrhs; l++)
      {
        for(e = 0; e < edges_n; e++)
        {
          double sum = decomposed_gmres_dznrm2(l, wrs[e], wrs_n[e]) *
                       decomposed_gmres_dznrm2(l, wrs[e], wrs_n[e]) ;

          double* complex_h = (double*)(&HH_ORIGIN(l, i+1, i));
          #pragma omp atomic update
          complex_h[0] += sum;
        }
        #pragma omp barrier

        #pragma omp single
        HH_ORIGIN(l, i+1, i) = csqrt( HH_ORIGIN(l, i+1, i) );
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- v[i+1] = v[i+1] + w / h[i+1][i]                            -- * //
      // * ---------------------------------------------------------------- * //
      for(l = 0; l < nrhs; l++)
      {
        for(e = 0; e < edges_n; e++)
        {
          double_complex *ivrs = &vrs[e][ (i+1) * vrs_n[e] * nrhs];

          double_complex  rec_hh = 1. / HH_ORIGIN(l, i+1, i);

          decomposed_gmres_zaxpy(l, ivrs, vrs_n[e], wrs[e], wrs_n[e], rec_hh);
        }
        #pragma omp barrier
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- GIVEN ROTATION                                             -- * //
      // * ---------------------------------------------------------------- * //
      #pragma omp single
      {
        printf("    iteration %lu, ", j);
        for(l = 0; l < nrhs; l++)
        {
          for(k = 0; k < i; k++)
          {
            hxtzarot(&HH_ORIGIN(l, k, i), &HH_ORIGIN(l, k+1, i), &CS_ORIGIN(l, k), &SN_ORIGIN(l, k));
          }
          hxtzgrot(&HH_ORIGIN(l, i, i), &HH_ORIGIN(l, i+1, i), &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));
          hxtzarot(&HH_ORIGIN(l, i, i), &HH_ORIGIN(l, i+1, i), &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));
          hxtzarot(&SS_ORIGIN(l, i)   , &SS_ORIGIN(l, i+1)   , &CS_ORIGIN(l, i), &SN_ORIGIN(l, i));

          // * residual = cabs(ss[i+1]) / norm_b
          residual[l] = cabs ( SS_ORIGIN(l, i+1) ) / norm_b[l];
          printf("res %lu: %.3e  ", l, residual[l]);
        }
        printf("\n");
      }
      #pragma omp barrier

      // * ---------------------------------------------------------------- * //
      // * -- UPDATE                                                     -- * //
      // * ---------------------------------------------------------------- * //
      double resmax = 0.;
      for(l = 0; l < nrhs; l++) if(residual[l] > resmax) resmax = residual[l];

    //if(residual[0] < tolerance)
      if(resmax      < tolerance)
      {
        for(l = 0; l < nrhs; l++)
        {
          HXT_CHECK( hxtSystemRHSZero(sys, l) );

          decomposed_gmres_update(grs, grs_n, edges_n,
                                  zrs, zrs_n, edges_n,
                                  l, nrhs, i, iters_n);

          for(e = 0; e < edges_n; e++)
          {
            HXT_CHECK( hxtSystemArrToSol(bcSys[e], l, grs  [e],     grs_n[e], 0.) );
            HXT_CHECK( hxtSystemBMxToRHS(sys     , 0, bcSys[e], 1., bcSys[e], l ) );
          }
        }
        return HXT_STATUS_OK;
      }


    }
  }
  // * ---------------------------------------------------------------- * //
  // * -- UPDATE                                                     -- * //
  // * ---------------------------------------------------------------- * //
  for(l = 0; l < nrhs; l++)
  {
    HXT_CHECK( hxtSystemRHSZero(sys, l) );

    decomposed_gmres_update(grs, grs_n  , edges_n,
                            zrs, zrs_n  , edges_n,
                            l, nrhs, i-1, iters_n);

    for(e = 0; e < edges_n; e++)
    {
      HXT_CHECK( hxtSystemArrToSol(bcSys[e], l, grs  [e],     grs_n[e], 0.) );
      HXT_CHECK( hxtSystemBMxToRHS(sys     , 0, bcSys[e], 1., bcSys[e], l ) );
    }
  }
  return HXT_STATUS_OK;
}

static inline int decomposed_gmres_zaxpy(const int             irhs,
                                               double_complex    *y,
                                         const size_t           y_n,
                                         const double_complex    *x,
                                         const size_t           x_n,
                                         const double_complex alpha)
{
  if(y_n == 0) return 1;
  hxtzaxpy(&y[irhs*y_n], &x[irhs*y_n], alpha, y_n);
  return 0;
}

static inline double decomposed_gmres_dznrm2(const int             irhs,
                                             const double_complex    *x,
                                             const size_t           x_n)
{
  if(x_n == 0) return 0.;
  else return hxtdznrm2(&x[irhs*x_n], x_n);
}

static inline int    decomposed_gmres_zscal (const int             irhs,
                                                   double_complex    *s,
                                             const size_t           s_n,
                                             const double_complex alpha)
{
  if(s_n == 0) return 1;
  hxtzscal(&s[irhs*s_n], alpha, s_n);
  return 0;
}

static inline double_complex decomposed_gmres_zdotc(const int             irhs,
                                                    const double_complex  *x  ,
                                                    const size_t           x_n,
                                                    const double_complex  *y  ,
                                                    const size_t           y_n)
{
  double_complex zero = 0.;
  if(x_n == 0) return zero;
  else return hxtzdotc(&x[irhs*x_n], &y[irhs*x_n], x_n);
}



// * ================================================================ * //
// * ======                                                    ====== * //
// * ==                       PRECONDITIONER                       == * //
// * ======                                                    ====== * //
// * ================================================================ * //
HXTStatus SGSFwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  HXTSystem     *  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  int Loop = 0;
  int Ic   = 0;

  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;
       if(strcmp( type, "L1" ) == 0 )
  {
    arrs[0] = 1;
    arrs[1] = 2;
    tmls[0] = 0;
    tmls[1] = 3;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;
    Ic   = kry-> Ix +
           kry-> Iy ;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    arrs[0] = 0;
    arrs[1] = 1;
    tmls[0] = 2;
    tmls[1] = 3;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;
    Ic   = kry-> Ix -
           kry-> Iy +
           kry-> Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    arrs[0] = 1;
    tmls[0] = 3;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;
    Ic   = kry-> Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    arrs[0] = 2;
    tmls[0] = 0;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;
    Ic   = kry-> Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

  size_t l, e, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                      FORWARD OPERATOR                      -- * //
  // * ---------------------------------------------------------------- * //
  for(k = 0; k < Loop; k++)
  {
    for(l = 0; l < nrhs; l++)
    {
      HXT_CHECK( hxtSystemRHSZero(sys, l) );  
      HXT_CHECK( hxtSystemSolZero(sys, l) );  
      if( Ic == k)
      {
        for(e = 0; e < edges_n; e++)
        {
          c = e;

          const size_t    ins_n =  xs_n[c];
          double_complex *ins   = &xs  [c][ itx * ins_n * nrhs ];

          HXT_CHECK( hxtSystemArrToSol(bcSys[c], l , ins, ins_n, 0.             ) );
          HXT_CHECK( hxtSystemBMxToRHS(sys     , 0., bcSys[c],   1., bcSys[c], l) );
        }
        for(i = 0; i < kry->diric_n; i++) 
          HXT_CHECK( hxtSystemCSTR (sys, l, kry->diric[i], fZero) ); 

        HXT_CHECK( hxtSystemPhase(sys, l, 1, 33) );
  
        for(e = 0; e < arrs_n; e++)
        {
          c = arrs[e];

          const size_t    ins_n =  xs_n[c];
          double_complex *ins   = &xs  [c][ itx * ins_n * nrhs ];
      
          HXT_CHECK( hxtSystemRHSZero (bcSys[c], l) );
          HXT_CHECK( hxtSystemSolZero (bcSys[c], l) );
    
          HXT_CHECK( hxtSystemArrToSol(bcSys[c], l , ins, ins_n, 0.             ) );
          HXT_CHECK( hxtSystemBMxToRHS(bcSys[c], 0., bcSys[c],  -1., bcSys[c], l) );
          HXT_CHECK( hxtSystemCMxToRHS(bcSys[c], 0., bcSys[c],   2., sys     , l) );
    
          HXT_CHECK( hxtSystemPhase(bcSys[c], l, 1, 33) );
        }
      }
      #pragma omp barrier
    }

    // *--- exchange functions 
    HXT_CHECK( gmres_exchange_do (kry) );
    
    for(l = 0; l < nrhs; l++)
    {
      if( Ic == ((k+1)%Loop) )
      {
        for(e = 0; e < tmls_n; e++)
        {
          c = tmls[e];

          const size_t    outs_n =  ys_n[c];
          double_complex *outs   = &ys  [c][ ity * outs_n * nrhs ];
    
          HXT_CHECK( hxtSystemSolToArr(bcSys[c], l, outs, outs_n, 1.) );
        }
      }
    }
    #pragma omp barrier
  }

  return HXT_STATUS_OK;
}

HXTStatus SGSBwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  HXTSystem     *  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;
  const size_t edges_n = kry->edges_n;

  int Loop = 0;
  int Ic   = 0;

  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;
       if(strcmp( type, "L1" ) == 0 )
  {
    arrs[0] = 0;
    arrs[1] = 3;
    tmls[0] = 1;
    tmls[1] = 2;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;
    Ic   = kry-> Ix +
           kry-> Iy ;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    arrs[0] = 2;
    arrs[1] = 3;
    tmls[0] = 0;
    tmls[1] = 1;

    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;
    Ic   = kry-> Ix -
           kry-> Iy +
           kry-> Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    arrs[0] = 3;
    tmls[0] = 1;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;
    Ic   = kry-> Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    arrs[0] = 0;
    tmls[0] = 2;

    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;
    Ic   = kry-> Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

  size_t l, e, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                     BACKWARD OPERATOR                      -- * //
  // * ---------------------------------------------------------------- * //
  for(k = Loop-1; k >= 0; k--)
  {
    for(l = 0; l < nrhs; l++)
    {
      HXT_CHECK( hxtSystemRHSZero(sys, l) );  
      HXT_CHECK( hxtSystemSolZero(sys, l) );  
      if( Ic == k)
      {
        for(e = 0; e < edges_n; e++)
        {
          c = e;

          const size_t    ins_n =  xs_n[c];
          double_complex *ins   = &xs  [c][ itx * ins_n * nrhs ];
    
          HXT_CHECK( hxtSystemArrToSol(bcSys[c], l , ins, ins_n, 0.             ) );
          HXT_CHECK( hxtSystemBMxToRHS(sys     , 0., bcSys[c],   1., bcSys[c], l) );
        }
        for(i = 0; i < kry->diric_n; i++) 
          HXT_CHECK( hxtSystemCSTR (sys, l, kry->diric[i], fZero) ); 

        HXT_CHECK( hxtSystemPhase(sys, l, 1, 33) );
  
        for(e = 0; e < arrs_n; e++)
        {
          c = arrs[e];

          const size_t    ins_n =  xs_n[c];
          double_complex *ins   = &xs  [c][ itx * ins_n * nrhs ];
    
          HXT_CHECK( hxtSystemRHSZero (bcSys[c], l) );
          HXT_CHECK( hxtSystemSolZero (bcSys[c], l) );
    
          HXT_CHECK( hxtSystemArrToSol(bcSys[c], l , ins, ins_n, 0.             ) );
          HXT_CHECK( hxtSystemBMxToRHS(bcSys[c], 0., bcSys[c],  -1., bcSys[c], l) );
          HXT_CHECK( hxtSystemCMxToRHS(bcSys[c], 0., bcSys[c],   2., sys     , l) );

          HXT_CHECK( hxtSystemPhase(bcSys[c], l, 1, 33) );
        }
      }
      #pragma omp barrier
    }

    // *--- exchange functions 
    HXT_CHECK( gmres_exchange_do (kry) );
    
    for(l = 0; l < nrhs; l++)
    {
      if( Ic == ((k-1+Loop)%Loop) )
      {
        for(e = 0; e < tmls_n; e++)
        {
          c = tmls[e];

          const size_t    outs_n =  ys_n[c];
          double_complex *outs   = &ys  [c][ ity * outs_n * nrhs ];
  
          HXT_CHECK( hxtSystemSolToArr(bcSys[c], l, outs, outs_n, 1.) );
        }
      }
    }
    #pragma omp barrier
  }

  return HXT_STATUS_OK;
}

HXTStatus SWPFwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  HXTSystem     *  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;

  int Loop = 0;
  int Ic   = 0;

  int deps[4] = {0}; size_t deps_n = 0;
  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;
       if(strcmp( type, "L1" ) == 0 )
  {
    deps[0] = 0;
    deps[1] = 3;
    arrs[0] = 1;
    arrs[1] = 2;
    tmls[0] = 0;
    tmls[1] = 3;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + kry-> Ny - 1;
    Ic   = kry-> Ix + kry-> Iy ;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    deps[0] = 2;
    deps[1] = 3;
    arrs[0] = 0;
    arrs[1] = 1;
    tmls[0] = 2;
    tmls[1] = 3;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + kry-> Ny - 1;
    Ic   = kry-> Ix - kry-> Iy +
           kry-> Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    deps[0] = 0;
    deps[1] = 2;
    deps[2] = 3;
    arrs[0] = 1;
    tmls[0] = 3;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;
    Ic   = kry-> Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    deps[0] = 0;
    deps[1] = 1;
    deps[2] = 3;
    arrs[0] = 2;
    tmls[0] = 0;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;
    Ic   = kry-> Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

//size_t l, e;
  size_t e, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                   FORWARD TRANSFER OPERATOR                -- * //
  // * ---------------------------------------------------------------- * //
  for(k = Loop; k > 0; k--)
  {
    const int Id = (k-1+Ic)%Loop;
    const int l  = (k  +Ic)%Loop;
//for(k = 0; k < Loop; k++)
//{
  //for(l = 0; l < nrhs; l++)
  //{
    //if( Ic == k)
    //{
        HXT_CHECK( hxtSystemRHSZero(sys, l) );  
        HXT_CHECK( hxtSystemSolZero(sys, l) );  
        for(e = 0; e < deps_n; e++)
        {
          c = deps[e];

          const size_t    ins_n =  xs_n[c];
          double_complex *ins   = &xs  [c][ itx * ins_n * nrhs ];
      
          HXT_CHECK( hxtSystemArrToSol(bcSys[c], l , ins, ins_n, 0.             ) );
          HXT_CHECK( hxtSystemBMxToRHS(sys     , 0., bcSys[c],   1., bcSys[c], l) );
        }
        for(i = 0; i < kry->diric_n; i++) 
          HXT_CHECK( hxtSystemCSTR (sys, l, kry->diric[i], fZero) ); 

        HXT_CHECK( hxtSystemPhase(sys, l, 1, 33) );
  
        for(e = 0; e < arrs_n; e++)
        {
          c = arrs[e];
    
          HXT_CHECK( hxtSystemRHSZero (bcSys[c], l) );
          HXT_CHECK( hxtSystemSolZero (bcSys[c], l) );
          HXT_CHECK( hxtSystemCMxToRHS(bcSys[c], 0., bcSys[c], 2., sys, l) );
          HXT_CHECK( hxtSystemPhase   (bcSys[c], l, 1, 33) );
        }
    //}
      #pragma omp barrier

      // * --- exchange functions 
      HXT_CHECK( gmres_exchange_do (kry) );
    
    //if( Ic == ((k+1)%Loop) )
    //{
        for(e = 0; e < tmls_n; e++)
        {
          c = tmls[e];

          const size_t    outs_n =  ys_n[c];
          double_complex *outs   = &ys  [c][ ity * outs_n * nrhs ];
        //HXT_CHECK( hxtSystemSolToArr(bcSys[c], l , outs, outs_n, 1.) );
          HXT_CHECK( hxtSystemSolToArr(bcSys[c], Id, outs, outs_n, 1.) );
        }
    //}
      #pragma omp barrier
  //}
    #pragma omp barrier
  }

  return HXT_STATUS_OK;
}

HXTStatus SWPBwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n)
{
  HXTSystem     *  sys = kry->    sys;
  HXTSystem    **bcSys = kry->  bcSys;
  const size_t    nrhs = kry->   nrhs;

  int Loop = 0;
  int Ic   = 0;

  int deps[4] = {0}; size_t deps_n = 0;
  int arrs[4] = {0}; size_t arrs_n = 0;
  int tmls[4] = {0}; size_t tmls_n = 0;

       if(strcmp( type, "L1" ) == 0 )
  {
    deps[0] = 1;
    deps[1] = 2;
    arrs[0] = 0;
    arrs[1] = 3;
    tmls[0] = 1;
    tmls[1] = 2;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;
    Ic   = kry-> Ix +
           kry-> Iy ;
  }
  else if(strcmp( type, "L2") == 0 )
  {
    deps[0] = 0;
    deps[1] = 1;
    arrs[0] = 2;
    arrs[1] = 3;
    tmls[0] = 0;
    tmls[1] = 1;

    deps_n  = 2;
    arrs_n  = 2;
    tmls_n  = 2;

    Loop = kry-> Nx + 
           kry-> Ny - 1;
    Ic   = kry-> Ix -
           kry-> Iy +
           kry-> Ny - 1;
  }
  else if(strcmp( type, "BH") == 0 )
  {
    deps[0] = 1;
    deps[1] = 0;
    deps[2] = 2;
    arrs[0] = 3;
    tmls[0] = 1;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Nx;
    Ic   = kry-> Ix;
  }
  else if(strcmp( type, "BV") == 0 )
  {
    deps[0] = 2;
    deps[1] = 1;
    deps[2] = 3;
    arrs[0] = 0;
    tmls[0] = 2;

    deps_n  = 3;
    arrs_n  = 1;
    tmls_n  = 1;

    Loop = kry-> Ny;
    Ic   = kry-> Iy;
  }
  else return HXT_ERROR(HXT_STATUS_FORMAT_ERROR);

//size_t l, e;
  size_t e, i;
  int    k, c;

  // * ---------------------------------------------------------------- * //
  // * --                  BACKWARD TRANSFER OPERATOR                -- * //
  // * ---------------------------------------------------------------- * //
  for(k = 0; k < Loop; k++)
  {
    const int Id = (k+2+Ic)%Loop;
    const int l  = (k+1+Ic)%Loop;
//for(k = Loop-1; k >= 0; k--)
//{
  //for(l = 0; l < nrhs; l++)
  //{
    //if( Ic == k)
    //{
        HXT_CHECK( hxtSystemRHSZero(sys, l) );  
        HXT_CHECK( hxtSystemSolZero(sys, l) );  
        for(e = 0; e < deps_n; e++)
        {
          c = deps[e];

          const size_t    ins_n =  xs_n[c];
          double_complex *ins   = &xs  [c][ itx * ins_n * nrhs ];
    
          HXT_CHECK( hxtSystemArrToSol(bcSys[c], l , ins, ins_n, 0.             ) );
          HXT_CHECK( hxtSystemBMxToRHS(sys     , 0., bcSys[c],   1., bcSys[c], l) );
        }
        for(i = 0; i < kry->diric_n; i++) 
          HXT_CHECK( hxtSystemCSTR (sys, l, kry->diric[i], fZero) ); 

        HXT_CHECK( hxtSystemPhase(sys, l, 1, 33) );
  
        for(e = 0; e < arrs_n; e++)
        {
          c = arrs[e];
    
          HXT_CHECK( hxtSystemRHSZero (bcSys[c], l) );
          HXT_CHECK( hxtSystemSolZero (bcSys[c], l) );
          HXT_CHECK( hxtSystemCMxToRHS(bcSys[c], 0., bcSys[c], 2., sys, l) );
          HXT_CHECK( hxtSystemPhase   (bcSys[c], l, 1, 33) );
        }
    //}
      #pragma omp barrier

      // * --- exchange functions 
      HXT_CHECK( gmres_exchange_do (kry) );
    
    //if( Ic == ((k-1+Loop)%Loop) )
    //{
        for(e = 0; e < tmls_n; e++)
        {
          c = tmls[e];

          const size_t    outs_n =  ys_n[c];
          double_complex *outs   = &ys  [c][ ity * outs_n * nrhs ];
  
        //HXT_CHECK( hxtSystemSolToArr(bcSys[c], l , outs, outs_n, 1.) );
          HXT_CHECK( hxtSystemSolToArr(bcSys[c], Id, outs, outs_n, 1.) );
        }
    //}
      #pragma omp barrier
  //}
    #pragma omp barrier
//}
  }

  return HXT_STATUS_OK;
}
