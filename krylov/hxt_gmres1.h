#ifndef HXTWAVE_GMRES1_H
#define HXTWAVE_GMRES1_H

#include "hxt_system.h"

typedef struct HXTGMRES1Struct
{
  int threadid;
  int mthreads;

  size_t edges_n;
  size_t    nrhs;

  bool me;
  double_complex  **your;
  double_complex  **ptrs;
  double_complex ***ptps;

  double_complex **grs;
  double_complex **brs;
  double_complex **xrs;
  double_complex **rrs;
  double_complex **vrs;
  double_complex **zrs;
  double_complex **wrs;

  size_t *grs_n;
  size_t *brs_n;
  size_t *xrs_n;
  size_t *rrs_n;
  size_t *vrs_n;
  size_t *zrs_n;
  size_t *wrs_n;

  HXTSystem  *  sys;
  HXTSystem **bcSys;

  int Ix;
  int Iy;
  int Nx;
  int Ny;

  int iters_n;

  HXTGroup **diric  ;
  size_t     diric_n;

} HXTGMRES1;

HXTStatus hxtGMRES1Create(HXTGMRES1 **kry);
HXTStatus hxtGMRES1Solve (HXTGMRES1  *kry);
HXTStatus hxtGMRES1Delete(HXTGMRES1 **kry);

HXTStatus hxtGMRES1Build (HXTGMRES1    *kry, size_t iters_n, 
		          HXTSystem    *sys, size_t    nrhs, 
                          HXTSystem **bcSys, size_t edges_n, 
			  HXTGroup  **diric, size_t diric_n, 
		          const int   Ix, const int      Iy, 
			  const int   Nx, const int      Ny);

HXTStatus SGSFwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);
HXTStatus SGSBwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);
HXTStatus SWPFwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);
HXTStatus SWPBwd1(HXTGMRES1   *kry, const char    *type,
                  const size_t itx, double_complex **xs, const size_t *xs_n,
                  const size_t ity, double_complex **ys, const size_t *ys_n);


HXTStatus hxtGMRES1Rectangle(HXTSystem *sys, HXTSystem **bcSys, 
		             const int   Ix, const int      Iy, 
		 	     const int   Nx, const int      Ny, 
			                     const int  niters);

HXTStatus hxtGMRES1Triangle (HXTSystem *sys, HXTSystem **bcSys, 
		             const int   Ix, const int      Iy, 
			     const int   Nx, const int      Ny, 
			                     const int  niters);

#endif
