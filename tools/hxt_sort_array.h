#ifndef HXTWAVE_ARRAY_SORT_H
#define HXTWAVE_ARRAY_SORT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "hxt_omp.h"
#include "hxt_types.h"

/* To use pragma in a macro */                                                           
#if defined(_MSC_VER)
#define _HXTSORTARRAY_PRAGMA(x) __pragma (#x);
#define _HXTSORTARRAY_PRAGMA_ALIGNED(...)
#else
#define _HXTSORTARRAY_PRAGMA(x) _Pragma (#x)
#if defined(HXTSORTARRAY_MALLOC_NO_ALIGN)
#define _HXTSORTARRAY_PRAGMA_ALIGNED(...)
#else
#define _HXTSORTARRAY_PRAGMA_ALIGNED(...) _HXTSORTARRAY_PRAGMA(omp simd aligned(__VA_ARGS__:64))
#endif
#endif

/* define SIMD ALIGNMENT */
#ifndef SIMD_ALIGN // must imperatively be a power of two, greater than 2*sizeof(unsigned)
#ifdef __AVX512F__ // 64 bytes for AVX-512
#define SIMD_ALIGN 64
#elif defined(__AVX2__)
#define SIMD_ALIGN 32
#else
#define SIMD_ALIGN 16
#endif
#endif

#define SHIFT 11 //shift must be at least 3 with double of 8 bytes to ensure 64 bytes alignement
#define ONESHIFT (1<<SHIFT)

//typedef uint64_t* __restrict__ __attribute__((aligned(SIMD_ALIGN))) fast_uptr;
typedef uint64_t* fast_uptr;

#define _HXTSORTARRAY_SCAN(H){							\
    uint64_t sum = 0;                                                           \
    for (uint64_t i=0; i<ONESHIFT; i++){                                        \
      uint64_t tsum = sum + H[i];                                               \
      H[i] = sum;                                                               \
      sum = tsum;                                                               \
    }                                                                           \
}

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                  HXTSORTARRAYTRIPLET(type0, type1, type2)               * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
#define DEFINE_HXTSORTARRAYTRIPLET(type0, type1, type2)				\
										\
  static inline void lsb2_##type0##type1##type2(				\
                  type0* __restrict__   array0,					\
                  type0* __restrict__ b_array0,					\
                  type1* __restrict__   array1,					\
                  type1* __restrict__ b_array1,					\
                  type2* __restrict__   array2,					\
                  type2* __restrict__ b_array2,					\
                  fast_uptr h, const uint64_t size, const uint64_t numpass, 	\
                  const uint64_t divisor1, const uint64_t divisor2){ 		\
    uint64_t pass;								\
    for(pass=0; pass<numpass; pass++){						\
      memset(h, 0, ONESHIFT*sizeof(uint64_t));					\
										\
      uint64_t j;								\
      for(j = 0; j < size; j++){						\
        h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;			\
      }										\
										\
      _HXTSORTARRAY_SCAN(h);							\
										\
      for(j = 0; j < size; j++){						\
        uint64_t ye  = h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;		\
        b_array0[ye] = array0[j];						\
        memcpy(&b_array1[divisor1*ye], 						\
               &  array1[divisor1*j ], divisor1*sizeof(type1));			\
        memcpy(&b_array2[divisor2*ye], 						\
               &  array2[divisor2*j ], divisor2*sizeof(type2));			\
      }										\
      type0* t_array0 = NULL;							\
      t_array0 =   array0;							\
        array0 = b_array0;							\
      b_array0 = t_array0;							\
										\
      type1* t_array1 = NULL;							\
      t_array1 =   array1;							\
        array1 = b_array1;							\
      b_array1 = t_array1;							\
										\
      type2* t_array2 = NULL;							\
      t_array2 =   array2;							\
        array2 = b_array2;							\
      b_array2 = t_array2;							\
    }										\
  }										\
										\
										\
  static inline HXTStatus hxtSortArrayTriplet_##type0##type1##type2(		\
                   type0* const __restrict__ array0,				\
                   type1* const __restrict__ array1, const uint64_t divisor1,	\
                   type2* const __restrict__ array2, const uint64_t divisor2,	\
                   const uint64_t n, const uint64_t max){			\
										\
    if(n == 0){ return HXT_STATUS_OK; }						\
										\
    type0* const __restrict__ b_array0 = malloc(         n*sizeof(type0));	\
    type1* const __restrict__ b_array1 = malloc(divisor1*n*sizeof(type1));	\
    type2* const __restrict__ b_array2 = malloc(divisor2*n*sizeof(type2));	\
    uint64_t nthreads;								\
    void*    base_address;							\
										\
    fast_uptr h_all, h_tot;							\
										\
    /* calculate number of bits */						\
    uint64_t nbits=0;								\
    while(max>>nbits){								\
      nbits++;									\
    }										\
										\
    const uint64_t bits_rem = (nbits>SHIFT)?nbits-SHIFT:0;			\
    const uint64_t numpass = (bits_rem+SHIFT-1)/SHIFT;				\
										\
    /* 1.  parallel histogramming pass */					\
    _HXTSORTARRAY_PRAGMA(omp parallel)						\
    {										\
      _HXTSORTARRAY_PRAGMA(omp single)						\
      {										\
	nthreads = omp_get_num_threads();					\
	base_address = 								\
	  malloc((nthreads*ONESHIFT+ONESHIFT+1)*sizeof(uint64_t) + SIMD_ALIGN); \
	h_all =	(fast_uptr) 							\
	  (((size_t)base_address + SIMD_ALIGN -1) & ~(SIMD_ALIGN - 1));         \
	h_tot = h_all + nthreads*ONESHIFT;					\
	h_tot[ONESHIFT] = n;							\
      }										\
      const uint64_t threadID = omp_get_thread_num();				\
										\
      fast_uptr h_this = h_all + threadID*ONESHIFT;				\
      memset(h_this, 0, ONESHIFT*sizeof(uint64_t));				\
										\
      uint64_t i;								\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<n; i++){							\
	h_this[array0[i]>>bits_rem]++;						\
      }										\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
	uint64_t sum = 0;							\
	uint64_t j;								\
	for(j=0; j<nthreads+1; j++){						\
	  uint64_t tsum = h_all[j*ONESHIFT + i] + sum;				\
	  h_all[j*ONESHIFT + i] = sum;						\
	  sum = tsum;								\
	}									\
      }										\
										\
      /* calculate total sums */						\
      _HXTSORTARRAY_PRAGMA(omp single)						\
        _HXTSORTARRAY_SCAN(h_tot);						\
										\
      /* now every thread may calculate all it's starting indexes */		\
      _HXTSORTARRAY_PRAGMA_ALIGNED(h_this, h_tot)				\
      for (i=0; i<ONESHIFT; i++)						\
        h_this[i] += h_tot[i];							\
										\
      /* byte 2: read/write histogram, copy */					\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i = 0; i < n; i++){							\
        uint64_t yo = h_this[array0[i]>>bits_rem]++;				\
        b_array0[yo] = array0[i];						\
        memcpy(&b_array1[divisor1*yo], 						\
               &  array1[divisor1*i ], divisor1*sizeof(type1)); 		\
        memcpy(&b_array2[divisor2*yo], 						\
               &  array2[divisor2*i ], divisor2*sizeof(type2)); 		\
      }										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
        uint64_t start = h_tot[i], end = h_tot[i+1];				\
										\
        lsb2_##type0##type1##type2(						\
                  b_array0 +          start, array0 +          start,		\
                  b_array1 + divisor1*start, array1 + divisor1*start,		\
                  b_array2 + divisor2*start, array2 + divisor2*start,		\
                  h_this, end-start, numpass, divisor1, divisor2);		\
      }										\
										\
      if((numpass&1)==0){							\
        memcpy(array0, b_array0, n*sizeof(type0));				\
        memcpy(array1, b_array1, divisor1*n*sizeof(type1));			\
        memcpy(array2, b_array2, divisor2*n*sizeof(type2));			\
      }										\
										\
    }										\
										\
    free(base_address);								\
    free(b_array0);								\
    free(b_array1);								\
    free(b_array2);								\
										\
    return HXT_STATUS_OK;							\
  }										\

#define 									\
  hxtSortArrayTriplet(  type0, array0, type1, array1, divisor1, 		\
                                       type2, array2, divisor2, n, max)		\
  hxtSortArrayTriplet_##type0##type1##type2( array0, array1, divisor1, 		\
                                                     array2, divisor2, n, max)

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                            HXTSORTARRAY(type0)                          * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
#define DEFINE_HXTSORTARRAY(type0)						\
										\
  static inline void lsb0_##type0(type0* __restrict__   array0,			\
                                     type0* __restrict__ b_array0,		\
                                     fast_uptr h, const uint64_t size,		\
                                     const uint64_t numpass){ 			\
    uint64_t pass;								\
    for(pass=0; pass<numpass; pass++){						\
      memset(h, 0, ONESHIFT*sizeof(uint64_t));					\
										\
      uint64_t j;								\
      for(j = 0; j < size; j++){						\
        h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;			\
      }										\
										\
      _HXTSORTARRAY_SCAN(h);							\
										\
      for(j = 0; j < size; j++){						\
        uint64_t ye  = h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;		\
        b_array0[ye] = array0[j];						\
      }										\
      type0* t_array0 = NULL;							\
      t_array0 =   array0;							\
        array0 = b_array0;							\
      b_array0 = t_array0;							\
    }										\
  }										\
										\
  static inline HXTStatus hxtSortArray_##type0(					\
                                 type0* const __restrict__ array0,		\
                                 const uint64_t n, const uint64_t max){		\
										\
    if(n == 0){ return HXT_STATUS_OK; }						\
										\
    type0* const __restrict__ b_array0 = malloc(n*sizeof(type0));		\
    uint64_t nthreads;								\
    void*    base_address;							\
										\
    fast_uptr h_all, h_tot;							\
										\
    /* calculate number of bits */						\
    uint64_t nbits=0;								\
    while(max>>nbits){								\
      nbits++;									\
    }										\
										\
    const uint64_t bits_rem = (nbits>SHIFT)?nbits-SHIFT:0;			\
    const uint64_t numpass = (bits_rem+SHIFT-1)/SHIFT;				\
										\
    /* 1.  parallel histogramming pass */					\
    _HXTSORTARRAY_PRAGMA(omp parallel)						\
    {										\
      _HXTSORTARRAY_PRAGMA(omp single)						\
      {										\
	nthreads = omp_get_num_threads();					\
	base_address = 								\
	  malloc((nthreads*ONESHIFT+ONESHIFT+1)*sizeof(uint64_t) + SIMD_ALIGN); \
	h_all =	(fast_uptr) 							\
	  (((size_t)base_address + SIMD_ALIGN -1) & ~(SIMD_ALIGN - 1));         \
	h_tot = h_all + nthreads*ONESHIFT;					\
	h_tot[ONESHIFT] = n;							\
      }										\
      const uint64_t threadID = omp_get_thread_num();				\
										\
      fast_uptr h_this = h_all + threadID*ONESHIFT;				\
      memset(h_this, 0, ONESHIFT*sizeof(uint64_t));				\
										\
      uint64_t i;								\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<n; i++){							\
	h_this[array0[i]>>bits_rem]++;						\
      }										\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
	uint64_t sum = 0;							\
	uint64_t j;								\
	for(j=0; j<nthreads+1; j++){						\
	  uint64_t tsum = h_all[j*ONESHIFT + i] + sum;				\
	  h_all[j*ONESHIFT + i] = sum;						\
	  sum = tsum;								\
	}									\
      }										\
										\
      /* calculate total sums */						\
      _HXTSORTARRAY_PRAGMA(omp single)						\
        _HXTSORTARRAY_SCAN(h_tot);						\
										\
      /* now every thread may calculate all it's starting indexes */		\
      _HXTSORTARRAY_PRAGMA_ALIGNED(h_this, h_tot)				\
      for (i=0; i<ONESHIFT; i++)						\
        h_this[i] += h_tot[i];							\
										\
      /* byte 2: read/write histogram, copy */					\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i = 0; i < n; i++){							\
        uint64_t yo = h_this[array0[i]>>bits_rem]++;				\
        b_array0[yo] = array0[i];						\
      }										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
        uint64_t start = h_tot[i], end = h_tot[i+1];				\
										\
        lsb0_##type0(b_array0 + start, array0 + start,				\
                        h_this, end-start, numpass);				\
      }										\
										\
      if((numpass&1)==0){ memcpy(array0, b_array0, n*sizeof(type0)); }		\
										\
    }										\
										\
    free(base_address);								\
    free(b_array0);								\
										\
    return HXT_STATUS_OK;							\
  }

#define 									\
  hxtSortArray( type0, array0, n, max)						\
  hxtSortArray_##type0(array0, n, max)						

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                      HXTSORTARRAYPAIR(type0, type1)                     * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
#define DEFINE_HXTSORTARRAYPAIR(type0, type1)					\
										\
  static inline void lsb1_##type0##type1(					\
                                      type0* __restrict__   array0,		\
                                      type0* __restrict__ b_array0,		\
                                      type1* __restrict__   array1,		\
                                      type1* __restrict__ b_array1,		\
                                      fast_uptr h, const uint64_t size,		\
                                      const uint64_t numpass,			\
                                      const uint64_t divisor1){ 		\
    uint64_t pass;								\
    for(pass=0; pass<numpass; pass++){						\
      memset(h, 0, ONESHIFT*sizeof(uint64_t));					\
										\
      uint64_t j;								\
      for(j = 0; j < size; j++){						\
        h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;			\
      }										\
										\
      _HXTSORTARRAY_SCAN(h);							\
										\
      for(j = 0; j < size; j++){						\
        uint64_t ye  = h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;		\
        b_array0[ye] = array0[j];						\
        memcpy(&b_array1[divisor1*ye], 						\
               &  array1[divisor1*j ], divisor1*sizeof(type1));			\
      }										\
      type0* t_array0 = NULL;							\
      t_array0 =   array0;							\
        array0 = b_array0;							\
      b_array0 = t_array0;							\
										\
      type1* t_array1 = NULL;							\
      t_array1 =   array1;							\
        array1 = b_array1;							\
      b_array1 = t_array1;							\
    }										\
  }										\
										\
  static inline HXTStatus hxtSortArrayPair_##type0##type1(			\
                   type0* const __restrict__ array0,				\
                   type1* const __restrict__ array1, const uint64_t divisor1,	\
                   const uint64_t n, const uint64_t max){			\
										\
    if(n == 0){ return HXT_STATUS_OK; }						\
										\
    type0* const __restrict__ b_array0 = malloc(         n*sizeof(type0));	\
    type1* const __restrict__ b_array1 = malloc(divisor1*n*sizeof(type1));	\
    uint64_t nthreads;								\
    void*    base_address;							\
										\
    fast_uptr h_all, h_tot;							\
										\
    /* calculate number of bits */						\
    uint64_t nbits=0;								\
    while(max>>nbits){								\
      nbits++;									\
    }										\
										\
    const uint64_t bits_rem = (nbits>SHIFT)?nbits-SHIFT:0;			\
    const uint64_t numpass = (bits_rem+SHIFT-1)/SHIFT;				\
										\
    /* 1.  parallel histogramming pass */					\
    _HXTSORTARRAY_PRAGMA(omp parallel)						\
    {										\
      _HXTSORTARRAY_PRAGMA(omp single)						\
      {										\
	nthreads = omp_get_num_threads();					\
	base_address = 								\
	  malloc((nthreads*ONESHIFT+ONESHIFT+1)*sizeof(uint64_t) + SIMD_ALIGN); \
	h_all =	(fast_uptr) 							\
	  (((size_t)base_address + SIMD_ALIGN -1) & ~(SIMD_ALIGN - 1));         \
	h_tot = h_all + nthreads*ONESHIFT;					\
	h_tot[ONESHIFT] = n;							\
      }										\
      const uint64_t threadID = omp_get_thread_num();				\
										\
      fast_uptr h_this = h_all + threadID*ONESHIFT;				\
      memset(h_this, 0, ONESHIFT*sizeof(uint64_t));				\
										\
      uint64_t i;								\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<n; i++){							\
	h_this[array0[i]>>bits_rem]++;						\
      }										\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
	uint64_t sum = 0;							\
	uint64_t j;								\
	for(j=0; j<nthreads+1; j++){						\
	  uint64_t tsum = h_all[j*ONESHIFT + i] + sum;				\
	  h_all[j*ONESHIFT + i] = sum;						\
	  sum = tsum;								\
	}									\
      }										\
										\
      /* calculate total sums */						\
      _HXTSORTARRAY_PRAGMA(omp single)						\
        _HXTSORTARRAY_SCAN(h_tot);						\
										\
      /* now every thread may calculate all it's starting indexes */		\
      _HXTSORTARRAY_PRAGMA_ALIGNED(h_this, h_tot)				\
      for (i=0; i<ONESHIFT; i++)						\
        h_this[i] += h_tot[i];							\
										\
      /* byte 2: read/write histogram, copy */					\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i = 0; i < n; i++){							\
        uint64_t yo = h_this[array0[i]>>bits_rem]++;				\
        b_array0[yo] = array0[i];						\
        memcpy(&b_array1[divisor1*yo], 						\
               &  array1[divisor1*i ], divisor1*sizeof(type1)); 		\
      }										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
        uint64_t start = h_tot[i], end = h_tot[i+1];				\
										\
        lsb1_##type0##type1(							\
                   b_array0 +          start, array0 +          start,		\
                   b_array1 + divisor1*start, array1 + divisor1*start,		\
                   h_this, end-start, numpass, divisor1);			\
      }										\
										\
      if((numpass&1)==0){							\
        memcpy(array0, b_array0, n*sizeof(type0));				\
        memcpy(array1, b_array1, divisor1*n*sizeof(type1));			\
      }										\
										\
    }										\
										\
    free(base_address);								\
    free(b_array0);								\
    free(b_array1);								\
										\
    return HXT_STATUS_OK;							\
  }

#define 									\
  hxtSortArrayPair( type0, array0, type1, array1, divisor1, n, max)		\
  hxtSortArrayPair_##type0##type1(array0, array1, divisor1, n, max)	

// * ----------------------------------------------------------------------- * //
// * ---                                                                 --- * //
// *                HXTSORTARRAYHIERARCHY(type0, type1, type2)               * //
// * ---                                                                 --- * //
// * ----------------------------------------------------------------------- * //
#define DEFINE_HXTSORTARRAYHIERARCHY(type0, type1, type2)			\
										\
  static inline void lsb_ws_##type0##type1##type2(				\
                  type0* __restrict__   array0,					\
                  type0* __restrict__ b_array0,					\
                  type1* __restrict__   array1,					\
                  type1* __restrict__ b_array1,					\
                  type2* __restrict__   array2,					\
                  type2* __restrict__ b_array2,					\
                  fast_uptr h, const uint64_t size,				\
                  const uint64_t num0pass, const uint64_t num1pass,	 	\
                  const uint64_t divisor2){ 					\
										\
    uint64_t pass;								\
    for(pass=0; pass<num1pass; pass++){						\
      memset(h, 0, ONESHIFT*sizeof(uint64_t));					\
										\
      uint64_t j;								\
      for(j = 0; j < size; j++){						\
        h[ array1[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;			\
      }										\
										\
      _HXTSORTARRAY_SCAN(h);							\
										\
      for(j = 0; j < size; j++){						\
        uint64_t ye  = h[ array1[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;		\
        b_array0[ye] = array0[j];						\
        b_array1[ye] = array1[j];						\
        memcpy(&b_array2[divisor2*ye], 						\
               &  array2[divisor2*j ], divisor2*sizeof(type2));			\
      }										\
      type0* t_array0 = NULL;							\
      t_array0 =   array0;							\
        array0 = b_array0;							\
      b_array0 = t_array0;							\
										\
      type1* t_array1 = NULL;							\
      t_array1 =   array1;							\
        array1 = b_array1;							\
      b_array1 = t_array1;							\
										\
      type2* t_array2 = NULL;							\
      t_array2 =   array2;							\
        array2 = b_array2;							\
      b_array2 = t_array2;							\
    }										\
										\
    for(pass=0; pass<num0pass; pass++){						\
      memset(h, 0, ONESHIFT*sizeof(uint64_t));					\
										\
      uint64_t j;								\
      for(j = 0; j < size; j++){						\
        h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;			\
      }										\
										\
      _HXTSORTARRAY_SCAN(h);							\
										\
      for(j = 0; j < size; j++){						\
        uint64_t ye  = h[ array0[j] >> (SHIFT*pass) & (ONESHIFT-1) ]++;		\
        b_array0[ye] = array0[j];						\
        b_array1[ye] = array1[j];						\
        memcpy(&b_array2[divisor2*ye], 						\
               &  array2[divisor2*j ], divisor2*sizeof(type2));			\
      }										\
      type0* t_array0 = NULL;							\
      t_array0 =   array0;							\
        array0 = b_array0;							\
      b_array0 = t_array0;							\
										\
      type1* t_array1 = NULL;							\
      t_array1 =   array1;							\
        array1 = b_array1;							\
      b_array1 = t_array1;							\
										\
      type2* t_array2 = NULL;							\
      t_array2 =   array2;							\
        array2 = b_array2;							\
      b_array2 = t_array2;							\
    }										\
  }										\
										\
										\
  static inline HXTStatus hxtSortArrayHierarchy_##type0##type1##type2(		\
                   type0* const __restrict__ array0,				\
                   type1* const __restrict__ array1,				\
                   type2* const __restrict__ array2, const uint64_t divisor2,	\
                   const uint64_t n, const uint64_t max){			\
										\
    if(n == 0){ return HXT_STATUS_OK; }						\
										\
    type0* const __restrict__ b_array0 = malloc(         n*sizeof(type0));	\
    type1* const __restrict__ b_array1 = malloc(         n*sizeof(type1));	\
    type2* const __restrict__ b_array2 = malloc(divisor2*n*sizeof(type2));	\
    uint64_t nthreads;								\
    void*    base_address;							\
										\
    fast_uptr h_all, h_tot;							\
										\
    /* calculate number of bits */						\
    uint64_t nbits=0;								\
    while(max>>nbits){								\
      nbits++;									\
    }										\
										\
    const uint64_t bits_rem = (nbits>SHIFT)?nbits-SHIFT:0;			\
    const uint64_t num0pass = (bits_rem+SHIFT-1)/SHIFT;				\
    const uint64_t num1pass = (nbits   +SHIFT-1)/SHIFT;				\
										\
    /* 1.  parallel histogramming pass */					\
    _HXTSORTARRAY_PRAGMA(omp parallel)						\
    {										\
      _HXTSORTARRAY_PRAGMA(omp single)						\
      {										\
	nthreads = omp_get_num_threads();					\
	base_address = 								\
	  malloc((nthreads*ONESHIFT+ONESHIFT+1)*sizeof(uint64_t) + SIMD_ALIGN); \
	h_all =	(fast_uptr) 							\
	  (((size_t)base_address + SIMD_ALIGN -1) & ~(SIMD_ALIGN - 1));         \
	h_tot = h_all + nthreads*ONESHIFT;					\
	h_tot[ONESHIFT] = n;							\
      }										\
      const uint64_t threadID = omp_get_thread_num();				\
										\
      fast_uptr h_this = h_all + threadID*ONESHIFT;				\
      memset(h_this, 0, ONESHIFT*sizeof(uint64_t));				\
										\
      uint64_t i;								\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<n; i++){							\
	h_this[array0[i]>>bits_rem]++;						\
      }										\
										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
	uint64_t sum = 0;							\
	uint64_t j;								\
	for(j=0; j<nthreads+1; j++){						\
	  uint64_t tsum = h_all[j*ONESHIFT + i] + sum;				\
	  h_all[j*ONESHIFT + i] = sum;						\
	  sum = tsum;								\
	}									\
      }										\
										\
      /* calculate total sums */						\
      _HXTSORTARRAY_PRAGMA(omp single)						\
        _HXTSORTARRAY_SCAN(h_tot);						\
										\
      /* now every thread may calculate all it's starting indexes */		\
      _HXTSORTARRAY_PRAGMA_ALIGNED(h_this, h_tot)				\
      for (i=0; i<ONESHIFT; i++)						\
        h_this[i] += h_tot[i];							\
										\
      /* byte 2: read/write histogram, copy */					\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i = 0; i < n; i++){							\
        uint64_t yo = h_this[array0[i]>>bits_rem]++;				\
        b_array0[yo] = array0[i];						\
        b_array1[yo] = array1[i];						\
        memcpy(&b_array2[divisor2*yo], 						\
               &  array2[divisor2*i ], divisor2*sizeof(type2)); 		\
      }										\
      _HXTSORTARRAY_PRAGMA(omp for schedule(static))				\
      for (i=0; i<ONESHIFT; i++){						\
        uint64_t start = h_tot[i], end = h_tot[i+1];				\
										\
        lsb_ws_##type0##type1##type2(						\
                  b_array0 +          start, array0 +          start,		\
                  b_array1 +          start, array1 +          start,		\
                  b_array2 + divisor2*start, array2 + divisor2*start,		\
                  h_this, end-start, num0pass, num1pass, divisor2);		\
      }										\
										\
      if(((num0pass+num1pass)&1)==0){						\
        memcpy(array0, b_array0,          n*sizeof(type0));			\
        memcpy(array1, b_array1,          n*sizeof(type1));			\
        memcpy(array2, b_array2, divisor2*n*sizeof(type2));			\
      }										\
										\
    }										\
										\
    free(base_address);								\
    free(b_array0);								\
    free(b_array1);								\
    free(b_array2);								\
										\
    return HXT_STATUS_OK;							\
  }										\

#define 									\
  hxtSortArrayHierarchy(type0, array0,						\
                        type1, array1,				 		\
                        type2, array2, divisor2, n, max)			\
  hxtSortArrayHierarchy_##type0##type1##type2(array0, array1, array2, 		\
                                              divisor2, n, max)

DEFINE_HXTSORTARRAY       (int   );	
DEFINE_HXTSORTARRAYPAIR   (int   , int);	
DEFINE_HXTSORTARRAYPAIR   (int   , double);
DEFINE_HXTSORTARRAYPAIR   (size_t, double);
DEFINE_HXTSORTARRAYTRIPLET(int   , int   , int);

DEFINE_HXTSORTARRAYHIERARCHY(int, int, double_complex);
DEFINE_HXTSORTARRAYHIERARCHY(int, int, int           );

#endif
