#ifndef HXTWAVE_SEARCH_ARRAY_H
#define HXTWAVE_SEARCH_ARRAY_H

#include <stdint.h>
#include <stddef.h>

#define DEFINE_HXTSEARCHARRAY(type0)						\
										\
  static inline int hxtSearchArrayCmp_##type0(const type0 *a,			\
                                              const type0 *b){			\
    if(*a > *b)      return  1;							\
    else if(*a < *b) return -1;							\
    else             return  0;							\
  }										\
										\
  static inline type0* hxtSearchArray_##type0(register const type0 *key,	\
                                              const void  *base0,		\
                                              size_t nmemb,			\
                                              register size_t size){		\
                                                                                \
    register const char* base = base0;						\
    register size_t lim;							\
    register int cmp;								\
    register const void *p;							\
                                                                                \
    for (lim = nmemb; lim != 0; lim >>= 1) {					\
      p = base + (lim >> 1) * size;						\
      cmp = hxtSearchArrayCmp_##type0(key, (type0 *)p);				\
      if (cmp == 0)								\
        return ((type0 *)p);							\
      if (cmp > 0) {	/* key > p: move right */				\
	base = (char *)p + size;						\
	lim--;									\
      }		/* else move left */						\
    }										\
    return (NULL);								\
  }

#define hxtSearchArray(type0, key, base0, nmemb, size)				\
        hxtSearchArray_##type0(key, base0, nmemb, size)

#define DEFINE_HXTSEARCHARRAYPAIR(type0, type1)					\
										\
  static inline int hxtSearchArrayPairCmp_##type0##type1(const type0 *a0,	\
                                                         const type1 *a1,	\
                                                         const type0 *b0,	\
                                                         const type1 *b1){	\
    if (*a0 < *b0) return -1;							\
    if (*a0 > *b0) return  1;							\
    if (*a1 < *b1) return -1;							\
    if (*a1 > *b1) return  1;							\
    return 0;									\
  }										\
										\
  static inline int64_t hxtSearchArrayPair_##type0##type1(			\
                                       register const type0 *key0,		\
                                       const void  *base0,			\
                                       register size_t size0,			\
                                       register const type1 *key1,		\
                                       const void  *base1,			\
                                       register size_t size1,			\
                                       size_t nmemb){				\
                                                                                \
    register const char* baseZero = base0;					\
    register const char* baseOne  = base1;					\
    register size_t lim;							\
    register int cmp;								\
    register const void *p0;							\
    register const void *p1;							\
    ptrdiff_t index;								\
                                                                                \
    for (lim = nmemb; lim != 0; lim >>= 1) {					\
      p0 = baseZero + (lim >> 1) * size0;					\
      p1 = baseOne  + (lim >> 1) * size1;					\
      cmp = hxtSearchArrayPairCmp_##type0##type1(key0       , key1, 		\
                                                 (type0*) p0, (type1*) p1);	\
      if (cmp == 0){								\
        index = (type0*) p0 - (type0*) base0;					\
        return index;								\
      }										\
      if (cmp > 0) {	/* key > p: move right */				\
	baseZero = (char *)p0 + size0;						\
	baseOne  = (char *)p1 + size1;						\
	lim--;									\
      }		/* else move left */						\
    }										\
    return (-1);								\
  }

#define hxtSearchArrayPair(type0, key0, base0, size0,				\
                           type1, key1, base1, size1, nmemb)			\
        hxtSearchArrayPair_##type0##type1(key0, base0, size0,			\
                                          key1, base1, size1, nmemb)

DEFINE_HXTSEARCHARRAY(int   );	
DEFINE_HXTSEARCHARRAY(size_t);	

DEFINE_HXTSEARCHARRAYPAIR( int, int   );
DEFINE_HXTSEARCHARRAYPAIR( int, double);

DEFINE_HXTSEARCHARRAYPAIR(uint32_t, uint32_t);

#endif
