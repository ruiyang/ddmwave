#include "hxt_linear_algebra.h"

#if !defined(HXTF77NAME)
#define HXTF77NAME(x) (x##_)
#endif

#if defined(HXTWAVE_HAVE_BLAS)
void HXTF77NAME(daxpy)(const int *n, const double         *alpha, const double         *x, const int *incx, 
                                                                        double         *y, const int *incy);
void HXTF77NAME(zaxpy)(const int *n, const double_complex *alpha, const double_complex *x, const int *incx, 
                                                                        double_complex *y, const int *incy);

//void HXTF77NAME(dcopy)(int *n, double         *a, int *inca, double         *b, int *incb);
//void HXTF77NAME(zcopy)(int *n, double complex *a, int *inca, double complex *b, int *incb);

void HXTF77NAME(dgemm)(const char *transa, const char *transb, 
                       const int *m, const int *n, const int *k,
                       const double *alpha, const double *a, const int *lda,
                                            const double *b, const int *ldb, 
                       const double *beta ,       double *c, const int *ldc);
void HXTF77NAME(zgemm)(const char *transa, const char *transb, 
                       const int *m, const int *n, const int *k,
                       const double_complex *alpha, const double_complex *a, const int *lda,
                                                    const double_complex *b, const int *ldb, 
                       const double_complex *beta ,       double_complex *c, const int *ldc);

//void HXTF77NAME(dgemv)(const char *trans, int *m, int *n,
//                       double *alpha, double *a, int *lda,
//                       double *x, int *incx, double *beta,
//                       double *y, int *incy);
//void HXTF77NAME(zgemv)(const char *trans, int *m, int *n,
//                       double complex *alpha, double complex *a, int *lda,
//                       double complex*x, int *incx, double complex *beta,
//                       double complex*y, int *incy);

void HXTF77NAME(dscal)(const int *n, const double         *alpha, double         *x, const int *incx);
void HXTF77NAME(zscal)(const int *n, const double_complex *alpha, double_complex *x, const int *incx);

double_complex HXTF77NAME(zdotc)(const int *size, 
                                 const double_complex* x, const int* incx, 
                                 const double_complex* y, const int* incy);
double_complex HXTF77NAME(zdotu)(const int *size, 
                                 const double_complex* x, const int* incx, 
                                 const double_complex* y, const int* incy);

double HXTF77NAME(dznrm2)(const int *n, const double_complex *x, const int *incx);
double HXTF77NAME( dnrm2)(const int *n, const double         *x, const int *incx);
double HXTF77NAME(dzasum)(const int *n, const double_complex *x, const int *incx);

void HXTF77NAME(zdrot)(const int *n, 
                             double_complex *cx, const int *incx,
                             double_complex *cy, const int *incy,
                       const double_complex *c , 
                       const double_complex *s );
void HXTF77NAME(zrotg)(const double_complex *ca,
                       const double_complex *cb,
                             double         *c ,
                             double_complex *s );

void hxtdgemm(const double *a, 
              const double *b, 
              const double  alpha, 
                    double *c, 
              const double  beta, 
              const int m, 
              const int n, 
              const int k)
{
  //const double alpha = 1.;
  //const double beta  = 1.;
  // * ROW    MAJOR
  HXTF77NAME(dgemm)("N", "N", &n, &m, &k, &alpha, b, &n, a, &k, &beta, c, &n); 
  // * COLUMN MAJOR
//HXTF77NAME(dgemm)("N", "N", &m, &n, &k, &alpha, a, &m, b, &k, &beta, c, &m); 
}

void hxtzgemm(const double_complex *a, 
              const double_complex *b, 
              const double_complex  alpha,
                    double_complex *c, 
              const double_complex  beta,
              const int m, 
              const int n, 
              const int k)
{
  //const double_complex alpha = {1., 0.};
  //const double_complex beta  = {0., 0.};
  // * ROW    MAJOR
  HXTF77NAME(zgemm)("N", "N", &n, &m, &k, &alpha, b, &n, a, &k, &beta, c, &n); 
  // * COLUMN MAJOR
//HXTF77NAME(zgemm)("N", "N", &m, &n, &k, &alpha, a, &m, b, &k, &beta, c, &m); 
}

void hxtdaxpy(double *c, const double *a, const double coef, const int length)
{
  const int INCX = 1, INCY = 1;
  HXTF77NAME(daxpy)(&length, &coef, a, &INCX, c, &INCY);

}

void hxtzaxpy(      double_complex *c, 
              const double_complex *a, 
              const double_complex coef, 
              const int length)
{
  const int INCX = 1, INCY = 1;
  HXTF77NAME(zaxpy)(&length, &coef, a, &INCX, c, &INCY);

}

void hxtdscal(double *c, const double coef, const int length)
{
  const int INCX = 1;
  HXTF77NAME(dscal)(&length, &coef, c, &INCX);
}

void hxtzscal(double_complex *c, const double_complex coef, const int length)
{
  const int INCX = 1;
  HXTF77NAME(zscal)(&length, &coef, c, &INCX);
}

double_complex hxtzdotc(const double_complex *x, 
                        const double_complex *y, 
                        const int        length)
{
 const int INCX = 1;
 const int INCY = 1;
 return HXTF77NAME(zdotc)(&length, x, &INCX, y, &INCY);
}

double_complex hxtzdotu(const double_complex *x, 
                        const double_complex *y, 
                        const int        length)
{
 const int INCX = 1;
 const int INCY = 1;
 return HXTF77NAME(zdotu)(&length, x, &INCX, y, &INCY);
}

double hxtdznrm2(const double_complex *x, const int n)
{
  const int INCX = 1;
  return HXTF77NAME(dznrm2)(&n, x, &INCX);
}

double hxtdzasum(const double_complex *x, const int n)
{
  const int INCX = 1;
  return HXTF77NAME(dzasum)(&n, x, &INCX);
}

void hxtzdrot(const int n, double_complex *cx, 
                           double_complex *cy,
                     const double_complex *c ,
                     const double_complex *s )
{
  const int INCX = 1;
  const int INCY = 1;
  HXTF77NAME(zdrot)(&n, cx, &INCX, cy, &INCY, c, s);
}

void hxtzrotg(const double_complex *ca, 
              const double_complex *cb,
                    double         *c ,
                    double_complex *s )
{
  HXTF77NAME(zrotg)(ca, cb, c, s);
}

void hxtzarot(      double_complex *dx,       double_complex *dy, 
              const double_complex* cs, const double_complex* sn)
{
  double_complex tmp;
   tmp =      (*cs) * (*dx) + (*sn) * (*dy);
  *dy  = -conj(*sn) * (*dx) + (*cs) * (*dy);
  *dx  =  tmp;
}

void hxtzgrot(const double_complex* dx, const double_complex *dy, 
                    double_complex* cs,       double_complex* sn)
{

  if(*dy == 0.0) 
  {
    *cs = 1.0;
    *sn = 0.0;
  }
  else if(*dx == 0.)
  {
    *cs = 0.;
    double_complex dyBar = creal(*dy) - I * cimag(*dy);
    *sn = dyBar / cabs(dyBar);
  }
  else
  {
    double_complex xl = cabs(*dx);
    double_complex yl = cabs(*dy);
    double_complex vl = csqrt(xl*xl + yl*yl);
    *cs = cabs(*dx) / vl;
    *sn = ((*dx) / xl) * ( ( creal(*dy) - I * cimag(*dy) ) / vl);
  }
}

#endif
