set(src
  hxt_linear_algebra.c
  hxt_message.c
)

add_sources(tools "${src}")
