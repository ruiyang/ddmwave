#ifndef HXTWAVE_LINEAR_ALGEBRA_H
#define HXTWAVE_LINEAR_ALGEBRA_H

#include "hxt_types.h"

void hxtdgemm(const double *a, 
              const double *b, 
              const double  alpha, 
                    double *c, 
              const double  beta, 
              const int m, const int n, const int k);
void hxtzgemm(const double_complex *a, 
              const double_complex *b, 
              const double_complex  alpha,
                    double_complex *c, 
              const double_complex  beta,
              const int m, const int n, const int k);

void hxtdaxpy(double         *c, const double         *a, const double         coef, const int length);
void hxtzaxpy(double_complex *c, const double_complex *a, const double_complex coef, const int length);

void hxtdscal(double         *c, const double         coef, const int length);
void hxtzscal(double_complex *c, const double_complex coef, const int length);

double_complex hxtzdotc (const double_complex *x, const double_complex *y, const int length);
double_complex hxtzdotu (const double_complex *x, const double_complex *y, const int length);
double         hxtdznrm2(const double_complex *x, const int n);
double         hxtdzasum(const double_complex *x, const int n);

void hxtzdrot(const int n, 
                    double_complex *cx, 
                    double_complex *cy,
              const double_complex *c ,
              const double_complex *s );
void hxtzrotg(const double_complex *ca, 
              const double_complex *cb,
                    double         *c ,
                    double_complex *s );

void hxtzarot(      double_complex *dx,       
                    double_complex *dy, 
              const double_complex* cs, 
              const double_complex* sn);

void hxtzgrot(const double_complex* dx, 
              const double_complex *dy, 
                    double_complex* cs,       
                    double_complex* sn);

#endif
