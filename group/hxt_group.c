#include "hxt_group.h"

HXTStatus hxtGroupCreate(HXTGroup **group)
{
  HXT_CHECK( hxtMalloc(group, sizeof(HXTGroup)) );
  if((*group) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTGroup *ptr = *group;

  ptr->entities_n =    0;
  ptr->entities   = NULL;

  size_t i;

  for(i = 0; i < MDTS_N; i++)  ptr->dimTags[i] = -1;
  ptr->dimTags_n = 0;

  ptr->offset = 0;

  return HXT_STATUS_OK;
}

HXTStatus hxtGroupDelete(HXTGroup **group)
{
  if((*group) == NULL) return HXT_STATUS_OK;

  HXTGroup *ptr = *group;

  size_t i;

  for(i = 0; i < ptr->entities_n; i++)
  {
    HXT_CHECK( hxtEntityDelete(&ptr->entities[i]) );
    ptr->entities[i] = NULL;
  }
  HXT_CHECK( hxtFree(&ptr->entities) );
  ptr->entities = NULL;
  ptr->entities_n  = 0;

  for(i = 0; i < MDTS_N; i++)  ptr->dimTags[i] = -1;
  ptr->dimTags_n = 0;

  ptr->offset = 0;

  HXT_CHECK( hxtFree(group) );
  *group = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtGroupDummy (HXTGroup  *group)
{
  HXTGroup *ptr =  group;

  size_t i;

  for(i = 0; i < ptr->entities_n; i++)
  {
    HXT_CHECK( hxtEntityDelete(&ptr->entities[i]) );
    ptr->entities[i] = NULL;
  }
  HXT_CHECK( hxtFree(&ptr->entities) );
  ptr->entities = NULL;
  ptr->entities_n  = 0;

  for(i = 0; i < MDTS_N; i++)  ptr->dimTags[i] = -1;
  ptr->dimTags_n = 0;

  ptr->offset = 0;

  return HXT_STATUS_OK;
}

HXTStatus hxtGroupAddEntity(HXTGroup  *group,
                            const int    dim,
                            const int    tag)
{
  size_t c = group->dimTags_n;

  group->dimTags[c++] = dim;
  group->dimTags[c++] = tag;

  group->dimTags_n = c;

  return HXT_STATUS_OK;
}

HXTStatus hxtGroupBuild(HXTGroup *group, const int offset)
{
  HXTGroup *ptr =  group;

  size_t i;

  ptr->entities_n = ptr->dimTags_n / 2;

  HXT_CHECK( hxtMalloc(&ptr->entities, ptr->entities_n *sizeof(HXTEntity )) );

  for(i = 0; i < ptr->dimTags_n / 2; i++)
  {
    const int dim = ptr->dimTags[2*i+0];
    const int tag = ptr->dimTags[2*i+1];
  
    HXT_CHECK( hxtEntityCreate  (&ptr->entities[i]          ) );
    HXT_CHECK( hxtEntityBuild   ( ptr->entities[i], dim, tag) );
  }
  ptr->offset = offset;

  return HXT_STATUS_OK;
}

HXTStatus hxtGroupPrintf(HXTGroup *group)
{
  return HXT_STATUS_OK;
}
