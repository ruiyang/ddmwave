#ifndef HXTWAVE_GROUP_H
#define HXTWAVE_GROUP_H

#include "gmshc.h"
#include "hxt_tools.h"
#include "hxt_types.h"

#include <stdbool.h>
#include <limits.h>
#include "hxt_sort_array.h"
#include "hxt_search_array.h"

#include "hxt_entity.h"

// * Maximum of number of dimTags
#define MDTS_N 60

typedef struct HXTGroupStruct
{
  int offset;

  int dimTags_n      ;
  int dimTags[MDTS_N];

  size_t      entities_n;
  HXTEntity **entities  ;

} HXTGroup;

HXTStatus hxtGroupCreate   (HXTGroup   **group);
HXTStatus hxtGroupDelete   (HXTGroup   **group);
HXTStatus hxtGroupDummy    (HXTGroup    *group);
HXTStatus hxtGroupPrintf   (HXTGroup    *group);
HXTStatus hxtGroupAddEntity(HXTGroup    *group, 
                            const int      dim, 
                            const int      tag);
HXTStatus hxtGroupBuild    (HXTGroup    *group, 
		            const int   offset);

#endif
