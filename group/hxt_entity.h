#ifndef HXTWAVE_ENTITY_H
#define HXTWAVE_ENTITY_H

#include "gmshc.h"
#include "hxt_tools.h"
#include "hxt_types.h"

#include <stdbool.h>
#include <limits.h>
#include "hxt_sort_array.h"
#include "hxt_search_array.h"

typedef struct HXTEntityStruct
{
  int dim;
  int tag;

  bool dummy;

  struct {
    size_t *tags             ;
    size_t  tags_n           ;
    double *coord            ;
    size_t  coord_n          ;
    double *parametricCoord  ;
    size_t  parametricCoord_n;
    size_t *invMap           ;
    size_t  invMap_n         ;
  } nodes;

  struct {
    int     *types      ;
    size_t   types_n    ;
    size_t **tags       ;
    size_t  *tags_n     ;
    size_t   tags_nn    ;
    size_t **nodeTags   ;
    size_t  *nodeTags_n ;
    size_t   nodeTags_nn;
  } elements;

} HXTEntity;

HXTStatus hxtEntityCreate  (HXTEntity **entity);
HXTStatus hxtEntityDelete  (HXTEntity **entity);
HXTStatus hxtEntityDummy   (HXTEntity  *entity);
HXTStatus hxtEntityPrintf  (HXTEntity  *entity);
HXTStatus hxtEntityBuild   (HXTEntity  *entity, 
                            const int   dim   , 
                            const int   tag   );

#endif
