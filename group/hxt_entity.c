#include "hxt_entity.h"

HXTStatus hxtEntityCreate(HXTEntity **entity)
{
  HXT_CHECK( hxtMalloc(entity, sizeof(HXTEntity)) );
  if((*entity) == NULL) return HXT_ERROR(HXT_STATUS_OUT_OF_MEMORY);

  HXTEntity *ptr = *entity;

  ptr->dim = -1;
  ptr->tag = -1;

  ptr->dummy = true;

  ptr->nodes.tags              = NULL;
  ptr->nodes.tags_n            =    0;
  ptr->nodes.coord             = NULL;
  ptr->nodes.coord_n           =    0;
  ptr->nodes.parametricCoord   = NULL;
  ptr->nodes.parametricCoord_n =    0;
  ptr->nodes.invMap            = NULL;
  ptr->nodes.invMap_n          =    0;

  ptr->elements.types        = NULL;
  ptr->elements.types_n      =    0;
  ptr->elements.tags         = NULL;
  ptr->elements.tags_n       = NULL;
  ptr->elements.tags_nn      =    0;
  ptr->elements.nodeTags     = NULL;
  ptr->elements.nodeTags_n   = NULL;
  ptr->elements.nodeTags_nn  =    0;

  return HXT_STATUS_OK;
}

HXTStatus hxtEntityDelete(HXTEntity **entity)
{
  HXTEntity *ptr = *entity;

  size_t j;

  HXT_CHECK( hxtFree(&ptr->nodes.tags           ) );
  HXT_CHECK( hxtFree(&ptr->nodes.coord          ) );
  HXT_CHECK( hxtFree(&ptr->nodes.parametricCoord) );
  HXT_CHECK( hxtFree(&ptr->nodes.invMap         ) );
  ptr->nodes.tags_n            = 0;
  ptr->nodes.coord_n           = 0;
  ptr->nodes.parametricCoord_n = 0;
  ptr->nodes.invMap            = 0;

  for(j = 0; j < ptr->elements.types_n; j++)
  {
    HXT_CHECK( hxtFree(&ptr->elements.tags    [j]) );
    HXT_CHECK( hxtFree(&ptr->elements.nodeTags[j]) );
  }
  HXT_CHECK( hxtFree(&ptr->elements.types     ) );
  HXT_CHECK( hxtFree(&ptr->elements.tags      ) );
  HXT_CHECK( hxtFree(&ptr->elements.tags_n    ) );
  HXT_CHECK( hxtFree(&ptr->elements.nodeTags  ) );
  HXT_CHECK( hxtFree(&ptr->elements.nodeTags_n) );
  ptr->elements.types        = NULL;
  ptr->elements.types_n      =    0;
  ptr->elements.tags         = NULL;
  ptr->elements.tags_n       = NULL;
  ptr->elements.tags_nn      =    0;
  ptr->elements.nodeTags     = NULL;
  ptr->elements.nodeTags_n   = NULL;
  ptr->elements.nodeTags_nn  =    0;

  ptr->dim  = -1;
  ptr->tag  = -1;

  ptr->dummy = true;

  HXT_CHECK( hxtFree(entity) );
  *entity = NULL;

  return HXT_STATUS_OK;
}

HXTStatus hxtEntityDummy (HXTEntity  *entity)
{
  HXTEntity *ptr =  entity;

  size_t j;

  HXT_CHECK( hxtFree(&ptr->nodes.tags           ) );
  HXT_CHECK( hxtFree(&ptr->nodes.coord          ) );
  HXT_CHECK( hxtFree(&ptr->nodes.parametricCoord) );
  HXT_CHECK( hxtFree(&ptr->nodes.invMap         ) );
  ptr->nodes.tags_n            = 0;
  ptr->nodes.coord_n           = 0;
  ptr->nodes.parametricCoord_n = 0;
  ptr->nodes.invMap            = 0;

  for(j = 0; j < ptr->elements.types_n; j++)
  {
    HXT_CHECK( hxtFree(&ptr->elements.tags    [j]) );
    HXT_CHECK( hxtFree(&ptr->elements.nodeTags[j]) );
  }
  HXT_CHECK( hxtFree(&ptr->elements.types     ) );
  HXT_CHECK( hxtFree(&ptr->elements.tags      ) );
  HXT_CHECK( hxtFree(&ptr->elements.tags_n    ) );
  HXT_CHECK( hxtFree(&ptr->elements.nodeTags  ) );
  HXT_CHECK( hxtFree(&ptr->elements.nodeTags_n) );
  ptr->elements.types        = NULL;
  ptr->elements.types_n      =    0;
  ptr->elements.tags         = NULL;
  ptr->elements.tags_n       = NULL;
  ptr->elements.tags_nn      =    0;
  ptr->elements.nodeTags     = NULL;
  ptr->elements.nodeTags_n   = NULL;
  ptr->elements.nodeTags_nn  =    0;

  ptr->dim  = -1;
  ptr->tag  = -1;

  ptr->dummy = true;

  return HXT_STATUS_OK;
}

HXTStatus hxtEntityBuild(HXTEntity  *entity,
                         const int   dim   ,
                         const int   tag   )
{
  int ierr = 0;
 
  size_t i;

  entity->dim = dim;
  entity->tag = tag;

  if( entity->tag <=  0)
  { 
    entity->dummy = true;
    return HXT_STATUS_OK;
  }
  entity->dummy =  false;

  // * --------------------------------------------------------------- * //
  // * --                    GMSH GET NODES IN ENTITY               -- * //
  // * --------------------------------------------------------------- * //
  #pragma omp critical
  {
    const int includeBoundary       = true;
    const int returnParametricCoord = true;

    gmshModelMeshGetNodes(&entity->nodes.tags             , 
                          &entity->nodes.tags_n           , 
                          &entity->nodes.coord            , 
                          &entity->nodes.coord_n          , 
                          &entity->nodes.parametricCoord  , 
                          &entity->nodes.parametricCoord_n, 
                           dim, tag, includeBoundary      , 
                           returnParametricCoord, &ierr   );
  }
  hxtSortArrayPair(size_t, entity->nodes.tags ,    
                   double, entity->nodes.coord, 3, 
                   entity->nodes.tags_n, INT_MAX );

  for(i = 0; i < entity->nodes.tags_n; i++)
    if(entity->nodes.invMap_n <= entity->nodes.tags[i])
       entity->nodes.invMap_n  = entity->nodes.tags[i] + 1;

  HXT_CHECK( hxtCalloc(&entity->nodes.invMap,
                        entity->nodes.invMap_n,sizeof(size_t)) );

  for(i = 0; i < entity->nodes.tags_n; i++)
    entity->nodes.invMap[ entity->nodes.tags[i] ] = i;

  // * --------------------------------------------------------------- * //
  // * --                    GMSH GET ELEMENTS                      -- * //
  // * --------------------------------------------------------------- * //
  gmshModelMeshGetElements(&entity->elements.types      , 
                           &entity->elements.types_n    , 
                           &entity->elements.tags       , 
                           &entity->elements.tags_n     , 
                           &entity->elements.tags_nn    , 
                           &entity->elements.nodeTags   , 
                           &entity->elements.nodeTags_n , 
                           &entity->elements.nodeTags_nn, 
                            dim,  tag, &ierr);

  return HXT_STATUS_OK;
}

HXTStatus hxtEntityPrintf(HXTEntity *entity)
{
  printf("ENTITY: \n");
  printf("{\n");
  printf("  entity's dim and tag: [%d, %d]\n", entity->dim, entity->tag);

  if(entity->dummy == true ) printf("  entity is     dummy.\n");
  if(entity->dummy == false) printf("  entity is not dummy.\n");

  printf("\n}\n\n");

  return HXT_STATUS_OK;
}
